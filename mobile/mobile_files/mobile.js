"function" != typeof String.prototype.startsWith &&
  (String.prototype.startsWith = function (t) {
    return 0 === this.lastIndexOf(t, 0);
  }),
  "function" != typeof String.prototype.endsWith &&
    (String.prototype.endsWith = function (t) {
      return -1 !== this.indexOf(t, this.length - t.length);
    }),
  "function" != typeof String.prototype.trim &&
    (String.prototype.trim = function () {
      return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
    }),
  "function" != typeof Array.prototype.find &&
    (Array.prototype.find = function (t) {
      if (null == this) throw new TypeError('"this" is null or not defined');
      var e = Object(this),
        i = e.length >>> 0;
      if ("function" != typeof t)
        throw new TypeError("callback must be a function");
      for (var a = arguments[1], n = 0; n < i; ) {
        var o = e[n];
        if (t.call(a, o, n, e)) return o;
        n++;
      }
    }),
  "function" != typeof Array.prototype.findIndex &&
    Object.defineProperty(Array.prototype, "findIndex", {
      value: function (t) {
        if (null == this) throw new TypeError('"this" is null or not defined');
        var e = Object(this),
          i = e.length >>> 0;
        if ("function" != typeof t)
          throw new TypeError("predicate must be a function");
        for (var a = arguments[1], n = 0; n < i; ) {
          var o = e[n];
          if (t.call(a, o, n, e)) return n;
          n++;
        }
        return -1;
      },
      configurable: !0,
      writable: !0,
    }),
  "function" != typeof Array.prototype.filter &&
    (Array.prototype.filter = function (t, e) {
      "use strict";
      if ("function" != typeof t || !this) throw new TypeError();
      var i = this.length >>> 0,
        a = new Array(i),
        n = this,
        o = 0,
        r = -1;
      if (void 0 === e)
        for (; ++r !== i; )
          if (r in this)
            if (t(n[r], r, n)) a[o++] = n[r];
            else
              for (; ++r !== i; )
                r in this && t.call(e, n[r], r, n) && (a[o++] = n[r]);
      return (a.length = o), a;
    }),
  "function" != typeof Array.prototype.map &&
    (Array.prototype.map = function (t) {
      var e, i, a;
      if (null == this) throw new TypeError("this is null or not defined");
      var n = Object(this),
        o = n.length >>> 0;
      if ("function" != typeof t) throw new TypeError(t + " is not a function");
      for (
        arguments.length > 1 && (e = arguments[1]), i = new Array(o), a = 0;
        a < o;

      ) {
        var r, s;
        a in n && ((r = n[a]), (s = t.call(e, r, a, n)), (i[a] = s)), a++;
      }
      return i;
    }),
  "function" != typeof Array.prototype.reduce &&
    (Array.prototype.reduce = function (t) {
      if (null === this)
        throw new TypeError(
          "Array.prototype.reduce called on null or undefined"
        );
      if ("function" != typeof t) throw new TypeError(t + " is not a function");
      var e,
        i = Object(this),
        a = i.length >>> 0,
        n = 0;
      if (arguments.length >= 2) e = arguments[1];
      else {
        for (; n < a && !(n in i); ) n++;
        if (n >= a)
          throw new TypeError("Reduce of empty array with no initial value");
        e = i[n++];
      }
      for (; n < a; ) n in i && (e = t(e, i[n], n, i)), n++;
      return e;
    }),
  "function" != typeof Object.keys &&
    (Object.keys = (function () {
      "use strict";
      var t = Object.prototype.hasOwnProperty,
        e = !{ toString: null }.propertyIsEnumerable("toString"),
        i = [
          "toString",
          "toLocaleString",
          "valueOf",
          "hasOwnProperty",
          "isPrototypeOf",
          "propertyIsEnumerable",
          "constructor",
        ],
        a = i.length;
      return function (n) {
        if ("function" != typeof n && ("object" != typeof n || null === n))
          throw new TypeError("Object.keys called on non-object");
        var o,
          r,
          s = [];
        for (o in n) t.call(n, o) && s.push(o);
        if (e) for (r = 0; r < a; r++) t.call(n, i[r]) && s.push(i[r]);
        return s;
      };
    })()),
  (String.prototype.replaceAll = function (t, e) {
    return this.valueOf().split(t).join(e);
  }),
  (String.prototype.decode = function () {
    return this.valueOf()
      .replaceAll(/&#38;/g, "&")
      .replaceAll(/&#62;/g, ">")
      .replaceAll(/&#60;/g, "<")
      .replaceAll(/&#39;/g, "'")
      .replaceAll(/&#34;/g, '"')
      .replaceAll(/&#165;/g, "\\")
      .replaceAll(/&#123;/g, "{")
      .replaceAll(/&#125;/g, "}");
  }),
  (String.prototype.format = function () {
    for (var t = this, e = 0; e < arguments.length; e++) {
      var i = new RegExp("\\{" + e + "\\}", "gi");
      t = t.replace(i, arguments[e]);
    }
    return t.valueOf();
  }),
  (Array.prototype.removeSpace = function () {
    var t = [];
    return (
      this.forEach(function (e) {
        (e = e.trim()).length > 0 && t.push(e);
      }),
      t
    );
  }),
  (Array.prototype.random = function () {
    return this[Math.floor(Math.random() * this.length)];
  }),
  (Array.prototype.unique = function () {
    return this.filter(function (t, e, i) {
      return i.indexOf(t) == e;
    });
  }),
  (Array.prototype.except = function (t) {
    var e = this;
    return (
      Array.isArray(t) &&
        t.forEach(function (t) {
          var i = e.indexOf(t);
          -1 != i && e.splice(i, 1);
        }),
      e
    );
  }),
  (Array.prototype.only = function (t) {
    var e = [];
    return (
      Array.isArray(t) &&
        this.forEach(function (i) {
          -1 != t.indexOf(i) && e.push(i);
        }),
      e
    );
  }),
  (Array.prototype.insert = function (t, e) {
    this.splice(t, 0, e);
  });
var PrimePageScriptV2 = PrimePageScriptV2 || function () {};
(PrimePageScriptV2.prototype.init = function () {
  (this.const = {
    DESKTOP: "desktop",
    MOBILE: "mobile",
    DOMAIN_GOOGLE_DOCS: "docs.google.com",
    POWERED_BY_IMAGE: "",
    STATIC_W_DOMAIN: "w.ladicdn.com",
    STATIC_S_DOMAIN: "s.ladicdn.com",
    APP_RUNTIME_PREFIX: "_runtime",
    ACTION_TYPE: {
      action: "action",
      "1st_click": "1st_click",
      "2nd_click": "2nd_click",
      hover: "hover",
      complete: "complete",
    },
    DATA_ACTION_TYPE: {
      link: "link",
      section: "section",
      email: "email",
      phone: "phone",
      popup: "popup",
      dropbox: "dropbox",
      hidden_show: "hidden_show",
      collapse: "collapse",
      set_value: "set_value",
      copy_clipboard: "copy_clipboard",
      change_index: "change_index",
      set_style: "set_style",
      set_value_2nd: "set_value_2nd",
      lightbox: "lightbox",
      popup_cart: "popup_cart",
      popup_checkout: "popup_checkout",
    },
    DATA_ACTION_LIGHTBOX_TYPE: {
      image: "image",
      video: "video",
      iframe: "iframe",
    },
    COUNTDOWN_TYPE: {
      countdown: "countdown",
      daily: "daily",
      endtime: "endtime",
    },
    COUNTDOWN_ITEM_TYPE: {
      day: "day",
      hour: "hour",
      minute: "minute",
      seconds: "seconds",
    },
    VIDEO_TYPE: { youtube: "youtube", direct: "direct" },
    BACKGROUND_STYLE: {
      solid: "solid",
      gradient: "gradient",
      image: "image",
      video: "video",
    },
    PUBLISH_PLATFORM: {
      ladipage: "LADIPAGE",
      ladipagedns: "LADIPAGEDNS",
      wordpress: "WORDPRESS",
      haravan: "HARAVAN",
      sapo: "SAPO",
      shopify: "SHOPIFY",
      itop: "ITOPWEBSITE",
      ftp: "FTP",
      other: "OTHER",
    },
    SECTION_TYPE: { default: "DEFAULT", global: "GLOBAL" },
    TRACKING_NAME: "ladicid",
    ACCESS_KEY_NAME: "ladiack",
    REF_NAME: "ref",
    OTP_TYPE: {
      send: "OTP_SEND",
      resend: "OTP_RESEND",
      sms: "OTP_SMS",
      voice: "OTP_VOICE",
      zns: "OTP_ZNS",
    },
    STATUS_SEND: { capture: 1, otp: 1, sendform: 2 },
    PUBLISH_STYLE: { desktop_min_width: 768, mobile_small_min_width: 320 },
    ANIMATED_LIST: [
      "rotate-1",
      "rotate-2",
      "rotate-3",
      "type",
      "scale",
      "loading-bar",
      "slide",
      "clip",
      "zoom",
      "push",
    ],
    POSITION_TYPE: {
      default: "default",
      top: "top",
      bottom: "bottom",
      top_left: "top_left",
      top_center: "top_center",
      top_right: "top_right",
      center_left: "center_left",
      center_right: "center_right",
      bottom_left: "bottom_left",
      bottom_center: "bottom_center",
      bottom_right: "bottom_right",
    },
    COLLECTION_TYPE: { carousel: "carousel", readmore: "readmore" },
    INPUT_TYPE: {
      tel: "tel",
      password: "password",
      text: "text",
      date: "date",
      select_multiple: "select_multiple",
      number: "number",
      email: "email",
      textarea: "textarea",
      select: "select",
      radio: "radio",
      checkbox: "checkbox",
      facebook_checkbox_plugin_transactional:
        "facebook_checkbox_plugin_transactional",
      facebook_checkbox_plugin_promotional:
        "facebook_checkbox_plugin_promotional",
      file: "file",
      image_choices: "image_choices",
      product_variant: "product_variant",
    },
    CONTENT_TYPE: {
      form_data: "FORM_DATA",
      form_urlencoded: "X_WWW_FORM_URLENCODED",
      json: "JSON",
    },
    SORT_BY_TYPE: { asc: "asc", desc: "desc" },
    PRODUCT_VARIANT_TYPE: {
      combined: "combined",
      combobox: "combobox",
      label: "label",
    },
    PRODUCT_VARIANT_OPTION_TYPE: { color: 1, image: 2 },
    PRODUCT_VARIANT_TITLE: { left: "left", top: "top" },
    FORM_THANKYOU_TYPE: { default: "default", url: "url", popup: "popup" },
    GAME_RESULT_TYPE: { default: "default", popup: "popup" },
    PERCENT_TRACKING_SCROLL: [0, 25, 50, 75, 100],
    TIME_ONPAGE_TRACKING: [10, 30, 60, 120, 180, 300, 600],
    FORM_CONFIG_TYPE: {
      email: "EMAIL",
      mail_chimp: "MAIL_CHIMP",
      infusion_soft: "INFUSION_SOFT",
      infusion_soft_ladi: "INFUSION_SOFT_LADI",
      active_campaign: "ACTIVE_CAMPAIGN",
      sendgrid: "SENDGRID",
      hubspot: "HUBSPOT",
      smtp: "SMTP",
      esms: "ESMS",
      get_response: "GET_RESPONSE",
      convertkit: "CONVERTKIT",
      ladiflow: "LADIFLOW",
      telegram: "TELEGRAM",
      slack: "SLACK",
      zalo_zns: "ZALO_ZNS",
      mautic: "MAUTIC",
      google_sheet: "GOOGLE_SHEET",
      google_form: "GOOGLE_FORM",
      custom_api: "CUSTOM_API",
      ladisales: "LADISALES",
      haravan: "HARAVAN",
      sapo: "SAPO",
      shopify: "SHOPIFY",
      nhanh_vn: "NHANH_VN",
      google_recaptcha: "GOOGLE_RECAPTCHA",
      google_recaptcha_checkbox: "GOOGLE_RECAPTCHA_CHECKBOX",
      google_recaptcha_enterprise: "GOOGLE_RECAPTCHA_ENTERPRISE",
      kiotviet: "KIOTVIET",
      wordpress: "WORDPRESS",
      metu: "METU",
      ladichat: "LADICHAT",
      shopping: "SHOPPING",
      blog: "BLOG",
      conversion_api: "CONVERSION_API",
      popupx: "POPUPX",
    },
    FORM_UPLOAD_FILE_LENGTH: 20,
    FORM_UPLOAD_FILE_SIZE: 25,
    CART_LAYOUT: { editable: "editable", viewonly: "viewonly" },
    WIDTH_SECTION_RESIZE: {},
    RESIZE_ADD_PIXEL: 300,
    RESIZE_ADD_PIXEL_THUMB: 50,
    RESIZE_RANGE: 50,
    TOOLTIP_TYPE: {
      default: "default",
      info: "info",
      success: "success",
      error: "error",
      notice: "notice",
    },
    TOOLTIP_POSITION: {
      top_left: "top_left",
      top_middle: "top_middle",
      top_right: "top_right",
      bottom_left: "bottom_left",
      bottom_middle: "bottom_middle",
      bottom_right: "bottom_right",
      left_top: "left_top",
      left_middle: "left_middle",
      left_bottom: "left_bottom",
      right_top: "right_top",
      right_middle: "right_middle",
      right_bottom: "right_bottom",
    },
    TOOLTIP_SIZE: { small: "small", medium: "medium", big: "big" },
    STORY_PAGE: {
      horizontal: "horizontal",
      vertical: "vertical",
      none: "none",
    },
    COMBOBOX_TYPE: { delivery_method: "delivery_method" },
    PRODUCT_TYPE: { event: "Event", service: "Service", physical: "Physical" },
  }),
    (this.runtime = {
      backdrop_popup_id: "backdrop-popup",
      backdrop_dropbox_id: "backdrop-dropbox",
      lightbox_screen_id: "lightbox-screen",
      builder_section_popup_id: "SECTION_POPUP",
      builder_section_background_id: "BODY_BACKGROUND",
      ladipage_powered_by_classname: "ladipage_powered_by",
      current_element_mouse_down_carousel: null,
      current_element_mouse_down_carousel_position_x: 0,
      current_element_mouse_down_carousel_diff: 40,
      current_element_mouse_down_gallery_control: null,
      current_element_mouse_down_gallery_control_time: 0,
      current_element_mouse_down_gallery_control_time_click: 300,
      current_element_mouse_down_gallery_control_position_x: 0,
      current_element_mouse_down_gallery_view: null,
      current_element_mouse_down_gallery_view_position_x: 0,
      current_element_mouse_down_gallery_view_diff: 40,
      scroll_show_popup: {},
      scroll_depth: [],
      scroll_to_section: {},
      send_request_response: {},
      is_mobile_only: !1,
      interval_countdown: null,
      interval_gallery: null,
      timeout_gallery: {},
      interval_carousel: null,
      count_click_dom: {},
      timenext_carousel: {},
      time_click_dom: {},
      time_click: 300,
      time_otp: 6e4,
      isClient: !1,
      isDesktop: !0,
      isBrowserDesktop: !0,
      isIE: !1,
      isLoadHtmlGlobal: !1,
      isYouTubeIframeAPIReady: !1,
      isLoadYouTubeIframeAPI: !1,
      isVideoButtonUnmute: !0,
      device: this.const.DESKTOP,
      ladipage_id: null,
      func_get_code_otp: {},
      list_dataset_load: [],
      list_scroll_func: {},
      list_loaded_func: [],
      list_show_popup_func: {},
      list_youtube_ready_exec: [],
      list_scrolling_exec: {},
      list_scrolled_exec: {},
      list_lightbox_id: {},
      list_set_value_name_country: ["ward", "district", "state", "country"],
      tmp: {},
      tabindexForm: 0,
      eventData: {},
      eventDataGlobal: {},
      timenow: 0,
      widthScrollBar: 0,
      replaceStr: {},
      replacePrefixStart: "{{",
      replacePrefixEnd: "}}",
      replaceNewPrefixStart: "!::",
      replaceNewPrefixEnd: "::!",
    }),
    (this.const.WIDTH_SECTION_RESIZE[this.const.DESKTOP] = 1440),
    (this.const.WIDTH_SECTION_RESIZE[this.const.MOBILE] = 768);
}),
  (PrimePageScriptV2.prototype.isObject = function (t) {
    return !this.isFunction(t) && !this.isArray(t) && t instanceof Object;
  }),
  (PrimePageScriptV2.prototype.isArray = function (t) {
    return t instanceof Array;
  }),
  (PrimePageScriptV2.prototype.isFunction = function (t) {
    return "function" == typeof t || t instanceof Function;
  }),
  (PrimePageScriptV2.prototype.isBoolean = function (t) {
    return "boolean" == typeof t;
  }),
  (PrimePageScriptV2.prototype.isString = function (t) {
    return "string" == typeof t || t instanceof String;
  }),
  (PrimePageScriptV2.prototype.isEmpty = function (t) {
    return (
      !!this.isNull(t) ||
      (this.isString(t)
        ? 0 == (t = t.trim()).length || "undefined" == t.toLowerCase()
        : !!this.isArray(t) && 0 == t.length)
    );
  }),
  (PrimePageScriptV2.prototype.isNull = function (t) {
    return void 0 === t || null == t;
  }),
  (PrimePageScriptV2.prototype.equals = function (t, e) {
    return typeof t == typeof e && JSON.stringify(t) == JSON.stringify(e);
  }),
  (PrimePageScriptV2.prototype.copy = function (t) {
    return this.isNull(t) ? t : JSON.parse(JSON.stringify(t));
  });
var Base64 = {
    _keyStr:
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function (t) {
      var e,
        i,
        a,
        n,
        o,
        r,
        s,
        l = "",
        c = 0;
      for (t = Base64._utf8_encode(t); c < t.length; )
        (n = (e = t.charCodeAt(c++)) >> 2),
          (o = ((3 & e) << 4) | ((i = t.charCodeAt(c++)) >> 4)),
          (r = ((15 & i) << 2) | ((a = t.charCodeAt(c++)) >> 6)),
          (s = 63 & a),
          isNaN(i) ? (r = s = 64) : isNaN(a) && (s = 64),
          (l =
            l +
            Base64._keyStr.charAt(n) +
            Base64._keyStr.charAt(o) +
            Base64._keyStr.charAt(r) +
            Base64._keyStr.charAt(s));
      return l;
    },
    decode: function (t) {
      var e,
        i,
        a,
        n,
        o,
        r,
        s = "",
        l = 0;
      for (t = t.replace(/[^A-Za-z0-9\+\/\=]/g, ""); l < t.length; )
        (e =
          (Base64._keyStr.indexOf(t.charAt(l++)) << 2) |
          ((n = Base64._keyStr.indexOf(t.charAt(l++))) >> 4)),
          (i =
            ((15 & n) << 4) |
            ((o = Base64._keyStr.indexOf(t.charAt(l++))) >> 2)),
          (a = ((3 & o) << 6) | (r = Base64._keyStr.indexOf(t.charAt(l++)))),
          (s += String.fromCharCode(e)),
          64 != o && (s += String.fromCharCode(i)),
          64 != r && (s += String.fromCharCode(a));
      return Base64._utf8_decode(s);
    },
    _utf8_encode: function (t) {
      t = t.replace(/\r\n/g, "\n");
      for (var e = "", i = 0; i < t.length; i++) {
        var a = t.charCodeAt(i);
        a < 128
          ? (e += String.fromCharCode(a))
          : a > 127 && a < 2048
          ? ((e += String.fromCharCode((a >> 6) | 192)),
            (e += String.fromCharCode((63 & a) | 128)))
          : ((e += String.fromCharCode((a >> 12) | 224)),
            (e += String.fromCharCode(((a >> 6) & 63) | 128)),
            (e += String.fromCharCode((63 & a) | 128)));
      }
      return e;
    },
    _utf8_decode: function (t) {
      for (var e = "", i = 0, a = 0, n = 0, o = 0; i < t.length; )
        (a = t.charCodeAt(i)) < 128
          ? ((e += String.fromCharCode(a)), i++)
          : a > 191 && a < 224
          ? ((o = t.charCodeAt(i + 1)),
            (e += String.fromCharCode(((31 & a) << 6) | (63 & o))),
            (i += 2))
          : ((o = t.charCodeAt(i + 1)),
            (n = t.charCodeAt(i + 2)),
            (e += String.fromCharCode(
              ((15 & a) << 12) | ((63 & o) << 6) | (63 & n)
            )),
            (i += 3));
      return e;
    },
  },
  PrimePageScript = PrimePageScript || new PrimePageScriptV2();
PrimePageScript.init();
var LadiFormApi = LadiFormApi || {},
  LadiPageCommand = LadiPageCommand || [],
  parseFloatLadiPage = function (t, e) {
    var i = parseFloat(t);
    try {
      return parseFloat(PrimePageScript.formatNumber(i, 21, null, e || 6));
    } catch (t) {
      return NaN;
    }
  },
  decodeURIComponentLadiPage = function (t) {
    try {
      return decodeURIComponent(t);
    } catch (e) {
      return t;
    }
  };
(PrimePageScriptV2.prototype.loadDataset = function (t, e, i, a, n, o, r, s) {
  this.isArray(this.runtime.list_dataset_load) &&
    this.runtime.list_dataset_load.push({
      element_id: t,
      dataset_id: e,
      dataset_type: i,
      sort_name: a,
      sort_by: n,
      isFirst: o,
      is_client: r,
      callback: s,
    });
}),
  (PrimePageScriptV2.prototype.loadHtmlGlobal = function (t) {
    for (
      var e = this,
        i = 0,
        a = [],
        n = document.querySelectorAll(".prime-section[data-global-id]"),
        o = 0;
      o < n.length;
      o++
    ) {
      var r = n[o],
        s = r.getAttribute("data-global-id");
      e.isEmpty(s) || a.push({ id: r.id, data_global_id: s });
    }
    var l = function (n) {
      e.isObject(n) &&
        Object.keys(n).forEach(function (t) {
          e.runtime.eventDataGlobal[t] = n[t];
        }),
        i == a.length &&
          (function (t) {
            for (
              var i = [],
                a = document.querySelectorAll(
                  "div.prime-section-global[data-country-file]"
                ),
                n = 0;
              n < a.length;
              n++
            ) {
              var o = a[n].getAttribute("data-country-file");
              e.isEmpty(o) ||
                ((o = (o = o.split(";")).removeSpace()), (i = i.concat(o)));
            }
            if (
              (i = i.unique()).length > 0 &&
              e.runtime.hasOwnProperty("cdn_url") &&
              e.runtime.hasOwnProperty("version")
            ) {
              var r = 0;
              i.forEach(function (a) {
                var n = e.runtime.cdn_url + a + ".js?v=" + e.runtime.version;
                e.loadScript(n, null, !0, function (a) {
                  ++r < i.length ||
                    (e.runtime.load_location ||
                      ((e.runtime.load_location = !0), e.runAfterLocation()),
                    e.isFunction(t) && t());
                });
              });
            } else e.isFunction(t) && t();
          })(function () {
            document.querySelectorAll(
              '.prime-section-global[data-dataset="true"]'
            ).length > 0 &&
              !e.runtime.has_dataset &&
              ((e.runtime.has_dataset = !0),
              e.runtime.hasOwnProperty("cdn_url") &&
                e.runtime.hasOwnProperty("version") &&
                e.loadScript(
                  e.runtime.cdn_url + "dataset.min.js?v=" + e.runtime.version
                )),
              (e.runtime.isLoadHtmlGlobal = !0),
              e.run(t);
          });
    };
    a.forEach(function (t) {
      var a = t.id,
        n = document.getElementById(a),
        o = PrimePageScript.const.API_SECTION_GLOBAL_HTML.format(
          e.runtime.store_id,
          t.data_global_id
        );
      e.sendRequest("GET", o, null, !0, null, function (o, r, s) {
        if (s.readyState == XMLHttpRequest.DONE) {
          var c = document.createElement("div");
          (c.innerHTML = o),
            PrimePageScript.isEmpty(
              c.querySelector(
                'div.prime-section-global[data-id="' +
                  t.id +
                  '"] .prime-section[data-global-id="' +
                  t.data_global_id +
                  '"]'
              )
            )
              ? n.parentElement.removeChild(n)
              : (n.outerHTML = o),
            (function (t) {
              i++;
              var a = document.querySelector(
                '.prime-section-global[data-id="' + t + '"] > script'
              );
              if (e.isEmpty(a)) l();
              else {
                var n = null;
                try {
                  (n = JSON.parse(a.innerHTML)), (n = e.decodeValue(n));
                } catch (t) {}
                a.parentElement.removeChild(a), l(n);
              }
            })(a);
        }
      });
    }),
      l();
  }),
  (PrimePageScriptV2.prototype.postMessageWindow = function (t, e, i) {
    t.postMessage(JSON.stringify(e), i);
  }),
  (PrimePageScriptV2.prototype.updateHeightElement = function (
    t,
    e,
    i,
    a,
    n,
    o
  ) {
    var r = this;
    if (a != n) {
      var s = "style_update_height_element",
        l = !0;
      if (("fixed" == getComputedStyle(e).position && (l = !1), t)) {
        r.runtime.tmp.dimension_element_new = {};
        var c =
          "#" +
          r.runtime.builder_section_popup_id +
          " > .prime-container > .prime-element { max-height: none !important;}";
        r.createStyleElement(s, c);
      }
      for (
        var d,
          p,
          u = function (t, i) {
            return t.id == e.id && "height" == i
              ? n
              : r.isEmpty(r.runtime.tmp.dimension_element_new[t.id + i])
              ? parseFloatLadiPage(getComputedStyle(t)[i]) || 0
              : r.runtime.tmp.dimension_element_new[t.id + i];
          },
          m = function (t, e, i) {
            var a = u(t, e) + i;
            if (o) {
              var n = "transition-parent-collapse-" + e;
              t.classList.add(n);
              var s =
                1e3 *
                parseFloatLadiPage(getComputedStyle(t).transitionDuration);
              r.runTimeout(function () {
                t.classList.remove(n);
              }, s);
            }
            t.style.setProperty(e, a + "px"),
              (r.runtime.tmp.dimension_element_new[t.id + e] = a);
          },
          _ = n - a,
          y = u(e, "top") + a,
          g = 0;
        g < e.parentElement.children.length;
        g++
      ) {
        var f = e.parentElement.children[g];
        f.id != e.id && u(f, "top") >= y && m(f, "top", _);
      }
      if (!r.isEmpty(i) && i.id != r.runtime.builder_section_popup_id) {
        var v = u(i, "height"),
          h = (function () {
            for (var t = 0, i = 0; i < e.parentElement.children.length; i++) {
              var a = e.parentElement.children[i];
              if (
                "none" != getComputedStyle(a).display &&
                0 != u(a, "height")
              ) {
                var n = u(a, "top") + u(a, "height");
                n > t && (t = n);
              }
            }
            return t;
          })();
        if (
          ((i.classList.contains("prime-section") ||
            i.getElementsByClassName("prime-popup").length > 0) &&
            (h = v + _),
          m(i, "height", h - v),
          l)
        ) {
          var E =
            ((d = i),
            (p = r.findAncestor(d.parentElement, "prime-element")),
            r.isEmpty(p) &&
              (p = r.findAncestor(d.parentElement, "prime-section")),
            p);
          this.updateHeightElement(!1, i, E, v, h, o);
        }
      }
      if (t) {
        if (this.runtime.tmp.is_loaded_func_done) {
          var P = document.getElementById(s);
          this.isEmpty(P) || P.parentElement.removeChild(P);
        }
        delete r.runtime.tmp.dimension_element_new,
          r.fireEvent(window, "resize");
      }
    }
  }),
  (PrimePageScriptV2.prototype.showParentVisibility = function (t, e) {
    var i = this.findAncestor(t, "prime-popup");
    if (
      !this.isEmpty(i) &&
      ((i = this.findAncestor(i, "prime-element")), !this.isEmpty(i))
    )
      return (
        "none" == getComputedStyle(i).display &&
          i.classList.add("hide-visibility"),
        this.isFunction(e) && e(),
        void i.classList.remove("hide-visibility")
      );
    this.isFunction(e) && e();
  }),
  (PrimePageScriptV2.prototype.pauseAllVideo = function (t) {
    var e = document
      .getElementById(this.runtime.lightbox_screen_id)
      .getElementsByClassName("lightbox-close")[0];
    if (!this.isEmpty(e)) return e.click(), this.pauseAllVideo(t);
    delete this.runtime.tmp.gallery_playing_video;
    for (
      var i = (t = t || document).querySelectorAll(
          ".iframe-video-preload:not(.no-pause)"
        ),
        a = 0;
      a < i.length;
      a++
    )
      this.runEventReplayVideo(
        i[a].id,
        i[a].getAttribute("data-video-type"),
        !1
      );
  }),
  (PrimePageScriptV2.prototype.runEventReplayVideo = function (t, e, i) {
    var a = this,
      n = document.getElementById(t),
      o = null;
    if (!a.isEmpty(n)) {
      var r = document.getElementById(t + "_button_unmute"),
        s = a.isEmpty(r);
      if (e == a.const.VIDEO_TYPE.youtube) {
        o = i ? "playVideo" : "pauseVideo";
        var l = !1;
        if (a.runtime.isYouTubeIframeAPIReady) {
          var c = window.YT.get(t);
          if (!a.isEmpty(c)) {
            if (
              (s && i && a.isFunction(c.unMute) && c.unMute(),
              !s && i && a.isFunction(c.mute) && c.mute(),
              !a.isFunction(c[o]))
            )
              return void a.runTimeout(function () {
                a.runEventReplayVideo(t, e, i);
              }, 100);
            c[o](), (l = !0);
          }
        }
        l ||
          (s &&
            i &&
            a.postMessageWindow(
              n.contentWindow,
              { event: "command", func: "unMute", args: [] },
              "*"
            ),
          !s &&
            i &&
            a.postMessageWindow(
              n.contentWindow,
              { event: "command", func: "mute", args: [] },
              "*"
            ),
          a.postMessageWindow(
            n.contentWindow,
            { event: "command", func: o, args: [] },
            "*"
          ));
      }
      e == a.const.VIDEO_TYPE.direct &&
        ((o = i ? "play" : "pause"),
        s && i && (n.muted = !1),
        !s && i && (n.muted = !0),
        a.isFunction(n[o]) && n[o]()),
        i
          ? (n.classList.remove("prime-hidden"),
            a.isEmpty(r) || r.style.removeProperty("display"))
          : (n.classList.add("prime-hidden"),
            a.isEmpty(r) || r.style.setProperty("display", "none"));
    }
  }),
  (PrimePageScriptV2.prototype.runEventPlayVideo = function (
    t,
    e,
    i,
    a,
    n,
    o,
    r,
    s,
    l,
    c
  ) {
    var d = this,
      p = d.runtime.isVideoButtonUnmute;
    (d.runtime.isDesktop || r || s || n || a) && (p = !1);
    var u = function (t, i) {
        if (p && !t.hasAttribute("data-remove-button-unmute")) {
          var a = t.id + "_button_unmute",
            n = document.getElementById(a);
          d.isEmpty(n) &&
            (((n = document.createElement("div")).id = a),
            (n.innerHTML = "<div></div>"),
            (n.className = "button-unmute prime-hidden"),
            n.addEventListener("click", function (i) {
              if (
                (i.stopPropagation(),
                (t = document.getElementById(t.id)),
                e == d.const.VIDEO_TYPE.youtube)
              ) {
                var a = !1;
                if (d.runtime.isYouTubeIframeAPIReady) {
                  var o = window.YT.get(t.id);
                  !d.isEmpty(o) &&
                    d.isFunction(o.unMute) &&
                    (o.unMute(), (a = !0));
                }
                a ||
                  d.postMessageWindow(
                    t.contentWindow,
                    { event: "command", func: "unMute", args: [] },
                    "*"
                  );
              }
              e == d.const.VIDEO_TYPE.direct && (t.muted = !1),
                n.parentElement.removeChild(n),
                t.setAttribute("data-remove-button-unmute", !0);
            }),
            t.parentElement.appendChild(n)),
            i && n.classList.remove("prime-hidden");
          var o = d.findAncestor(t, "lightbox-screen");
          d.isEmpty(o) ||
            (n.style.setProperty("width", getComputedStyle(t).width),
            n.style.setProperty("height", getComputedStyle(t).height));
        }
      },
      m = document.getElementById(t);
    if (!d.isEmpty(m)) {
      var _ = d.findAncestor(m, "prime-video");
      d.isEmpty(_) || (_ = d.findAncestor(_, "prime-element")),
        !a ||
          o ||
          d.isEmpty(_) ||
          (_.classList.add("pointer-events-none"), m.classList.add("no-pause")),
        r && m.classList.add("prime-hidden");
      var y = "";
      if (!d.isEmpty(_)) {
        var g = _.getElementsByClassName("prime-video-background")[0];
        if (!d.isEmpty(g)) {
          var f = getComputedStyle(g).backgroundImage;
          f.toLowerCase().startsWith('url("') &&
            f.toLowerCase().endsWith('")') &&
            ((f = (f = f.substr('url("'.length)).substr(
              0,
              f.length - '")'.length
            )),
            d.isEmpty(f) || (y = f));
        }
      }
      if (e == d.const.VIDEO_TYPE.youtube) {
        var v = d.getVideoId(e, i),
          h = function () {
            try {
              if (
                ((d.runtime.isLoadYouTubeIframeAPI &&
                  d.runtime.isYouTubeIframeAPIReady) ||
                  !d.isObject(window.YT) ||
                  !d.isFunction(window.YT.Player) ||
                  ((d.runtime.isLoadYouTubeIframeAPI = !0),
                  (d.runtime.isYouTubeIframeAPIReady = !0)),
                d.runtime.isLoadYouTubeIframeAPI ||
                  ((d.runtime.isLoadYouTubeIframeAPI = !0),
                  (window.onYouTubeIframeAPIReady = function () {
                    for (
                      d.runtime.isYouTubeIframeAPIReady = !0;
                      d.runtime.list_youtube_ready_exec.length > 0;

                    )
                      d.runtime.list_youtube_ready_exec.shift()();
                  }),
                  d.loadScript("https://www.youtube.com/iframe_api")),
                !d.runtime.isYouTubeIframeAPIReady)
              )
                return void d.runtime.list_youtube_ready_exec.push(h);
              (m.outerHTML = m.outerHTML
                .replaceAll("<iframe", "<div")
                .replaceAll("</iframe>", "</div>")),
                (m = document.getElementById(t)),
                n && m.classList.add("opacity-0");
              var e = function () {
                  n &&
                    (m = document.getElementById(t)).classList.remove(
                      "opacity-0"
                    );
                },
                i = e,
                _ = function (e) {
                  m = document.getElementById(t);
                  var i = window.YT.get(t);
                  d.isEmpty(i) || d.isEmpty(m)
                    ? d.runTimeout(_, 100)
                    : (d.runResizeAll(),
                      a || p ? i.mute() : i.unMute(),
                      r || s || i.playVideo(),
                      d.isFunction(c) && c(),
                      u(m));
                };
              new window.YT.Player(t, {
                videoId: v,
                playerVars: {
                  rel: 0,
                  modestbranding: 0,
                  playsinline: n || a || l || p ? 1 : 0,
                  controls: !n && o ? 1 : 0,
                },
                events: {
                  onReady: _,
                  onStateChange: function (i) {
                    if (i.data == window.YT.PlayerState.PLAYING) {
                      if (((m = document.getElementById(t)), n)) {
                        var a = function () {
                          window.YT.get(t).getCurrentTime() >= 0.1
                            ? e()
                            : d.runTimeout(a, 100);
                        };
                        a();
                      }
                      u(m, !0);
                      var o = document.getElementById(t + "_button_unmute");
                      d.isEmpty(o) || window.YT.get(t).mute();
                    }
                    i.data == window.YT.PlayerState.ENDED &&
                      window.YT.get(t).playVideo();
                  },
                  onError: i,
                },
              });
            } catch (t) {}
          };
        h();
      }
      if (e == d.const.VIDEO_TYPE.direct) {
        d.isEmpty(y) || m.setAttribute("poster", y),
          m.setAttribute("preload", "auto"),
          m.setAttribute("controlslist", "nodownload"),
          m.setAttribute("loop", ""),
          r || s || m.setAttribute("autoplay", ""),
          (n || a || l || p) &&
            (m.setAttribute("playsinline", ""),
            m.setAttribute("webkit-playsinline", "")),
          !n && o && m.setAttribute("controls", ""),
          a || p ? m.setAttribute("muted", "") : m.removeAttribute("muted"),
          n && m.classList.add("opacity-0");
        var E = function () {
          n && (m = document.getElementById(t)).classList.remove("opacity-0");
        };
        m.removeAttribute("src"),
          m.setAttribute("data-src", i),
          (m.outerHTML = m.outerHTML.replaceAll("data-src=", "src=")),
          (m = document.getElementById(t)),
          d.isFunction(c) && c(m),
          m.addEventListener("loadedmetadata", function (t) {
            u(m);
          }),
          m.addEventListener("error", E),
          m.addEventListener("playing", function (t) {
            if (n) {
              var e = function () {
                m.currentTime >= 0.1 ? E() : d.runTimeout(e, 100);
              };
              e();
            }
            u(m, !0);
            var i = document.getElementById(m.id + "_button_unmute");
            d.isEmpty(i) || (m.muted = !0);
          });
      }
    }
  }),
  (PrimePageScriptV2.prototype.playVideo = function (t, e, i, a, n, o) {
    var r = document.getElementById(t);
    if (!this.isEmpty(r)) {
      var s = document.getElementById(t + "_player");
      if ((o || n || this.pauseAllVideo(), this.isEmpty(s))) {
        var l = r.getElementsByClassName("prime-video")[0],
          c = t + "_player";
        e == this.const.VIDEO_TYPE.youtube &&
          ((l.innerHTML =
            l.innerHTML +
            '<iframe id="' +
            c +
            '" class="iframe-video-preload" data-video-type="' +
            e +
            '" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0;" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),
          this.runEventPlayVideo(c, e, i, n, !1, a, o)),
          e == this.const.VIDEO_TYPE.direct &&
            ((l.innerHTML =
              l.innerHTML +
              '<video id="' +
              c +
              '" class="iframe-video-preload" data-video-type="' +
              e +
              '" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; object-fit: cover;"></video>'),
            this.runEventPlayVideo(c, e, i, n, !1, a, o));
      } else this.runEventReplayVideo(s.id, e, !0);
    }
  }),
  (PrimePageScriptV2.prototype.checkResizeImage = function (t) {
    var e = [".jpg", ".jpeg", ".png"],
      i = function (t) {
        for (var i = !1, a = 0; a < e.length; a++)
          if (t.endsWith(e[a])) {
            i = !0;
            break;
          }
        return i;
      };
    if (!i(t.toLowerCase())) {
      var a = this.getElementAHref(t, !0);
      return (a.search = ""), i(a.href);
    }
    return !0;
  }),
  (PrimePageScriptV2.prototype.getOptimizeImage = function (
    t,
    e,
    i,
    a,
    n,
    o,
    r
  ) {
    if (this.isEmpty(t) || !this.isString(t)) return t;
    var s = t.split("/"),
      l = s.indexOf("");
    if (
      (-1 != l && s.length > l + 1 && (s[l + 1] = s[l + 1].toLowerCase()),
      (l = s.indexOf(this.const.STATIC_W_DOMAIN.toLowerCase())),
      this.checkResizeImage(t) &&
        -1 != l &&
        (s.length == l + 3 ||
          (s.length == l + 6 && "ls" == s[3] && "product" == s[5]) ||
          (s.length == l + 4 && "luid" == s[3] && "avatar" == s[4]) ||
          (s.length == l + 4 && "rbg" == s[4])))
    ) {
      var c = s[l + 1].toLowerCase(),
        d = !0;
      if (c.startsWith("s")) {
        var p = c.split("x");
        2 == p.length && parseFloatLadiPage(p[1]) == p[1] && (d = !1);
      }
      if (d) {
        if (r || n) {
          if (((e = parseInt(e) || 0), (i = parseInt(i) || 0), a)) {
            var u =
              this.const.RESIZE_RANGE +
              (o
                ? this.const.RESIZE_ADD_PIXEL_THUMB
                : this.const.RESIZE_ADD_PIXEL);
            (e <= 0 || i <= 0) && (u *= 2),
              (e = e - (e % this.const.RESIZE_RANGE) + u),
              (i = i - (i % this.const.RESIZE_RANGE) + u);
          }
        } else
          (e = this.const.WIDTH_SECTION_RESIZE[LadiPage.data.device_screen]),
            (i = this.const.WIDTH_SECTION_RESIZE[LadiPage.data.device_screen]);
        s.insert(l + 1, "s" + e + "x" + i);
      }
    }
    return s.join("/");
  }),
  (PrimePageScriptV2.prototype.historyReplaceState = function (t) {
    try {
      window.history.replaceState(null, null, t);
    } catch (t) {}
  }),
  (PrimePageScriptV2.prototype.resetViewport = function () {
    this.isEmpty(this.runtime.tmp.timeoutViewport) ||
      this.removeTimeout(this.runtime.tmp.timeoutViewport),
      this.isFunction(window.ladi_viewport) &&
        (this.runtime.tmp.timeoutViewport = this.runTimeout(
          window.ladi_viewport,
          10
        ));
  }),
  (PrimePageScriptV2.prototype.runStoryPage = function () {
    var t = this,
      e = t.runtime.story_page;
    if (this.isObject(e)) {
      var i = document.getElementsByClassName("prime-wraper")[0];
      if (!this.isEmpty(i)) {
        var a = document.getElementsByClassName(
          "prime-story-page-progress-bar"
        )[0];
        this.isEmpty(a) || a.parentElement.removeChild(a);
        var n = document.querySelectorAll(
          '.prime-section:not([id="' +
            this.runtime.builder_section_popup_id +
            '"]):not([id="' +
            this.runtime.builder_section_background_id +
            '"])'
        );
        if (0 != n.length) {
          (a = document.createElement("div")).className =
            "prime-story-page-progress-bar";
          for (
            var o = null,
              r =
                (n[0],
                function (e, i) {
                  e.addEventListener("click", function (e) {
                    e.stopPropagation(),
                      t.removeTimeout(t.runtime.tmp.story_page_next_timeout_id),
                      window.ladi(i.id, i).scroll();
                  });
                }),
              s = 0;
            s < n.length;
            s++
          ) {
            var l = document.createElement("div");
            (l.className = "prime-story-page-progress-bar-item"),
              l.style.setProperty(
                "width",
                "calc(100% / " + n.length + " - 10px)"
              ),
              r(l, n[s]),
              a.appendChild(l),
              0 == s && (o = l);
          }
          i.appendChild(a),
            (t.runtime.tmp.story_page_mouse_down = !1),
            (t.runtime.tmp.story_page_current_page = 1);
          var c = function (i, a) {
              if (!t.isEmpty(i)) {
                a && i.click();
                for (var n = i; !t.isEmpty(n.previousElementSibling); )
                  (n = n.previousElementSibling).classList.add("active");
                for (var o = i; !t.isEmpty(o.nextElementSibling); )
                  (o = o.nextElementSibling).classList.remove("active");
                i.classList.remove("active");
                var r = i.parentElement.getElementsByTagName("span")[0];
                t.isEmpty(r) || r.parentElement.removeChild(r),
                  (r = document.createElement("span")),
                  i.appendChild(r),
                  e.is_autoplay || r.style.setProperty("width", "100%");
              }
            },
            d = function () {
              if (e.is_autoplay) {
                var i = 300,
                  a = null;
                if (
                  t.isEmpty(t.runtime.tmp.current_default_popup_id) &&
                  !t.runtime.tmp.story_page_mouse_down &&
                  !t.runtime.tmp.story_page_scroll &&
                  ((a = document.querySelector(
                    ".prime-story-page-progress-bar-item span"
                  )),
                  !t.isEmpty(a))
                ) {
                  var n =
                    parseFloatLadiPage(a.style.getPropertyValue("width")) || 0;
                  (n =
                    (n =
                      (((n = (1e3 * e.autoplay_time) / (100 / n)) + i) /
                        (1e3 * e.autoplay_time)) *
                      100) > 100
                      ? 100
                      : n),
                    a.style.setProperty("width", n + "%"),
                    n >= 100 &&
                      ((t.runtime.tmp.story_page_next_timeout_id = t.runTimeout(
                        function () {
                          t.isEmpty(a.parentElement) ||
                            c(a.parentElement.nextElementSibling, !0);
                        },
                        i
                      )),
                      (i *= 2));
                }
                t.runtime.tmp.story_page_timeout_id = t.runTimeout(d, i);
              }
            };
          c(o, !1),
            (t.runtime.tmp.story_page_timeout_id = t.runTimeout(d, 300)),
            document.addEventListener("mousedown", function (e) {
              t.runtime.tmp.story_page_mouse_down = !0;
            }),
            document.addEventListener(
              "touchstart",
              function (e) {
                t.runtime.tmp.story_page_mouse_down = !0;
              },
              t.runtime.scrollEventPassive
            ),
            document.addEventListener("mouseup", function (e) {
              t.runtime.tmp.story_page_mouse_down = !1;
            }),
            document.addEventListener("touchend", function (e) {
              t.runtime.tmp.story_page_mouse_down = !1;
            }),
            i.addEventListener(
              "scroll",
              function (a) {
                t.isEmpty(t.runtime.tmp.current_default_popup_id) &&
                  ((t.runtime.tmp.story_page_scroll = !0),
                  t.removeTimeout(t.runtime.tmp.story_page_scroll_timeout_id),
                  t.removeTimeout(t.runtime.tmp.story_page_timeout_id),
                  (t.runtime.tmp.story_page_scroll_timeout_id = t.runTimeout(
                    function () {
                      var a = 0,
                        n =
                          e.type == t.const.STORY_PAGE.horizontal
                            ? i.scrollLeft / i.clientWidth
                            : i.scrollTop / i.clientHeight;
                      if (
                        (n = Math.floor(n + 1.5)) !=
                        t.runtime.tmp.story_page_current_page
                      ) {
                        t.runtime.tmp.story_page_current_page = n;
                        var o = document.querySelector(
                          ".prime-story-page-progress-bar-item:nth-child(" +
                            n +
                            ")"
                        );
                        t.removeTimeout(
                          t.runtime.tmp.story_page_next_timeout_id
                        ),
                          c(o, !1),
                          (a = 100);
                      }
                      (t.runtime.tmp.story_page_scroll = !1),
                        delete t.runtime.tmp.story_page_scroll_timeout_id,
                        (t.runtime.tmp.story_page_timeout_id = t.runTimeout(
                          d,
                          a
                        ));
                    },
                    300
                  )));
              },
              t.runtime.scrollEventPassive
            );
        }
      }
    }
  }),
  (PrimePageScriptV2.prototype.runThankyouPage = function () {
    var t = this.runtime.thankyou_page;
    if (this.isObject(t)) {
      var e = { is_thankyou_page: !0 };
      (e.conversion_name = t.event_value),
        (e.google_ads_conversion = t.event_id),
        (e.google_ads_label = t.event_label),
        (e.purchase_value = t.purchase_value),
        (e.event_category = "LadiPageThankYouPage"),
        this.runEventTracking(null, e),
        delete this.runtime.thankyou_page;
    }
  }),
  (PrimePageScriptV2.prototype.runResizeSectionBackground = function () {
    var t = this;
    t instanceof PrimePageScriptV2 || (t = PrimePageScript);
    try {
      for (
        var e = document.querySelectorAll(
            ".prime-section .prime-section-background iframe.prime-section-background-video"
          ),
          i = 0;
        i < e.length;
        i++
      ) {
        var a = e[i],
          n = parseFloatLadiPage(a.getAttribute("width")) || 0,
          o = parseFloatLadiPage(a.getAttribute("height")) || 0;
        if (!(n <= 0 || o <= 0)) {
          var r = t.findAncestor(a, "prime-section-background"),
            s = r.clientWidth,
            l = (o / n) * s;
          l < r.clientHeight &&
            ((s = (r.clientHeight / l) * s), (l = r.clientHeight));
          var c = (r.clientHeight - l) / 2,
            d = (r.clientWidth - s) / 2;
          a.style.setProperty("top", (parseFloatLadiPage(c) || 0) + "px"),
            a.style.setProperty("left", (parseFloatLadiPage(d) || 0) + "px"),
            a.style.setProperty("width", (parseFloatLadiPage(s) || 0) + "px"),
            a.style.setProperty("height", (parseFloatLadiPage(l) || 0) + "px");
        }
      }
    } catch (t) {}
  }),
  (PrimePageScriptV2.prototype.runResizeAll = function () {
    var t = this;
    t instanceof PrimePageScriptV2 || (t = PrimePageScript);
    try {
      t.isFunction(window.ladi_viewport) && window.ladi_viewport(),
        t.runtime.tmp.generateCart();
      for (
        var e = document.querySelectorAll(
            "#" +
              t.runtime.builder_section_popup_id +
              " .prime-container > .prime-element"
          ),
          i = 0;
        i < e.length;
        i++
      )
        "none" != getComputedStyle(e[i]).display &&
          window.ladi(e[i].id).show(!0);
      t.runResizeSectionBackground();
    } catch (t) {}
  }),
  (PrimePageScriptV2.prototype.runEventResize = function (t) {
    var e = this;
    e instanceof PrimePageScriptV2 || (e = PrimePageScript),
      e.isEmpty(e.runtime.tmp.timeoutResizeAll) ||
        e.removeTimeout(e.runtime.tmp.timeoutResizeAll),
      (e.runtime.tmp.timeoutResizeAll = e.runTimeout(e.runResizeAll, 10));
  }),
  (PrimePageScriptV2.prototype.runEventOrientationChange = function (t) {
    var e = this;
    e instanceof PrimePageScriptV2 || (e = PrimePageScript), e.runEventResize(t);
  }),
  (PrimePageScriptV2.prototype.runAfterLocation = function () {
    var t = this;
    if (
      (t instanceof PrimePageScriptV2 || (t = PrimePageScript), t.runtime.isRun)
    ) {
      for (; t.runtime.tmp.listAfterLocation.length > 0; )
        t.runtime.tmp.listAfterLocation.shift()();
      t.reloadFeeShipping();
    } else t.runTimeout(t.runAfterLocation, 100);
  }),
  (PrimePageScriptV2.prototype.removeLazyloadPopup = function (t) {
    var e = document.getElementById(t);
    if (!this.isEmpty(e))
      for (var i = e.getElementsByClassName("prime-lazyload"); i.length > 0; )
        i[0].classList.remove("prime-lazyload");
  }),
  (PrimePageScriptV2.prototype.reloadLazyload = function (t) {
    var e = this;
    if (
      (e instanceof PrimePageScriptV2 || (e = PrimePageScript),
      e.runtime.tmp.is_loaded_func_done)
    ) {
      var i = [];
      if (t && e.isObject(e.runtime.story_page)) {
        var a = document.getElementsByClassName("prime-section")[0];
        if (!e.isEmpty(a))
          for (i = a.getElementsByClassName("prime-lazyload"); i.length > 0; )
            i[0].classList.remove("prime-lazyload");
      } else {
        i = document.getElementsByClassName("prime-lazyload");
        for (var n = [], o = 0; o < i.length; o++) {
          var r = e.getElementBoundingClientRect(i[o]).y + window.scrollY;
          window.scrollY + e.getHeightDevice() > r &&
            r + i[o].offsetHeight > window.scrollY &&
            n.push(i[o]);
        }
        n.forEach(function (t) {
          t.classList.remove("prime-lazyload");
        });
        for (
          var s = document.querySelectorAll(
              ".prime-gallery .prime-gallery-view-item.selected:not(.prime-lazyload)"
            ),
            l = 0;
          l < s.length;
          l++
        )
          if (e.isEmpty(s[l].getAttribute("data-lazyload"))) {
            s[l].setAttribute("data-lazyload", !0);
            for (
              var c =
                s[l].parentElement.getElementsByClassName("prime-lazyload");
              c.length > 0;

            )
              c[0].classList.remove("prime-lazyload");
          }
      }
    }
  }),
  (PrimePageScriptV2.prototype.documentLoaded = function () {
    var t = this;
    t instanceof PrimePageScriptV2 || (t = PrimePageScript),
      (t.const.LANG = JSON.stringify(t.const.LANG)),
      (t.const.LANG = t.convertReplacePrefixStr(t.const.LANG)),
      (t.const.LANG = JSON.parse(t.const.LANG));
    var e = t.getURLSearchParams(null, null, !0),
      i = e.ladishow,
      a = e.ladihide,
      n = e.laditop,
      o = window.location.hash;
    t.isEmpty(i) ? (i = []) : t.isArray(i) || (i = i.split(",").removeSpace()),
      t.isEmpty(a)
        ? (a = [])
        : t.isArray(a) || (a = a.split(",").removeSpace()),
      t.isEmpty(n)
        ? (n = [])
        : t.isArray(n) || (n = n.split(",").removeSpace().reverse());
    try {
      var r = window.ladi("LADI_CAMP_END_DATE").get_cookie(),
        s = window.ladi("LADI_CAMP_CONFIG").get_cookie();
      if (!t.isEmpty(r) && !t.isEmpty(s)) {
        s = JSON.parse(Base64.decode(s));
        var l = ((r = parseInt(r) || 0) - Date.now()) / 24 / 60 / 60 / 1e3;
        if (l > 0 && t.isArray(s.dynamic_content.cookie)) {
          var c = [];
          s.dynamic_content.cookie.forEach(function (e) {
            var i = e.split("=");
            2 != (i = i.removeSpace()).length ||
              t.isEmpty(i[0]) ||
              t.isEmpty(i[1]) ||
              c.push({ name: i[0], value: i[1] });
          }),
            c.forEach(function (t) {
              window.ladi(t.name).set_cookie(t.value, l);
            });
        }
        (a = s.dynamic_content.hide || []),
          (i = s.dynamic_content.show || []),
          (n = s.dynamic_content.top || []),
          t.isArray(s.dynamic_content.scroll) &&
            s.dynamic_content.scroll.length > 0 &&
            (o = "#" + s.dynamic_content.scroll.pop());
      }
    } catch (t) {}
    var d = null,
      p = [];
    if (
      (i.forEach(function (e) {
        var i = document.getElementById(e);
        t.isEmpty(i) ||
          (i.getElementsByClassName("prime-popup").length > 0 && p.push(e));
      }),
      p.length > 0 && !t.isEmpty(o))
    )
      try {
        (d = document.querySelector(o)),
          t.isEmpty(d) ||
            t.isEmpty(d.id) ||
            !d.classList.contains("prime-section") ||
            (window.ladi(d.id).scroll(!1, !0), (o = null));
      } catch (t) {}
    if (
      (a.forEach(function (t) {
        window.ladi(t).hide();
      }),
      i.forEach(function (t) {
        window.ladi(t).show();
      }),
      n.forEach(function (t) {
        window.ladi(t).top();
      }),
      !t.isEmpty(o))
    )
      try {
        (d = document.querySelector(o)),
          t.isEmpty(d) ||
            t.isEmpty(d.id) ||
            t.runTimeout(function () {
              window.ladi(d.id).scroll();
            }, 300);
      } catch (t) {}
    if (
      (t.resetViewport(),
      t.runEventScroll(),
      !t.isEmpty(t.runtime.tracking_global_url))
    ) {
      var u = !1,
        m = window;
      t.isObject(t.runtime.story_page) &&
        (m = document.getElementsByClassName("prime-wraper")[0]);
      var _ = function () {
        u ||
          ((u = !0),
          t.loadScript(t.runtime.tracking_global_url + "?v=" + Date.now()),
          m.removeEventListener("scroll", _),
          document.removeEventListener("mousemove", _),
          document.removeEventListener("touchstart", _));
      };
      m.addEventListener("scroll", _, t.runtime.scrollEventPassive),
        document.addEventListener("mousemove", _),
        document.addEventListener(
          "touchstart",
          _,
          t.runtime.scrollEventPassive
        ),
        t.runTimeout(_, t.runtime.tracking_global_delay);
    }
    for (; t.runtime.list_loaded_func.length > 0; )
      t.runtime.list_loaded_func.shift()();
    t.runtime.tmp.is_loaded_func_done = !0;
    var y = document.getElementById("style_update_height_element");
    t.isEmpty(y) || y.parentElement.removeChild(y);
    var g = document.getElementById("style_animation");
    t.isEmpty(g) || g.parentElement.removeChild(g), t.reloadLazyload(!1);
  }),
  (PrimePageScriptV2.prototype.runConversionApi = function (t, e, i) {
    if (
      (!this.isEmpty(t) &&
        !this.isEmpty(e) &&
        this.isObject(window.ladi_conversion_api) &&
        this.isObject(window.ladi_conversion_api[t]) &&
        (window.ladi_conversion_api[t][e] = i),
      this.isObject(window.ladi_conversion_api) &&
        this.isObject(window.ladi_conversion_api.facebook) &&
        this.isArray(window.ladi_conversion_api.facebook.pixels) &&
        this.isArray(window.ladi_conversion_api.facebook.events) &&
        this.isFunction(window.fbq))
    ) {
      (window.ladi_conversion_api.facebook.pixels =
        window.ladi_conversion_api.facebook.pixels.unique()),
        (window.ladi_conversion_api.facebook.fbc = window
          .ladi("_fbc")
          .get_cookie()),
        (window.ladi_conversion_api.facebook.fbp = window
          .ladi("_fbp")
          .get_cookie());
      for (
        var a = 0;
        a < window.ladi_conversion_api.facebook.events.length;
        a++
      ) {
        var n = window.ladi_conversion_api.facebook.events[a].data;
        (n.event_id = n.eventID),
          delete n.eventID,
          (window.ladi_conversion_api.facebook.events[a].data = n);
      }
      this.runtime.tmp.runTrackingAnalytics("ConversionApi", {
        data: window.ladi_conversion_api,
      }),
        delete window.ladi_conversion_api.facebook.fbc,
        delete window.ladi_conversion_api.facebook.fbp,
        delete window.ladi_conversion_api.facebook.events;
    }
  }),
  (PrimePageScriptV2.prototype.getWidthDevice = function (t) {
    if (this.runtime.is_mobile_only) {
      var e = document.getElementsByClassName("prime-wraper")[0];
      if (!this.isEmpty(e)) return e.clientWidth;
    }
    var i = window.ladi_screen_width || window.screen.width;
    return t
      ? window.innerWidth > 0
        ? window.innerWidth
        : i
      : window.outerWidth > 0
      ? window.outerWidth
      : i;
  }),
  (PrimePageScriptV2.prototype.getHeightDevice = function (t) {
    return window.outerHeight > 0 &&
      !this.runtime.isDesktop &&
      ((t && window.outerHeight > window.innerHeight) ||
        (!t && window.innerHeight > window.outerHeight))
      ? window.outerHeight
      : window.innerHeight;
  }),
  (PrimePageScriptV2.prototype.startAutoScroll = function (t, e, i, a) {
    if (this.runtime.isDesktop ? i : a) {
      var n = document.getElementById(t);
      if (!this.isEmpty(n) && !n.classList.contains("prime-auto-scroll")) {
        var o = 0;
        if ("section" != e) {
          if (n.clientWidth <= this.getWidthDevice()) return;
          o =
            (o = parseFloatLadiPage(getComputedStyle(n).left) || 0) > 0
              ? 0
              : -1 * o;
        } else {
          for (
            var r = n.querySelectorAll(".prime-container > .prime-element"),
              s = 0;
            s < r.length;
            s++
          ) {
            var l = parseFloatLadiPage(getComputedStyle(r[s]).left) || 0;
            l < o && (o = l);
          }
          (o = o > 0 ? 0 : -1 * o),
            n
              .querySelector(".prime-container")
              .style.setProperty("margin-left", o + "px");
        }
        n.classList.add("prime-auto-scroll"), (n.scrollLeft = o);
      }
    }
  }),
  (PrimePageScriptV2.prototype.getLinkUTMRedirect = function (t, e) {
    var i = this.createTmpElement("a", "", { href: t }),
      a = this.getURLSearchParams(e, null, !1),
      n = a.utm_source;
    if (!this.isEmpty(n)) {
      n = "utm_source=" + encodeURIComponent(n);
      var o = a.utm_medium,
        r = a.utm_campaign,
        s = a.utm_term,
        l = a.utm_content;
      this.isEmpty(o) || (n += "&utm_medium=" + encodeURIComponent(o)),
        this.isEmpty(r) || (n += "&utm_campaign=" + encodeURIComponent(r)),
        this.isEmpty(s) || (n += "&utm_term=" + encodeURIComponent(s)),
        this.isEmpty(l) || (n += "&utm_content=" + encodeURIComponent(l)),
        this.isEmpty(i.href) ||
          this.isEmpty(i.host) ||
          !this.isEmpty(this.getURLSearchParams(i.search, "utm_source", !1)) ||
          (i.search = i.search + (this.isEmpty(i.search) ? "?" : "&") + n);
    }
    return i.href;
  }),
  (PrimePageScriptV2.prototype.randomInt = function (t, e) {
    return (
      (t = Math.ceil(t)),
      (e = Math.floor(e)),
      Math.floor(Math.random() * (e - t + 1)) + t
    );
  }),
  (PrimePageScriptV2.prototype.randomString = function (t) {
    for (
      var e = "",
        i = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
        a = i.length,
        n = 0;
      n < t;
      n++
    )
      e += i.charAt(Math.floor(Math.random() * a));
    return e;
  }),
  (PrimePageScriptV2.prototype.runCallback = function (t, e) {
    if (this.isFunction(e)) {
      var i = this;
      if (t) {
        var a = i.runInterval(function () {
          i.removeInterval(a), i.runCallback(!1, e);
        }, 0);
        return;
      }
      e();
    }
  }),
  (PrimePageScriptV2.prototype.runTimeout = function (t, e) {
    if (this.isFunction(t)) {
      if (!this.isEmpty(e) && e > 0) return setTimeout(t, e);
      t();
    }
  }),
  (PrimePageScriptV2.prototype.removeTimeout = function (t) {
    return clearTimeout(t);
  }),
  (PrimePageScriptV2.prototype.removeInterval = function (t) {
    return clearInterval(t);
  }),
  (PrimePageScriptV2.prototype.runInterval = function (t, e) {
    if (this.isFunction(t)) return setInterval(t, e);
  }),
  (PrimePageScriptV2.prototype.getURLSearchParams = function (t, e, i) {
    var a = this,
      n = {},
      o = this.isNull(t);
    if (((t = o ? window.location.search : t), !this.isEmpty(t)))
      for (var r = t.substr(1).split("&"), s = 0; s < r.length; ++s) {
        var l = r[s].split("=", 2);
        if (this.isNull(n[l[0]])) {
          1 == l.length
            ? (n[l[0]] = "")
            : (n[l[0]] = decodeURIComponentLadiPage(l[1].replace(/\+/g, " ")));
          try {
            if (i) {
              var c = JSON.parse(n[l[0]]);
              Number.isInteger(c) ||
                ((n[l[0]] = c),
                0 == n[l[0]].length
                  ? (n[l[0]] = "")
                  : 1 == n[l[0]].length && (n[l[0]] = n[l[0]][0]));
            }
          } catch (t) {}
        } else
          i &&
            (this.isArray(n[l[0]]) || (n[l[0]] = [n[l[0]]]),
            1 == l.length
              ? n[l[0]].push("")
              : n[l[0]].push(
                  decodeURIComponentLadiPage(l[1].replace(/\+/g, " "))
                ));
      }
    return (
      o &&
        ["email", "phone"].forEach(function (t) {
          try {
            var e = n[t];
            if (!a.isEmpty(e)) {
              var i = Base64.decode(e);
              e == Base64.encode(i) && (n[t] = i);
            }
          } catch (t) {}
        }),
      this.isEmpty(e) ? n : n[e]
    );
  }),
  (PrimePageScriptV2.prototype.getVideoId = function (t, e) {
    if (this.isEmpty(e)) return e;
    if (t == this.const.VIDEO_TYPE.youtube) {
      var i = this.createTmpElement("a", "", { href: e });
      -1 != e.toLowerCase().indexOf("watch")
        ? (e = this.getURLSearchParams(i.search, "v", !1))
        : -1 != e.toLowerCase().indexOf("embed/")
        ? (e = i.pathname.substring("/embed/".length))
        : -1 != e.toLowerCase().indexOf("shorts/")
        ? (e = i.pathname.substring("/shorts/".length))
        : -1 != e.toLowerCase().indexOf("youtu.be") &&
          (e = i.pathname.substring("/".length));
    }
    return e;
  }),
  (PrimePageScriptV2.prototype.sendRequest = function (t, e, i, a, n, o) {
    var r = this,
      s = function (l) {
        var c = l[e],
          d = function (l, p) {
            if (c.list.length <= l)
              0 == l
                ? s({})
                : r.isFunction(o) &&
                  o(r.const.LANG.REQUEST_SEND_ERROR, 0, p, e);
            else {
              var u = {};
              (u.timeout = c.timeout),
                (u.onreadystatechange = function () {
                  this.readyState == XMLHttpRequest.DONE &&
                    (200 == this.status
                      ? o(this.responseText, this.status, this, e)
                      : d(l + 1, this));
                }),
                r.sendRequestWithOption(t, c.list[l], i, a, n, u);
            }
          };
        if (r.isObject(c)) return d(0, null);
        r.sendRequestWithOption(t, e, i, a, n, null, o);
      },
      l = function () {
        var t = r.runtime.tmp.send_request_configs;
        if (!r.isObject(t))
          return (r.runtime.tmp.send_request_configs = {}), void l();
        s(t);
      };
    l();
  }),
  (PrimePageScriptV2.prototype.sendRequestWithOption = function (
    t,
    e,
    i,
    a,
    n,
    o,
    r
  ) {
    var s = new XMLHttpRequest();
    (this.isFunction(this.runtime.send_request_response[e]) &&
      this.runtime.send_request_response[e](e, i, r)) ||
      (this.isFunction(r) &&
        (s.onreadystatechange = function () {
          var t = null;
          try {
            t = this.responseText;
          } catch (t) {}
          r(t, this.status, this, e);
        }),
      s.open(t, e, a),
      this.isObject(n) &&
        Object.keys(n).forEach(function (t) {
          s.setRequestHeader(t, n[t]);
        }),
      this.isObject(o) &&
        Object.keys(o).forEach(function (t) {
          s[t] = o[t];
        }),
      s.send(i));
  }),
  (PrimePageScriptV2.prototype.deleteCookie = function (t) {
    return window.ladi(t).delete_cookie();
  }),
  (PrimePageScriptV2.prototype.setCookie = function (t, e, i, a, n, o) {
    return window.ladi(e).set_cookie(i, a, o, t, n);
  }),
  (PrimePageScriptV2.prototype.getCookie = function (t) {
    return window.ladi(t).get_cookie();
  }),
  (PrimePageScriptV2.prototype.removeSticky = function () {
    var t = this;
    t instanceof PrimePageScriptV2 || (t = PrimePageScript);
    for (
      var e = document.querySelectorAll('[data-sticky="true"]'), i = 0;
      i < e.length;
      i++
    )
      e[i].removeAttribute("data-top"),
        e[i].removeAttribute("data-left"),
        e[i].style.removeProperty("top"),
        e[i].style.removeProperty("left"),
        e[i].style.removeProperty("width"),
        e[i].style.removeProperty("position"),
        e[i].style.removeProperty("z-index");
    t.runEventScroll();
  }),
  (PrimePageScriptV2.prototype.runEventBackdropPopupClick = function (t) {
    t.stopPropagation();
    var e = this;
    e instanceof PrimePageScriptV2 || (e = PrimePageScript);
    for (
      var i = null,
        a = document.querySelectorAll('[data-popup-backdrop="true"]'),
        n = 0;
      n < a.length;
      n++
    )
      "none" != getComputedStyle(a[n]).display && (i = a[n].id);
    if (!e.isEmpty(i)) {
      var o = document.querySelector("#" + i + " .popup-close");
      if (!e.isEmpty(o) && "none" == getComputedStyle(o).display) return;
    }
    e.runRemovePopup(i, e.runtime.isClient);
  }),
  (PrimePageScriptV2.prototype.runEventBackdropDropboxClick = function (t) {
    t.stopPropagation();
    var e = this;
    e instanceof PrimePageScriptV2 || (e = PrimePageScript);
    for (
      var i = document.querySelectorAll('[data-dropbox-backdrop="true"]'),
        a = 0;
      a < i.length;
      a++
    )
      window.ladi(i[a].id).hide();
    document
      .getElementById(e.runtime.backdrop_dropbox_id)
      .style.removeProperty("display");
  }),
  (PrimePageScriptV2.prototype.runEventMouseMove = function (t) {
    t.stopPropagation();
    var e = this;
    e instanceof PrimePageScriptV2 || (e = PrimePageScript),
      (t = e.getEventCursorData(t));
    var i = null,
      a = 0,
      n = 0,
      o = 0;
    if (
      (e.isEmpty(e.runtime.current_element_mouse_down_carousel) ||
        ((i = document.getElementById(
          e.runtime.current_element_mouse_down_carousel
        )),
        (a =
          t.pageX - e.runtime.current_element_mouse_down_carousel_position_x),
        (n =
          parseFloatLadiPage(
            i
              .getElementsByClassName("prime-carousel-content")[0]
              .getAttribute("data-left")
          ) || 0),
        (n += a) <
          (o = -(
            (parseFloatLadiPage(
              e.runtime.eventData[
                e.runtime.current_element_mouse_down_carousel
              ][e.runtime.device + ".option.carousel_crop.width"]
            ) || 0) - i.clientWidth
          )) && (n = o),
        n > 0 && (n = 0),
        i
          .getElementsByClassName("prime-carousel-content")[0]
          .style.setProperty("left", n + "px")),
      !e.isEmpty(e.runtime.current_element_mouse_down_gallery_view))
    ) {
      (i = document.querySelector(
        '[data-runtime-id="' +
          e.runtime.current_element_mouse_down_gallery_view +
          '"]'
      )),
        (a =
          t.pageX -
          e.runtime.current_element_mouse_down_gallery_view_position_x);
      var r = parseFloatLadiPage(i.getAttribute("data-current")) || 0;
      r == (parseFloatLadiPage(i.getAttribute("data-max-item")) || 0) - 1 &&
        a < 0 &&
        (a = 0),
        a > 0 && 0 == r && (a = 0),
        a >= e.runtime.current_element_mouse_down_gallery_view_diff
          ? ((e.runtime.current_element_mouse_down_gallery_view = null),
            (e.runtime.current_element_mouse_down_gallery_view_position_x = 0),
            i.getElementsByClassName("prime-gallery-view-arrow-left")[0].click())
          : a <= -e.runtime.current_element_mouse_down_gallery_view_diff
          ? ((e.runtime.current_element_mouse_down_gallery_view = null),
            (e.runtime.current_element_mouse_down_gallery_view_position_x = 0),
            i
              .getElementsByClassName("prime-gallery-view-arrow-right")[0]
              .click())
          : i.querySelectorAll(".prime-gallery-view-item.selected").length > 0 &&
            i
              .querySelectorAll(".prime-gallery-view-item.selected")[0]
              .style.setProperty("left", a + "px");
    }
    e.isEmpty(e.runtime.current_element_mouse_down_gallery_control) ||
      ((i = document.querySelector(
        '[data-runtime-id="' +
          e.runtime.current_element_mouse_down_gallery_control +
          '"]'
      )),
      (a =
        t.pageX -
        e.runtime.current_element_mouse_down_gallery_control_position_x),
      (n =
        parseFloatLadiPage(
          i
            .getElementsByClassName("prime-gallery-control-box")[0]
            .getAttribute("data-left")
        ) || 0),
      (n += a) <
        (o =
          (parseFloatLadiPage(
            getComputedStyle(
              i.getElementsByClassName("prime-gallery-control")[0]
            ).width
          ) || 0) -
          (parseFloatLadiPage(
            getComputedStyle(
              i.getElementsByClassName("prime-gallery-control-box")[0]
            ).width
          ) || 0)) && (n = o),
      n > 0 && (n = 0),
      i
        .getElementsByClassName("prime-gallery-control-box")[0]
        .style.setProperty("left", n + "px"));
  }),
  (PrimePageScriptV2.prototype.runEventMouseUp = function (t) {
    t.stopPropagation();
    var e = this;
    e instanceof PrimePageScriptV2 || (e = PrimePageScript),
      (t = e.getEventCursorData(t));
    var i = null,
      a = 0;
    if (!e.isEmpty(e.runtime.current_element_mouse_down_carousel)) {
      delete e.runtime.timenext_carousel[
        e.runtime.current_element_mouse_down_carousel
      ],
        (a =
          t.pageX - e.runtime.current_element_mouse_down_carousel_position_x);
      var n = (i = document.getElementById(
        e.runtime.current_element_mouse_down_carousel
      ))
        .getElementsByClassName("prime-carousel-content")[0]
        .getAttribute("data-left");
      i
        .getElementsByClassName("prime-carousel-content")[0]
        .removeAttribute("data-left"),
        i
          .getElementsByClassName("prime-carousel-content")[0]
          .style.removeProperty("transition-duration"),
        (e.runtime.current_element_mouse_down_carousel = null),
        a >= e.runtime.current_element_mouse_down_carousel_diff
          ? i.getElementsByClassName("prime-carousel-arrow-left")[0].click()
          : a <= -e.runtime.current_element_mouse_down_carousel_diff
          ? i.getElementsByClassName("prime-carousel-arrow-right")[0].click()
          : i.getElementsByClassName("prime-carousel-content").length > 0 &&
            (i
              .getElementsByClassName("prime-carousel-content")[0]
              .style.setProperty("transition-duration", "100ms"),
            i
              .getElementsByClassName("prime-carousel-content")[0]
              .style.setProperty("left", n),
            e.runTimeout(function () {
              i.getElementsByClassName(
                "prime-carousel-content"
              )[0].style.removeProperty("transition-duration");
            }, 1));
    }
    if (
      (e.isEmpty(e.runtime.current_element_mouse_down_gallery_view) ||
        ((i = document.querySelector(
          '[data-runtime-id="' +
            e.runtime.current_element_mouse_down_gallery_view +
            '"]'
        )).querySelectorAll(".prime-gallery-view-item.selected").length > 0 &&
          i
            .querySelectorAll(".prime-gallery-view-item.selected")[0]
            .style.removeProperty("left")),
      !e.isEmpty(e.runtime.current_element_mouse_down_gallery_control))
    )
      if (
        ((i = document.querySelector(
          '[data-runtime-id="' +
            e.runtime.current_element_mouse_down_gallery_control +
            '"]'
        ))
          .getElementsByClassName("prime-gallery-control-box")[0]
          .removeAttribute("data-left"),
        i
          .getElementsByClassName("prime-gallery-control-box")[0]
          .style.removeProperty("transition-duration"),
        i
          .getElementsByClassName("prime-gallery-control-arrow-left")[0]
          .classList.remove("opacity-0"),
        i
          .getElementsByClassName("prime-gallery-control-arrow-right")[0]
          .classList.remove("opacity-0"),
        i
          .getElementsByClassName("prime-gallery")[0]
          .classList.contains("prime-gallery-top") ||
          i
            .getElementsByClassName("prime-gallery")[0]
            .classList.contains("prime-gallery-bottom"))
      ) {
        var o =
            parseFloatLadiPage(
              i
                .getElementsByClassName("prime-gallery-control-box")[0]
                .style.getPropertyValue("left")
            ) || 0,
          r =
            parseFloatLadiPage(
              getComputedStyle(
                i.getElementsByClassName("prime-gallery-control-box")[0]
              ).width
            ) || 0;
        (r =
          (r = -(r -=
            parseFloatLadiPage(
              getComputedStyle(
                i.getElementsByClassName("prime-gallery-control")[0]
              ).width
            ) || 0)) > 0
            ? 0
            : r),
          o > 0 && (o = 0),
          o >= 0 &&
            i
              .getElementsByClassName("prime-gallery-control-arrow-left")[0]
              .classList.add("opacity-0"),
          o <= r &&
            i
              .getElementsByClassName("prime-gallery-control-arrow-right")[0]
              .classList.add("opacity-0");
      } else {
        var s =
            parseFloatLadiPage(
              i
                .getElementsByClassName("prime-gallery-control-box")[0]
                .style.getPropertyValue("top")
            ) || 0,
          l =
            parseFloatLadiPage(
              getComputedStyle(
                i.getElementsByClassName("prime-gallery-control-box")[0]
              ).height
            ) || 0;
        (l =
          (l = -(l -=
            parseFloatLadiPage(
              getComputedStyle(
                i.getElementsByClassName("prime-gallery-control")[0]
              ).height
            ) || 0)) > 0
            ? 0
            : l),
          s > 0 && (s = 0),
          s >= 0 &&
            i
              .getElementsByClassName("prime-gallery-control-arrow-left")[0]
              .classList.add("opacity-0"),
          s <= l &&
            i
              .getElementsByClassName("prime-gallery-control-arrow-right")[0]
              .classList.add("opacity-0");
      }
    (e.runtime.current_element_mouse_down_carousel_position_x = 0),
      (e.runtime.current_element_mouse_down_gallery_view = null),
      (e.runtime.current_element_mouse_down_gallery_view_position_x = 0);
    var c = 0;
    e.runtime.current_element_mouse_down_gallery_control_time +
      e.runtime.current_element_mouse_down_gallery_control_time_click <
      Date.now() && (c = 5),
      e.runTimeout(function () {
        (e.runtime.current_element_mouse_down_gallery_control = null),
          (e.runtime.current_element_mouse_down_gallery_control_time = 0),
          (e.runtime.current_element_mouse_down_gallery_control_position_x = 0);
      }, c);
  }),
  (PrimePageScriptV2.prototype.runEventMouseLeave = function (t) {
    var e = this;
    e instanceof PrimePageScriptV2 || (e = PrimePageScript),
      Object.keys(e.runtime.eventData).forEach(function (t) {
        var i = e.runtime.eventData[t];
        if ("popup" == i.type && i["option.show_popup_exit_page"]) {
          if (
            (e.isObject(e.runtime.tmp.popup_leave_show) ||
              (e.runtime.tmp.popup_leave_show = {}),
            e.runtime.tmp.popup_leave_show[t])
          )
            return;
          (e.runtime.tmp.popup_leave_show[t] = !0), window.ladi(t).show();
        }
      });
  }),
  (PrimePageScriptV2.prototype.runEventScroll = function (t) {
    var e = this;
    if (
      (e instanceof PrimePageScriptV2 || (e = PrimePageScript), e.runtime.isRun)
    ) {
      if (!e.isEmpty(e.runtime.eventData) && !e.runtime.tmp.is_run_show_popup) {
        for (
          var i = null,
            a = document.querySelectorAll('[data-popup-backdrop="true"]'),
            n = 0;
          n < a.length;
          n++
        )
          "none" != getComputedStyle(a[n]).display && (i = a[n].id);
        var o = [];
        if (!e.isEmpty(i))
          for (
            var r = document.querySelectorAll("#" + i + " .prime-element"),
              s = 0;
            s < r.length;
            s++
          )
            o.push(r[s].id);
        if (
          (Object.keys(e.runtime.eventData).forEach(function (t) {
            var a,
              n = e.runtime.eventData[t],
              r = null,
              s = null,
              l = null,
              c = Object.keys(e.runtime.list_scroll_func);
            a =
              "gallery" == n.type
                ? document.querySelectorAll("#" + t)
                : [(l = document.getElementById(t))];
            for (var d = 0; d < a.length; d++)
              if (((l = a[d]), !e.isEmpty(l))) {
                var p =
                  "gallery" == n.type
                    ? l.getAttribute("data-runtime-id")
                    : l.getAttribute("id");
                if (-1 != c.indexOf(p)) {
                  var u = !1;
                  "section" == n.type
                    ? ((r = l.offsetTop),
                      ((window.scrollY >= r &&
                        window.scrollY <= r + l.offsetHeight) ||
                        (window.scrollY + e.getHeightDevice() >= r &&
                          window.scrollY + e.getHeightDevice() <=
                            r + l.offsetHeight) ||
                        (r >= window.scrollY &&
                          window.scrollY + e.getHeightDevice() >= r)) &&
                        (u = !0))
                    : ((r =
                        e.getElementBoundingClientRect(l).y + window.scrollY),
                      ((window.scrollY >= r &&
                        window.scrollY <= r + l.offsetHeight) ||
                        (window.scrollY + e.getHeightDevice(!0) >= r &&
                          window.scrollY + e.getHeightDevice(!0) <=
                            r + l.offsetHeight) ||
                        (r >= window.scrollY &&
                          window.scrollY + e.getHeightDevice(!0) >= r)) &&
                        (u = !0)),
                    u &&
                      ((s = e.runtime.list_scroll_func[p]),
                      delete e.runtime.list_scroll_func[p],
                      s());
                }
              }
            var m = n[e.runtime.device + ".style.animation-name"];
            if (
              !e.isEmpty(m) &&
              ((l = document.getElementById(t)),
              !e.isEmpty(l) && !l.classList.contains("prime-animation"))
            ) {
              var _ =
                parseFloatLadiPage(
                  n[e.runtime.device + ".style.animation-delay"]
                ) || 0;
              r = e.getElementBoundingClientRect(l).y + window.scrollY;
              var y =
                (window.scrollY >= r && window.scrollY <= r + l.offsetHeight) ||
                (window.scrollY + e.getHeightDevice(!0) >= r &&
                  window.scrollY + e.getHeightDevice(!0) <=
                    r + l.offsetHeight) ||
                (r >= window.scrollY &&
                  window.scrollY + e.getHeightDevice(!0) >= r);
              e.runtime.tmp.isFirstScroll &&
                _ > 0 &&
                l.classList.add("prime-animation-hidden"),
                y &&
                  (l.classList.add("prime-animation"),
                  e.runTimeout(function () {
                    l.classList.remove("prime-animation-hidden");
                  }, 1e3 * _));
            }
            if (e.isEmpty(i) || -1 != o.indexOf(t)) {
              var g = null,
                f = null,
                v = null;
              if (
                (n[e.runtime.device + ".option.sticky"] &&
                  ((g = n[e.runtime.device + ".option.sticky_position"]),
                  (f = parseFloatLadiPage(
                    n[e.runtime.device + ".option.sticky_position_top"]
                  )),
                  (v = parseFloatLadiPage(
                    n[e.runtime.device + ".option.sticky_position_bottom"]
                  ))),
                !e.runtime.has_popupx && !e.isEmpty(g))
              ) {
                var h = document.getElementById(t);
                if (
                  !e.isEmpty(h) &&
                  h.clientWidth > 0 &&
                  h.clientHeight > 0 &&
                  -1 != ["default", "top", "bottom"].indexOf(g)
                ) {
                  var E = e.getElementBoundingClientRect(h),
                    P = h.getAttribute("data-top"),
                    b = h.getAttribute("data-left");
                  e.isEmpty(P)
                    ? (h.setAttribute("data-top", E.y + window.scrollY),
                      (P = E.y))
                    : (P = parseFloatLadiPage(P)),
                    e.isEmpty(b)
                      ? (h.setAttribute("data-left", E.x + window.scrollX),
                        (b = E.x))
                      : (b = parseFloatLadiPage(b));
                  var L = null,
                    A = null,
                    w = e.getHeightDevice(),
                    S = document.getElementsByClassName("prime-wraper")[0],
                    T = 0,
                    O = 0,
                    C = 0,
                    N = 0;
                  if (
                    ("default" == g &&
                      (f >= P - window.scrollY
                        ? ((L = f + "px"),
                          (A = b + "px"),
                          f <= 0 && (T = h.clientHeight),
                          (C = h.clientHeight))
                        : w - v - h.clientHeight <= P - window.scrollY &&
                          ((L = "calc(100% - " + (v + h.clientHeight) + "px)"),
                          (A = b + "px"),
                          v <= 0 && (O = h.clientHeight),
                          (N = h.clientHeight))),
                    "top" == g &&
                      (f >= P - window.scrollY || w - 0 < P - window.scrollY) &&
                      ((L = f + "px"),
                      (A = b + "px"),
                      f <= 0 && (T = h.clientHeight),
                      (C = h.clientHeight)),
                    "bottom" == g &&
                      (w - v - h.clientHeight <= P - window.scrollY ||
                        0 > P + h.clientHeight - window.scrollY) &&
                      ((L = "calc(100% - " + (v + h.clientHeight) + "px)"),
                      (A = b + "px"),
                      v <= 0 && (O = h.clientHeight),
                      (N = h.clientHeight)),
                    e.isEmpty(L) || e.isEmpty(A))
                  )
                    h.style.removeProperty("top"),
                      h.style.removeProperty("left"),
                      h.style.removeProperty("width"),
                      h.style.removeProperty("position"),
                      h.style.removeProperty("z-index"),
                      S.getAttribute("data-padding-top-id") == t &&
                        (S.removeAttribute("data-padding-top-id"),
                        S.style.removeProperty("padding-top")),
                      S.getAttribute("data-padding-bottom-id") == t &&
                        (S.removeAttribute("data-padding-bottom-id"),
                        S.style.removeProperty("padding-bottom"));
                  else if (
                    ("section" == n.type &&
                      (e.runtime.is_mobile_only
                        ? h.style.setProperty("width", S.clientWidth + "px")
                        : e.runtime.isDesktop &&
                          h.style.setProperty("width", "100%"),
                      T > 0 &&
                        (S.setAttribute("data-padding-top-id", t),
                        S.style.setProperty("padding-top", T + "px")),
                      O > 0 &&
                        (S.setAttribute("data-padding-bottom-id", t),
                        S.style.setProperty("padding-bottom", O + "px")),
                      C > 0 && S.setAttribute("data-scroll-padding-top", C),
                      N > 0 && S.setAttribute("data-scroll-padding-bottom", N)),
                    h.style.setProperty("top", L),
                    h.style.setProperty("left", A),
                    h.style.setProperty("position", "fixed"),
                    h.style.setProperty("z-index", "90000050"),
                    !h.hasAttribute("data-sticky"))
                  ) {
                    h.setAttribute("data-sticky", !0),
                      h.classList.contains("prime-animation-hidden") &&
                        (h.classList.remove("prime-animation-hidden"),
                        h.classList.add("prime-animation"));
                    for (
                      var I = h.getElementsByClassName("prime-animation-hidden");
                      I.length > 0;

                    )
                      I[0].classList.add("prime-animation"),
                        I[0].classList.remove("prime-animation-hidden");
                    h.classList.remove("prime-lazyload");
                    for (
                      var k = h.getElementsByClassName("prime-lazyload");
                      k.length > 0;

                    )
                      k[0].classList.remove("prime-lazyload");
                  }
                }
              }
            }
            if ("popup" == n.type) {
              if (!e.isEmpty(e.runtime.scroll_show_popup[t])) return;
              e.isEmpty(n["option.popup_welcome_page_scroll_to"]) ||
                ((l = document.getElementById(
                  n["option.popup_welcome_page_scroll_to"]
                )),
                !e.isEmpty(l) &&
                  l.offsetHeight > 0 &&
                  ((r = l.offsetTop),
                  ((window.scrollY >= r &&
                    window.scrollY <= r + l.offsetHeight) ||
                    (window.scrollY + e.getHeightDevice() >= r &&
                      window.scrollY + e.getHeightDevice() <=
                        r + l.offsetHeight) ||
                    (r >= window.scrollY &&
                      window.scrollY + e.getHeightDevice() >= r)) &&
                    ((e.runtime.scroll_show_popup[t] = !0),
                    window.ladi(t).show())));
            }
            if ("section" == n.type) {
              if (!e.isEmpty(e.runtime.scroll_to_section[t])) return;
              (l = document.getElementById(t)),
                e.isEmpty(l) ||
                  ((r = l.offsetTop),
                  ((window.scrollY >= r &&
                    window.scrollY <= r + l.offsetHeight) ||
                    (window.scrollY + e.getHeightDevice() >= r &&
                      window.scrollY + e.getHeightDevice() <=
                        r + l.offsetHeight) ||
                    (r >= window.scrollY &&
                      window.scrollY + e.getHeightDevice() >= r)) &&
                    ((e.runtime.scroll_to_section[t] = !0),
                    e.runEventTracking(t, { is_form: !1 })));
            }
          }),
          e.runtime.isClient)
        ) {
          if (e.runtime.is_popupx) return;
          for (
            var l = Math.round(
                ((window.scrollY + e.getHeightDevice()) /
                  document.body.clientHeight) *
                  100
              ),
              c = function (t) {
                e.runLadiPageCommand(function (e) {
                  if (e.tiktokViewContent && t >= e.scrollPercent)
                    return window.ttq.track(e.name), !0;
                });
              },
              d = 1;
            d < e.const.PERCENT_TRACKING_SCROLL.length;
            d++
          ) {
            var p = e.const.PERCENT_TRACKING_SCROLL[d];
            l > e.const.PERCENT_TRACKING_SCROLL[d - 1] &&
              l <= p &&
              -1 == e.runtime.scroll_depth.indexOf(p) &&
              (e.runtime.scroll_depth.push(p),
              e.isFunction(window.gtag) &&
                window.gtag("event", "ScrollDepth_" + p + "_percent", {
                  event_category: "LadiPageScrollDepth",
                  event_label: window.location.host + window.location.pathname,
                  non_interaction: !0,
                }),
              e.isFunction(window.fbq) &&
                window.fbq("trackCustom", "ScrollDepth_" + p + "_percent"),
              e.isNull(window.ttq) || c(l));
          }
        }
        if (((e.runtime.tmp.isFirstScroll = !1), !e.isEmpty(t))) {
          var u = 0,
            m = Object.values(e.runtime.list_scrolling_exec);
          if (e.isEmpty(e.runtime.tmp.timeout_scrolling_id))
            for (u = 0; u < m.length; u++) m[u]();
          e.removeTimeout(e.runtime.tmp.timeout_scrolling_id),
            (e.runtime.tmp.timeout_scrolling_id = e.runTimeout(function () {
              for (
                delete e.runtime.tmp.timeout_scrolling_id,
                  m = Object.values(e.runtime.list_scrolled_exec),
                  u = 0;
                u < m.length;
                u++
              )
                m[u]();
            }, 1e3));
        }
      }
    } else
      e.runTimeout(function () {
        e.runEventScroll(t);
      }, 100);
  }),
  (PrimePageScriptV2.prototype.runRemovePopup = function (t, e, i, a, n) {
    var o = this,
      r = document.querySelectorAll(
        "#" +
          this.runtime.builder_section_popup_id +
          " .prime-container > .prime-element"
      ),
      s = !1,
      l = !1;
    e ||
      (LadiPagePlugin.getPlugin("popup").removeStyleShowPopupBuilder(),
      LadiPagePlugin.getPlugin("popup").removeStyleShowPopupBuilderScroll());
    var c = function () {
        var t = document.getElementById("style_popup");
        o.isEmpty(t) || t.parentElement.removeChild(t);
      },
      d = [],
      p = 0;
    if (this.isEmpty(t)) for (p = 0; p < r.length; p++) d.push(r[p].id);
    else d.push(t);
    d.forEach(function (t) {
      var i = document.getElementById(t);
      if (!o.isEmpty(i)) {
        o.runtime.has_popupx &&
          ("none" != getComputedStyle(i).display && (l = !0),
          document.body.style.removeProperty("height"));
        var a = parseFloatLadiPage(i.getAttribute("data-timeout-id")) || null;
        o.removeTimeout(a),
          i.removeAttribute("data-timeout-id"),
          (a = parseFloatLadiPage(i.getAttribute("data-timeout-id-2")) || null),
          o.removeTimeout(a),
          i.removeAttribute("data-timeout-id-2"),
          (a = parseFloatLadiPage(i.getAttribute("data-timeout-id-3")) || null),
          o.removeTimeout(a),
          i.removeAttribute("data-timeout-id-3");
        var n = i.getElementsByClassName("popup-close")[0];
        o.isEmpty(n) ||
          ((a = parseFloatLadiPage(n.getAttribute("data-timeout-id")) || null),
          o.removeTimeout(a),
          n.removeAttribute("data-timeout-id")),
          o.pauseAllVideo(i),
          o.isEmpty(i.style.getPropertyValue("display")) || (s = !0),
          i.style.removeProperty("display"),
          i.style.removeProperty("z-index");
        var d = i.hasAttribute("data-popup-backdrop"),
          p = o.runtime.eventData[t];
        if (
          (o.isObject(p) &&
            p[o.runtime.device + ".option.popup_position"] ==
              o.const.POSITION_TYPE.default &&
            (d = !0),
          d)
        ) {
          c(),
            e &&
              (o.isEmpty(o.runtime.tmp.bodyScrollY) ||
                window.scrollTo(0, o.runtime.tmp.bodyScrollY)),
            (o.runtime.tmp.bodyScrollY = null);
          for (var u = 0; u < r.length; u++)
            r[u].style.removeProperty("z-index");
        }
        i.removeAttribute("data-scroll"),
          i.removeAttribute("data-fixed-close"),
          i.style.removeProperty("overflow-y"),
          i.style.removeProperty("overflow-x"),
          i
            .getElementsByClassName("prime-popup")[0]
            .style.removeProperty("height"),
          i.style.removeProperty("max-height");
      }
    }),
      s && this.isFunction(i) && i(),
      a && c(),
      !n &&
        l &&
        o.runtime.tmp.runActionPopupX({
          id: t,
          dimension: { display: "none" },
          action: { type: "set_iframe_dimension" },
        }),
      delete this.runtime.tmp.current_default_popup_id;
  }),
  (PrimePageScriptV2.prototype.runShowPopup = function (t, e, i, a, n, o) {
    var r = this;
    if (!r.isEmpty(e)) {
      var s = document.getElementById(e),
        l = null;
      if (
        !n ||
        (!r.isEmpty(s) &&
          r.isObject(r.runtime.eventData) &&
          !r.isEmpty(r.runtime.eventData[e]))
      ) {
        n &&
          ((i =
            r.runtime.eventData[e][
              r.runtime.device + ".option.popup_position"
            ]),
          (a =
            r.runtime.eventData[e][
              r.runtime.device + ".option.popup_backdrop"
            ]));
        var c = function (t) {
            r.runtime.tmp.is_run_show_popup = !0;
            var d = 0,
              p = "";
            n || LadiPagePlugin.getPlugin("popup").showStyleShowPopupBuilder(e),
              r.isEmpty(a)
                ? (p += n
                    ? "#" +
                      r.runtime.backdrop_popup_id +
                      " { display: none !important;}"
                    : "#" +
                      r.runtime.backdrop_popup_id +
                      " { display: block !important;}")
                : ((p +=
                    "#" +
                    r.runtime.backdrop_popup_id +
                    " { display: block !important; " +
                    a +
                    "}"),
                  s.setAttribute("data-popup-backdrop", !0)),
              i == r.const.POSITION_TYPE.default &&
                "true" != s.getAttribute("data-dropbox") &&
                ((function () {
                  if (!r.runtime.has_popupx && n) {
                    var t = window.scrollY;
                    if (!r.isEmpty(r.runtime.tmp.bodyScrollY)) {
                      var i = getComputedStyle(document.body);
                      "fixed" == i.position &&
                        (parseFloatLadiPage(i.top) || 0) ==
                          -1 * r.runtime.tmp.bodyScrollY &&
                        (t = r.runtime.tmp.bodyScrollY);
                    }
                    "block" != s.style.getPropertyValue("display") ||
                      r.isEmpty(r.runtime.tmp.bodyScrollY) ||
                      (t = r.runtime.tmp.bodyScrollY),
                      (p += "body {"),
                      0 ==
                        s.getElementsByClassName(
                          "prime-google-recaptcha-checkbox"
                        ).length && (p += "position: fixed !important;"),
                      (p += "width: 100% !important;"),
                      (p += "top: -" + t + "px !important;"),
                      (p += "}"),
                      (r.runtime.tmp.bodyScrollY = t),
                      (l = function () {
                        r.runtime.tmp.bodyScrollY = t;
                      });
                  }
                  for (
                    var a = document.querySelectorAll(
                        "#" +
                          r.runtime.builder_section_popup_id +
                          " .prime-container > .prime-element"
                      ),
                      o = 0;
                    o < a.length;
                    o++
                  )
                    a[o].id != e &&
                      a[o].style.setProperty("z-index", "1", "important");
                  d = 500;
                })(),
                (r.runtime.tmp.current_default_popup_id = e));
            var u = "block" == s.style.getPropertyValue("display");
            if (
              (r.isFunction(o) && !u && (o(), r.isFunction(l) && l()),
              r.isArray(r.runtime.list_show_popup_func[e]))
            )
              for (; r.runtime.list_show_popup_func[e].length > 0; )
                r.runtime.list_show_popup_func[e].shift()();
            var m = s.hasAttribute("data-scroll"),
              _ = s.hasAttribute("data-fixed-close"),
              y = !1,
              g = s.getElementsByClassName("prime-popup")[0],
              f =
                1e3 *
                (parseFloatLadiPage(getComputedStyle(g).animationDuration) ||
                  0),
              v =
                1e3 *
                (parseFloatLadiPage(getComputedStyle(g).animationDelay) || 0),
              h = parseFloatLadiPage(s.getAttribute("data-timeout-id")) || null;
            r.removeTimeout(h),
              (h =
                parseFloatLadiPage(s.getAttribute("data-timeout-id-2")) ||
                null),
              r.removeTimeout(h),
              s.classList.add("prime-animation-hidden"),
              "block" != getComputedStyle(s).display &&
                s.style.setProperty("display", "block", "important"),
              (h = r.runTimeout(function () {
                s.classList.remove("prime-animation-hidden"),
                  s.removeAttribute("data-timeout-id");
              }, v)),
              r.isEmpty(h) || s.setAttribute("data-timeout-id", h),
              t &&
                (!m &&
                  s.clientHeight > 0.8 * r.getHeightDevice() &&
                  (n
                    ? r.runtime.has_popupx
                      ? (y = !0)
                      : (s.setAttribute("data-scroll", !0),
                        s.style.setProperty("overflow-y", "auto", "important"),
                        s.style.setProperty(
                          "overflow-x",
                          "hidden",
                          "important"
                        ),
                        (m = !0))
                    : (_ = !0)),
                r.runtime.has_popupx &&
                  (y
                    ? (s.setAttribute("data-fixed-close", !0),
                      document.body.style.setProperty(
                        "height",
                        s.clientHeight + "px"
                      ))
                    : document.body.style.removeProperty("height")),
                m &&
                  n &&
                  (s
                    .getElementsByClassName("prime-popup")[0]
                    .style.removeProperty("height"),
                  s.style.removeProperty("max-height"),
                  s
                    .getElementsByClassName("prime-popup")[0]
                    .style.setProperty(
                      "height",
                      s.clientHeight + "px",
                      "important"
                    ),
                  s.style.setProperty("max-height", "80vh"))),
              !n &&
                _ &&
                LadiPagePlugin.getPlugin("popup").styleShowPopupBuilderScroll(
                  e
                ),
              r.runtime.has_popupx &&
                ((p += n
                  ? "#" +
                    r.runtime.backdrop_popup_id +
                    " { display: none !important;}"
                  : "#" +
                    r.runtime.backdrop_popup_id +
                    " { display: block !important;}"),
                s.style.removeProperty("max-height"),
                s.style.removeProperty("overflow-y"),
                s.style.removeProperty("overflow-x")),
              r.isEmpty(p) || r.createStyleElement("style_popup", p);
            var E = null;
            if (
              n &&
              !r.isEmpty(s) &&
              "true" != s.getAttribute("data-dropbox")
            ) {
              var P = s.getElementsByClassName("popup-close")[0];
              r.isEmpty(P) &&
                (((P = document.createElement("div")).className =
                  "popup-close"),
                s.appendChild(P),
                P.addEventListener("click", function (t) {
                  t.stopPropagation(), r.runRemovePopup(e, n);
                })),
                (h =
                  parseFloatLadiPage(P.getAttribute("data-timeout-id")) ||
                  null),
                r.removeTimeout(h),
                P.classList.add("opacity-0"),
                r.runtime.has_popupx &&
                  r.runtime.tmp.popupx_is_inline &&
                  P.classList.add("prime-hidden");
              var b = function () {
                var t = s.getElementsByClassName("popup-close")[0];
                if (!r.isEmpty(t)) {
                  var e = r.getElementBoundingClientRect(s),
                    i = e.y,
                    a = window.innerWidth - e.x - e.width;
                  (m || y) && (a += r.runtime.widthScrollBar),
                    t.style.setProperty("right", a + "px"),
                    t.style.setProperty("top", i + "px"),
                    t.style.setProperty("position", "fixed");
                }
              };
              (E = function () {
                (h = r.runTimeout(function () {
                  b(),
                    P.classList.remove("opacity-0"),
                    P.removeAttribute("data-timeout-id");
                }, f + v + 100)),
                  P.setAttribute("data-timeout-id", h);
              }),
                (m || y) &&
                  (s.hasAttribute("data-resize-scroll") ||
                    (s.setAttribute("data-resize-scroll", !0),
                    window.addEventListener("resize", b)));
            }
            n && !u && r.runEventTracking(e, { is_form: !1 });
            var L = function () {
              var t = d;
              (h = r.runTimeout(function () {
                delete r.runtime.tmp.is_run_show_popup,
                  r.runEventScroll(),
                  r.isFunction(E) && E(),
                  s.removeAttribute("data-timeout-id-2");
              }, t)),
                r.isEmpty(h) || s.setAttribute("data-timeout-id-2", h);
            };
            t
              ? L()
              : ((h = r.runTimeout(function () {
                  "none" == getComputedStyle(s).display
                    ? ((d -= 100), L())
                    : c(!0),
                    s.removeAttribute("data-timeout-id-2");
                }, 100)),
                s.setAttribute("data-timeout-id-2", h));
          },
          d = 0;
        i == r.const.POSITION_TYPE.default && (d = 100);
        var p = r.runTimeout(function () {
          c(t);
        }, d);
        r.isEmpty(s) || s.setAttribute("data-timeout-id-3", p);
      }
    }
  }),
  (PrimePageScriptV2.prototype.convertPhoneNumberTiktok = function (t) {
    for (
      var e = null,
        i = [
          {
            startStr: "+84",
            endStr: "(9|8|7|5|3)[0-9]{8}",
            listRegex: [
              { str_start: "0", str_input: "0" },
              { str_start: "\\+84", str_input: "+84" },
            ],
          },
        ],
        a = 0;
      a < i.length;
      a++
    )
      for (var n = 0; n < i[a].listRegex.length; n++)
        new RegExp(
          "^(" + i[a].listRegex[n].str_start + ")" + i[a].endStr + "$",
          "gi"
        ).test(t) &&
          (e = i[a].startStr + t.substring(i[a].listRegex[n].str_input.length));
    return e;
  }),
  (PrimePageScriptV2.prototype.runGlobalTrackingScript = function () {
    if (
      this.isObject(window.ladi_global_tracking) &&
      this.isArray(window.ladi_global_tracking.thankyou_page)
    )
      for (; window.ladi_global_tracking.thankyou_page.length > 0; ) {
        var t = window.ladi_global_tracking.thankyou_page.shift();
        this.runEventTracking(null, t);
      }
  }),
  (PrimePageScriptV2.prototype.runLadiPageCommand = function (t) {
    if (this.isArray(LadiPageCommand))
      for (var e = 0; e < LadiPageCommand.length; e++)
        if (this.isFunction(t) && t(LadiPageCommand[e])) {
          LadiPageCommand.splice(e, 1), this.runLadiPageCommand(t);
          break;
        }
  }),
  (PrimePageScriptV2.prototype.runEventTracking = function (t, e, i, a, n) {
    var o = this;
    if (this.runtime.isClient) {
      var r = null,
        s = null,
        l = null,
        c = null,
        d = null,
        p = o.runtime.currency,
        u = o.runtime.eventData[t],
        m = null,
        _ = e.is_form,
        y =
          e.is_click &&
          e.count_data_event > 0 &&
          o.runtime.tracking_button_click,
        g = e.is_thankyou_page;
      if (g)
        (r = e.conversion_name),
          (s = e.google_ads_conversion),
          (l = e.google_ads_label),
          (d = o.isEmpty(e.purchase_value)
            ? null
            : parseFloatLadiPage(e.purchase_value) || 0);
      else {
        if (this.isEmpty(t) || !o.isObject(u)) return;
        (m = u.type),
          _ && "form" == m
            ? ((r = u["option.form_conversion_name"]),
              (s = u["option.form_google_ads_conversion"]),
              (l = u["option.form_google_ads_label"]),
              (c = u["option.form_event_custom_script"]),
              (d = o.isEmpty(u["option.form_purchase_value"])
                ? null
                : parseFloatLadiPage(u["option.form_purchase_value"]) || 0),
              o.runtime.shopping && (d = o.getCartCheckoutPrice(d)))
            : ((r = u["option.conversion_name"]),
              (s = u["option.google_ads_conversion"]),
              (l = u["option.google_ads_label"]),
              (c = u["option.event_custom_script"]));
      }
      var f = function (t) {
          var e = null,
            a = null,
            n = "trackCustom",
            s = [],
            l = null,
            c = function () {
              return {
                eventID:
                  "ladi." +
                  Date.now() +
                  "." +
                  (Math.floor(9e10 * Math.random()) + 1e10),
              };
            };
          o.isEmpty(r) ||
            (-1 !=
              [
                "AddPaymentInfo",
                "AddToCart",
                "AddToWishlist",
                "CompleteRegistration",
                "Contact",
                "CustomizeProduct",
                "Donate",
                "FindLocation",
                "InitiateCheckout",
                "Lead",
                "PageView",
                "Purchase",
                "Schedule",
                "Search",
                "StartTrial",
                "SubmitApplication",
                "Subscribe",
                "ViewContent",
              ].indexOf(r) && (n = "track"),
            o.isEmpty(d) ||
              o.isEmpty(p) ||
              (((e = {}).value = d), (e.currency = p)),
            !t &&
              o.isObject(window.ladi_conversion_api) &&
              o.isObject(window.ladi_conversion_api.facebook) &&
              o.isArray(window.ladi_conversion_api.facebook.pixels) &&
              (a = c()),
            s.push({
              fbq_track_name: n,
              conversion_name: r,
              fbq_data: e,
              fbq_event_data: a,
            }),
            s.forEach(function (e) {
              o.isFunction(window.fbq) &&
                window.fbq(
                  e.fbq_track_name,
                  e.conversion_name,
                  e.fbq_data,
                  e.fbq_event_data
                );
              var a = o.copy(e.fbq_event_data);
              if (o.runtime.shopping && _) {
                var n = o.getCartProducts();
                o.isNull(n) ||
                  (o.isObject(a) || (a = {}), (a.cart_products = n));
              }
              o.isObject(i) &&
                (o.isObject(a) || (a = {}),
                (a.email = i.email),
                (a.phone = i.phone)),
                (l = [
                  {
                    key: e.fbq_track_name,
                    name: e.conversion_name,
                    custom_data: e.fbq_data,
                    data: a,
                  },
                ]),
                o.runConversionApi("facebook", "events", l),
                t &&
                  (o.runtime.tmp.runActionPopupX({
                    fbq_track_name: e.fbq_track_name,
                    conversion_name: e.conversion_name,
                    fbq_data: e.fbq_data,
                    fbq_event_data: e.fbq_event_data,
                    action: { type: "run_tracking_fbq" },
                  }),
                  o.runtime.tmp.runActionPopupX({
                    type: "facebook",
                    key: "events",
                    keyData: l,
                    action: { type: "run_conversion_api" },
                  }));
            })),
            y &&
              ((n = "trackCustom"),
              (e = null),
              (a = null),
              (s = []),
              !t &&
                o.isObject(window.ladi_conversion_api) &&
                o.isObject(window.ladi_conversion_api.facebook) &&
                o.isArray(window.ladi_conversion_api.facebook.pixels) &&
                (a = c()),
              s.push({
                fbq_track_name: n,
                conversion_name: "ClickButton",
                fbq_data: e,
                fbq_event_data: a,
              }),
              s.forEach(function (e) {
                o.isFunction(window.fbq) &&
                  window.fbq(
                    e.fbq_track_name,
                    e.conversion_name,
                    e.fbq_data,
                    e.fbq_event_data
                  ),
                  (l = [
                    {
                      key: e.fbq_track_name,
                      name: e.conversion_name,
                      custom_data: e.fbq_data,
                      data: e.fbq_event_data,
                    },
                  ]),
                  o.runConversionApi("facebook", "events", l),
                  t &&
                    (o.runtime.tmp.runActionPopupX({
                      fbq_track_name: e.fbq_track_name,
                      conversion_name: e.conversion_name,
                      fbq_data: e.fbq_data,
                      fbq_event_data: e.fbq_event_data,
                      action: { type: "run_tracking_fbq" },
                    }),
                    o.runtime.tmp.runActionPopupX({
                      type: "facebook",
                      key: "events",
                      keyData: l,
                      action: { type: "run_conversion_api" },
                    }));
              }));
        },
        v = function (t) {
          o.isEmpty(s) ||
            o.isEmpty(l) ||
            (t
              ? o.runtime.tmp.runActionPopupX({
                  conversion_name: "conversion",
                  gtag_data: { send_to: "AW-" + s + "/" + l },
                  action: { type: "run_tracking_gtag" },
                })
              : o.isFunction(window.gtag) &&
                window.gtag("event", "conversion", {
                  send_to: "AW-" + s + "/" + l,
                }));
          var i = null;
          if (!o.isEmpty(r)) {
            var a = "";
            (a =
              "section" == m
                ? "LadiPageSection"
                : "popup" == m
                ? "LadiPagePopup"
                : "form" == m
                ? "LadiPageConversion"
                : "LadiPageClick"),
              g && (a = e.event_category);
            var n = LadiPageApp[m + o.const.APP_RUNTIME_PREFIX];
            if (!o.isEmpty(n)) {
              var c = n(u, o.runtime.isClient);
              o.isFunction(c.getEventTrackingCategory) &&
                (a = n(u, o.runtime.isClient).getEventTrackingCategory());
            }
            (i = {
              event_category: a,
              event_label: window.location.host + window.location.pathname,
            }),
              o.isEmpty(d) || o.isEmpty(p) || ((i.value = d), (i.currency = p)),
              t
                ? o.runtime.tmp.runActionPopupX({
                    conversion_name: r,
                    gtag_data: i,
                    action: { type: "run_tracking_gtag" },
                  })
                : o.isFunction(window.gtag) && window.gtag("event", r, i);
          }
        },
        h = function (t) {
          var e = null;
          if (
            (o.isObject(i) &&
              (e = {
                email: i.email,
                phone_number: o.convertPhoneNumberTiktok(i.phone),
              }),
            o.isObject(e) &&
              !o.isEmpty(e.phone_number) &&
              (t
                ? o.runtime.tmp.runActionPopupX({
                    ttq_identify_data: e,
                    action: { type: "run_identify_ttq" },
                  })
                : o.isNull(window.ttq) || window.ttq.identify(e)),
            !o.isEmpty(r))
          ) {
            if (-1 != ["Purchase", "Lead"].indexOf(r)) return;
            var a = null;
            "CompletePayment" != r ||
              o.isEmpty(d) ||
              o.isEmpty(p) ||
              (((a = {}).value = d), (a.currency = p)),
              t
                ? o.runtime.tmp.runActionPopupX({
                    conversion_name: r,
                    ttq_data: a,
                    action: { type: "run_tracking_ttq" },
                  })
                : o.isNull(window.ttq) || window.ttq.track(r, a);
          }
          y &&
            (o.runLadiPageCommand(function (e) {
              if (e.tiktokViewContent && e.clickButton)
                return (
                  t
                    ? o.runtime.tmp.runActionPopupX({
                        conversion_name: e.name,
                        action: { type: "run_tracking_ttq" },
                      })
                    : o.isNull(window.ttq) || window.ttq.track(e.name),
                  !0
                );
            }),
            t
              ? o.runtime.tmp.runActionPopupX({
                  conversion_name: "ClickButton",
                  action: { type: "run_tracking_ttq" },
                })
              : o.isNull(window.ttq) || window.ttq.track("ClickButton"));
        };
      if (!o.isEmpty(a)) {
        var E = !1;
        return (
          o.isEmpty(s) || o.isEmpty(l) || (E = !0),
          o.isEmpty(r) || (E = !0),
          o.isEmpty(c) || (E = !0),
          y && (E = !0),
          -1 != ["section", "popup", "countdown"].indexOf(m) && (E = !1),
          void (
            E &&
            a.addEventListener("click", function (r) {
              (o.isFunction(n) && !n(a, r)) || o.runEventTracking(t, e, i);
            })
          )
        );
      }
      if (o.runtime.is_popupx)
        return (
          f(!0),
          v(!0),
          h(!0),
          void o.runtime.tmp.runActionPopupX({
            script: c,
            action: { type: "event_custom_script" },
          })
        );
      f(), v(), h(), o.isEmpty(c) || o.runFunctionString(c);
    }
  }),
  (PrimePageScriptV2.prototype.runFunctionString = function (t) {
    try {
      new Function(t)();
    } catch (t) {}
  }),
  (PrimePageScriptV2.prototype.convertReplacePrefixStr = function (t, e) {
    var i = t,
      a = this.runtime.replacePrefixStart,
      n = this.runtime.replacePrefixEnd,
      o = this.runtime.replaceNewPrefixStart,
      r = this.runtime.replaceNewPrefixEnd;
    if (e) {
      var s = o;
      (o = a), (a = s), (s = r), (r = n), (n = s);
    }
    for (
      var l = new RegExp(a + "[^" + a + "$" + n + "]*" + n, "gi"),
        c = null,
        d = function (t) {
          i = i.replaceAll(t, t.replaceAll(a, o).replaceAll(n, r));
        };
      null !== (c = l.exec(t));

    )
      c.index === l.lastIndex && l.lastIndex++, c.forEach(d);
    return i;
  }),
  (PrimePageScriptV2.prototype.formatCurrency = function (t, e, i, a) {
    var n = {
      VND: "{0}đ",
      KHR: "{0}៛",
      USD: "${0}",
      EUR: "€{0}",
      GBP: "£{0}",
      THB: "฿{0}",
      LAK: "₭{0}",
      PHP: "₱{0}",
      SGD: "S${0}",
      HKD: "HK${0}",
      TWD: "NT${0}",
      MYR: "RM{0}",
      IDR: "Rp{0}",
      INR: "₹{0}",
    };
    if (
      (Object.keys(n).forEach(function (t) {
        var i = n[t].replaceAll("{0}", "");
        (i = i.trim()) == e && (e = t);
      }),
      a)
    )
      return this.isEmpty(n[e]) ? e : n[e].format("").trim();
    var o = this.formatNumber(
      t,
      3,
      null,
      {
        VND: 0,
        USD: 2,
        EUR: 2,
        GBP: 2,
        SGD: 2,
        MYR: 2,
        HKD: 2,
        TWD: 0,
        THB: 0,
        PHP: 0,
        KHR: 0,
        LAK: 0,
        IDR: 0,
        INR: 0,
      }[e] || 0
    );
    return i && (o = this.isEmpty(n[e]) ? o + " " + e : n[e].format(o)), o;
  }),
  (PrimePageScriptV2.prototype.formatNumber = function (t, e, i, a) {
    if (null != t) {
      null == i && (i = 0), null == a && (a = 0);
      var n = "\\d(?=(\\d{" + (e || 3) + "})+" + (a > 0 ? "\\." : "$") + ")";
      t = t.toFixed(Math.max(0, ~~a)).replace(new RegExp(n, "g"), "$&,");
      var o = null,
        r = null;
      i >= 1 &&
        ((r = (o = t.split("."))[0]),
        (t = r = new Array(i - r.length + 1).join("0") + r),
        2 == o.length && (t += "." + o[1])),
        a >= 1 &&
          2 == (o = t.split(".")).length &&
          ((r = o[1]),
          (r = new Array(a - r.length + 1).join("0") + r),
          (t = o[0] + "." + r));
    }
    return t;
  }),
  (PrimePageScriptV2.prototype.setDataReplaceStr = function (t, e) {
    this.runtime.replaceStr[t] = e;
  }),
  (PrimePageScriptV2.prototype.getDataReplaceStr = function (t, e) {
    var i = null;
    return (
      this.isNull(e) || (i = e[t]),
      this.isNull(i) && (i = this.runtime.replaceStr[t]),
      i
    );
  }),
  (PrimePageScriptV2.prototype.highlightText = function (t, e, i, a) {
    if (!this.isEmpty(t) && 0 != e.length) {
      var n = i ? "gi" : "g",
        o = [];
      e.forEach(function (t) {
        o.push(t.replaceAll("|", "\\|"));
      }),
        o.sort(function (t, e) {
          return e.length - t.length;
        });
      for (
        var r = this,
          s = function (t) {
            var s = new RegExp(o.join("|"), n);
            if (3 !== t.nodeType) r.highlightText(t, e, i, a);
            else if (s.test(t.textContent)) {
              var l = document.createDocumentFragment(),
                c = 0;
              t.textContent.replace(s, function (e, i) {
                var n = document.createTextNode(t.textContent.slice(c, i)),
                  o = null;
                r.isFunction(a)
                  ? (o = a(e))
                  : ((o = document.createElement("span")).textContent = e),
                  l.appendChild(n),
                  l.appendChild(o),
                  (c = i + e.length);
              });
              var d = document.createTextNode(t.textContent.slice(c));
              l.appendChild(d), t.parentNode.replaceChild(l, t);
            }
          },
          l = 0;
        l < t.childNodes.length;
        l++
      )
        s(t.childNodes[l]);
    }
  }),
  (PrimePageScriptV2.prototype.convertDataReplaceStr = function (
    t,
    e,
    i,
    a,
    n,
    o
  ) {
    var r = this,
      s = r.runtime.replacePrefixStart,
      l = r.runtime.replacePrefixEnd;
    r.runtime.convert_replace_str &&
      ((s = r.runtime.replaceNewPrefixStart),
      (l = r.runtime.replaceNewPrefixEnd));
    for (
      var c = (t = r.isEmpty(t) ? "" : t),
        d = new RegExp(s + "[^" + s + "$" + l + "]*" + l, "gi"),
        p = null,
        u = [],
        m = function (t) {
          if (a) u.push(t);
          else {
            var i = t,
              d = i.split("|");
            (i = d[0].substr(s.length)),
              1 == d.length && (i = i.substr(0, i.length - l.length));
            var p = r.getDataReplaceStr(i, n);
            if (r.isEmpty(p))
              if (d.length > 1) {
                var m = r.randomInt(1, d.length - 1);
                (p = d[m]),
                  m == d.length - 1 && (p = p.substr(0, p.length - l.length));
              } else p = "";
            e &&
              (r.isArray(p) && p.length > 1 && (p = JSON.stringify(p)),
              (p = encodeURIComponent(p))),
              (c = o && r.isArray(p) && p.length > 1 ? p : c.replaceAll(t, p));
          }
        };
      null !== (p = d.exec(t));

    )
      p.index === d.lastIndex && d.lastIndex++, p.forEach(m);
    return (
      (u = u.unique()),
      r.highlightText(i, u, !0, function (t) {
        var e = document.createElement("span");
        return e.setAttribute("data-replace-str", t), e;
      }),
      r.runtime.isDesktop &&
        e &&
        !r.isEmpty(c) &&
        ["fb://profile/", "fb://page/?id=", "fb://page/"].forEach(function (t) {
          c.startsWith(t) && (c = c.replaceAll(t, "https://www.facebook.com/"));
        }),
      c
    );
  }),
  (PrimePageScriptV2.prototype.setDataReplaceElement = function (t, e, i, a, n) {
    var o = this.isEmpty(a) ? document : document.getElementById(a);
    if (!this.isEmpty(o)) {
      for (
        var r = o.querySelectorAll("span[data-replace-str]"), s = 0;
        s < r.length;
        s++
      ) {
        var l = r[s].getAttribute("data-replace-str");
        r[s].innerHTML = this.convertDataReplaceStr(l, !1, null, !1, i);
      }
      for (
        var c = o.querySelectorAll("a[data-replace-href]"), d = 0;
        d < c.length;
        d++
      ) {
        var p = c[d].getAttribute("data-replace-href");
        (p = this.convertDataReplaceStr(p, !0, null, !1, i)), (c[d].href = p);
      }
      for (
        var u = o.querySelectorAll(
            ".prime-element .prime-form-item-container [name]"
          ),
          m = 0;
        m < u.length;
        m++
      ) {
        var _ = null,
          y = null,
          g = !1,
          f = null,
          v = u[m].getAttribute("type");
        if (((v = this.isEmpty(v) ? v : v.trim()), t)) {
          var h = u[m].getAttribute("name").trim();
          (g = !0) && -1 == n.indexOf(h) && (g = !1),
            g &&
              "country" == h &&
              "true" == u[m].getAttribute("data-is-select-country") &&
              (g = !1),
            g && (_ = this.getDataReplaceStr(h, i));
        }
        if (!g) {
          if (((f = this.findAncestor(u[m], "prime-element")), this.isEmpty(f)))
            continue;
          var E = this.runtime.eventData[f.id];
          if (this.isEmpty(E)) continue;
          var P = E["option.input_default_value"];
          if (this.isEmpty(P)) continue;
          var b = !1;
          "INPUT" == u[m].tagName && "checkbox" == v && (b = !0),
            (_ = this.convertDataReplaceStr(P, !1, null, !1, i, b));
        }
        if (!this.isNull(_)) {
          if (
            ((y = this.isArray(_) ? _[0] : _),
            (y = this.isNull(y) ? "" : y),
            (y += ""),
            "INPUT" == u[m].tagName)
          )
            if ("checkbox" == v || "radio" == v) {
              var L = !1;
              if ("checkbox" == v) {
                var A = _;
                this.isArray(A) || (A = [A]),
                  (L = -1 != A.indexOf(u[m].getAttribute("value")));
              }
              "radio" == v && (L = u[m].getAttribute("value") == y.trim()),
                L
                  ? ((u[m].checked = !0),
                    e && u[m].setAttribute("checked", "checked"))
                  : ((u[m].checked = !1), e && u[m].removeAttribute("checked"));
              var w = this.findAncestor(u[m], "prime-form-checkbox-item");
              if (!this.isEmpty(w)) {
                var S = w.querySelector("span");
                this.isEmpty(S) || S.setAttribute("data-checked", u[m].checked);
              }
            } else
              (f = this.findAncestor(u[m], "prime-element")),
                (this.isEmpty(f) ||
                  "true" != f.getAttribute("data-quantity")) &&
                  ((u[m].value = y.trim()),
                  e && u[m].setAttribute("value", u[m].value));
          if (
            ("TEXTAREA" == u[m].tagName &&
              ((u[m].value = y.trim()), e && (u[m].innerHTML = u[m].value)),
            "SELECT" == u[m].tagName)
          ) {
            var T = u[m].querySelector('option[value="' + y.trim() + '"]');
            if (
              !this.isEmpty(T) &&
              ((u[m].value = T.getAttribute("value")),
              e && !T.hasAttribute("selected"))
            ) {
              for (
                var O = u[m].querySelectorAll("option"), C = 0;
                C < O.length;
                C++
              )
                O[C].removeAttribute("selected");
              T.setAttribute("selected", "selected");
            }
          }
        }
      }
    }
  }),
  (PrimePageScriptV2.prototype.setDataReplaceStart = function () {
    for (
      var t = document.querySelectorAll(
          ".prime-headline, .prime-paragraph, .prime-list-paragraph ul"
        ),
        e = 0;
      e < t.length;
      e++
    )
      this.convertDataReplaceStr(t[e].innerHTML, !1, t[e], !0);
    this.setDataReplaceElement(
      !0,
      !0,
      null,
      null,
      Object.keys(this.runtime.replaceStr)
    );
  }),
  (PrimePageScriptV2.prototype.runLimitRequest = function (t, e) {
    var i = this,
      a = 1e3 / t;
    if (i.runtime.tmp.time_limit_request_next > Date.now())
      return i.runTimeout(function () {
        i.runLimitRequest(t, e);
      }, a / 5);
    (i.runtime.tmp.time_limit_request_next = Date.now() + a),
      i.isFunction(e) && e();
  }),
  (PrimePageScriptV2.prototype.showMessage = function (t, e, i) {
    var a = this;
    if (
      ((t = a.convertDataReplaceStr(t, !1, null, !1, e)), a.runtime.has_popupx)
    )
      (a.runtime.tmp.popupx_show_message_callback = i),
        a.runtime.tmp.runActionPopupX({
          message: t,
          action: { type: "show_message" },
        });
    else {
      var n = document.getElementsByClassName("primeace-message")[0];
      if ((this.isEmpty(n) || n.parentElement.removeChild(n), this.isEmpty(t)))
        return void (this.isFunction(i) && i());
      ((n = document.createElement("div")).className = "primeace-message"),
        document.body.appendChild(n);
      var o = document.createElement("div");
      (o.className = "primeace-message-box"), n.appendChild(o);
      var r = document.createElement("span");
      o.appendChild(r), (r.textContent = this.const.LANG.ALERT_TITLE);
      var s = document.createElement("div");
      (s.className = "primeace-message-text"),
        o.appendChild(s),
        (s.innerHTML = t);
      var l = document.createElement("button");
      (l.className = "primeace-message-close"),
        o.appendChild(l),
        (l.textContent = this.const.LANG.ALERT_BUTTON_TEXT),
        l.focus(),
        l.addEventListener("click", function (t) {
          t.stopPropagation(),
            n.parentElement.removeChild(n),
            a.isFunction(i) && i();
        });
    }
  }),
  (PrimePageScriptV2.prototype.getTextClipboard = function (t, e) {
    var i = this,
      a = function (a) {
        i.isFunction(e) &&
          ((a = i.isEmpty(a) ? (i.isEmpty(t) ? "" : t) : a), e(!0, a));
      },
      n = function () {
        try {
          var t = document.createElement("textarea");
          t.setAttribute(
            "style",
            "position: fixed; top: 0, left: 0, width: 1px; height: 1px; opacity: 0;"
          ),
            document.body.appendChild(t),
            t.focus();
          var n = document.execCommand("paste"),
            o = t.value;
          if ((t.parentElement.removeChild(t), n)) return void a(o);
        } catch (t) {}
        i.isFunction(e) && e(!1, null);
      };
    window.navigator.clipboard
      ? window.navigator.clipboard.readText().then(a).catch(n)
      : n();
  }),
  (PrimePageScriptV2.prototype.copyTextClipboard = function (t, e) {
    var i = this,
      a = function () {
        i.isFunction(e) && e(!0, t);
      },
      n = function () {
        try {
          var n = document.createElement("textarea");
          n.setAttribute(
            "style",
            "position: fixed; top: 0, left: 0, width: 1px; height: 1px; opacity: 0;"
          ),
            document.body.appendChild(n),
            (n.value = t),
            n.select();
          var o = document.execCommand("copy");
          if ((n.parentElement.removeChild(n), o)) return void a();
        } catch (t) {}
        i.isFunction(e) && e(!1, null);
      };
    window.navigator.clipboard
      ? window.navigator.clipboard.writeText(t).then(a).catch(n)
      : n();
  }),
  (PrimePageScriptV2.prototype.fireEvent = function (t, e, i) {
    t = this.isString(t) ? document.querySelector(t) : t;
    var a = document.createEvent("HTMLEvents");
    return (
      a.initEvent(e, !0, !0),
      this.isObject(i) &&
        Object.keys(i).forEach(function (t) {
          a[t] = i[t];
        }),
      !t.dispatchEvent(a)
    );
  }),
  (PrimePageScriptV2.prototype.tapEventListener = function (t, e) {
    var i = this,
      a = function (t) {
        i.isFunction(e) && e(t);
      };
    if ((t.addEventListener("click", a), "ontouchstart" in window)) {
      var n = 0,
        o = 0,
        r = i.getWidthDevice(),
        s = Math.max(1, Math.floor(0.01 * r)),
        l = null;
      t.addEventListener(
        "touchstart",
        function (e) {
          (e = i.getEventCursorData(e)),
            i.removeTimeout(l),
            (n = e.screenX),
            (o = e.screenY),
            t.removeEventListener("click", a);
        },
        i.runtime.scrollEventPassive
      ),
        t.addEventListener("touchend", function (e) {
          (e = i.getEventCursorData(e)),
            Math.abs(e.screenX - n) <= s &&
              Math.abs(e.screenY - o) <= s &&
              a(e),
            (l = i.runTimeout(function () {
              t.addEventListener("click", a);
            }, i.runtime.time_click));
        });
    }
  }),
  (PrimePageScriptV2.prototype.findAncestor = function (t, e) {
    var i = this;
    e = i.isArray(e) ? e : [e];
    for (
      var a = function (t, e) {
          if (i.isNull(t) || i.isNull(t.classList) || !t.classList.contains(e))
            for (; (t = t.parentElement) && !t.classList.contains(e); );
          return t;
        },
        n = 0;
      n < e.length && ((t = a(t, e[n])), !i.isEmpty(t));
      n++
    );
    return t;
  }),
  (PrimePageScriptV2.prototype.createStyleElement = function (t, e) {
    var i = document.getElementById(t);
    return (
      this.isEmpty(i) &&
        (((i = document.createElement("style")).id = t),
        (i.type = "text/css"),
        document.head.appendChild(i)),
      i.innerHTML != e && (i.innerHTML = e),
      i
    );
  }),
  (PrimePageScriptV2.prototype.createTmpElement = function (t, e, i, a, n) {
    var o = document.createElement(t);
    this.isEmpty(i) ||
      Object.keys(i).forEach(function (t) {
        o.setAttribute(t, i[t]);
      });
    var r = document.createElement("div");
    return (
      r.appendChild(o),
      a ? (o.outerHTML = e) : (o.innerHTML = e),
      n ? r : r.firstChild
    );
  }),
  (PrimePageScriptV2.prototype.getSource2ndClick = function (t) {
    var e = this.runtime.eventData[t];
    if (!this.isEmpty(e))
      return "image" == e.type &&
        e[this.runtime.device + ".option.image_setting.2nd_click"]
        ? e[this.runtime.device + ".option.image_setting.source_2nd_click"]
        : "shape" == e.type && e["option.shape_setting.2nd_click"]
        ? e[this.runtime.device + ".option.shape_setting.source_2nd_click"]
        : void 0;
  }),
  (PrimePageScriptV2.prototype.getCountdownTime = function (t, e) {
    var i = Math.floor(t / 1e3),
      a = i % 86400,
      n = a % 3600,
      o = Math.floor(i / 86400),
      r = Math.floor(a / 3600),
      s = Math.floor(n / 60),
      l = n % 60;
    (o = (o = o < 0 ? 0 : o) < 10 ? "0" + o : o),
      (r = (r = r < 0 ? 0 : r) < 10 ? "0" + r : r),
      (s = (s = s < 0 ? 0 : s) < 10 ? "0" + s : s),
      (l = (l = l < 0 ? 0 : l) < 10 ? "0" + l : l);
    var c = {};
    return (
      (c[this.const.COUNTDOWN_ITEM_TYPE.day] = o),
      (c[this.const.COUNTDOWN_ITEM_TYPE.hour] = r),
      (c[this.const.COUNTDOWN_ITEM_TYPE.minute] = s),
      (c[this.const.COUNTDOWN_ITEM_TYPE.seconds] = l),
      this.isEmpty(e) ? c : c[e]
    );
  }),
  (PrimePageScriptV2.prototype.getElementBoundingClientRect = function (t) {
    this.isString(t) && (t = document.getElementById(t));
    var e = t.getBoundingClientRect();
    return (
      (this.isNull(e.x) || this.isNull(e.y)) && ((e.x = e.left), (e.y = e.top)),
      e
    );
  }),
  (PrimePageScriptV2.prototype.getElementViewBox = function (t) {
    var e = { x: 0, y: 0, width: 0, height: 0 },
      i = t.getAttribute("viewBox").split(" ");
    return (
      (e.x = parseFloatLadiPage(i[0]) || 0),
      (e.y = parseFloatLadiPage(i[1]) || 0),
      (e.width = parseFloatLadiPage(i[2]) || 0),
      (e.height = parseFloatLadiPage(i[3]) || 0),
      e
    );
  }),
  (PrimePageScriptV2.prototype.getEventCursorData = function (t) {
    var e = this;
    return (
      ["pageX", "pageY", "screenX", "screenY"].forEach(function (i) {
        e.isNull(t[i]) &&
          (!e.isEmpty(t.touches) && t.touches.length > 0
            ? (t[i] = t.touches[0][i])
            : !e.isEmpty(t.targetTouches) && t.targetTouches.length > 0
            ? (t[i] = t.targetTouches[0][i])
            : !e.isEmpty(t.changedTouches) &&
              t.changedTouches.length > 0 &&
              (t[i] = t.changedTouches[0][i]));
      }),
      t
    );
  }),
  (PrimePageScriptV2.prototype.getElementAHref = function (t, e) {
    var i = document.createElement("a");
    return (
      !e ||
        t.toLowerCase().startsWith("http://") ||
        t.toLowerCase().startsWith("https://") ||
        t.startsWith("//") ||
        (t = "http://" + t),
      (i.href = t),
      i
    );
  }),
  (PrimePageScriptV2.prototype.loadScript = function (t, e, i, a) {
    var n = document.createElement("script");
    (n.type = "text/javascript"),
      this.isFunction(i) && ((a = i), (i = e), (e = null)),
      i && (n.async = !0),
      this.isObject(e) &&
        Object.keys(e).forEach(function (t) {
          n.setAttribute(t, e[t]);
        }),
      n.addEventListener("load", a),
      (n.src = t),
      document.head.appendChild(n);
  }),
  (PrimePageScriptV2.prototype.showLoadingBlur = function () {
    var t = document.getElementsByClassName("prime-loading")[0];
    this.isEmpty(t) &&
      (((t = document.createElement("div")).className = "prime-loading"),
      (t.innerHTML =
        '<div class="loading"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>'),
      document.body.appendChild(t));
  }),
  (PrimePageScriptV2.prototype.hideLoadingBlur = function () {
    var t = document.getElementsByClassName("prime-loading")[0];
    this.isEmpty(t) || t.parentElement.removeChild(t);
  }),
  (PrimePageScriptV2.prototype.randomId = function () {
    var t = Date.now(),
      e =
        (window.performance &&
          window.performance.now &&
          1e3 * window.performance.now()) ||
        0;
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
      /[xy]/g,
      function (i) {
        var a = 16 * Math.random();
        return (
          t > 0
            ? ((a = (t + a) % 16 | 0), (t = Math.floor(t / 16)))
            : ((a = (e + a) % 16 | 0), (e = Math.floor(e / 16))),
          ("x" === i ? a : (3 & a) | 8).toString(16)
        );
      }
    );
  }),
  (PrimePageScriptV2.prototype.decodeValue = function (t) {
    var e = this;
    if (
      (e.isObject(t) &&
        Object.keys(t).forEach(function (i) {
          t[i] = e.decodeValue(t[i]);
        }),
      e.isArray(t))
    )
      for (var i = 0; i < t.length; i++) t[i] = e.decodeValue(t[i]);
    return e.isString(t) && (t = t.decode()), t;
  }),
  (PrimePageScriptV2.prototype.run = function (t) {
    var e = this;
    if (e.runtime.isLoadHtmlGlobal) {
      (e.runtime.isIE = !!document.documentMode),
        (e.runtime.isIE = e.runtime.isIE
          ? e.runtime.isIE
          : !e.runtime.isIE && !!window.StyleMedia),
        (e.runtime.scrollEventPassive = null);
      try {
        var i = Object.defineProperty({}, "passive", {
          get: function () {
            e.runtime.scrollEventPassive = { passive: !0 };
          },
        });
        window.addEventListener("testPassive", null, i),
          window.removeEventListener("testPassive", null, i);
      } catch (t) {}
      (e.runtime.isClient = t),
        (e.runtime.timenow = window.ladi("_timenow").get_cookie()),
        e.isEmpty(e.runtime.timenow)
          ? ((e.runtime.timenow = Date.now()),
            window.ladi("_timenow").set_cookie(e.runtime.timenow, 1))
          : (e.runtime.timenow = parseFloatLadiPage(e.runtime.timenow) || 0);
      try {
        e.runtime.widthScrollBar =
          window.innerWidth - document.documentElement.clientWidth;
      } catch (t) {}
      if (e.isString(e.runtime.eventData))
        try {
          var a = decodeURIComponentLadiPage(e.runtime.eventData);
          e.runtime.eventData = JSON.parse(a);
        } catch (t) {
          var n = e.runtime.eventData
            .replaceAll(/&amp;/g, "&")
            .replaceAll(/&gt;/g, ">")
            .replaceAll(/&lt;/g, "<")
            .replaceAll(/&quot;/g, '"');
          (n = n.replaceAll("\r\n", "").replaceAll("\n", "")),
            (e.runtime.eventData = JSON.parse(n));
        }
      else {
        var o = document.getElementById("script_event_data");
        if (!e.isEmpty(o))
          try {
            (e.runtime.eventData = JSON.parse(o.innerHTML)),
              (e.runtime.eventData = e.decodeValue(e.runtime.eventData));
          } catch (t) {}
      }
      e.isNull(window.ladi_is_desktop)
        ? (e.runtime.isDesktop = t
            ? !/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(
                window.navigator.userAgent.toLowerCase()
              )
            : LadiPage.isDesktop())
        : (e.runtime.isDesktop = t
            ? window.ladi_is_desktop
            : LadiPage.isDesktop()),
        (e.runtime.isBrowserDesktop = !("ontouchstart" in window)),
        (e.runtime.device = e.runtime.isDesktop
          ? e.const.DESKTOP
          : e.const.MOBILE),
        (e.runtime.tmp.isFirstScroll = !0),
        (e.runtime.tmp.capture_form_data_last = {}),
        (e.runtime.tmp.listAfterLocation = []),
        (e.runtime.tmp.product_info = {}),
        (e.runtime.tmp.timeout_product_info = {}),
        (e.runtime.tmp.product_tag_info = {}),
        (e.runtime.tmp.timeout_product_tag_info = {}),
        (e.runtime.tmp.dataset_check_load = !1),
        (e.runtime.tmp.dataset_data = {}),
        (e.runtime.tmp.timeout_dataset_data = {}),
        (e.runtime.tmp.cart = []),
        (e.runtime.tmp.add_to_cart_discount = 0),
        (e.runtime.tmp.add_to_cart_fee_shipping = 0),
        (e.runtime.tmp.is_click_add_to_cart = !1),
        (e.runtime.tmp.is_click_check_price_discount = !1),
        (e.runtime.tmp.current_use_coupon = null);
      var r = Object.keys(e.runtime.eventDataGlobal);
      r.forEach(function (t) {
        (e.runtime.eventData[t] = e.runtime.eventDataGlobal[t]),
          delete e.runtime.eventDataGlobal[t];
      }),
        (r = Object.keys(e.runtime.eventData)).forEach(function (t) {
          Object.keys(e.runtime.eventData[t]).forEach(function (i) {
            if (e.runtime.isDesktop) {
              if (i.toLowerCase().startsWith(e.const.DESKTOP)) {
                var a = e.const.MOBILE + i.substring(e.const.DESKTOP.length);
                e.runtime.eventData[t][a] = e.runtime.eventData[t][i];
              }
            } else if (i.toLowerCase().startsWith(e.const.MOBILE)) {
              var n = e.const.DESKTOP + i.substring(e.const.MOBILE.length);
              e.runtime.eventData[t][n] = e.runtime.eventData[t][i];
            }
          });
        });
      try {
        var s = window.ladi("LADI_DATA").get_cookie();
        (s = JSON.parse(Base64.decode(s || Base64.encode("{}")))),
          Object.keys(s).forEach(function (t) {
            e.setDataReplaceStr(t, s[t]);
          });
      } catch (t) {}
      var l = function (t) {
          var i = e.copy(t);
          return (
            e.isObject(i) &&
              e.runtime.list_set_value_name_country.forEach(function (t) {
                if (!e.isEmpty(i[t])) {
                  var a = i[t].split(":");
                  a.length > 1 && a.shift(), (i[t] = a.join(":"));
                }
              }),
            i
          );
        },
        c = e.getURLSearchParams(null, null, !0),
        d = e.getURLSearchParams(window.location.search, null, !0),
        p = l(c),
        u = Object.keys(p),
        m = "";
      u.forEach(function (t) {
        if (t != e.const.TRACKING_NAME && t != e.const.ACCESS_KEY_NAME) {
          if (
            (e.setDataReplaceStr(t, p[t]), "products" == t && e.isString(p[t]))
          ) {
            var i = p[t].split("|");
            2 == i.length &&
              -1 == u.indexOf("product_id") &&
              e.setDataReplaceStr("product_value", i[0]),
              2 == i.length &&
                -1 == u.indexOf("product_name") &&
                e.setDataReplaceStr("product_name", i[1]);
          }
          (e.isArray(d[t]) ? d[t] : [d[t]]).forEach(function (i) {
            e.isEmpty(m) ? (m += "?") : (m += "&"),
              (m += t + "=" + encodeURIComponent(i));
          });
        }
      }),
        window.ladi(e.const.TRACKING_NAME).delete_cookie("/"),
        m != window.location.search &&
          e.historyReplaceState(
            window.location.pathname + m + window.location.hash
          );
      var _ = d[e.const.REF_NAME];
      e.isEmpty(_)
        ? (_ = window.ladi("ladi_ref").get_cookie())
        : window.ladi("ladi_ref").set_cookie(_, 90, "/", window.location.host);
      var y = Object.keys(e.runtime.eventData),
        g = [],
        f = window.ladi("LADI_CLIENT_ID").get_cookie(),
        v = parseFloatLadiPage(window.ladi("LADI_PAGE_VIEW").get_cookie()) || 0,
        h =
          parseFloatLadiPage(window.ladi("LADI_FORM_SUBMIT").get_cookie()) || 0,
        E = window.ladi("LADI_FUNNEL_NEXT_URL").get_cookie(),
        P = window.ladi("LADI_CAMP_ID").get_cookie(),
        b = window.ladi("LADI_CAMP_NAME").get_cookie(),
        L = window.ladi("LADI_CAMP_TYPE").get_cookie(),
        A = window.ladi("LADI_CAMP_TARGET_URL").get_cookie(),
        w = window.ladi("LADI_CAMP_ORIGIN_URL").get_cookie(),
        S =
          parseFloatLadiPage(window.ladi("LADI_CAMP_PAGE_VIEW").get_cookie()) ||
          0,
        T =
          parseFloatLadiPage(
            window.ladi("LADI_CAMP_FORM_SUBMIT").get_cookie()
          ) || 0,
        O = window.ladi("LADI_CAMP_CONFIG").get_cookie(),
        C = function (t, i, a) {
          if ("FormSubmit" == t && e.isEmpty(f)) e.isFunction(a) && a();
          else if (!e.runtime.is_popupx || e.runtime.has_popupx) {
            var n = e.runtime.publish_platform,
              o = e.runtime.store_id,
              r = e.runtime.time_zone,
              s = window.location.host,
              l = window.location.href,
              c = e.runtime.ladipage_id,
              d = {
                event: t,
                pixel_id: o,
                time_zone: r,
                domain: s,
                url: l,
                ladipage_id: c,
                publish_platform: n,
                data: [],
              };
            Object.keys(i).forEach(function (t) {
              d[t] = i[t];
            }),
              e.runtime.is_popupx &&
              ((d.type = "POPUPX"),
              (d.origin_store_id = e.runtime.tmp.popupx_origin_store_id),
              (d.origin_referer = e.runtime.tmp.popupx_origin_referer),
              (d.origin_domain = e.runtime.tmp.popupx_origin_domain),
              (d.origin_url = e.runtime.tmp.popupx_origin_url),
              (d.element_id = e.runtime.tmp.popupx_current_element_id),
              e.isEmpty(d.element_id))
                ? e.runTimeout(function () {
                    C(t, i, a);
                  }, 100)
                : ("FormSubmit" == t &&
                    (e.isEmpty(P) ? h++ : (A == w && h++, T++),
                    window.ladi("LADI_CAMP_FORM_SUBMIT").set_cookie(T, 3650),
                    window.ladi("LADI_FORM_SUBMIT").set_cookie(h, 3650),
                    e.runtime.is_popupx &&
                      e.runtime.tmp.runActionPopupX({
                        action: { type: "set_submit_form" },
                      })),
                  "PageView" == t &&
                    e.runtime.has_popupx &&
                    (v++, window.ladi("LADI_PAGE_VIEW").set_cookie(v, 3650)),
                  e.sendRequest(
                    "POST",
                    e.const.API_ANALYTICS_EVENT,
                    JSON.stringify(d),
                    !0,
                    {
                      "Content-Type": "application/json",
                      LADI_CLIENT_ID: f,
                      LADI_PAGE_VIEW: v,
                      LADI_FORM_SUBMIT: h,
                      LADI_CAMP_ID: P,
                      LADI_CAMP_NAME: b,
                      LADI_CAMP_TYPE: L,
                      LADI_CAMP_TARGET_URL: A,
                      LADI_CAMP_ORIGIN_URL: w,
                      LADI_CAMP_PAGE_VIEW: S,
                      LADI_CAMP_FORM_SUBMIT: T,
                    },
                    function (t, i, n) {
                      n.readyState == XMLHttpRequest.DONE &&
                        e.isFunction(a) &&
                        a(i, t);
                    }
                  ));
          } else
            e.runTimeout(function () {
              C(t, i, a);
            }, 100);
        },
        N = function (t, i, a, n) {
          var o = null,
            r = null,
            s = null,
            l = 0;
          if (e.isEmpty(i) || "POPUP_PRODUCT" != i.id)
            if (e.isEmpty(i) || "POPUP_BLOG" != i.id) e.isFunction(n) && n();
            else {
              if (
                ((o = e.generateVariantProduct(
                  a,
                  !1,
                  null,
                  null,
                  null,
                  null,
                  !0,
                  !0,
                  function (e) {
                    N(t, i, a, n);
                  }
                )),
                !e.isObject(o) ||
                  !e.isObject(o.product) ||
                  !e.isObject(o.store_info))
              )
                return;
              var c = function () {
                var t,
                  a,
                  d = !0;
                if (
                  (Object.keys(o.product).forEach(function (t) {
                    if (
                      d &&
                      e.isString(o.product[t]) &&
                      o.product[t].startsWith(
                        e.const.DATASET_CONTENT_SOURCE_URL
                      ) &&
                      o.product[t].endsWith(
                        e.const.DATASET_CONTENT_SOURCE_ENDSWITH
                      )
                    ) {
                      var i = o.product[t].replaceAll(
                        e.const.DATASET_CONTENT_SOURCE_URL,
                        e.const.API_DATASET_BLOG
                      );
                      (d = !1),
                        e.showLoadingBlur(),
                        e.sendRequest(
                          "GET",
                          i,
                          null,
                          !0,
                          null,
                          function (e, i, a) {
                            a.readyState == XMLHttpRequest.DONE &&
                              ((o.product[t] = e), c());
                          }
                        );
                    }
                  }),
                  d)
                ) {
                  e.hideLoadingBlur(),
                    i.classList.add("opacity-0"),
                    (r = document.querySelectorAll(
                      "#" + i.id + " .prime-element"
                    ));
                  var p = null,
                    u = function (t) {
                      e.removeTimeout(p);
                      var a = function (t, i) {
                        var a = e.findAncestor(t.parentElement, "prime-element");
                        e.updateHeightElement(!0, t, a, i, t.clientHeight);
                      };
                      p = e.runTimeout(
                        function () {
                          e.showParentVisibility(r[0], function () {
                            for (l = 0; l < r.length; l++)
                              if (
                                r[l].querySelectorAll(
                                  ".prime-headline, .prime-paragraph"
                                ).length > 0 &&
                                r[l].hasAttribute("data-height")
                              ) {
                                var t =
                                  parseFloatLadiPage(
                                    r[l].getAttribute("data-height")
                                  ) || 0;
                                t != r[l].clientHeight &&
                                  (r[l].setAttribute(
                                    "data-height",
                                    r[l].clientHeight
                                  ),
                                  a(r[l], t));
                              }
                          }),
                            e.runShowPopup(!0, i.id, null, null, !0);
                        },
                        e.isEmpty(t) ? 0 : 100
                      );
                    };
                  e.showParentVisibility(r[0], function () {
                    for (l = 0; l < r.length; l++)
                      r[l].querySelectorAll(".prime-headline, .prime-paragraph")
                        .length > 0 &&
                        !r[l].hasAttribute("data-height") &&
                        r[l].setAttribute("data-height", r[l].clientHeight);
                  });
                  var m = function (t) {
                    (!e.runtime.isDesktop ||
                      (e.isEmpty(t.getAttribute("height")) &&
                        e.isEmpty(t.style.getPropertyValue("height")))) &&
                      (t.addEventListener("load", u),
                      t.addEventListener("error", u));
                  };
                  for (l = 0; l < r.length; l++)
                    e.runtime.tmp.runLadiSaleProductKey(
                      r[l].id,
                      !1,
                      !1,
                      s,
                      o,
                      !0,
                      null,
                      !0
                    ),
                      (t = r[l]),
                      (a = e.runtime.eventData[t.id]),
                      e.isFunction(e.runtime.tmp.runOptionAction) &&
                        e.isObject(a) &&
                        e.runtime.tmp.runOptionAction(t, t.id, a.type, a, o);
                  for (l = 0; l < r.length; l++)
                    for (
                      var _ = r[l].querySelectorAll(
                          ".prime-headline img, .prime-paragraph img"
                        ),
                        y = 0;
                      y < _.length;
                      y++
                    )
                      m(_[y]);
                  e.isFunction(n) && n(),
                    u(),
                    e.runTimeout(function () {
                      i.classList.remove("opacity-0");
                    }, 150);
                }
              };
              c();
            }
          else {
            if (
              ((o = e.generateVariantProduct(
                a,
                !1,
                null,
                null,
                null,
                null,
                !0,
                !0,
                function (e) {
                  N(t, i, a, n);
                }
              )),
              !e.isObject(o) ||
                !e.isObject(o.store_info) ||
                !e.isObject(o.product) ||
                !e.isArray(o.product.variants) ||
                o.product.variants.length <= 0)
            )
              return;
            if (e.isEmpty(a["option.product_variant_id"])) {
              s = o.product.variants[0];
              var d = e.findAncestor(t, "prime-collection-item"),
                p = null;
              if (e.isEmpty(d)) {
                for (
                  var u = document.querySelectorAll(
                      '[data-variant="true"] select[data-store-id="' +
                        o.store_info.id +
                        '"][data-product-id="' +
                        o.product.product_id +
                        '"]'
                    ),
                    m = 0;
                  m < u.length;
                  m++
                )
                  if (e.isEmpty(e.findAncestor(u[m], "prime-collection-item"))) {
                    p = u[m];
                    break;
                  }
              } else p = d.querySelector('[data-variant="true"]');
              if (!e.isEmpty(p)) {
                p = e.findAncestor(p, "prime-element");
                var _ = e.getProductVariantId(p, o.product);
                e.isEmpty(_) ||
                  (s = o.product.variants.find(function (t) {
                    return t.product_variant_id == _;
                  }));
              }
            } else
              s = o.product.variants.find(function (t) {
                return t.product_variant_id == a["option.product_variant_id"];
              });
            if (e.isEmpty(s)) return;
            var y = function (t) {
              var i = e.runtime.eventData[t.id];
              e.isFunction(e.runtime.tmp.runOptionAction) &&
                e.isObject(i) &&
                e.runtime.tmp.runOptionAction(t, t.id, i.type, i, o);
            };
            for (
              r = document.querySelectorAll("#" + i.id + " .prime-element"),
                l = 0;
              l < r.length;
              l++
            )
              e.runtime.tmp.runLadiSaleProductKey(r[l].id, !1, !1, s, o),
                y(r[l]);
            e.isFunction(n) && n();
          }
        },
        I = function (t, i) {
          i = e.isArray(i) ? i : [];
          var a = e.runtime.eventData[t.id],
            n = i.findIndex(function (t) {
              return (
                t.action_type == e.const.ACTION_TYPE.complete &&
                (t.type == e.const.DATA_ACTION_TYPE.popup ||
                  t.type == e.const.DATA_ACTION_TYPE.popup_cart ||
                  t.type == e.const.DATA_ACTION_TYPE.popup_checkout)
              );
            });
          (n = -1 != n),
            i.forEach(function (i) {
              if (i.action_type == e.const.ACTION_TYPE.complete) {
                var o = null,
                  r = null;
                if (i.type == e.const.DATA_ACTION_TYPE.section) {
                  var s = 0,
                    l = document.getElementById(i.action);
                  if (!e.isEmpty(l)) {
                    if (n) return void window.ladi(l.id).scroll(!1, !0);
                    if (
                      ((o = e.findAncestor(t, "prime-popup")), !e.isEmpty(o))
                    ) {
                      var c = e.findAncestor(o, "prime-element");
                      c.hasAttribute("data-popup-backdrop") &&
                        (window.ladi(c.id).hide(), (s = 100));
                    }
                    e.runTimeout(function () {
                      window.ladi(l.id).scroll();
                    }, s);
                  }
                }
                if (
                  (i.type == e.const.DATA_ACTION_TYPE.popup &&
                    ((o = document.getElementById(i.action)),
                    e.isEmpty(o) ||
                      N(t, o, a, function () {
                        window.ladi(i.action).show();
                      })),
                  i.type == e.const.DATA_ACTION_TYPE.hidden_show &&
                    (e.isArray(i.hidden_ids) &&
                      i.hidden_ids.forEach(function (t) {
                        window.ladi(t).hide();
                      }),
                    e.isArray(i.show_ids) &&
                      i.show_ids.forEach(function (t) {
                        window.ladi(t).show();
                      })),
                  i.type == e.const.DATA_ACTION_TYPE.change_index &&
                    ((r = window.ladi(i.action)),
                    e.isFunction(r[i.change_index_type])
                      ? r[i.change_index_type]()
                      : r.index(i.change_index_number || 1)),
                  i.type == e.const.DATA_ACTION_TYPE.set_value &&
                    ((r = window.ladi(i.action)),
                    e.isEmpty(r) ||
                      (i.is_clipboard
                        ? e.getTextClipboard(i.str, function (e, a) {
                            r.value(e ? a : i.str), k(t, e, !0);
                          })
                        : r.value(i.str))),
                  i.type == e.const.DATA_ACTION_TYPE.link)
                ) {
                  var d = i.action;
                  e.isEmpty(d) ||
                    ((d = e.getLinkUTMRedirect(d, null)),
                    (d = e.convertDataReplaceStr(d, !0)),
                    window.ladi(d).open_url(i.target, i.nofollow));
                }
              }
            });
        },
        k = function (t, i, a) {
          var n =
            parseFloatLadiPage(t.getAttribute("data-timeout-id-copied")) || 0;
          e.removeTimeout(n);
          var o = "hint-{0}-middle-s-small-hint-anim-d-short",
            r = !0;
          e.getElementBoundingClientRect(t).y < 35 && (r = !1),
            r
              ? (t.classList.add(o.format("top")),
                t.classList.remove(o.format("bottom")))
              : (t.classList.remove(o.format("top")),
                t.classList.add(o.format("bottom"))),
            i
              ? a
                ? t.setAttribute("data-hint", e.const.LANG.PASTED)
                : t.setAttribute("data-hint", e.const.LANG.COPIED)
              : t.setAttribute("data-hint", e.const.LANG.FALIED),
            (n = e.runTimeout(function () {
              t.classList.remove(o.format("top")),
                t.classList.remove(o.format("bottom")),
                t.removeAttribute("data-hint"),
                t.removeAttribute("data-timeout-id-copied");
            }, 1e3)),
            t.setAttribute("data-timeout-id-copied", n);
        },
        x = function (t, i, a, n, o) {
          if (((t = t || document.getElementById(i)), !e.isEmpty(t))) {
            var r = function (t) {
                if (!e.isEmpty(t))
                  return "true" == t.getAttribute("data-dropbox")
                    ? t
                    : r(t.parentElement);
              },
              s = function (t, i) {
                if ("false" == t.getAttribute("data-click")) return !1;
                var a = r(i.target);
                if (!e.isEmpty(a)) {
                  var n = document.getElementById(
                    a.getAttribute("data-from-doc-id")
                  );
                  if (!e.isEmpty(n)) return n.id != t.id && s(t, { target: n });
                }
                return !0;
              },
              l = function () {
                return e.runtime.count_click_dom[t.id] || 0;
              },
              c = function (t) {
                var i = l();
                return (
                  t.action_type == e.const.ACTION_TYPE.action ||
                  (t.action_type == e.const.ACTION_TYPE["1st_click"] &&
                    i % 2 == 1) ||
                  (t.action_type == e.const.ACTION_TYPE["2nd_click"] &&
                    i % 2 == 0) ||
                  void 0
                );
              };
            t.addEventListener("click", function () {
              e.runtime.count_click_dom[t.id] = l() + 1;
            });
            var d = n["option.is_submit_form"],
              p = n["option.data_submit_form_id"];
            if (!n["option.action_funnel"] || e.isEmpty(E))
              if (!d || e.isEmpty(p)) {
                var u = n["option.data_event"];
                if (
                  !e.isArray(u) &&
                  ((u = []), e.isObject(n["option.data_action"]))
                ) {
                  var m = e.copy(n["option.data_action"]);
                  (m.action_type = e.const.ACTION_TYPE.action), u.push(m);
                }
                var _ = e.getSource2ndClick(t.id);
                e.isEmpty(_) ||
                  (t.classList.add("is-2nd-click"),
                  u.push({
                    action_type: e.const.ACTION_TYPE.action,
                    type: e.const.DATA_ACTION_TYPE.set_value_2nd,
                    source: _,
                  }));
                var y = function (e, i) {
                    return k(t, e, !1);
                  },
                  g = u.findIndex(function (t) {
                    return (
                      t.action_type == e.const.ACTION_TYPE.action &&
                      (t.type == e.const.DATA_ACTION_TYPE.popup ||
                        t.type == e.const.DATA_ACTION_TYPE.popup_cart ||
                        t.type == e.const.DATA_ACTION_TYPE.popup_checkout)
                    );
                  });
                g = -1 != g;
                var f = 0;
                u.forEach(function (a) {
                  if (
                    a.action_type == e.const.ACTION_TYPE.action ||
                    a.action_type == e.const.ACTION_TYPE["1st_click"] ||
                    a.action_type == e.const.ACTION_TYPE["2nd_click"]
                  ) {
                    if (
                      (f++,
                      a.type == e.const.DATA_ACTION_TYPE.set_value_2nd &&
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var n = window.ladi(t.id, t);
                            e.isEmpty(n) || n.set_value_2nd(a.source);
                          }
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.link)
                    ) {
                      var r = null;
                      t.addEventListener("click", function (i) {
                        s(t, i) &&
                          c(a) &&
                          "true" == t.getAttribute("data-action") &&
                          ((r = a.action),
                          e.isEmpty(a.action_mapping) || (r = a.action_mapping),
                          e.isEmpty(r) ||
                            ((r = e.getLinkUTMRedirect(r, null)),
                            (r = e.convertDataReplaceStr(r, !0)),
                            window.ladi(r).open_url(a.target, a.nofollow)));
                      });
                      var l = function () {
                        if (
                          (e.isNull(o) &&
                            (o = e.generateVariantProduct(
                              n,
                              !1,
                              null,
                              null,
                              null,
                              null,
                              !0,
                              !0,
                              l
                            )),
                          e.isObject(o) &&
                            e.isObject(o.store_info) &&
                            e.isObject(o.product))
                        ) {
                          r = a.action;
                          var i = a.link_mapping;
                          e.isEmpty(i) && (i = a.link_mapping_custom),
                            e.isEmpty(i) ||
                              ((a.action_mapping = o.product[i]),
                              e.isEmpty(a.action_mapping) ||
                                (r = a.action_mapping)),
                            e.isEmpty(r)
                              ? (t.removeAttribute("data-replace-href"),
                                t.removeAttribute("href"))
                              : ((r = e.getLinkUTMRedirect(r, null)),
                                t.setAttribute("data-replace-href", r),
                                (t.href = e.convertDataReplaceStr(r, !0)));
                        }
                      };
                      l();
                    }
                    if (
                      (a.type == e.const.DATA_ACTION_TYPE.email &&
                        t.addEventListener("click", function (i) {
                          s(t, i) &&
                            c(a) &&
                            ("true" != t.getAttribute("data-action") ||
                              e.isEmpty(a.action) ||
                              window.ladi("mailto:" + a.action).open_url());
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.phone &&
                        t.addEventListener("click", function (i) {
                          s(t, i) &&
                            c(a) &&
                            ("true" != t.getAttribute("data-action") ||
                              e.isEmpty(a.action) ||
                              window.ladi("tel:" + a.action).open_url());
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.collapse &&
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var n = document.getElementById(a.action);
                            e.isEmpty(n) || window.ladi(a.action).collapse();
                          }
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.section &&
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var n = 0,
                              o = document.getElementById(a.action);
                            if (!e.isEmpty(o)) {
                              if (g)
                                return void window.ladi(o.id).scroll(!1, !0);
                              var r = e.findAncestor(t, "prime-popup");
                              if (!e.isEmpty(r)) {
                                var l = e.findAncestor(r, "prime-element");
                                l.hasAttribute("data-popup-backdrop") &&
                                  (window.ladi(l.id).hide(), (n = 100));
                              }
                              e.runTimeout(function () {
                                window.ladi(o.id).scroll();
                              }, n);
                            }
                          }
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.popup &&
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var o = document.getElementById(a.action);
                            e.isEmpty(o) ||
                              N(t, o, n, function () {
                                window.ladi(a.action).show();
                              });
                          }
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.dropbox &&
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var n = document.getElementById(a.action);
                            e.isEmpty(n) ||
                              window
                                .ladi(a.action)
                                .showDropbox(t, a.dropbox, !1);
                          }
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.hidden_show &&
                        t.addEventListener("click", function (i) {
                          s(t, i) &&
                            c(a) &&
                            (e.isArray(a.hidden_ids) &&
                              a.hidden_ids.forEach(function (t) {
                                window.ladi(t).hide();
                              }),
                            e.isArray(a.show_ids) &&
                              a.show_ids.forEach(function (t) {
                                window.ladi(t).show();
                              }));
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.change_index &&
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var n = window.ladi(a.action);
                            e.isEmpty(n) ||
                              (e.isFunction(n[a.change_index_type])
                                ? n[a.change_index_type]()
                                : n.index(a.change_index_number || 1));
                          }
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.set_style)
                    ) {
                      var d = window.ladi(a.action);
                      e.isEmpty(d) || d.set_style(t, a, !0),
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var n = window.ladi(a.action);
                            e.isEmpty(n) || n.set_style(t, a);
                          }
                        });
                    }
                    a.type == e.const.DATA_ACTION_TYPE.set_value &&
                      t.addEventListener("click", function (i) {
                        if (s(t, i) && c(a)) {
                          var n = window.ladi(a.action);
                          e.isEmpty(n) ||
                            (a.is_clipboard
                              ? e.getTextClipboard(a.str, function (e, i) {
                                  n.value(e ? i : a.str), k(t, e, !0);
                                })
                              : n.value(a.str));
                        }
                      }),
                      a.type == e.const.DATA_ACTION_TYPE.copy_clipboard &&
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var n = null,
                              o = window.ladi(a.action);
                            e.isEmpty(o) ||
                              (n = o.value(null, null, { only_text: !0 })),
                              (n = e.isEmpty(n) ? a.str : n),
                              e.isEmpty(n) || e.copyTextClipboard(n, y);
                          }
                        });
                    var p = null;
                    if (
                      (a.type == e.const.DATA_ACTION_TYPE.lightbox
                        ? (p = a.lightbox_type)
                        : "lightbox_image" == a.type
                        ? (p = e.const.DATA_ACTION_LIGHTBOX_TYPE.image)
                        : "lightbox_video" == a.type
                        ? (p = e.const.DATA_ACTION_LIGHTBOX_TYPE.video)
                        : "lightbox_iframe" == a.type &&
                          (p = e.const.DATA_ACTION_LIGHTBOX_TYPE.iframe),
                      p == e.const.DATA_ACTION_LIGHTBOX_TYPE.image &&
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var n = a.image_url;
                            e.isEmpty(n) &&
                              (n = a["image_url_" + e.runtime.device]),
                              lightbox_image(n);
                          }
                        }),
                      p == e.const.DATA_ACTION_LIGHTBOX_TYPE.video)
                    ) {
                      var u =
                        document.querySelectorAll("#" + i + ".preload").length >
                        0;
                      u && lightbox_video(a.video_url, a.video_type, u),
                        t.addEventListener("click", function (e) {
                          s(t, e) &&
                            c(a) &&
                            lightbox_video(a.video_url, a.video_type, !1);
                        });
                    }
                    p == e.const.DATA_ACTION_LIGHTBOX_TYPE.iframe &&
                      t.addEventListener("click", function (e) {
                        s(t, e) && c(a) && lightbox_iframe(a.iframe_url);
                      }),
                      a.type == e.const.DATA_ACTION_TYPE.popup_cart &&
                        t.addEventListener("click", function (i) {
                          if (s(t, i) && c(a)) {
                            var n = document.getElementById("POPUP_CART");
                            e.isEmpty(n) || window.ladi(n.id).show();
                          }
                        }),
                      a.type == e.const.DATA_ACTION_TYPE.popup_checkout &&
                        t.addEventListener("click", function (i) {
                          s(t, i) &&
                            c(a) &&
                            (e.runtime.shopping_third_party
                              ? e.getThirdPartyCheckoutUrl(!0)
                              : window.ladi("POPUP_CHECKOUT").show());
                        });
                  }
                }),
                  e.runEventTracking(
                    i,
                    { count_data_event: f, is_click: !0, is_form: !1 },
                    null,
                    t,
                    s
                  );
              } else
                t.addEventListener("click", function (i) {
                  if (s(t, i)) {
                    var a = document.getElementById(p);
                    e.isEmpty(a) ||
                      a.setAttribute("data-button-submit-other", t.id),
                      window.ladi(p).submit();
                  }
                });
            else
              t.addEventListener("click", function (a) {
                if (s(t, a)) {
                  a.preventDefault();
                  var n = E;
                  (n = e.getLinkUTMRedirect(n, null)),
                    (n = e.convertDataReplaceStr(n, !0)),
                    window.ladi(n).open_url(),
                    e.runEventTracking(i, { is_form: !1 });
                }
              });
          }
        },
        D = function (t, i, a, n) {
          if (((t = t || document.getElementById(i)), !e.isEmpty(t))) {
            if (!e.isArray(n)) {
              var o = e.copy(n);
              (n = []),
                e.isObject(o) &&
                  ((o.action_type = e.const.ACTION_TYPE.hover), n.push(o));
            }
            n.forEach(function (i) {
              if (
                i.action_type == e.const.ACTION_TYPE.hover &&
                (i.type == e.const.DATA_ACTION_TYPE.dropbox &&
                  (t.addEventListener("mouseenter", function (e) {
                    window.ladi(i.action).showDropbox(t, i.dropbox, !0);
                  }),
                  t.addEventListener("mouseleave", function (t) {
                    window.ladi(i.action).hide();
                  })),
                i.type == e.const.DATA_ACTION_TYPE.hidden_show &&
                  (t.addEventListener("mouseenter", function (t) {
                    e.isArray(i.hidden_ids) &&
                      i.hidden_ids.forEach(function (t) {
                        window.ladi(t).hide();
                      }),
                      e.isArray(i.show_ids) &&
                        i.show_ids.forEach(function (t) {
                          window.ladi(t).show();
                        });
                  }),
                  t.addEventListener("mouseleave", function (t) {
                    e.isArray(i.hidden_ids) &&
                      i.hidden_ids.forEach(function (t) {
                        window.ladi(t).show();
                      }),
                      e.isArray(i.show_ids) &&
                        i.show_ids.forEach(function (t) {
                          window.ladi(t).hide();
                        });
                  })),
                i.type == e.const.DATA_ACTION_TYPE.change_index &&
                  t.addEventListener("mouseenter", function (t) {
                    var a = window.ladi(i.action);
                    e.isFunction(a[i.change_index_type])
                      ? a[i.change_index_type]()
                      : a.index(i.change_index_number || 1);
                  }),
                i.type == e.const.DATA_ACTION_TYPE.set_style)
              ) {
                var a = window.ladi(i.action);
                e.isEmpty(a) || a.set_style(t, i, !0),
                  t.addEventListener("mouseenter", function (e) {
                    window.ladi(i.action).set_style(t, i);
                  }),
                  t.addEventListener("mouseleave", function (e) {
                    window.ladi(i.action).remove_style(t, i);
                  });
              }
            });
          }
        },
        R = function (t) {
          var i = document.getElementById(t);
          if (
            !e.isEmpty(i) &&
            t != e.runtime.builder_section_popup_id &&
            t != e.runtime.builder_section_background_id
          ) {
            var a = i.classList.contains("prime-section") ? "section" : null;
            if (e.runtime.is_popupx && "section" == a) {
              var n = document.createElement("div");
              (n.className = "prime-section-close"),
                n.addEventListener("click", function (e) {
                  e.stopPropagation(), window.ladi(t).hide();
                }),
                i.appendChild(n);
            }
          }
        },
        F = function (t, i, a, n, o, r, s, l) {
          "countdown" != a ||
            e.isEmpty(n) ||
            ((i = i || document.getElementById(t)),
            e.isEmpty(i) ||
              (i.setAttribute("data-type", n),
              n != e.const.COUNTDOWN_TYPE.countdown ||
                e.isEmpty(o) ||
                i.setAttribute("data-minute", o),
              n != e.const.COUNTDOWN_TYPE.endtime ||
                e.isEmpty(l) ||
                i.setAttribute("data-endtime", l),
              n != e.const.COUNTDOWN_TYPE.daily ||
                e.isEmpty(r) ||
                e.isEmpty(s) ||
                (i.setAttribute("data-daily-start", r),
                i.setAttribute("data-daily-end", s))));
        },
        q = function (t, i, a, n) {
          "countdown_item" != a ||
            e.isEmpty(n) ||
            ((i = i || document.getElementById(t)),
            e.isEmpty(i) || i.setAttribute("data-item-type", n));
        },
        B = function (t, i, a, n) {
          if (
            e.isEmpty(e.runtime.current_element_mouse_down_gallery_view) &&
            e.isEmpty(e.runtime.current_element_mouse_down_gallery_control)
          ) {
            var o = t.getAttribute("data-runtime-id");
            if (
              e.isEmpty(e.runtime.timeout_gallery[o]) &&
              (!e.runtime.tmp.gallery_playing_video || !i)
            ) {
              var r = t.getElementsByClassName("prime-gallery-view-item"),
                s = t.getElementsByClassName("prime-gallery-control-item");
              if (0 != r.length && 0 != r.length) {
                var l = t.getAttribute("data-is-next") || "true";
                l = "true" == l.toLowerCase();
                var c = parseFloatLadiPage(t.getAttribute("data-current")) || 0,
                  d = parseFloatLadiPage(t.getAttribute("data-max-item")) || 0;
                i
                  ? l
                    ? c >= d - 1
                      ? ((c = d - 2), (l = !1))
                      : c++
                    : c <= 0
                    ? ((c = 1), (l = !0))
                    : c--
                  : l
                  ? c++
                  : c--,
                  c < 0 && (c = 0),
                  c >= d - 1 && (c = d - 1),
                  e.isEmpty(a) && (a = l ? "next" : "prev"),
                  e.isEmpty(n) && (n = l ? "left" : "right"),
                  e.runtime.tmp.gallery_playing_video &&
                    !r[c].classList.contains("selected") &&
                    e.pauseAllVideo(),
                  r[c].classList.add(a);
                var p = t.querySelectorAll(
                  ".prime-gallery-view-item.selected"
                )[0];
                e.isEmpty(p) || p.classList.add(n);
                var u =
                  1e3 *
                  (parseFloatLadiPage(
                    getComputedStyle(r[c]).transitionDuration
                  ) || 0);
                e.runtime.timeout_gallery[o] = e.runTimeout(function () {
                  r[c].classList.add(n),
                    (e.runtime.timeout_gallery[o] = e.runTimeout(function () {
                      for (var t = 0; t < r.length; t++)
                        t == c
                          ? r[t].classList.add("selected")
                          : r[t].classList.remove("selected"),
                          r[t].style.removeProperty("left"),
                          r[t].classList.remove(a),
                          r[t].classList.remove(n);
                      delete e.runtime.timeout_gallery[o];
                    }, u - 5));
                }, 5);
                for (var m = 0; m < s.length; m++)
                  (parseFloatLadiPage(s[m].getAttribute("data-index")) || 0) ==
                  c
                    ? s[m].classList.add("selected")
                    : s[m].classList.remove("selected");
                var _ = e.getElementBoundingClientRect(t),
                  y = e.getElementBoundingClientRect(
                    t.getElementsByClassName("prime-gallery-control-item")[c]
                  );
                if (
                  (t
                    .getElementsByClassName(
                      "prime-gallery-control-arrow-left"
                    )[0]
                    .classList.remove("opacity-0"),
                  t
                    .getElementsByClassName(
                      "prime-gallery-control-arrow-right"
                    )[0]
                    .classList.remove("opacity-0"),
                  t
                    .getElementsByClassName("prime-gallery")[0]
                    .classList.contains("prime-gallery-top") ||
                    t
                      .getElementsByClassName("prime-gallery")[0]
                      .classList.contains("prime-gallery-bottom"))
                ) {
                  var g =
                      parseFloatLadiPage(
                        getComputedStyle(
                          t.getElementsByClassName("prime-gallery-control")[0]
                        ).width
                      ) || 0,
                    f =
                      parseFloatLadiPage(
                        getComputedStyle(
                          t.getElementsByClassName("prime-gallery-control-item")[
                            c
                          ]
                        ).width
                      ) || 0,
                    v = y.x - _.x - (g - f) / 2;
                  v =
                    -(v -=
                      parseFloatLadiPage(
                        t
                          .getElementsByClassName("prime-gallery-control-box")[0]
                          .style.getPropertyValue("left")
                      ) || 0) > 0
                      ? 0
                      : -v;
                  var h =
                    parseFloatLadiPage(
                      getComputedStyle(
                        t.getElementsByClassName("prime-gallery-control-box")[0]
                      ).width
                    ) || 0;
                  v <
                    (h =
                      (h = -(h -=
                        parseFloatLadiPage(
                          getComputedStyle(
                            t.getElementsByClassName("prime-gallery-control")[0]
                          ).width
                        ) || 0)) > 0
                        ? 0
                        : h) && (v = h),
                    t
                      .getElementsByClassName("prime-gallery-control-box")[0]
                      .style.setProperty("left", v + "px"),
                    v >= 0 &&
                      t
                        .getElementsByClassName(
                          "prime-gallery-control-arrow-left"
                        )[0]
                        .classList.add("opacity-0"),
                    v <= h &&
                      t
                        .getElementsByClassName(
                          "prime-gallery-control-arrow-right"
                        )[0]
                        .classList.add("opacity-0");
                } else {
                  var E =
                      parseFloatLadiPage(
                        getComputedStyle(
                          t.getElementsByClassName("prime-gallery-control")[0]
                        ).height
                      ) || 0,
                    P =
                      parseFloatLadiPage(
                        getComputedStyle(
                          t.getElementsByClassName("prime-gallery-control-item")[
                            c
                          ]
                        ).height
                      ) || 0,
                    b = y.y - _.y - (E - P) / 2;
                  b =
                    -(b -=
                      parseFloatLadiPage(
                        t
                          .getElementsByClassName("prime-gallery-control-box")[0]
                          .style.getPropertyValue("top")
                      ) || 0) > 0
                      ? 0
                      : -b;
                  var L =
                    parseFloatLadiPage(
                      getComputedStyle(
                        t.getElementsByClassName("prime-gallery-control-box")[0]
                      ).height
                    ) || 0;
                  b <
                    (L =
                      (L = -(L -=
                        parseFloatLadiPage(
                          getComputedStyle(
                            t.getElementsByClassName("prime-gallery-control")[0]
                          ).height
                        ) || 0)) > 0
                        ? 0
                        : L) && (b = L),
                    t
                      .getElementsByClassName("prime-gallery-control-box")[0]
                      .style.setProperty("top", b + "px"),
                    b >= 0 &&
                      t
                        .getElementsByClassName(
                          "prime-gallery-control-arrow-left"
                        )[0]
                        .classList.add("opacity-0"),
                    b <= L &&
                      t
                        .getElementsByClassName(
                          "prime-gallery-control-arrow-right"
                        )[0]
                        .classList.add("opacity-0");
                }
                t.setAttribute("data-is-next", l),
                  t.setAttribute("data-current", c),
                  t
                    .getElementsByClassName("prime-gallery-view-arrow-left")[0]
                    .classList.remove("opacity-0"),
                  t
                    .getElementsByClassName("prime-gallery-view-arrow-right")[0]
                    .classList.remove("opacity-0"),
                  c <= 0 &&
                    t
                      .getElementsByClassName("prime-gallery-view-arrow-left")[0]
                      .classList.add("opacity-0"),
                  c >= d - 1 &&
                    t
                      .getElementsByClassName(
                        "prime-gallery-view-arrow-right"
                      )[0]
                      .classList.add("opacity-0"),
                  (t
                    .getElementsByClassName("prime-gallery")[0]
                    .classList.contains("prime-gallery-left") ||
                    t
                      .getElementsByClassName("prime-gallery")[0]
                      .classList.contains("prime-gallery-right")) &&
                    e.reloadLazyload(!1),
                  !i &&
                    t.hasAttribute("data-loaded") &&
                    t.setAttribute("data-stop", !0);
              }
            }
          }
        },
        M = function (t, i, a) {
          var n = i.getAttribute("data-video-type"),
            o = i.getAttribute("data-video-url"),
            r = i.getAttribute("data-index"),
            s = t.getAttribute("data-runtime-id") + "_" + r + "_player",
            l = document.getElementById(s);
          a || (e.pauseAllVideo(), (e.runtime.tmp.gallery_playing_video = !0)),
            e.isEmpty(l)
              ? (n == e.const.VIDEO_TYPE.youtube &&
                  ((l = document.createElement("iframe")),
                  i.parentElement.insertBefore(l, i.nextSibling),
                  (l.outerHTML =
                    '<iframe id="' +
                    s +
                    '" class="iframe-video-preload" data-video-type="' +
                    n +
                    '" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0;" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'),
                  e.runEventPlayVideo(s, n, o, !1, !1, !0, a, !1, !0)),
                n == e.const.VIDEO_TYPE.direct &&
                  ((l = document.createElement("video")),
                  i.parentElement.insertBefore(l, i.nextSibling),
                  (l.outerHTML =
                    '<video id="' +
                    s +
                    '" class="iframe-video-preload" data-video-type="' +
                    n +
                    '" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; object-fit: cover;"></video>'),
                  e.runEventPlayVideo(s, n, o, !1, !1, !0, a, !1, !0)))
              : e.runEventReplayVideo(s, n, !0);
        },
        Y = function (t, i, a, n) {
          if (
            "gallery" == n &&
            (a || (i = document.getElementById(t)), !e.isEmpty(i))
          ) {
            var o = i.getElementsByClassName(
              "prime-gallery-control-item"
            ).length;
            i.setAttribute("data-max-item", o),
              i.setAttribute("data-runtime-id", e.randomString(12));
            var r = function (t) {
                t.stopPropagation(), M(i, t.target, !1);
              },
              s = i.classList.contains("preload");
            if (o > 0) {
              for (var l = 0; l < o; l++) {
                var c = i.getElementsByClassName("prime-gallery-view-item")[l];
                e.isEmpty(c) ||
                  (s && M(i, c, s),
                  c.classList.contains("play-video") &&
                    c.addEventListener("click", r));
              }
              i.setAttribute("data-current", 0),
                i.setAttribute("data-is-next", !0);
            }
            for (
              var d = i.getElementsByClassName("prime-gallery-view-arrow"),
                p = 0;
              p < d.length;
              p++
            )
              o <= 1
                ? d[p].classList.add("prime-hidden")
                : d[p].classList.remove("prime-hidden");
          }
        },
        V = function (t, i) {
          t.stopPropagation();
          var a = e.runtime.eventData[i.id],
            n = a[e.runtime.device + ".option.gallery_control.autoplay"],
            o = a[e.runtime.device + ".option.gallery_control.autoplay_time"],
            r = 0;
          n && !e.isEmpty(o) && (r = o);
          var s = parseFloatLadiPage(t.target.getAttribute("data-index")) || 0,
            l = null,
            c = null;
          (parseFloatLadiPage(i.getAttribute("data-current")) || 0) > s
            ? ((l = "prev"), (c = "right"))
            : ((l = "next"), (c = "left"));
          var d = i.getAttribute("data-is-next") || "true";
          (d = "true" == d.toLowerCase()) ? s-- : s++,
            i.setAttribute("data-current", s),
            i.setAttribute("data-next-time", Date.now() + 1e3 * r),
            B(i, !1, l, c);
        },
        H = function () {
          y.forEach(function (t) {
            var i = e.runtime.eventData[t];
            if ("gallery" == i.type)
              for (
                var a = document.querySelectorAll("#" + t), n = 0;
                n < a.length;
                n++
              ) {
                var o = a[n];
                if (
                  "true" == o.getAttribute("data-scrolled") &&
                  "true" != o.getAttribute("data-stop")
                ) {
                  var r =
                      i[e.runtime.device + ".option.gallery_control.autoplay"],
                    s =
                      i[
                        e.runtime.device +
                          ".option.gallery_control.autoplay_time"
                      ],
                    l = 0;
                  if ((r && !e.isEmpty(s) && (l = s), l > 0)) {
                    var c = o.getAttribute("data-next-time"),
                      d = Date.now();
                    e.isEmpty(c) &&
                      ((c = d + 1e3 * (l - 1)),
                      o.setAttribute("data-next-time", c)),
                      d >= c &&
                        (B(o, !0),
                        o.setAttribute("data-next-time", d + 1e3 * l));
                  }
                }
              }
          });
        },
        j = function (t, i) {
          var a = e.runtime.eventData[t];
          if ("gallery" == a.type) {
            var n = i.getAttribute("data-runtime-id");
            i.hasAttribute("data-scrolled") ||
              (i.setAttribute("data-scrolled", !1),
              (e.runtime.list_scroll_func[n] = function () {
                i.setAttribute("data-scrolled", !0);
              }));
            var o = a[e.runtime.device + ".option.gallery_control.autoplay"],
              r = a[e.runtime.device + ".option.gallery_control.autoplay_time"],
              s = 0;
            o && !e.isEmpty(r) && (s = r);
            var l = function (t) {
                V(t, i);
              },
              c = function (t) {
                if (
                  (t.stopPropagation(),
                  !(t = e.getEventCursorData(t)).target.classList.contains(
                    "prime-gallery-view-arrow"
                  ))
                ) {
                  var a = i.getAttribute("data-runtime-id");
                  e.isEmpty(e.runtime.timeout_gallery[a]) &&
                    ((e.runtime.current_element_mouse_down_gallery_view = a),
                    (e.runtime.current_element_mouse_down_gallery_view_position_x =
                      t.pageX));
                }
              },
              d = function (t) {
                t.stopPropagation(),
                  (t = e.getEventCursorData(t)),
                  (i
                    .getElementsByClassName("prime-gallery")[0]
                    .classList.contains("prime-gallery-top") ||
                    i
                      .getElementsByClassName("prime-gallery")[0]
                      .classList.contains("prime-gallery-bottom")) &&
                    (t.target.classList.contains(
                      "prime-gallery-control-arrow"
                    ) ||
                      ((e.runtime.current_element_mouse_down_gallery_control =
                        n),
                      (e.runtime.current_element_mouse_down_gallery_control_time =
                        Date.now()),
                      (e.runtime.current_element_mouse_down_gallery_control_position_x =
                        t.pageX),
                      i
                        .getElementsByClassName("prime-gallery-control-box")[0]
                        .style.setProperty("transition-duration", "0ms"),
                      i
                        .getElementsByClassName("prime-gallery-control-box")[0]
                        .setAttribute(
                          "data-left",
                          getComputedStyle(
                            i.getElementsByClassName(
                              "prime-gallery-control-box"
                            )[0]
                          ).left
                        )));
              };
            i
              .getElementsByClassName("prime-gallery-view-arrow-left")[0]
              .addEventListener("click", function (t) {
                t.stopPropagation(),
                  i.setAttribute("data-is-next", !1),
                  i.setAttribute("data-next-time", Date.now() + 1e3 * s),
                  B(i, !1);
              }),
              i.getElementsByClassName("prime-gallery-view-item").length > 1 &&
                i
                  .getElementsByClassName("prime-gallery-view-arrow-right")[0]
                  .classList.remove("opacity-0"),
              i
                .getElementsByClassName("prime-gallery-view-arrow-right")[0]
                .addEventListener("click", function (t) {
                  t.stopPropagation(),
                    i.setAttribute("data-is-next", !0),
                    i.setAttribute("data-next-time", Date.now() + 1e3 * s),
                    B(i, !1);
                }),
              i
                .getElementsByClassName("prime-gallery-control-arrow-left")[0]
                .addEventListener("click", function (t) {
                  t.stopPropagation();
                  var a = i.getElementsByClassName(
                    "prime-gallery-control-item"
                  )[0];
                  if (!e.isEmpty(a)) {
                    var n = getComputedStyle(a);
                    if (
                      (i
                        .getElementsByClassName(
                          "prime-gallery-control-arrow-left"
                        )[0]
                        .classList.remove("opacity-0"),
                      i
                        .getElementsByClassName(
                          "prime-gallery-control-arrow-right"
                        )[0]
                        .classList.remove("opacity-0"),
                      i
                        .getElementsByClassName("prime-gallery")[0]
                        .classList.contains("prime-gallery-top") ||
                        i
                          .getElementsByClassName("prime-gallery")[0]
                          .classList.contains("prime-gallery-bottom"))
                    ) {
                      var o =
                        (parseFloatLadiPage(n.width) || 0) +
                        (parseFloatLadiPage(n.marginRight) || 0);
                      o +=
                        parseFloatLadiPage(
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-box"
                            )[0]
                            .style.getPropertyValue("left")
                        ) || 0;
                      var r =
                        parseFloatLadiPage(
                          getComputedStyle(
                            i.getElementsByClassName(
                              "prime-gallery-control-box"
                            )[0]
                          ).width
                        ) || 0;
                      (r =
                        (r = -(r -=
                          parseFloatLadiPage(
                            getComputedStyle(
                              i.getElementsByClassName(
                                "prime-gallery-control"
                              )[0]
                            ).width
                          ) || 0)) > 0
                          ? 0
                          : r),
                        o > 0 && (o = 0),
                        i
                          .getElementsByClassName("prime-gallery-control-box")[0]
                          .style.setProperty("left", o + "px"),
                        o >= 0 &&
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-arrow-left"
                            )[0]
                            .classList.add("opacity-0"),
                        o <= r &&
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-arrow-right"
                            )[0]
                            .classList.add("opacity-0");
                    } else {
                      var l =
                        (parseFloatLadiPage(n.height) || 0) +
                        (parseFloatLadiPage(n.marginBottom) || 0);
                      l +=
                        parseFloatLadiPage(
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-box"
                            )[0]
                            .style.getPropertyValue("top")
                        ) || 0;
                      var c =
                        parseFloatLadiPage(
                          getComputedStyle(
                            i.getElementsByClassName(
                              "prime-gallery-control-box"
                            )[0]
                          ).height
                        ) || 0;
                      (c =
                        (c = -(c -=
                          parseFloatLadiPage(
                            getComputedStyle(
                              i.getElementsByClassName(
                                "prime-gallery-control"
                              )[0]
                            ).height
                          ) || 0)) > 0
                          ? 0
                          : c),
                        l > 0 && (l = 0),
                        i
                          .getElementsByClassName("prime-gallery-control-box")[0]
                          .style.setProperty("top", l + "px"),
                        l >= 0 &&
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-arrow-left"
                            )[0]
                            .classList.add("opacity-0"),
                        l <= c &&
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-arrow-right"
                            )[0]
                            .classList.add("opacity-0");
                    }
                    i.setAttribute("data-next-time", Date.now() + 1e3 * s);
                  }
                }),
              (parseFloatLadiPage(
                getComputedStyle(
                  i.getElementsByClassName("prime-gallery-control-box")[0]
                ).width
              ) || 0) >
                (parseFloatLadiPage(
                  getComputedStyle(
                    i.getElementsByClassName("prime-gallery-control")[0]
                  ).width
                ) || 0) &&
                i
                  .getElementsByClassName("prime-gallery-control-arrow-right")[0]
                  .classList.remove("opacity-0"),
              i
                .getElementsByClassName("prime-gallery-control-arrow-right")[0]
                .addEventListener("click", function (t) {
                  t.stopPropagation();
                  var a = i.getElementsByClassName(
                    "prime-gallery-control-item"
                  )[0];
                  if (!e.isEmpty(a)) {
                    var n = getComputedStyle(a);
                    if (
                      (i
                        .getElementsByClassName(
                          "prime-gallery-control-arrow-left"
                        )[0]
                        .classList.remove("opacity-0"),
                      i
                        .getElementsByClassName(
                          "prime-gallery-control-arrow-right"
                        )[0]
                        .classList.remove("opacity-0"),
                      i
                        .getElementsByClassName("prime-gallery")[0]
                        .classList.contains("prime-gallery-top") ||
                        i
                          .getElementsByClassName("prime-gallery")[0]
                          .classList.contains("prime-gallery-bottom"))
                    ) {
                      var o =
                        (parseFloatLadiPage(n.width) || 0) +
                        (parseFloatLadiPage(n.marginRight) || 0);
                      o =
                        -o +
                        (parseFloatLadiPage(
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-box"
                            )[0]
                            .style.getPropertyValue("left")
                        ) || 0);
                      var r =
                        parseFloatLadiPage(
                          getComputedStyle(
                            i.getElementsByClassName(
                              "prime-gallery-control-box"
                            )[0]
                          ).width
                        ) || 0;
                      o <
                        (r =
                          (r = -(r -=
                            parseFloatLadiPage(
                              getComputedStyle(
                                i.getElementsByClassName(
                                  "prime-gallery-control"
                                )[0]
                              ).width
                            ) || 0)) > 0
                            ? 0
                            : r) && (o = r),
                        i
                          .getElementsByClassName("prime-gallery-control-box")[0]
                          .style.setProperty("left", o + "px"),
                        o >= 0 &&
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-arrow-left"
                            )[0]
                            .classList.add("opacity-0"),
                        o <= r &&
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-arrow-right"
                            )[0]
                            .classList.add("opacity-0");
                    } else {
                      var l =
                        (parseFloatLadiPage(n.height) || 0) +
                        (parseFloatLadiPage(n.marginBottom) || 0);
                      l =
                        -l +
                        (parseFloatLadiPage(
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-box"
                            )[0]
                            .style.getPropertyValue("top")
                        ) || 0);
                      var c =
                        parseFloatLadiPage(
                          getComputedStyle(
                            i.getElementsByClassName(
                              "prime-gallery-control-box"
                            )[0]
                          ).height
                        ) || 0;
                      l <
                        (c =
                          (c = -(c -=
                            parseFloatLadiPage(
                              getComputedStyle(
                                i.getElementsByClassName(
                                  "prime-gallery-control"
                                )[0]
                              ).height
                            ) || 0)) > 0
                            ? 0
                            : c) && (l = c),
                        i
                          .getElementsByClassName("prime-gallery-control-box")[0]
                          .style.setProperty("top", l + "px"),
                        l >= 0 &&
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-arrow-left"
                            )[0]
                            .classList.add("opacity-0"),
                        l <= c &&
                          i
                            .getElementsByClassName(
                              "prime-gallery-control-arrow-right"
                            )[0]
                            .classList.add("opacity-0");
                    }
                    i.setAttribute("data-next-time", Date.now() + 1e3 * s);
                  }
                }),
              i
                .getElementsByClassName("prime-gallery-view")[0]
                .addEventListener("mousedown", c),
              i
                .getElementsByClassName("prime-gallery-view")[0]
                .addEventListener(
                  "touchstart",
                  c,
                  e.runtime.scrollEventPassive
                ),
              i
                .getElementsByClassName("prime-gallery-control")[0]
                .addEventListener("mousedown", d),
              i
                .getElementsByClassName("prime-gallery-control")[0]
                .addEventListener(
                  "touchstart",
                  d,
                  e.runtime.scrollEventPassive
                );
            for (
              var p = i.getElementsByClassName("prime-gallery-control-item"),
                u = 0;
              u < p.length;
              u++
            )
              p[u].addEventListener("click", l);
            e.isEmpty(a["option.product_mapping_name"]) &&
              !i.hasAttribute("data-loaded") &&
              e.runTimeout(function () {
                i.setAttribute("data-loaded", !0);
              }, 300);
          }
        },
        G = function (t, i) {
          if (
            (e.isEmpty(e.runtime.timenext_carousel[t]) ||
              !(e.runtime.timenext_carousel[t] > Date.now())) &&
            e.isEmpty(e.runtime.current_element_mouse_down_carousel)
          ) {
            var a = document.getElementById(t);
            if (!e.isEmpty(a)) {
              var n = a.getAttribute("data-is-next") || "true";
              n = "true" == n.toLowerCase();
              var o = parseFloatLadiPage(a.getAttribute("data-current")) || 0,
                r =
                  parseFloatLadiPage(
                    e.runtime.eventData[t][
                      e.runtime.device + ".option.carousel_crop.width"
                    ]
                  ) || 0,
                s =
                  parseFloatLadiPage(
                    e.runtime.eventData[t][
                      e.runtime.device + ".option.carousel_crop.width_item"
                    ]
                  ) || 0;
              s > a.clientWidth && (s = a.clientWidth);
              var l = Math.ceil(r / s);
              i
                ? n
                  ? o >= l - 1
                    ? ((o = l - 2), (n = !1))
                    : o++
                  : o <= 0
                  ? ((o = 1), (n = !0))
                  : o--
                : n
                ? o++
                : o--,
                o < 0 && (o = 0),
                o >= l - 1 && (o = l - 1);
              var c =
                1e3 *
                (parseFloatLadiPage(
                  getComputedStyle(
                    a.getElementsByClassName("prime-carousel-content")[0]
                  ).transitionDuration
                ) || 0);
              e.runtime.timenext_carousel[t] = Date.now() + c;
              var d = e.getElementBoundingClientRect(a),
                p = d.x + o * s - d.x - (a.clientWidth - s) / 2;
              p = -p > 0 ? 0 : -p;
              var u = -(r - a.clientWidth);
              p < u && (p = u),
                a
                  .getElementsByClassName("prime-carousel-content")[0]
                  .style.setProperty("left", p + "px"),
                a.setAttribute("data-is-next", n),
                a.setAttribute("data-current", o),
                i || a.setAttribute("data-stop", !0);
              var m = a.getElementsByClassName("prime-carousel-arrow-left")[0],
                _ = a.getElementsByClassName("prime-carousel-arrow-right")[0];
              e.isEmpty(m) || m.classList.remove("opacity-0"),
                e.isEmpty(_) || _.classList.remove("opacity-0"),
                p >= 0 && m.classList.add("opacity-0"),
                p <= u && _.classList.add("opacity-0");
            }
          }
        },
        U = function (t, i) {
          var a = function (t) {
              t.addEventListener("click", function (i) {
                i.stopPropagation(),
                  (t.classList.contains("accordion-menu") &&
                    e.runtime.time_click_dom[t.id] > Date.now()) ||
                    ((e.runtime.time_click_dom[t.id] = Date.now() + 250),
                    t.classList.contains("selected")
                      ? t.classList.remove("selected")
                      : t.classList.add("selected"));
              });
            },
            n = [];
          i
            ? t.classList.contains("element-click-selected") && n.push(t)
            : (n = document.getElementsByClassName("element-click-selected"));
          for (var o = 0; o < n.length; o++) a(n[o]);
        },
        W = function (t) {
          if (
            e.runtime.isClient &&
            !e.runtime.isDesktop &&
            !e.isEmpty(e.runtime.bodyFontSize)
          ) {
            var i =
              (parseFloatLadiPage(getComputedStyle(document.body).fontSize) ||
                0) / e.runtime.bodyFontSize;
            if (1 != i)
              for (
                var a = document.querySelectorAll(
                    ".prime-paragraph, .prime-list-paragraph, .prime-headline, .prime-countdown, .prime-form, .prime-table, .prime-spin-lucky"
                  ),
                  n = 0;
                n < a.length;
                n++
              ) {
                var o =
                  (parseFloatLadiPage(getComputedStyle(a[n]).fontSize) || 0) /
                  (i * i);
                a[n].style.setProperty("font-size", o + "px");
              }
            else
              t > Date.now() &&
                e.runTimeout(function () {
                  W(t);
                }, 100);
          }
        },
        X = function (t) {
          var i = null;
          return (
            e.isEmpty(t) ||
              (i = t.classList.contains("no-value")
                ? null
                : t.getAttribute("data-value")),
            e.isEmpty(i) ? "" : i
          );
        },
        z = function (t, i) {
          var a = t.querySelectorAll(".prime-form-label-item");
          i = e.isEmpty(i) ? "" : i;
          for (var n = 0; n < a.length; n++)
            X(a[n]) == i
              ? a[n].classList.add("selected")
              : a[n].classList.remove("selected");
        },
        K = function (t) {
          var e = t.querySelector(".prime-form-label-item.selected");
          return X(e);
        },
        J = function (t, i) {
          var a = t.target;
          if (a.classList.contains("disabled"))
            for (
              var n = e
                  .findAncestor(a, "prime-element")
                  .querySelectorAll(".prime-form-label-item"),
                o = 0;
              o < n.length;
              o++
            )
              n[o].classList.contains("no-value")
                ? n[o].classList.add("selected")
                : n[o].classList.remove("selected"),
                n[o].classList.remove("disabled");
          var r = X(a);
          !t.is_fire_event && a.classList.contains("selected") && (r = "");
          var s = e.findAncestor(a, "prime-form-label-container");
          z(s, r), e.isFunction(i) && i(s);
        },
        $ = function (i, a, n, o) {
          if ("form" == a) {
            var r = e.runtime.eventData[i];
            if (!e.isEmpty(r) && r["option.is_add_to_cart"]) {
              var s = document.getElementById(i);
              if (
                !e.isEmpty(s) &&
                (!n || e.isEmpty(e.findAncestor(s, "prime-collection-item")))
              ) {
                var l = s.querySelector('[data-variant="true"]');
                if (!e.isEmpty(l)) {
                  var c = e.runtime.eventData[l.id];
                  if (!e.isEmpty(c)) {
                    var d = r["option.product_type"],
                      p = r["option.product_id"];
                    if (!e.isEmpty(d) && !e.isEmpty(p)) {
                      var u = e.generateVariantProduct(
                        r,
                        !1,
                        null,
                        null,
                        null,
                        null,
                        !0,
                        !0,
                        function (t) {
                          $(i, a, n, o);
                        }
                      );
                      if (
                        !(
                          e.isEmpty(u) ||
                          e.isEmpty(u.store_info) ||
                          e.isEmpty(u.product)
                        )
                      ) {
                        var m = e.generateVariantProduct(
                            r,
                            !0,
                            c["option.product_variant_type"],
                            c["option.product_variant_title"],
                            c["option.product_variant_price"],
                            c["option.input_tabindex"],
                            t,
                            !0,
                            function (t) {
                              $(i, a, n, o);
                            }
                          ),
                          _ = function (t) {
                            e.updateProductVariantSelectOption(
                              t,
                              r,
                              c,
                              o,
                              function () {
                                if (o) {
                                  var a = e.generateVariantProduct(
                                      r,
                                      !1,
                                      null,
                                      null,
                                      null,
                                      null,
                                      !0,
                                      !0
                                    ),
                                    n = e.getProductVariantId(
                                      t.target,
                                      a.product
                                    );
                                  if (!e.isArray(a.product.variants)) return;
                                  var s = a.product.variants.find(function (t) {
                                    return t.product_variant_id == n;
                                  });
                                  if (e.isEmpty(s)) return;
                                  for (
                                    var l = document.querySelectorAll(
                                        "#POPUP_PRODUCT .prime-element"
                                      ),
                                      c = 0;
                                    c < l.length;
                                    c++
                                  )
                                    l[c].id != i &&
                                      e.runtime.tmp.runLadiSaleProductKey(
                                        l[c].id,
                                        !1,
                                        !0,
                                        s,
                                        a
                                      );
                                } else
                                  e.runtime.tmp.generateLadiSaleProduct(
                                    !1,
                                    !0,
                                    t
                                  );
                              }
                            );
                          },
                          y = function (t) {
                            J(t, function (t) {
                              _({ target: t });
                            });
                          };
                        e.showParentVisibility(l, function () {
                          for (
                            var t = l.clientHeight,
                              i = t,
                              a = l.querySelectorAll(
                                "select.prime-form-control"
                              ),
                              n = {},
                              o = 0;
                            o < a.length;
                            o++
                          )
                            n[
                              a[o].getAttribute("data-store-id") +
                                "_" +
                                a[o].getAttribute("data-product-id") +
                                "_" +
                                a[o].getAttribute("data-product-option-id")
                            ] = a[o].value;
                          var d = l.querySelectorAll(
                            ".prime-form-label-container"
                          );
                          for (o = 0; o < d.length; o++)
                            n[
                              d[o].getAttribute("data-store-id") +
                                "_" +
                                d[o].getAttribute("data-product-id") +
                                "_" +
                                d[o].getAttribute("data-product-option-id")
                            ] = K(d[o]);
                          l.innerHTML = m;
                          for (
                            var p = null,
                              u = null,
                              g = l.querySelectorAll(
                                "select.prime-form-control"
                              ),
                              f = 0;
                            f < g.length;
                            f++
                          )
                            g[f].addEventListener("change", _),
                              (p =
                                n[
                                  g[f].getAttribute("data-store-id") +
                                    "_" +
                                    g[f].getAttribute("data-product-id") +
                                    "_" +
                                    g[f].getAttribute("data-product-option-id")
                                ]),
                              e.isNull(p) &&
                                ((u = g[f].querySelector("option")),
                                e.isEmpty(u) || (p = u.getAttribute("value"))),
                              (g[f].value = p);
                          var v = l.querySelectorAll(
                            ".prime-form-label-container"
                          );
                          for (f = 0; f < v.length; f++) {
                            for (
                              var h = v[f].querySelectorAll(
                                  ".prime-form-label-item"
                                ),
                                E = 0;
                              E < h.length;
                              E++
                            )
                              e.tapEventListener(h[E], y);
                            (p =
                              n[
                                v[f].getAttribute("data-store-id") +
                                  "_" +
                                  v[f].getAttribute("data-product-id") +
                                  "_" +
                                  v[f].getAttribute("data-product-option-id")
                              ]),
                              e.isNull(p) &&
                                ((u = h[1]), e.isEmpty(u) || (p = X(u))),
                              z(v[f], p);
                          }
                          if (
                            (e.updateProductVariantSelectOptionFirst(r, c, l),
                            c["option.product_variant_type"] !=
                              e.const.PRODUCT_VARIANT_TYPE.combined)
                          )
                            l.style.setProperty("height", "auto"),
                              (i = l.clientHeight),
                              l.style.removeProperty("height"),
                              i > 0 &&
                                t != i &&
                                (l.style.setProperty("height", i + "px"),
                                e.updateHeightElement(!0, l, s, t, i));
                          else if (!e.isEmpty(r["option.product_variant_id"]))
                            for (var P = 0; P < g.length; P++) {
                              var b = g[P].querySelector(
                                'option[data-product-variant-id="' +
                                  r["option.product_variant_id"] +
                                  '"]'
                              );
                              e.isEmpty(b) ||
                                g[P].value == b.getAttribute("value") ||
                                ((g[P].value = b.getAttribute("value")),
                                e.fireEvent(g[P], "change"));
                            }
                        });
                      }
                    }
                  }
                }
              }
            }
          }
        },
        Z = function (i, a, n) {
          if (e.isObject(a) && e.isObject(a.variant) && e.isObject(a.product)) {
            var o = a.variant.src;
            if (
              (e.isEmpty(o) &&
                ((o = a.product.image), e.isObject(o) && (o = o.src)),
              !e.isEmpty(o))
            ) {
              !e.isString(o) ||
                o.startsWith("http://") ||
                o.startsWith("https://") ||
                o.startsWith("//") ||
                (o = "https://" + e.const.STATIC_W_DOMAIN + "/" + o);
              var r = e.findAncestor(i, "prime-collection-item"),
                s = [],
                l = 0,
                c = null;
              if (e.isEmpty(r)) {
                var d = document.querySelectorAll("[data-runtime-id]");
                for (l = 0; l < d.length; l++)
                  (r = e.findAncestor(d[l], "prime-collection-item")),
                    e.isEmpty(r) &&
                      ((c = e.runtime.eventData[d[l].id]),
                      e.isEmpty(c) ||
                        c["option.product_type"] != n["option.product_type"] ||
                        c["option.product_id"] != n["option.product_id"] ||
                        s.push(d[l]));
              } else s = r.querySelectorAll("[data-runtime-id]");
              for (l = 0; l < s.length; l++)
                if (
                  ((c = e.runtime.eventData[s[l].id]),
                  !e.isEmpty(c) && !e.isEmpty(c["option.product_mapping_name"]))
                ) {
                  var p = s[l].getElementsByClassName("prime-gallery-view")[0],
                    u = e.getOptimizeImage(
                      o,
                      p.clientWidth,
                      p.clientHeight,
                      !0,
                      !1,
                      !1,
                      t
                    );
                  u = 'url("' + u + '")';
                  var m = e.getOptimizeImage(o, 0, 0, !0, !1, !1, t);
                  m = 'url("' + m + '")';
                  for (
                    var _ = p.getElementsByClassName("prime-gallery-view-item"),
                      y = 0;
                    y < _.length;
                    y++
                  )
                    if (
                      u == getComputedStyle(_[y]).backgroundImage ||
                      m == getComputedStyle(_[y]).backgroundImage
                    ) {
                      var g =
                        (parseFloatLadiPage(_[y].getAttribute("data-index")) ||
                          0) + 1;
                      window.ladi(s[l].id, s[l]).index(g);
                    }
                }
            }
          }
        },
        Q = function (i, a, n, o, r, s, l, c) {
          var d = e.runtime.eventData[i];
          if (!e.isEmpty(d)) {
            var p = d["option.product_mapping_name"],
              u = !e.isEmpty(p),
              m = d.type,
              _ = JSON.stringify(d),
              g = null,
              f = null;
            if (s) g = r.product[p];
            else if (e.isEmpty(o)) {
              if (
                e.isEmpty(d) ||
                e.isEmpty(d["option.product_type"]) ||
                e.isEmpty(d["option.product_id"]) ||
                e.isEmpty(p)
              )
                return;
              var v = d["option.product_variant_id"],
                h = !1;
              if (
                e.isEmpty(v) &&
                (a &&
                  (h = !(function () {
                    for (var t = !1, i = 0; i < y.length; i++) {
                      var a = e.runtime.eventData[y[i]];
                      if (
                        "form" == a.type &&
                        a["option.product_type"] == d["option.product_type"] &&
                        a["option.product_id"] == d["option.product_id"]
                      ) {
                        t = !0;
                        break;
                      }
                    }
                    return t;
                  })()),
                !e.isEmpty(l))
              ) {
                if (
                  d["option.product_id"] !=
                  l.target.getAttribute("data-product-id")
                )
                  return;
                var E = e.generateVariantProduct(
                  d,
                  !1,
                  null,
                  null,
                  null,
                  null,
                  !0,
                  !0,
                  function (t) {
                    Q(i, a, n, o, r, !1, l);
                  }
                );
                e.isObject(E) &&
                  (v = e.getProductVariantId(l.target, E.product));
              }
              if (
                _ ===
                (g = (f = e.generateProductKey(
                  !0,
                  _,
                  !0,
                  d,
                  h,
                  v,
                  o,
                  function (t) {
                    Q(i, a, n, o, r, s, l);
                  }
                )).value)
              )
                return;
            } else {
              if ("form" == m && d["option.is_add_to_cart"])
                return (
                  (d["option.product_id"] = o.product_id),
                  (d["option.product_variant_id"] = o.product_variant_id),
                  void $(i, m, !1, !0)
                );
              if (!u) return;
              g =
                (g = (f = e.generateProductKey(
                  !0,
                  null,
                  !0,
                  d,
                  !1,
                  o.product_variant_id,
                  o
                )).value) || "";
            }
            var P = null,
              b = null,
              L = null;
            if ("headline" == m || "paragraph" == m) {
              var A = c ? "prime-html" : null;
              window.ladi(i).value(e.isNull(g) ? "" : g, A);
            }
            if ("image" == m) {
              if (((P = document.getElementById(i)), e.isEmpty(P))) return;
              (L = e.getOptimizeImage(
                g,
                P.clientWidth,
                P.clientHeight,
                !0,
                !1,
                !1,
                t
              )),
                (b = "style_add_to_cart_image_" + i);
              var w;
              (w = e.isEmpty(L)
                ? "#" +
                  i +
                  "  > .prime-image > .prime-image-background {background-image: none;}"
                : "#" +
                  i +
                  '  > .prime-image > .prime-image-background {background-image: url("' +
                  L +
                  '");}'),
                e.createStyleElement(b, w);
            }
            if ("gallery" == m) {
              if (!e.isArray(g)) return;
              if (((P = document.getElementById(i)), e.isEmpty(P))) return;
              if (n && "true" == P.getAttribute("data-loaded"))
                return void Z(P, f, d);
              for (
                var S = P.getElementsByClassName("prime-gallery-view")[0],
                  T = P.getElementsByClassName("prime-gallery-view-item");
                T.length < g.length;

              ) {
                var O = e.createTmpElement(
                  "div",
                  '<div class="prime-gallery-view-item" data-index="' +
                    T.length +
                    '"></div>',
                  null,
                  !0
                );
                P.getElementsByClassName("prime-gallery-view")[0].appendChild(O);
              }
              for (; T.length > g.length; )
                T[T.length - 1].parentElement.removeChild(T[T.length - 1]);
              for (
                var C = P.getElementsByClassName("prime-gallery-control-item"),
                  N = function (t) {
                    V(t, P);
                  };
                C.length < g.length;

              ) {
                var I = e.createTmpElement(
                  "div",
                  '<div class="prime-gallery-control-item" data-index="' +
                    C.length +
                    '"></div>',
                  null,
                  !0
                );
                I.addEventListener("click", N),
                  P.getElementsByClassName(
                    "prime-gallery-control-box"
                  )[0].appendChild(I);
              }
              for (; C.length > g.length; )
                C[C.length - 1].parentElement.removeChild(C[C.length - 1]);
              b = "style_add_to_cart_gallery_" + i;
              var k = "";
              g.length <= 1 &&
                ((k +=
                  "#" +
                  i +
                  " .prime-gallery .prime-gallery-view .prime-gallery-view-arrow {display: none;}"),
                (k +=
                  "#" +
                  i +
                  " > .prime-gallery > .prime-gallery-view {height: 100%;}"),
                (k +=
                  "#" +
                  i +
                  " > .prime-gallery > .prime-gallery-control {display: none;}"));
              var x = P.getElementsByClassName("prime-gallery-control-item")[0];
              g.forEach(function (a, n) {
                (L = e.getOptimizeImage(
                  a.src,
                  S.clientWidth,
                  S.clientHeight,
                  !0,
                  !1,
                  !1,
                  t
                )),
                  (k +=
                    "#" +
                    i +
                    ' .prime-gallery .prime-gallery-view-item[data-index="' +
                    n +
                    '"] {background-image: url("' +
                    L +
                    '");}'),
                  (L = e.getOptimizeImage(
                    a.src,
                    x.clientWidth,
                    x.clientHeight,
                    !0,
                    !1,
                    !1,
                    t
                  )),
                  (k +=
                    "#" +
                    i +
                    ' .prime-gallery .prime-gallery-control-item[data-index="' +
                    n +
                    '"] {background-image: url("' +
                    L +
                    '");}');
              }),
                P.setAttribute("data-max-item", g.length),
                P.setAttribute("data-loaded", !0),
                e.createStyleElement(b, k);
            }
          }
        },
        tt = function (t) {
          var i = { type: "POPUPX", iframe_id: e.runtime.tmp.popupx_iframe_id };
          Object.keys(t).forEach(function (e) {
            i[e] = t[e];
          }),
            e.postMessageWindow(window.parent, i, "*");
        },
        et = function (t) {
          if (!e.runtime.tmp.popupx_is_desktop && !e.isEmpty(t)) {
            var i = parseFloatLadiPage(t);
            (window.innerWidth = i),
              (window.outerWidth = i),
              (window.ladi_screen_width = i),
              e.isFunction(window.ladi_viewport) && window.ladi_viewport();
          }
        },
        it = function (t, i, a, n) {
          var o = e.isEmpty(e.runtime.tmp.popupx_current_element_id);
          (e.runtime.tmp.popupx_current_element_id = t),
            !i || o || a || n || C("PageView", {});
        },
        at = function (t, i) {
          for (
            var a = !1,
              n = !1,
              o = document.querySelectorAll(
                "#" +
                  e.runtime.builder_section_popup_id +
                  " .prime-container > .prime-element"
              ),
              r = 0;
            r < o.length;
            r++
          )
            "none" != getComputedStyle(o[r]).display &&
              (o[r].id == t && (a = !0),
              e.runRemovePopup(o[r].id, !0, null, !1, !0));
          for (
            o = document.querySelectorAll(
              ".prime-section:not(#" + e.runtime.builder_section_popup_id + ")"
            ),
              r = 0;
            r < o.length;
            r++
          )
            "none" != getComputedStyle(o[r]).display &&
              (o[r].id == t && (n = !0), window.ladi(o[r].id).hide(!0));
          return { isCurrentPopup: a, isCurrentSection: n };
        },
        nt = function (t, i) {
          var a = e.runtime.eventData[t],
            n = document.getElementById(t),
            o = null,
            r = null,
            s = !1,
            l = !1,
            c = { width_device: e.runtime.desktop_width },
            d = document.getElementById("style_container_desktop");
          if (
            ((e.isEmpty(d) || "print" == d.getAttribute("media")) &&
              (c = { width_device: e.runtime.mobile_width }),
            !e.isEmpty(a) && !e.isEmpty(n))
          ) {
            var p = getComputedStyle(n);
            if ("popup" == a.type) {
              (o = {}),
                (s = (r = at(t)).isCurrentPopup),
                (l = r.isCurrentSection);
              var u = a[e.runtime.device + ".option.popup_position"];
              [
                "width",
                "height",
                "position",
                "margin",
                "top",
                "left",
                "bottom",
                "right",
                "z-index",
              ].forEach(function (t) {
                o[t] = p[t];
              }),
                et(o.width),
                (c.width = o.width);
              var m = a[e.runtime.device + ".option.popup_backdrop"];
              return (
                tt({
                  id: t,
                  position: u,
                  data_backdrop: m,
                  data_scale: c,
                  is_opacity: !s,
                  set_scroll_popup: !0,
                  dimension: o,
                  action: { type: "set_iframe_dimension" },
                }),
                window.ladi(t).show(!0),
                it(t, i, s, l),
                !0
              );
            }
            if ("section" == a.type) {
              if (
                ((s = (r = at(t)).isCurrentPopup),
                (l = r.isCurrentSection),
                a[e.runtime.device + ".option.sticky"])
              ) {
                o = { height: p.height };
                var _ = n.getElementsByClassName("prime-container")[0],
                  y = getComputedStyle(_);
                et(y.width),
                  (c.width = y.width),
                  (c.is_sticky_bar = !0),
                  tt({
                    id: t,
                    data_scale: c,
                    dimension: o,
                    element: a,
                    device: e.runtime.device,
                    action: { type: "set_iframe_sticky" },
                  }),
                  window.ladi(t).show(!0),
                  it(t, i, s, l);
              }
              return !0;
            }
          }
          return !1;
        },
        ot = function (t, i) {
          var a = e.runtime.eventData[t],
            n = document.getElementById(t),
            o = null;
          if (!e.isEmpty(a) && !e.isEmpty(n)) {
            var r = getComputedStyle(n),
              s = at(t),
              l = s.isCurrentPopup,
              c = s.isCurrentSection,
              d = { width_device: e.runtime.desktop_width },
              p = document.getElementById("style_container_desktop");
            if (
              ((e.isEmpty(p) || "print" == p.getAttribute("media")) &&
                (d = { width_device: e.runtime.mobile_width }),
              (o = { width: r.width, height: r.height }),
              "popup" == a.type && (d.width = o.width),
              "section" == a.type)
            ) {
              var u = n.getElementsByClassName("prime-container")[0],
                m = getComputedStyle(u);
              (o.width = m.width), (d.width = m.width), (d.is_sticky_bar = !0);
            }
            return (
              et(o.width),
              tt({
                id: t,
                data_scale: d,
                dimension: o,
                action: { type: "set_iframe_dimension" },
              }),
              window.ladi(t).show(!0),
              it(t, i, l, c),
              !0
            );
          }
          return !1;
        },
        rt = function (t) {
          for (
            var i = [
                "style_element_desktop",
                "style_container_desktop",
                "style_ladi_media_desktop",
              ],
              a = [
                "style_element_mobile",
                "style_container_mobile",
                "style_ladi_media_mobile",
              ],
              n = 0;
            n < i.length;
            n++
          ) {
            var o = document.getElementById(i[n]);
            e.isEmpty(o) ||
              (t
                ? o.removeAttribute("media")
                : o.setAttribute("media", "print"));
          }
          for (n = 0; n < a.length; n++) {
            var r = document.getElementById(a[n]);
            e.isEmpty(r) ||
              (t
                ? r.setAttribute("media", "print")
                : r.removeAttribute("media"));
          }
        },
        st = function () {
          var i;
          e.changeTotalPriceCart(),
            e.runtime.tmp.generateLadiSaleProduct(!0),
            e.runtime.shopping && e.createCartData(),
            e.loadDataset(null, null, null, null, null, !0, t),
            y.forEach(function (i) {
              var a = e.runtime.eventData[i],
                n = LadiPageApp[a.type + e.const.APP_RUNTIME_PREFIX];
              e.isEmpty(n)
                ? ((function (t, i, a, n, o, r) {
                    var s = document.getElementById(t);
                    if (
                      !e.isEmpty(s) &&
                      (R(t), "section" == i && !e.isEmpty(o) && !e.isEmpty(r))
                    ) {
                      var l = s.getElementsByClassName(
                        "prime-section-background"
                      )[0];
                      e.isEmpty(l) ||
                        (e.runtime.list_scroll_func[t] = function () {
                          if (
                            (!e.runtime.isDesktop ||
                              a == e.const.BACKGROUND_STYLE.video) &&
                            (e.runtime.isDesktop ||
                              n == e.const.BACKGROUND_STYLE.video)
                          ) {
                            var i = "",
                              s = t + "_background_video";
                            o == e.const.VIDEO_TYPE.youtube &&
                              ((i =
                                '<iframe id="' +
                                s +
                                '" class="prime-section-background-video" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0;" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>'),
                              (l.innerHTML += i),
                              e.runEventPlayVideo(s, o, r, !0, !0, !1)),
                              o == e.const.VIDEO_TYPE.direct &&
                                ((i =
                                  '<video id="' +
                                  s +
                                  '" class="prime-section-background-video" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; object-fit: cover;"></video>'),
                                (l.innerHTML += i),
                                e.runEventPlayVideo(s, o, r, !0, !0, !1));
                          }
                        });
                    }
                  })(
                    i,
                    a.type,
                    a[e.const.DESKTOP + ".option.background-style"],
                    a[e.const.MOBILE + ".option.background-style"],
                    a["option.background_video.video_type"],
                    a["option.background_video.video_value"]
                  ),
                  x(null, i, a.type, a),
                  (function (t, i, a) {
                    e.runtime.list_loaded_func.push(function () {
                      var n = 0;
                      if (
                        (-1 !=
                          ["headline", "paragraph", "list_paragraph"].indexOf(
                            i
                          ) && (n = 1e3),
                        !e.isArray(a))
                      ) {
                        var o = e.copy(a);
                        (a = []),
                          e.isObject(o) &&
                            ((o.action_type = e.const.ACTION_TYPE.action),
                            a.push(o));
                      }
                      e.runTimeout(function () {
                        a.forEach(function (i) {
                          if (
                            i.action_type == e.const.ACTION_TYPE.action &&
                            i.type == e.const.DATA_ACTION_TYPE.collapse &&
                            !e.isEmpty(i.action) &&
                            (e.isNull(i.collapse_start_is_show) ||
                              !i.collapse_start_is_show)
                          ) {
                            window.ladi(i.action).collapse(!1);
                            for (
                              var a = document.querySelectorAll(
                                  "#" +
                                    t +
                                    " > .prime-frame > .prime-element.prime-accordion-shape"
                                ),
                                n = 0;
                              n < a.length;
                              n++
                            ) {
                              var o = e.getSource2ndClick(a[n].id);
                              e.isEmpty(o) ||
                                window.ladi(a[n].id, a[n]).set_value_2nd(o);
                            }
                          }
                        });
                      }, n);
                    });
                  })(
                    i,
                    a.type,
                    a["option.data_event"] || a["option.data_action"]
                  ),
                  D(
                    null,
                    i,
                    a.type,
                    a["option.data_event"] || a["option.data_hover"]
                  ),
                  (function (t, i, a, n, o, r, s) {
                    if ("video" == i && !e.isEmpty(a)) {
                      var l = document.getElementById(t);
                      if (!e.isEmpty(l)) {
                        var c = function () {
                            var i = e.runtime.eventData[t];
                            e.isEmpty(i) ||
                              ((n = i["option.video_type"]),
                              (a = i["option.video_value"]),
                              (o = i["option.video_control"]));
                          },
                          d =
                            (e.runtime.isDesktop && r) ||
                            (!e.runtime.isDesktop && s);
                        if (d) {
                          var p = function () {
                              c(), e.playVideo(t, n, a, o, d);
                            },
                            u = e.findAncestor(l, "prime-popup");
                          e.isEmpty(u)
                            ? (e.runtime.list_scroll_func[t] = p)
                            : ((u = e.findAncestor(u, "prime-element")),
                              e.isArray(e.runtime.list_show_popup_func[u.id]) ||
                                (e.runtime.list_show_popup_func[u.id] = []),
                              e.runtime.list_show_popup_func[u.id].push(p));
                        } else {
                          var m =
                            document.querySelectorAll("#" + t + ".preload")
                              .length > 0;
                          m && e.playVideo(t, n, a, o, !1, m);
                        }
                        l.addEventListener("click", function (i) {
                          i.stopPropagation(),
                            c(),
                            (n == e.const.VIDEO_TYPE.direct &&
                              "VIDEO" == i.target.tagName) ||
                              (n == e.const.VIDEO_TYPE.youtube &&
                                "IFRAME" == i.target.tagName) ||
                              e.playVideo(t, n, a, o);
                        });
                      }
                    }
                  })(
                    i,
                    a.type,
                    a["option.video_value"],
                    a["option.video_type"],
                    a["option.video_control"],
                    a[e.const.DESKTOP + ".option.video_autoplay"],
                    a[e.const.MOBILE + ".option.video_autoplay"]
                  ),
                  (function (t, i, a, n) {
                    "popup" == i &&
                      a &&
                      ((e.isEmpty(n) || n < 0) && (n = 0),
                      e.runTimeout(function () {
                        window.ladi(t).show();
                      }, 1e3 * n));
                  })(
                    i,
                    a.type,
                    a["option.show_popup_welcome_page"],
                    a["option.delay_popup_welcome_page"]
                  ),
                  F(
                    i,
                    null,
                    a.type,
                    a["option.countdown_type"],
                    a["option.countdown_minute"],
                    a["option.countdown_daily_start"],
                    a["option.countdown_daily_end"],
                    a["option.countdown_endtime"]
                  ),
                  q(i, null, a.type, a["option.countdown_item_type"]),
                  (function (t, i, a, n) {
                    if ("section" == i) {
                      var o = document.getElementById(t);
                      if (!e.isEmpty(o)) {
                        var r = o.getElementsByClassName(
                          "prime-section-arrow-down"
                        )[0];
                        g.push(function () {
                          if (e.isEmpty(r)) {
                            if (e.runtime.isDesktop) {
                              if (e.isEmpty(a))
                                return void o.removeAttribute("data-opacity");
                              var t = (parseFloatLadiPage(a) || 0) + 50;
                              if (t > o.clientHeight)
                                return void o.removeAttribute("data-opacity");
                              o.setAttribute("data-height", o.clientHeight),
                                o.style.setProperty("height", t + "px"),
                                o.classList.add("overflow-hidden");
                            } else {
                              if (e.isEmpty(n))
                                return void o.removeAttribute("data-opacity");
                              var i = (parseFloatLadiPage(n) || 0) + 50;
                              if (i > o.clientHeight)
                                return void o.removeAttribute("data-opacity");
                              o.setAttribute("data-height", o.clientHeight),
                                o.style.setProperty("height", i + "px"),
                                o.classList.add("overflow-hidden");
                            }
                            ((r = document.createElement("div")).className =
                              "prime-section-arrow-down"),
                              o.appendChild(r),
                              o.removeAttribute("data-opacity"),
                              r.addEventListener("click", function (t) {
                                t.stopPropagation(),
                                  o.classList.add("transition-readmore"),
                                  o.style.removeProperty("height"),
                                  r.parentElement.removeChild(r),
                                  e.runTimeout(function () {
                                    o.classList.remove("transition-readmore"),
                                      o.classList.remove("overflow-hidden"),
                                      o.clientHeight !=
                                        o.getAttribute("data-height") &&
                                        o.style.setProperty(
                                          "height",
                                          o.getAttribute("data-height") + "px"
                                        ),
                                      o.removeAttribute("data-height"),
                                      e.runTimeout(e.removeSticky, 100);
                                  }, 1e3 *
                                    parseFloatLadiPage(
                                      getComputedStyle(o).transitionDuration
                                    ));
                              });
                          }
                        });
                      }
                    }
                  })(
                    i,
                    a.type,
                    a[e.const.DESKTOP + ".option.readmore_range"],
                    a[e.const.MOBILE + ".option.readmore_range"]
                  ),
                  (function (t, i, a) {
                    if ("form_item" == i) {
                      var n = null,
                        o = null;
                      if (
                        (a == e.const.INPUT_TYPE.select ||
                          a == e.const.INPUT_TYPE.select_multiple) &&
                        ((n = document.getElementById(t)), !e.isEmpty(n))
                      )
                        for (
                          var r = n.getElementsByClassName("prime-form-control"),
                            s = 0;
                          s < r.length;
                          s++
                        )
                          r[s].addEventListener("change", function (t) {
                            t.target.setAttribute(
                              "data-selected",
                              t.target.value
                            );
                          });
                      if (a == e.const.INPUT_TYPE.checkbox) {
                        n = document.getElementById(t);
                        var l = function (t) {
                            t.stopPropagation();
                            var i = e.findAncestor(
                              t.target,
                              "prime-form-checkbox-item"
                            );
                            e.isEmpty(i) ||
                              i
                                .getElementsByTagName("span")[0]
                                .setAttribute("data-checked", t.target.checked);
                          },
                          c = function (t) {
                            t.stopPropagation();
                            var i = e.findAncestor(
                              t.target,
                              "prime-form-checkbox-item"
                            );
                            e.isEmpty(i) ||
                              i.getElementsByTagName("input")[0].click();
                          };
                        if (!e.isEmpty(n)) {
                          o = n.getElementsByClassName(
                            "prime-form-checkbox-item"
                          );
                          for (var d = 0; d < o.length; d++) {
                            var p = o[d].getElementsByTagName("input")[0];
                            o[d]
                              .getElementsByTagName("span")[0]
                              .addEventListener("click", c),
                              p.addEventListener("change", l);
                          }
                        }
                      }
                      if (a == e.const.INPUT_TYPE.radio) {
                        n = document.getElementById(t);
                        var u = function (t) {
                            var i = e.findAncestor(
                                t.target,
                                "prime-form-checkbox-item"
                              ),
                              a = e.findAncestor(i, "prime-form-checkbox");
                            if (!e.isEmpty(a)) {
                              for (
                                var n = a.querySelectorAll(
                                    ".prime-form-checkbox-item span"
                                  ),
                                  o = 0;
                                o < n.length;
                                o++
                              )
                                n[o].setAttribute("data-checked", !1);
                              e.isEmpty(i) ||
                                i
                                  .getElementsByTagName("span")[0]
                                  .setAttribute(
                                    "data-checked",
                                    t.target.checked
                                  );
                            }
                          },
                          m = function (t) {
                            t.stopPropagation();
                            var i = e.findAncestor(
                              t.target,
                              "prime-form-checkbox-item"
                            );
                            e.isEmpty(i) ||
                              i.getElementsByTagName("input")[0].click();
                          };
                        if (!e.isEmpty(n)) {
                          o = n.getElementsByClassName(
                            "prime-form-checkbox-item"
                          );
                          for (var _ = 0; _ < o.length; _++) {
                            var y = o[_].getElementsByTagName("input")[0];
                            o[_].getElementsByTagName(
                              "span"
                            )[0].addEventListener("click", m),
                              y.addEventListener("change", u);
                          }
                        }
                      }
                    }
                  })(i, a.type, a["option.input_type"]),
                  Y(i, null, !1, a.type),
                  e.startAutoScroll(
                    i,
                    a.type,
                    a[e.const.DESKTOP + ".option.auto_scroll"],
                    a[e.const.MOBILE + ".option.auto_scroll"]
                  ),
                  $(i, a.type, !0, !1),
                  (function (t, i) {
                    if ("form" == i) {
                      var a = document.getElementById(t);
                      if (!e.isEmpty(a)) {
                        var n = a.querySelector('input[name="quantity"]');
                        if (!e.isEmpty(n)) {
                          var o = function (i) {
                            if (!e.isEmpty(i.target.value)) {
                              var a = e.runtime.eventData[t];
                              if (!e.isEmpty(a) && a["option.is_add_to_cart"]) {
                                var n = e.generateVariantProduct(
                                  a,
                                  !1,
                                  null,
                                  null,
                                  null,
                                  null,
                                  !0,
                                  !0,
                                  function () {
                                    o(i);
                                  }
                                );
                                if (
                                  !(
                                    e.isEmpty(n) ||
                                    e.isEmpty(n.store_info) ||
                                    e.isEmpty(n.product)
                                  )
                                ) {
                                  var r = e.getProductVariantIndex(t, a);
                                  if (-1 != r) {
                                    var s = n.product.variants[r].quantity,
                                      l = n.product.variants[r].quantity_stock;
                                    s = e.isNull(l) ? s : l;
                                    var c = parseInt(i.target.value) || 0,
                                      d = 1;
                                    d = n.product.variants[r].min_buy || d;
                                    var p = n.product.variants[r].max_buy,
                                      u = 0,
                                      m = e.runtime.tmp.cart.findIndex(
                                        function (t) {
                                          return (
                                            t.product_id ==
                                              n.product.variants[r]
                                                .product_id &&
                                            t.product_variant_id ==
                                              n.product.variants[r]
                                                .product_variant_id
                                          );
                                        }
                                      );
                                    -1 != m &&
                                      (u = e.runtime.tmp.cart[m].quantity),
                                      d > c + u && (c = d - u),
                                      1 ==
                                        n.product.variants[r]
                                          .inventory_checked &&
                                        c + u > s &&
                                        (c = s - u),
                                      !e.isEmpty(p) && c + u > p && (c = p - u),
                                      (c = c < 1 ? 1 : c),
                                      i.target.setAttribute("min", d),
                                      e.isEmpty(p) ||
                                        i.target.setAttribute("max", p),
                                      (i.target.value = c);
                                  }
                                }
                              }
                            }
                          };
                          n.addEventListener("input", o),
                            e.fireEvent(n, "input");
                          var r = a.querySelectorAll(".button")[0],
                            s = a.querySelectorAll(".button")[1];
                          e.isEmpty(r) ||
                            e.isEmpty(s) ||
                            (r.addEventListener("click", function (t) {
                              (n.value =
                                (parseFloatLadiPage(n.value) || 0) - 1),
                                e.fireEvent(n, "input");
                            }),
                            s.addEventListener("click", function (t) {
                              (n.value =
                                (parseFloatLadiPage(n.value) || 0) + 1),
                                e.fireEvent(n, "input");
                            }));
                        }
                      }
                    }
                  })(i, a.type),
                  (function (t, i) {
                    if ("collection" == i) {
                      var a = document.getElementById(t);
                      if (!e.isEmpty(a)) {
                        var n = e.runtime.eventData[t];
                        if (!e.isEmpty(n)) {
                          var o = n["option.collection_setting.type"],
                            r = a.getElementsByClassName("prime-collection")[0];
                          if (
                            (o == e.const.COLLECTION_TYPE.carousel &&
                              r.classList.add("carousel"),
                            o == e.const.COLLECTION_TYPE.carousel)
                          ) {
                            var s = document.createElement("div");
                            s.className =
                              "prime-collection-arrow prime-collection-arrow-left opacity-0";
                            var l = document.createElement("div");
                            (l.className =
                              "prime-collection-arrow prime-collection-arrow-right opacity-0"),
                              r.appendChild(s),
                              r.appendChild(l),
                              s.addEventListener("click", function (i) {
                                i.stopPropagation();
                                var o =
                                  parseFloatLadiPage(
                                    a.getAttribute("data-page")
                                  ) || 1;
                                (o = (o -= 1) < 1 ? 1 : o),
                                  e.loadCollectionData(t, n, o, !1);
                              }),
                              l.addEventListener("click", function (i) {
                                i.stopPropagation();
                                var o =
                                  parseFloatLadiPage(
                                    a.getAttribute("data-page")
                                  ) || 1;
                                if (
                                  ((o += 1), a.hasAttribute("data-max-page"))
                                ) {
                                  var r =
                                    parseFloatLadiPage(
                                      a.getAttribute("data-max-page")
                                    ) || 1;
                                  o = o > r ? r : o;
                                }
                                e.loadCollectionData(t, n, o, !1);
                              });
                          }
                          if (o == e.const.COLLECTION_TYPE.readmore) {
                            var c = document.createElement("div");
                            (c.className =
                              "prime-collection-button-next opacity-0"),
                              r.appendChild(c),
                              c.addEventListener("click", function (i) {
                                i.stopPropagation();
                                var o =
                                  parseFloatLadiPage(
                                    a.getAttribute("data-page")
                                  ) || 1;
                                if (
                                  ((o += 1), a.hasAttribute("data-max-page"))
                                ) {
                                  var r =
                                    parseFloatLadiPage(
                                      a.getAttribute("data-max-page")
                                    ) || 1;
                                  o = o > r ? r : o;
                                }
                                e.loadCollectionData(t, n, o, !1, !0);
                              });
                          }
                          e.loadCollectionData(t, n, 1, !0);
                        }
                      }
                    }
                  })(i, a.type),
                  (function (t, i, a, n) {
                    if ("survey" == i) {
                      var o = document.getElementById(t);
                      if (!e.isEmpty(o)) {
                        a && o.setAttribute("data-multiple", !0);
                        for (
                          var r = o.getElementsByClassName(
                              "prime-survey-select-item"
                            )[0],
                            s = o.getElementsByClassName(
                              "prime-survey-radio-item"
                            ),
                            l = o.getElementsByClassName(
                              "prime-survey-checkbox-item"
                            ),
                            c = o.getElementsByClassName("prime-survey-option"),
                            d = o.querySelector(
                              ".prime-survey-button-next button"
                            ),
                            p = [],
                            u = n.mapping_form_name,
                            m = n.mapping_form_id,
                            _ = n.input_name,
                            y = e.findAncestor(o, [
                              "prime-form",
                              "prime-element",
                            ]),
                            g = e.runtime.eventData[t]["option.data_event"],
                            f = function () {
                              var i = window.ladi(t).value(),
                                n = u || "";
                              m.forEach(function (t) {
                                var o = document.getElementById(t);
                                if (!e.isEmpty(o)) {
                                  for (
                                    var r = null;
                                    0 !=
                                    (r = o.querySelectorAll(
                                      '.prime-form-item-survey[data-name="' +
                                        n +
                                        '"]'
                                    )).length;

                                  )
                                    r[0].parentElement.removeChild(r[0]);
                                  var s = [],
                                    l = o.querySelectorAll(
                                      ".prime-element .prime-form-item-container [name]"
                                    ),
                                    c = null,
                                    d = 0;
                                  for (A = 0; A < l.length; A++) {
                                    l[A].getAttribute("name") == n &&
                                      ((c = e.findAncestor(
                                        l[A],
                                        "prime-element"
                                      )),
                                      s.push(c.id));
                                    var u =
                                      parseFloatLadiPage(
                                        l[A].getAttribute("tabindex")
                                      ) || 0;
                                    u > d && (d = u);
                                  }
                                  if (0 == (s = s.unique()).length) {
                                    d++,
                                      ((c =
                                        document.createElement(
                                          "div"
                                        )).className =
                                        "prime-element prime-hidden prime-form-item-survey"),
                                      (c.id = e.randomString(10)),
                                      c.setAttribute("data-name", n);
                                    var m = "";
                                    if (
                                      ((m +=
                                        '<div class="prime-form-item-container">'),
                                      (m +=
                                        '   <div class="prime-form-item-background"></div>'),
                                      a)
                                    ) {
                                      m +=
                                        '   <div class="prime-form-item prime-form-checkbox prime-form-checkbox-vertical">';
                                      for (var _ = 0; _ < p.length; _++)
                                        m +=
                                          '   <div class="prime-form-checkbox-item"><input tabindex="' +
                                          d +
                                          '" name="' +
                                          n +
                                          '" type="checkbox" value="' +
                                          p[_] +
                                          '"><span data-checked="false">' +
                                          p[_] +
                                          "</span></div>";
                                      m += "   </div>";
                                    } else
                                      (m += '   <div class="prime-form-item">'),
                                        (m +=
                                          '       <input autocomplete="off" tabindex="' +
                                          d +
                                          '" name="' +
                                          n +
                                          '" class="prime-form-control" type="text">'),
                                        (m += "   </div>");
                                    (m += "</div>"),
                                      (c.innerHTML = m),
                                      o
                                        .getElementsByClassName("prime-form")[0]
                                        .appendChild(c),
                                      s.push(c.id);
                                  }
                                  for (A = 0; A < s.length; A++)
                                    window.ladi(s[A]).value(i);
                                }
                              });
                            },
                            v = function () {
                              for (var t = !1, e = 0; e < c.length; e++)
                                if (c[e].classList.contains("selected")) {
                                  t = !0;
                                  break;
                                }
                              return t;
                            },
                            h = function () {
                              if (
                                !(!e.isEmpty(r) || s.length > 0 || l.length > 0)
                              ) {
                                for (var t = [], i = 0; i < c.length; i++)
                                  c[i].classList.contains("selected") &&
                                    t.push(c[i].getAttribute("data-value"));
                                for (
                                  var n = null;
                                  0 !=
                                  (n = y.querySelectorAll(
                                    '.prime-form-item-survey[data-name="' +
                                      _ +
                                      '"]'
                                  )).length;

                                )
                                  n[0].parentElement.removeChild(n[0]);
                                var o = document.createElement("div");
                                (o.className =
                                  "prime-element prime-hidden prime-form-item-survey"),
                                  (o.id = e.randomString(10)),
                                  o.setAttribute("data-name", _);
                                var d = "";
                                if (
                                  ((d +=
                                    '<div class="prime-form-item-container">'),
                                  (d +=
                                    '   <div class="prime-form-item-background"></div>'),
                                  a)
                                ) {
                                  d +=
                                    '   <div class="prime-form-item prime-form-checkbox prime-form-checkbox-vertical">';
                                  for (var u = 0; u < p.length; u++)
                                    d +=
                                      '   <div class="prime-form-checkbox-item"><input name="' +
                                      _ +
                                      '" type="checkbox" value="' +
                                      p[u] +
                                      '"><span data-checked="false">' +
                                      p[u] +
                                      "</span></div>";
                                  d += "   </div>";
                                } else
                                  (d += '   <div class="prime-form-item">'),
                                    (d +=
                                      '       <input autocomplete="off" name="' +
                                      _ +
                                      '" class="prime-form-control" type="text">'),
                                    (d += "   </div>");
                                (d += "</div>"),
                                  (o.innerHTML = d),
                                  y
                                    .getElementsByClassName("prime-form")[0]
                                    .appendChild(o),
                                  window.ladi(o.id).value(a ? t : t[0]);
                              }
                            },
                            E = function () {
                              v() && I(o, g), f();
                            },
                            P = function (t) {
                              e.tapEventListener(t, function (i) {
                                if ((i.stopPropagation(), a))
                                  t.classList.contains("selected")
                                    ? t.classList.remove("selected")
                                    : t.classList.add("selected");
                                else
                                  for (
                                    var n =
                                        t.parentElement.getElementsByClassName(
                                          "prime-survey-option"
                                        ),
                                      o = 0;
                                    o < n.length;
                                    o++
                                  )
                                    n[o] === t
                                      ? n[o].classList.add("selected")
                                      : n[o].classList.remove("selected");
                                e.isEmpty(y) ? (L(), e.isEmpty(d) && E()) : h();
                              });
                            },
                            b = function (t) {
                              var i = t.getElementsByTagName("input")[0],
                                a = t.getElementsByTagName("span")[0];
                              e.isEmpty(i) ||
                                i.addEventListener("change", function (t) {
                                  e.isEmpty(a) ||
                                    a.setAttribute(
                                      "data-checked",
                                      t.target.checked
                                    );
                                }),
                                e.isEmpty(a) ||
                                  a.addEventListener("click", function (t) {
                                    t.stopPropagation(),
                                      e.isEmpty(i) || i.click();
                                  });
                            },
                            L = function () {
                              e.isEmpty(d) ||
                                (v()
                                  ? d.parentElement.classList.remove(
                                      "no-select"
                                    )
                                  : d.parentElement.classList.add("no-select"));
                            },
                            A = 0;
                          A < c.length;
                          A++
                        )
                          p.push(c[A].getAttribute("data-value")), P(c[A]);
                        for (
                          e.isEmpty(r) ||
                            r.addEventListener("change", function (t) {
                              t.target.setAttribute(
                                "data-selected",
                                t.target.value
                              );
                            }),
                            A = 0;
                          A < s.length;
                          A++
                        )
                          p.push(s[A].getElementsByTagName("input")[0].value),
                            b(s[A]);
                        for (A = 0; A < l.length; A++)
                          p.push(l[A].getElementsByTagName("input")[0].value),
                            b(l[A]);
                        if (e.isEmpty(y)) {
                          if (!e.isArray(g)) {
                            var w = e.copy(n);
                            if (
                              ((g = []), e.isObject(w) && !e.isEmpty(w.value))
                            ) {
                              if (
                                ((w.type != e.const.DATA_ACTION_TYPE.section &&
                                  w.type != e.const.DATA_ACTION_TYPE.popup) ||
                                  g.push({
                                    action_type: e.const.ACTION_TYPE.complete,
                                    type: w.type,
                                    action: w.value,
                                  }),
                                (w.type == e.const.DATA_ACTION_TYPE.section &&
                                  w.is_hide_parent) ||
                                  w.type == e.const.DATA_ACTION_TYPE.popup)
                              ) {
                                var S = e.findAncestor(o, "prime-popup"),
                                  T = e.findAncestor(o, "prime-section"),
                                  O = null;
                                e.isEmpty(S)
                                  ? e.isEmpty(T) || (O = T.id)
                                  : (O = (S = e.findAncestor(S, "prime-element"))
                                      .id),
                                  e.isEmpty(O) ||
                                    g.push({
                                      action_type: e.const.ACTION_TYPE.complete,
                                      type: e.const.DATA_ACTION_TYPE
                                        .hidden_show,
                                      hidden_ids: [O],
                                      show_ids: [],
                                    });
                              }
                              w.type == e.const.DATA_ACTION_TYPE.change_index &&
                                g.push({
                                  action_type: e.const.ACTION_TYPE.complete,
                                  type: w.type,
                                  action: w.value,
                                  change_index_type: w.change_index_type,
                                  change_index_number: w.change_index_number,
                                });
                            }
                          }
                          e.isEmpty(d) ||
                            d.addEventListener("click", function (t) {
                              t.stopPropagation(), E();
                            }),
                            L(),
                            e.runtime.list_loaded_func.push(f);
                        } else h();
                      }
                    }
                  })(
                    i,
                    a.type,
                    a["option.survey_setting.is_multiple"],
                    a["option.survey_setting"] ||
                      a["option.survey_setting.event"]
                  ))
                : n(a, t).run(i, e.runtime.isDesktop);
            }),
            W(Date.now() + 1e3),
            (function () {
              var i,
                a = document.getElementsByClassName("prime-form"),
                n = null,
                o = null,
                r = null,
                s = null,
                d = null,
                p = null,
                u = null,
                m = null,
                y = null,
                g = null,
                f = null,
                v = null,
                h = null,
                P = null,
                b = null,
                L = e.runtime.shopping,
                w = null,
                S = [
                  "utm_source",
                  "utm_medium",
                  "utm_campaign",
                  "utm_term",
                  "utm_content",
                ],
                T = [
                  "name",
                  "email",
                  "phone",
                  "address",
                  "ward",
                  "district",
                  "state",
                  "country",
                ],
                N = ["email", "phone"],
                k = e.copy(e.runtime.list_set_value_name_country).reverse(),
                x = function (t, i) {
                  var a = "_capture_" + t,
                    n = window.ladi(a).get_cookie(),
                    o = !1,
                    r = e.runtime.tmp["cookie_cache_otp_" + t];
                  if (
                    (e.isEmpty(n) &&
                      e.isObject(r) &&
                      !e.isEmpty(r[a]) &&
                      ((n = r[a]), (o = !0)),
                    e.isEmpty(n))
                  ) {
                    n =
                      i +
                      "|" +
                      e.runtime.ladipage_id +
                      "|" +
                      Date.now() +
                      "|" +
                      e.randomId();
                    var s = new Date();
                    s.setTime(s.getTime() + 9e5),
                      o
                        ? ((r[a] = n),
                          (e.runtime.tmp["cookie_cache_otp_" + t] = r))
                        : window.ladi(a).set_cookie(n, s);
                  }
                  return n;
                },
                D = function (t, i, a) {
                  var o = e.runtime.tmp["form_data_ladi_" + m];
                  if (e.isObject(o)) return !0;
                  if (i && e.isEmpty(n[a])) return !1;
                  var r = [];
                  if (
                    (P.forEach(function (t) {
                      e.isEmpty(n[t]) && r.push(t);
                    }),
                    i && (r = r.only([a])),
                    r.length > 0)
                  )
                    return (
                      i ||
                        e.showMessage(
                          e.const.LANG.FORM_INPUT_REQUIRED_ERROR,
                          null,
                          function () {
                            var i = t.querySelector('[name="' + r[0] + '"]');
                            e.isEmpty(i) || i.focus();
                          }
                        ),
                      !1
                    );
                  var s = !0,
                    l = t.getElementsByClassName("prime-survey");
                  for (g = 0; g < l.length; g++) {
                    var c = e.findAncestor(l[g], "prime-element");
                    if (!e.isEmpty(c)) {
                      var d = e.runtime.eventData[c.id];
                      if (
                        !e.isEmpty(d) &&
                        d["option.survey_setting.input_required"] &&
                        !e.isEmpty(d["option.survey_setting.input_name"])
                      ) {
                        var p = c.id,
                          u = t.querySelector(
                            '.prime-element.prime-form-item-survey[data-name="' +
                              d["option.survey_setting.input_name"] +
                              '"]'
                          );
                        e.isEmpty(u) || (p = u.id);
                        var _ = window.ladi(p).value();
                        if (e.isEmpty(_)) {
                          s = !1;
                          break;
                        }
                      }
                    }
                  }
                  if (!s)
                    return (
                      e.showMessage(e.const.LANG.FORM_INPUT_REQUIRED_ERROR), !1
                    );
                  var y = !0,
                    g = 0,
                    f = function () {
                      var i = t.querySelector('[name="' + b[g].name + '"]');
                      e.isEmpty(i) || i.focus();
                    };
                  for (g = 0; g < b.length; g++)
                    if (!i || b[g].name == a) {
                      var v = n[b[g].name];
                      if (!e.isEmpty(v))
                        try {
                          if (
                            !new RegExp(
                              "^" + b[g].pattern + "$",
                              b[g].pattern_flag
                            ).test(v)
                          ) {
                            i || e.showMessage(b[g].title, null, f), (y = !1);
                            break;
                          }
                        } catch (t) {}
                    }
                  return y;
                },
                R = function (t, i) {
                  (n = {}), (o = {}), (r = {});
                  for (
                    var a = t.querySelectorAll(
                        ".prime-element .prime-form-item-container [name]"
                      ),
                      c = {},
                      d = null,
                      u = 0;
                    u < a.length;
                    u++
                  )
                    (d = a[u].getAttribute("name")),
                      (c[d] = parseInt(a[u].getAttribute("tabindex")) || 0);
                  var m = Object.keys(c).sort(function (t, e) {
                    return c[t] - c[e];
                  });
                  if (
                    m.only(e.runtime.list_set_value_name_country).length ==
                    e.runtime.list_set_value_name_country.length
                  )
                    for (var _ = 0; _ < m.length; _++) {
                      var y = e.runtime.list_set_value_name_country.indexOf(
                        m[_]
                      );
                      -1 != y && (m[_] = k[y]);
                    }
                  for (var g = 0; g < m.length; g++) n[m[g]] = "";
                  p = m;
                  for (var f = 0; f < a.length; f++) {
                    (d = a[f].getAttribute("name")),
                      a[f].required && -1 == P.indexOf(d) && P.push(d);
                    var v = null;
                    if ("INPUT" == a[f].tagName) {
                      v = a[f].getAttribute("type").trim().toLowerCase();
                      var h = a[f].getAttribute("pattern"),
                        E = a[f].getAttribute("title");
                      if (
                        ("email" == v
                          ? b.push({
                              name: d,
                              pattern:
                                '(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))',
                              pattern_flag: "gi",
                              title: e.const.LANG.FORM_INPUT_EMAIL_REGEX,
                            })
                          : e.isEmpty(h) ||
                            b.push({
                              name: d,
                              pattern: h,
                              title: e.isEmpty(E)
                                ? e.const.LANG.FORM_INPUT_TEXT_REGEX
                                : E,
                            }),
                        "checkbox" == v)
                      ) {
                        e.isArray(n[d]) || (n[d] = []),
                          a[f].checked && n[d].push(a[f].value);
                        continue;
                      }
                      if ("radio" == v) {
                        a[f].checked && (n[d] = a[f].value);
                        continue;
                      }
                    }
                    if (
                      ((n[d] = a[f].value),
                      a[f].classList.contains("prime-form-control-file") &&
                        ((n[d] = JSON.parse(
                          a[f].getAttribute("data-path-file") || "[]"
                        )),
                        (r[d] = !0)),
                      "coupon" == d &&
                        "INPUT" == a[f].tagName &&
                        "text" == v &&
                        "true" == a[f].getAttribute("data-replace-coupon") &&
                        (n[d] = e.runtime.tmp.current_use_coupon || ""),
                      "INPUT" == a[f].tagName &&
                        "date" == v &&
                        !e.isEmpty(n[d]))
                    ) {
                      var L = a[f].getAttribute("date-format") || "dd-mm-yyyy",
                        A = new Date(n[d]);
                      (L = (L = (L = (L = L.replaceAll(
                        "dd",
                        (A.getDate() < 10 ? "0" : "") + A.getDate()
                      )).replaceAll(
                        "mm",
                        (A.getMonth() + 1 < 10 ? "0" : "") + (A.getMonth() + 1)
                      )).replaceAll("yyyy", A.getFullYear())).replaceAll(
                        "yy",
                        A.getFullYear() - 2e3
                      )),
                        (n[d] = L);
                    }
                  }
                  i ||
                    T.forEach(function (t) {
                      e.isNull(n[t]) ||
                        (window
                          .ladi("_ladipage_" + t)
                          .set_cookie(n[t], 365, "/", window.location.host),
                        e.isArray(e.runtime.DOMAIN_SET_COOKIE) &&
                          e.runtime.DOMAIN_SET_COOKIE.forEach(function (e) {
                            e != window.location.host &&
                              window
                                .ladi("_ladipage_" + t)
                                .set_cookie(n[t], 365, "/", e);
                          }));
                    }),
                    (s = l(n));
                },
                F = function (t, i, a, o, s) {
                  var l = {
                    form_config_id: u,
                    ladi_form_id: m,
                    ladipage_id: e.runtime.ladipage_id,
                    tracking_form: [],
                    form_data: [],
                    data_key: o,
                  };
                  if (a) l.status_send = e.const.STATUS_SEND.capture;
                  else if (
                    ((l.status_send = e.const.STATUS_SEND.sendform), L)
                  ) {
                    var c = window.ladi("_checkout_token").get_cookie();
                    e.isEmpty(c) || (l.checkout_token = c);
                  }
                  if (!e.isEmpty(y)) {
                    var d = y.getElementsByClassName("ladiflow-widget")[0];
                    e.isEmpty(d) ||
                      (e.isObject(l.options) || (l.options = {}),
                      (l.options.ladiflow_trigger_id = d.getAttribute(
                        "ladiflow-trigger-id"
                      )),
                      (l.options.ladiflow_ref = d.getAttribute("ladiflow-ref")),
                      (l.options.ladiflow_checkbox_user_ref =
                        d.getAttribute("user_ref")),
                      (l.options.ladiflow_store_id =
                        d.getAttribute("ladiflow-store-id")));
                  }
                  e.isObject(l.options) &&
                    (l.options = JSON.stringify(l.options)),
                    e.isEmpty(g) || (l.total_revenue = g),
                    e.isEmpty(e.runtime.time_zone) ||
                      (l.time_zone = e.runtime.time_zone);
                  var E = Object.keys(LadiFormApi);
                  if (
                    (p.forEach(function (t) {
                      var i = n[t];
                      e.isArray(i) && 0 == i.length && (i = "");
                      var a = {
                        name: t,
                        value: (i = -1 != E.indexOf(t) ? LadiFormApi[t] : i),
                      };
                      r[t] && (a.is_file = !0), l.form_data.push(a);
                    }),
                    (E = E.except(p)).forEach(function (t) {
                      l.form_data.push({ name: t, value: LadiFormApi[t] });
                    }),
                    L)
                  ) {
                    if (!e.isNull(f)) {
                      l.form_data.push({
                        name: "cart_products",
                        value: f,
                        is_ladipage: !0,
                      }),
                        e.isEmpty(
                          e.runtime.tmp.add_to_cart_shipping_method_id
                        ) ||
                          l.form_data.push({
                            name: "cart_shipping",
                            value:
                              e.runtime.tmp.add_to_cart_shipping_method_id +
                              "|" +
                              (e.runtime.tmp.add_to_cart_fee_shipping || 0),
                            is_ladipage: !0,
                          });
                      var P = l.form_data.findIndex(function (t) {
                        return "coupon" == t.name;
                      });
                      -1 != P && (l.form_data[P].is_ladipage = !0);
                    }
                    e.isEmpty(g) ||
                      l.form_data.push({
                        name: "cart_revenue",
                        value: g,
                        is_ladipage: !0,
                      }),
                      e.isEmpty(e.runtime.tmp.current_use_coupon) ||
                        l.form_data.push({
                          name: "cart_coupon_amount",
                          value: e.runtime.tmp.add_to_cart_discount || 0,
                          is_ladipage: !0,
                        });
                  }
                  l.tracking_form.push({
                    name: "url_page",
                    value: window.location.href,
                  }),
                    e.runtime.has_popupx &&
                      l.tracking_form.push({
                        name: "origin_url_page",
                        value: e.runtime.tmp.popupx_origin_url,
                      }),
                    S.forEach(function (t) {
                      var i = w[t];
                      (i = e.isNull(i) ? "" : i),
                        l.tracking_form.push({ name: t, value: i });
                    }),
                    l.tracking_form.push({ name: "variant_url", value: A }),
                    l.tracking_form.push({
                      name: "variant_content",
                      value: e.generateVariantContentString(O, !0),
                    }),
                    e.isEmpty(_) ||
                      l.tracking_form.push({
                        name: e.const.REF_NAME,
                        value: _,
                      }),
                    (e.runtime.tmp["form_data_ladi_tmp_" + m] = e.copy(l));
                  var b = e.runtime.tmp["form_data_ladi_" + m];
                  e.isObject(b) && (l = e.copy(b)),
                    e.isEmpty(v) ||
                      ((l.captcha_token = v), (l.captcha_type = h)),
                    i.isFormOtp &&
                      (i.isSetOtpId &&
                        (function () {
                          var t = "_otp_id_" + m,
                            a = window.ladi(t).get_cookie(),
                            n = !1,
                            o = e.runtime.tmp["cookie_cache_otp_" + m];
                          e.isEmpty(a) &&
                            e.isObject(o) &&
                            !e.isEmpty(o[t]) &&
                            ((a = o[t]), (n = !0));
                          var r = new Date(),
                            s = [];
                          e.isEmpty(a)
                            ? (s = [e.randomId(), r.getTime(), 1])
                            : (((s = a.split("|"))[1] =
                                parseInt(s[1]) || Date.now()),
                              (s[2] = parseInt(s[2]) || 1),
                              i.isSendOtp || (s[2] += 1),
                              s.splice(3)),
                            s[2] <= 1
                              ? (l.otp_send = e.const.OTP_TYPE.send)
                              : (l.otp_send = e.const.OTP_TYPE.resend),
                            r.setTime(s[1] + 9e5),
                            (a = s.join("|")),
                            n
                              ? ((o[t] = a),
                                (e.runtime.tmp["cookie_cache_otp_" + m] = o))
                              : window.ladi(t).set_cookie(a, r),
                            s.splice(1),
                            (a = s.join("|")),
                            (l.otp_id = a);
                        })(),
                      (l.is_capture = i.isCapture),
                      (l.otp_type = e.const.OTP_TYPE.sms),
                      (l.status_send = e.const.STATUS_SEND.otp)),
                    i.isSendOtp &&
                      ((l.otp_code = i.otp_code),
                      (l.status_send = e.const.STATUS_SEND.sendform)),
                    e.isFunction(s) && s(l);
                },
                q = function (t) {
                  t.reset();
                  var i = t.getElementsByClassName("prime-survey-option");
                  for (K = 0; K < i.length; K++)
                    i[K].classList.remove("selected");
                  var a = t.querySelectorAll(
                    ".prime-element .prime-form-item-container .prime-form-checkbox-item input"
                  );
                  for (K = 0; K < a.length; K++) {
                    var n = e
                      .findAncestor(a[K], "prime-form-checkbox-item")
                      .querySelector("span");
                    e.isEmpty(n) ||
                      n.setAttribute("data-checked", a[K].checked);
                  }
                  var o = e.findAncestor(t, "prime-element");
                  if (!e.isEmpty(o)) {
                    for (
                      var r = document.querySelectorAll(
                          '.prime-form [data-submit-form-id="' + o.id + '"]'
                        ),
                        s = 0;
                      s < r.length;
                      s++
                    ) {
                      var l = e.findAncestor(r[s], "prime-form");
                      if (
                        !e.isEmpty(l) &&
                        ((l = e.findAncestor(l, "prime-element")), !e.isEmpty(l))
                      ) {
                        var c = l.querySelector(".prime-form-remove-coupon");
                        if (e.isEmpty(c)) {
                          var d = l.querySelector('input[name="coupon"]');
                          e.isEmpty(d) ||
                            ((d.value = ""), e.fireEvent(d, "change"));
                        } else c.click();
                      }
                    }
                    var p = document.getElementById(
                      o.getAttribute("data-form-id-before")
                    );
                    e.isEmpty(p) ||
                      ((p = p.getElementsByClassName("prime-form")[0]), q(p));
                  }
                  for (
                    var u = document.querySelectorAll(
                        '[data-combobox-type="delivery_method"]'
                      ),
                      m = 0;
                    m < u.length;
                    m++
                  )
                    u[m].hasAttribute("data-placeholder") &&
                      (u[m].innerHTML =
                        '<option value="">' +
                        u[m].getAttribute("data-placeholder") +
                        "</option>"),
                      u[m].setAttribute("data-selected", "");
                  for (
                    var _ = document.querySelectorAll(
                        ".prime-google-recaptcha-checkbox[data-widget-id]"
                      ),
                      y = 0;
                    y < _.length;
                    y++
                  ) {
                    var g = _[y].getAttribute("data-widget-id");
                    e.isEmpty(g) ||
                      (window.grecaptcha &&
                        e.isObject(e.runtime.tmp.google_captcha) &&
                        e.runtime.tmp.google_captcha.checkbox &&
                        e.isFunction(window.grecaptcha.reset) &&
                        window.grecaptcha.reset(g));
                  }
                  e.runtime.tmp.add_to_cart_shipping_method_id = null;
                },
                B = function (t, i, a, n) {
                  R(a, !1),
                    D(a, !1, null) &&
                      (F(0, i, !1, n, function (t) {
                        e.sendRequest(
                          "POST",
                          e.const.API_FORM_DATA,
                          JSON.stringify(t),
                          !0,
                          { "Content-Type": "application/json" }
                        );
                      }),
                      e.showMessage(e.const.LANG.FORM_SEND_DATA_NO_CONFIG),
                      q(a));
                },
                M = function () {
                  e.showMessage(e.const.LANG.FORM_CAPTCHA_ERROR);
                },
                Y = function (t, i, a, o, r) {
                  if (
                    ((s = {}),
                    (d = {}),
                    (p = []),
                    (P = []),
                    (b = []),
                    R(t, a),
                    D(t, a, o))
                  ) {
                    for (
                      var l = t.querySelectorAll(
                          ".prime-element .prime-form-item-container [name]"
                        ),
                        c = {},
                        u = null,
                        m = 0;
                      m < l.length;
                      m++
                    )
                      (u = e.findAncestor(l[m], "prime-element")),
                        (c[l[m].getAttribute("name")] = u.id);
                    var _ = i["option.form_setting"],
                      y = Object.keys(c);
                    if (
                      ((_.mapping_form_id || []).forEach(function (i) {
                        var n = document.getElementById(i);
                        if (!e.isEmpty(n)) {
                          n.setAttribute("data-form-id-before", t.id);
                          var s = e.runtime.eventData[n.id];
                          if (
                            !a ||
                            (!e.isEmpty(s) && s["option.form_auto_capture"])
                          ) {
                            e.isFunction(r.getOtp) &&
                              !e.isEmpty(s) &&
                              s["option.is_form_otp"] &&
                              (e.isFunction(r.beforeRunOtp) &&
                                r.beforeRunOtp(n),
                              (e.runtime.func_get_code_otp[n.id] = r.getOtp));
                            var d = [];
                            if (
                              (y.forEach(function (t) {
                                var i = window.ladi(c[t]).value();
                                l = n.querySelectorAll(
                                  '.prime-element .prime-form-item-container [name="' +
                                    t +
                                    '"]'
                                );
                                var a = [];
                                for (m = 0; m < l.length; m++)
                                  (u = e.findAncestor(l[m], "prime-element")),
                                    a.push(u.id),
                                    t == o && d.push(l[m]);
                                for (a = a.unique(), m = 0; m < a.length; m++)
                                  window.ladi(a[m]).value(i);
                              }),
                              a)
                            )
                              for (m = 0; m < d.length; m++)
                                e.fireEvent(d[m], "focusout");
                          }
                        }
                      }),
                      !a)
                    ) {
                      var g = i["option.data_event"];
                      if (!e.isArray(g)) {
                        var f = e.copy(_);
                        if (
                          ((g = []),
                          e.isObject(f) &&
                            ((f.type = f.event_type),
                            (f.value = f.event_value),
                            (f.is_hide_parent = f.event_is_hide_parent),
                            (f.change_index_type = f.event_change_index_type),
                            (f.change_index_number =
                              f.event_change_index_number),
                            !e.isEmpty(f.value)))
                        ) {
                          if (
                            ((f.type != e.const.DATA_ACTION_TYPE.section &&
                              f.type != e.const.DATA_ACTION_TYPE.popup) ||
                              g.push({
                                action_type: e.const.ACTION_TYPE.complete,
                                type: f.type,
                                action: f.value,
                              }),
                            (f.type == e.const.DATA_ACTION_TYPE.section &&
                              f.is_hide_parent) ||
                              f.type == e.const.DATA_ACTION_TYPE.popup)
                          ) {
                            var v = e.findAncestor(t, "prime-popup"),
                              h = e.findAncestor(t, "prime-section"),
                              E = null;
                            e.isEmpty(v)
                              ? e.isEmpty(h) || (E = h.id)
                              : (E = (v = e.findAncestor(v, "prime-element"))
                                  .id),
                              e.isEmpty(E) ||
                                g.push({
                                  action_type: e.const.ACTION_TYPE.complete,
                                  type: e.const.DATA_ACTION_TYPE.hidden_show,
                                  hidden_ids: [E],
                                  show_ids: [],
                                });
                          }
                          f.type == e.const.DATA_ACTION_TYPE.change_index &&
                            g.push({
                              action_type: e.const.ACTION_TYPE.complete,
                              type: f.type,
                              action: f.value,
                              change_index_type: f.change_index_type,
                              change_index_number: f.change_index_number,
                            });
                        }
                      }
                      I(t, g);
                    }
                    r.isRunTracking &&
                      e.runEventTracking(t.id, { is_form: !0 }, n);
                  }
                },
                V = function (i, a, l, c, _, A) {
                  if (
                    ((w = e.getURLSearchParams(null, null, !1)),
                    (n = {}),
                    (o = {}),
                    (r = {}),
                    (s = {}),
                    (d = {}),
                    (p = []),
                    (u = null),
                    (m = null),
                    (y = null),
                    (g = null),
                    (f = null),
                    (v = a.captcha_token),
                    e.isObject(e.runtime.tmp.google_captcha) &&
                      ((h = e.runtime.tmp.google_captcha.type),
                      !l && e.isEmpty(v) && a.hasOwnProperty("captcha_token")))
                  )
                    M();
                  else {
                    (P = []),
                      (b = []),
                      (L = e.findAncestor(i, "prime-popup")),
                      e.isEmpty(L)
                        ? (L = !1)
                        : ((L = e.findAncestor(L, "prime-element")),
                          (L = !e.isEmpty(L) && "POPUP_CHECKOUT" == L.id));
                    var T = i.getElementsByClassName("prime-form")[0];
                    if (!e.isEmpty(T)) {
                      var O = e.runtime.eventData[i.id];
                      if (!e.isEmpty(O)) {
                        if (O["option.is_add_to_cart"]) return;
                        (u = O["option.form_config_id"]),
                          (m = i.id),
                          (y = i),
                          (g =
                            parseFloatLadiPage(
                              O["option.form_purchase_value"]
                            ) || 0),
                          L &&
                            ((g = e.getCartCheckoutPrice(g)),
                            (f = e.getCartProducts()));
                      }
                      var N = null,
                        I = null,
                        k = null,
                        x = null;
                      if (l) {
                        if (e.isEmpty(c)) return;
                        if ((R(T, l), !D(T, l, _))) return;
                        if (
                          a.captcha &&
                          !e.isEmpty(O) &&
                          O["option.form_captcha"] &&
                          e.isObject(e.runtime.tmp.google_captcha)
                        ) {
                          if (window.grecaptcha)
                            if (
                              e.runtime.tmp.google_captcha.enterprise &&
                              window.grecaptcha.enterprise
                            )
                              window.grecaptcha.enterprise.ready(function () {
                                try {
                                  window.grecaptcha.enterprise
                                    .execute(
                                      e.runtime.tmp.google_captcha.api_key,
                                      { action: "submit" }
                                    )
                                    .then(function (t) {
                                      V(i, { captcha_token: t }, l, c, _, A);
                                    });
                                } catch (t) {}
                              });
                            else if (e.runtime.tmp.google_captcha.checkbox)
                              try {
                                (N = i.getElementsByClassName(
                                  "prime-google-recaptcha-checkbox"
                                )[0]),
                                  (I = i.getAttribute(
                                    "data-button-submit-other"
                                  )),
                                  e.isEmpty(I) ||
                                    (N = document.querySelector(
                                      "#" +
                                        I +
                                        " .prime-google-recaptcha-checkbox"
                                    )),
                                  (k = N.getAttribute("data-widget-id")),
                                  (x = window.grecaptcha.getResponse(k)),
                                  V(i, { captcha_token: x }, l, c, _, A);
                              } catch (t) {}
                            else
                              window.grecaptcha.ready(function () {
                                try {
                                  window.grecaptcha
                                    .execute(
                                      e.runtime.tmp.google_captcha.api_key,
                                      { action: "submit" }
                                    )
                                    .then(function (t) {
                                      V(i, { captcha_token: t }, l, c, _, A);
                                    });
                                } catch (t) {}
                              });
                          return;
                        }
                        F(0, A, l, c, function (t) {
                          var i = t.form_data.findIndex(function (t) {
                              return t.name == _;
                            }),
                            a = -1 != i ? t.form_data[i].value : null;
                          (!e.isEmpty(
                            e.runtime.tmp.capture_form_data_last[c + "_" + _]
                          ) &&
                            e.equals(
                              e.runtime.tmp.capture_form_data_last[c + "_" + _],
                              a
                            )) ||
                            ((e.runtime.tmp.capture_form_data_last[
                              c + "_" + _
                            ] = a),
                            e.sendRequest(
                              "POST",
                              e.const.API_FORM_DATA,
                              JSON.stringify(t),
                              !0,
                              { "Content-Type": "application/json" }
                            ));
                        });
                      } else if (e.isEmpty(O)) B(0, A, T, c);
                      else if (
                        (e.isNull(e.runtime.tmp.form_sending) &&
                          (e.runtime.tmp.form_sending = {}),
                        e.isNull(e.runtime.tmp.form_button_headline) &&
                          (e.runtime.tmp.form_button_headline = {}),
                        !e.runtime.tmp.form_sending[i.id])
                      ) {
                        var Y = function () {
                            e.runtime.tmp.form_sending[i.id] = !0;
                            var t = T.querySelector(
                              ".prime-button .prime-headline"
                            );
                            e.isNull(
                              e.runtime.tmp.form_button_headline[i.id]
                            ) &&
                              (e.runtime.tmp.form_button_headline[i.id] =
                                t.innerHTML),
                              A.isFormOtp || (t.innerHTML = "● ● ●");
                          },
                          H = function () {
                            delete e.runtime.tmp.form_sending[i.id],
                              (T.querySelector(
                                ".prime-button .prime-headline"
                              ).innerHTML =
                                e.runtime.tmp.form_button_headline[i.id]);
                          };
                        if (
                          a.captcha &&
                          !e.isEmpty(O) &&
                          O["option.form_captcha"] &&
                          e.isObject(e.runtime.tmp.google_captcha)
                        )
                          if (window.grecaptcha)
                            if (
                              (Y(),
                              e.runtime.tmp.google_captcha.enterprise &&
                                window.grecaptcha.enterprise)
                            )
                              window.grecaptcha.enterprise.ready(function () {
                                try {
                                  window.grecaptcha.enterprise
                                    .execute(
                                      e.runtime.tmp.google_captcha.api_key,
                                      { action: "submit" }
                                    )
                                    .then(function (t) {
                                      H(),
                                        V(i, { captcha_token: t }, l, c, _, A);
                                    })
                                    .catch(function () {
                                      M(), H();
                                    });
                                } catch (t) {
                                  M(), H();
                                }
                              });
                            else if (e.runtime.tmp.google_captcha.checkbox)
                              try {
                                (N = i.getElementsByClassName(
                                  "prime-google-recaptcha-checkbox"
                                )[0]),
                                  (I = i.getAttribute(
                                    "data-button-submit-other"
                                  )),
                                  e.isEmpty(I) ||
                                    (N = document.querySelector(
                                      "#" +
                                        I +
                                        " .prime-google-recaptcha-checkbox"
                                    )),
                                  (k = N.getAttribute("data-widget-id")),
                                  (x = window.grecaptcha.getResponse(k)),
                                  H(),
                                  V(i, { captcha_token: x }, l, c, _, A);
                              } catch (t) {
                                M(), H();
                              }
                            else
                              window.grecaptcha.ready(function () {
                                try {
                                  window.grecaptcha
                                    .execute(
                                      e.runtime.tmp.google_captcha.api_key,
                                      { action: "submit" }
                                    )
                                    .then(function (t) {
                                      H(),
                                        V(i, { captcha_token: t }, l, c, _, A);
                                    })
                                    .catch(function () {
                                      M(), H();
                                    });
                                } catch (t) {
                                  M(), H();
                                }
                              });
                          else e.showMessage(e.const.LANG.FORM_CAPTCHA_LOADING);
                        else {
                          var j = O["option.form_send_ladipage"],
                            G = O["option.form_api_data"],
                            U = O["option.thankyou_type"],
                            W = O["option.thankyou_value"],
                            X = O["option.deeplink_value"],
                            z = O["option.form_auto_funnel"],
                            K = O["option.form_thankyou_funnel"],
                            J = function () {
                              if (
                                (e.isObject(A) &&
                                  e.isFunction(A.callbackOtpShowThankYouDone) &&
                                  A.callbackOtpShowThankYouDone(),
                                U != e.const.FORM_THANKYOU_TYPE.url)
                              ) {
                                var a = e.findAncestor(i, "prime-popup");
                                e.isEmpty(a) ||
                                  ((a = e.findAncestor(a, "prime-element")),
                                  e.runRemovePopup(a.id, t));
                              }
                              var n = 0;
                              if (!e.runtime.isDesktop && !e.isEmpty(X)) {
                                n = 1e3;
                                var r = e.convertDataReplaceStr(
                                  X,
                                  !0,
                                  null,
                                  !1,
                                  d
                                );
                                window.ladi(r).open_url();
                              }
                              if (!K || e.isEmpty(E)) {
                                if (
                                  (U == e.const.FORM_THANKYOU_TYPE.default &&
                                    (e.isEmpty(W) || e.showMessage(W, d)),
                                  U == e.const.FORM_THANKYOU_TYPE.popup &&
                                    (z && e.setDataReplaceElement(!1, !1, d, W),
                                    window.ladi(W).show()),
                                  U == e.const.FORM_THANKYOU_TYPE.url &&
                                    !e.isEmpty(W))
                                ) {
                                  var s = window.ladi(W).get_url(o, z, !1);
                                  e.runTimeout(function () {
                                    window.ladi(s).open_url();
                                  }, n);
                                }
                              } else {
                                var l = window.ladi(E).get_url(o, z, !1);
                                e.runTimeout(function () {
                                  window.ladi(l).open_url();
                                }, n);
                              }
                            };
                          if (O["option.only_facebook_widget"]) {
                            if (
                              e.isObject(window.LadiFlow) &&
                              e.isFunction(window.LadiFlow.confirmCheckbox)
                            )
                              for (
                                var $ =
                                    i.getElementsByClassName("ladiflow-widget"),
                                  Z = 0;
                                Z < $.length;
                                Z++
                              )
                                window.LadiFlow.confirmCheckbox($[Z].id);
                            return (
                              e.runEventTracking(i.id, { is_form: !0 }, n),
                              void J()
                            );
                          }
                          if (e.isEmpty(u)) B(0, A, T, c);
                          else if ((R(T, l), D(T, l, _))) {
                            var Q = 0,
                              tt = 0,
                              et = null,
                              it = [],
                              at = !1,
                              nt = !1,
                              ot = !0,
                              rt = function (t) {
                                t && q(T), H();
                              },
                              st = function (t) {
                                if (
                                  -1 !=
                                    [
                                      e.const.FORM_CONFIG_TYPE.sapo,
                                      e.const.FORM_CONFIG_TYPE.shopify,
                                      e.const.FORM_CONFIG_TYPE.haravan,
                                      e.const.FORM_CONFIG_TYPE.wordpress,
                                    ].indexOf(
                                      e.runtime.shopping_product_type
                                    ) &&
                                  e.runtime.tmp.cart.length > 0
                                ) {
                                  var i = !1;
                                  -1 !=
                                    [
                                      e.const.FORM_CONFIG_TYPE.haravan,
                                      e.const.FORM_CONFIG_TYPE.wordpress,
                                    ].indexOf(
                                      e.runtime.shopping_product_type
                                    ) && (i = !0);
                                  var a = e.runtime.tmp.cart[0];
                                  return (
                                    (a = JSON.stringify(a)),
                                    (a = encodeURIComponent(a)),
                                    void e.removeAddToCartProduct(
                                      a,
                                      !1,
                                      i,
                                      function (i) {
                                        i ? st(t) : e.isFunction(t) && t();
                                      }
                                    )
                                  );
                                }
                                -1 !=
                                  [e.const.FORM_CONFIG_TYPE.ladisales].indexOf(
                                    e.runtime.shopping_product_type
                                  ) &&
                                  (window.ladi("_cart_token").delete_cookie(),
                                  window
                                    .ladi("_checkout_token")
                                    .delete_cookie(),
                                  e.createCartData()),
                                  e.isFunction(t) && t();
                              },
                              lt = function (t, a, r, l) {
                                if (r.readyState == XMLHttpRequest.DONE) {
                                  var c = {};
                                  try {
                                    c = JSON.parse(t);
                                  } catch (t) {}
                                  (c = e.isObject(c) ? c : {}),
                                    l == e.const.API_FORM_DATA
                                      ? 200 == c.code
                                        ? Q++
                                        : (1 != c.code ||
                                            e.isEmpty(c.message) ||
                                            (et = c.message),
                                          tt++,
                                          (ot = !1))
                                      : 200 == a ||
                                        201 == a ||
                                        e.getElementAHref(l).host ==
                                          e.const.DOMAIN_GOOGLE_DOCS
                                      ? Q++
                                      : tt++,
                                    Q + tt == it.length &&
                                      (ot && !at && Q >= 1
                                        ? ((at = !0),
                                          e.isFunction(A.callbackOtp) &&
                                            A.callbackOtp(!0),
                                          e.isObject(A) &&
                                          e.isFunction(A.callbackThankyou)
                                            ? (H(),
                                              A.callbackThankyou(
                                                A,
                                                function () {
                                                  st(function () {
                                                    rt(!0),
                                                      (e.runtime.tmp.current_use_coupon =
                                                        null);
                                                  });
                                                }
                                              ))
                                            : ((o = e.copy(n)),
                                              (d = e.copy(s)),
                                              Object.keys(
                                                e.runtime.replaceStr
                                              ).forEach(function (t) {
                                                o.hasOwnProperty(t) ||
                                                  (o[t] =
                                                    e.runtime.replaceStr[t]),
                                                  d.hasOwnProperty(t) ||
                                                    (d[t] =
                                                      e.runtime.replaceStr[t]);
                                              }),
                                              (function (t, i, a) {
                                                var n = null,
                                                  o = !1,
                                                  r = function (t, i) {
                                                    o ||
                                                      (e.isFunction(a) &&
                                                        a(t, i),
                                                      e.removeTimeout(n),
                                                      (o = !0));
                                                  };
                                                (n = e.runTimeout(r, 3e3)),
                                                  C(
                                                    "FormSubmit",
                                                    {
                                                      ladi_form_id: t,
                                                      total_revenue: i,
                                                    },
                                                    r
                                                  );
                                              })(i.id, g, function (t, a) {
                                                e.runEventTracking(
                                                  i.id,
                                                  { is_form: !0 },
                                                  n
                                                ),
                                                  window
                                                    .ladi("_capture_" + i.id)
                                                    .delete_cookie(),
                                                  e.isObject(A) &&
                                                    e.isFunction(
                                                      A.callbackOtpDone
                                                    ) &&
                                                    A.callbackOtpDone(),
                                                  st(function () {
                                                    e.runTimeout(function () {
                                                      rt(!0),
                                                        (e.runtime.tmp.current_use_coupon =
                                                          null),
                                                        J();
                                                    }, 500);
                                                  });
                                              })))
                                        : !nt &&
                                          tt >= 1 &&
                                          (e.isFunction(A.callbackOtp) &&
                                            A.callbackOtp(!1),
                                          (nt = !0),
                                          e.showMessage(
                                            et ||
                                              e.const.LANG.REQUEST_SEND_ERROR
                                          ),
                                          rt(!1)));
                                }
                              },
                              ct = function (t) {
                                it.push({
                                  url: e.const.API_FORM_DATA,
                                  data: JSON.stringify(t),
                                  async: !0,
                                  headers: {
                                    "Content-Type": "application/json",
                                  },
                                  callback: lt,
                                });
                              };
                            j && F(0, A, l, c, ct),
                              e.isArray(G) &&
                                G.forEach(function (t) {
                                  if (
                                    !e.isEmpty(t.api_url) &&
                                    e.isArray(t.custom_fields)
                                  ) {
                                    var i =
                                        e.getElementAHref(t.api_url).host ==
                                        e.const.DOMAIN_GOOGLE_DOCS,
                                      a = {},
                                      n = null,
                                      o = null,
                                      l = null,
                                      c = null;
                                    t.custom_fields.forEach(function (t) {
                                      if (
                                        (L &&
                                          ("cart_products" == t.ladi_name &&
                                            (n = t.name),
                                          "cart_revenue" == t.ladi_name &&
                                            (o = t.name),
                                          "cart_shipping" == t.ladi_name &&
                                            (l = t.name),
                                          "cart_coupon_amount" == t.ladi_name &&
                                            (c = t.name)),
                                        !r[t.ladi_name])
                                      ) {
                                        var i = s[t.ladi_name];
                                        e.isNull(i) ||
                                          (e.isArray(i)
                                            ? 0 == i.length
                                              ? (a[t.name] = "")
                                              : (a[t.name] = JSON.stringify(i))
                                            : (a[t.name] = i));
                                      }
                                    }),
                                      e.isEmpty(n) ||
                                        e.isNull(f) ||
                                        (a[n] = JSON.stringify(f)),
                                      e.isEmpty(o) || e.isNull(g) || (a[o] = g),
                                      e.isEmpty(l) ||
                                        e.isNull(
                                          e.runtime.tmp
                                            .add_to_cart_shipping_method_id
                                        ) ||
                                        (a[l] =
                                          e.runtime.tmp
                                            .add_to_cart_shipping_method_id +
                                          "|" +
                                          (e.runtime.tmp
                                            .add_to_cart_fee_shipping || 0)),
                                      e.isEmpty(c) ||
                                        e.isEmpty(
                                          e.runtime.tmp.current_use_coupon
                                        ) ||
                                        (a[c] =
                                          e.runtime.tmp.add_to_cart_discount ||
                                          0);
                                    var d = {};
                                    if (!e.isEmpty(t.api_request_header))
                                      try {
                                        var p = JSON.parse(
                                          t.api_request_header
                                        );
                                        Object.keys(p).forEach(function (t) {
                                          d[t] = p[t];
                                        });
                                      } catch (t) {}
                                    i ||
                                      ((a.link = window.location.href),
                                      e.runtime.has_popupx &&
                                        (a.origin_link =
                                          e.runtime.tmp.popupx_origin_url),
                                      Object.keys(LadiFormApi).forEach(
                                        function (t) {
                                          a[t] = LadiFormApi[t];
                                        }
                                      ),
                                      S.forEach(function (t) {
                                        var i = w[t];
                                        e.isNull(i) || (a[t] = i);
                                      }));
                                    var u = null,
                                      m =
                                        t.content_type ||
                                        e.const.CONTENT_TYPE.form_urlencoded;
                                    m == e.const.CONTENT_TYPE.form_urlencoded &&
                                      ((d["Content-Type"] =
                                        "application/x-www-form-urlencoded"),
                                      (u = Object.keys(a)
                                        .reduce(function (t, e) {
                                          return (
                                            t.push(
                                              e + "=" + encodeURIComponent(a[e])
                                            ),
                                            t
                                          );
                                        }, [])
                                        .join("&"))),
                                      m == e.const.CONTENT_TYPE.json &&
                                        ((d["Content-Type"] =
                                          "application/json"),
                                        (u = JSON.stringify(a))),
                                      m == e.const.CONTENT_TYPE.form_data &&
                                        ((u = new FormData()),
                                        Object.keys(a).forEach(function (t) {
                                          u.append(t, a[t]);
                                        })),
                                      it.push({
                                        url: t.api_url,
                                        data: u,
                                        async: !0,
                                        headers: d,
                                        callback: lt,
                                      });
                                  }
                                }),
                              it.length > 0
                                ? Y()
                                : j
                                ? B(0, A, T, c)
                                : (Y(), F(0, A, l, c, ct)),
                              it.forEach(function (t) {
                                e.sendRequest(
                                  "POST",
                                  t.url,
                                  t.data,
                                  t.async,
                                  t.headers,
                                  t.callback
                                );
                              });
                          }
                        }
                      }
                    }
                  }
                },
                H = function (t) {
                  var i = e.findAncestor(t.target, "prime-element");
                  if (!e.isEmpty(i))
                    for (
                      var a = i.querySelectorAll('[type="checkbox"]'), n = 0;
                      n < a.length;
                      n++
                    )
                      a[n].removeAttribute("required");
                },
                j = function (t) {
                  var i = e.findAncestor(t.target, "prime-element");
                  if (!e.isEmpty(i)) {
                    var a = e.runtime.eventData[i.id];
                    if (!e.isEmpty(a) && a["option.is_form_otp"]) return !1;
                    for (
                      var o = i.querySelectorAll(
                          '[prime-checkbox-required="true"]'
                        ),
                        r = -1,
                        l = 0;
                      l < o.length;
                      l++
                    )
                      if (
                        0 ==
                        o[l].querySelectorAll('[type="checkbox"]:checked')
                          .length
                      ) {
                        r = l;
                        break;
                      }
                    if (-1 == r) {
                      var c = function (t) {
                          var n = !1,
                            o = null;
                          e.isEmpty(a["option.form_config_id"]) ||
                            ((a["option.form_auto_capture"] || t.isFormOtp) &&
                              (o = x(i.id, a["option.form_config_id"])),
                            a["option.form_auto_capture"] && (n = !0));
                          var r = { captcha: !0 };
                          (t.isResendOtp || t.isSendOtp) && (r.captcha = !1),
                            t.isFormOtp && (t.isCapture = n),
                            V(i, r, !1, o, null, t);
                        },
                        u = function (t) {
                          window.ladi("_capture_" + i.id).delete_cookie(),
                            window.ladi("_otp_id_" + i.id).delete_cookie();
                          var n = [];
                          e.isObject(a["option.form_setting"]) &&
                            (n =
                              a["option.form_setting"].mapping_form_id || []),
                            n.forEach(function (i) {
                              window.ladi("_otp_time_" + i).delete_cookie(),
                                t &&
                                  delete e.runtime.tmp["cookie_cache_otp_" + i];
                            }),
                            t &&
                              (delete e.runtime.tmp["cookie_cache_otp_" + i.id],
                              delete e.runtime.tmp["form_data_ladi_" + i.id],
                              delete e.runtime.tmp[
                                "form_data_ladi_tmp_" + i.id
                              ]);
                        },
                        m = function (t) {
                          var i = t.getElementsByClassName("prime-form")[0],
                            a = e.findAncestor(t, [
                              "prime-popup",
                              "prime-element",
                            ]),
                            n = function () {
                              var a = t.getAttribute("data-form-id-before");
                              delete e.runtime.func_get_code_otp[t.id],
                                delete e.runtime.tmp[
                                  "cookie_cache_otp_" + t.id
                                ],
                                delete e.runtime.tmp["cookie_cache_otp_" + a],
                                delete e.runtime.tmp["form_data_ladi_" + a],
                                delete e.runtime.tmp["form_data_ladi_tmp_" + a],
                                window
                                  .ladi("_otp_time_" + t.id)
                                  .delete_cookie(),
                                window.ladi("_otp_id_" + a).delete_cookie(),
                                t.removeAttribute("data-form-id-before"),
                                (i.onsubmit = function () {
                                  return !1;
                                });
                            },
                            o = function () {
                              e.isEmpty(a) || window.ladi(a.id).hide(),
                                i.reset();
                            };
                          i.onsubmit = function () {
                            var i = t.querySelector('input[name="otp"]');
                            return (
                              e.isEmpty(i) ||
                                ((i = i.value.trim()),
                                c({
                                  isFormOtp: !0,
                                  isSetOtpId: !0,
                                  isSendOtp: !0,
                                  otp_code: i,
                                  callbackOtpDone: n,
                                  callbackOtpShowThankYouDone: o,
                                })),
                              !1
                            );
                          };
                        },
                        y = function (t, e) {
                          c({
                            isFormOtp: !0,
                            isSetOtpId: !0,
                            isResendOtp: t,
                            callbackOtp: e,
                            callbackThankyou: g,
                          });
                        },
                        g = function (t, n) {
                          t.isFormOtp &&
                            !t.isResendOtp &&
                            (function () {
                              var t = {};
                              (t["_capture_" + i.id] = null),
                                (t["_otp_id_" + i.id] = null);
                              var n = Object.keys(t);
                              n.forEach(function (e) {
                                t[e] = window.ladi(e).get_cookie();
                              }),
                                (e.runtime.tmp["cookie_cache_otp_" + i.id] = t);
                              var o = [];
                              e.isObject(a["option.form_setting"]) &&
                                (o =
                                  a["option.form_setting"].mapping_form_id ||
                                  []),
                                o.forEach(function (i) {
                                  ((t = {})["_otp_time_" + i] = null),
                                    (n = Object.keys(t)).forEach(function (e) {
                                      t[e] = window.ladi(e).get_cookie();
                                    }),
                                    (e.runtime.tmp["cookie_cache_otp_" + i] =
                                      t);
                                }),
                                (e.runtime.tmp["form_data_ladi_" + i.id] =
                                  e.runtime.tmp["form_data_ladi_tmp_" + i.id]);
                            })(),
                            u(!1),
                            Y(i, a, !1, null, {
                              isRunTracking: !1,
                              getOtp: y,
                              beforeRunOtp: m,
                            }),
                            e.isFunction(n) && n();
                        };
                      if (!e.isEmpty(a) && a["option.is_form_login"])
                        !(function (t, i) {
                          var a = t.querySelector('input[name="access_key"]');
                          if (!e.isEmpty(a) && !e.isEmpty(a.value)) {
                            for (var n = [], o = 0, r = 1; r <= 50; r++) {
                              var s = window
                                .ladi("_login_token_" + r)
                                .get_cookie();
                              0 == o && e.isEmpty(s) && (o = r), n.push(s);
                            }
                            if (0 == o) {
                              for (r = 1; r <= 50; r++)
                                window
                                  .ladi("_login_token_" + r)
                                  .delete_cookie();
                              (n = []), (o = 1);
                            }
                            e.sendRequest(
                              "POST",
                              e.const.API_ACCESS_KEY_LOGIN,
                              JSON.stringify({
                                tokens: n.removeSpace(),
                                url: window.location.href,
                                code: a.value.toUpperCase(),
                              }),
                              !0,
                              { "Content-Type": "application/json" },
                              function (t, i, a, r) {
                                if (a.readyState == XMLHttpRequest.DONE) {
                                  var s = {},
                                    l = e.const.LANG.FORM_LOGIN_SEND_ERROR;
                                  try {
                                    if (200 == (s = JSON.parse(t)).code) {
                                      var c = n.findIndex(function (t) {
                                        return t == s.data.token;
                                      });
                                      -1 != c && (o = c + 1),
                                        window
                                          .ladi("_login_token_" + o)
                                          .set_cookie(s.data.token, 7);
                                      var d = e.getElementAHref(s.data.url, !0),
                                        p = window.location.search;
                                      return (
                                        (p.startsWith("?") ||
                                          p.startsWith("&")) &&
                                          (p = p.substring(1)),
                                        e.isEmpty(p) ||
                                          (d.search =
                                            d.search +
                                            (e.isEmpty(d.search) ? "?" : "&") +
                                            p),
                                        void window.ladi(d.href).open_url()
                                      );
                                    }
                                    l = s.message || l;
                                  } catch (t) {}
                                  e.showMessage(l);
                                }
                              }
                            );
                          }
                        })(i);
                      else if (
                        !e.isEmpty(a) &&
                        e.isObject(a["option.form_setting"]) &&
                        a["option.form_setting"].is_multiple
                      )
                        a["option.form_setting"].is_multiple_otp
                          ? (u(!0),
                            y(!1, function (t) {
                              if (t) {
                                var i = [];
                                e.isObject(a["option.form_setting"]) &&
                                  (i =
                                    a["option.form_setting"].mapping_form_id ||
                                    []),
                                  i.forEach(function (t) {
                                    var i = document.getElementById(t);
                                    if (!e.isEmpty(i)) {
                                      var a = i.querySelector(
                                        ".prime-form-item .button-get-code"
                                      );
                                      e.isEmpty(a) ||
                                        (i.setAttribute(
                                          "data-start-countdown-otp",
                                          !0
                                        ),
                                        a.click());
                                    }
                                  });
                              }
                            }))
                          : Y(i, a, !1, null, { isRunTracking: !0 });
                      else {
                        if (
                          e.runtime.shopping_product_type ==
                            e.const.FORM_CONFIG_TYPE.ladisales &&
                          !e.isNull(e.runtime.shopping_config_checkout_id)
                        ) {
                          var f = e.findAncestor(i, "prime-popup");
                          if (
                            !e.isEmpty(f) &&
                            ((f = e.findAncestor(f, "prime-element")),
                            !e.isEmpty(f) && "POPUP_CHECKOUT" == f.id)
                          )
                            return (
                              (function (t) {
                                var i = window.ladi("_cart_token").get_cookie(),
                                  a = window
                                    .ladi("_checkout_token")
                                    .get_cookie();
                                (s = {}),
                                  (d = {}),
                                  (p = []),
                                  (P = []),
                                  (b = []),
                                  R(t, !1);
                                var o = n.email,
                                  r = n.phone;
                                if (e.isEmpty(i) || e.isEmpty(a))
                                  e.showMessage(
                                    e.const.LANG.FORM_INPUT_REQUIRED_ERROR
                                  );
                                else if (e.isEmpty(o) && e.isEmpty(r))
                                  e.showMessage(
                                    e.const.LANG.FORM_INPUT_REQUIRED_ERROR
                                  );
                                else {
                                  var l = (w = e.getURLSearchParams(
                                      null,
                                      null,
                                      !1
                                    )).utm_source,
                                    c = w.utm_medium,
                                    u = w.utm_campaign,
                                    m = w.utm_term,
                                    y = w.utm_content,
                                    g = n.name,
                                    f = n.address,
                                    v = n.message,
                                    h = n.country || "";
                                  h = h.split(":")[0];
                                  var E = n.state || "";
                                  E = E.split(":")[0];
                                  var L = n.district || "";
                                  L = L.split(":")[0];
                                  var A = n.ward || "";
                                  A = A.split(":")[0];
                                  var S = n.coupon,
                                    T =
                                      e.runtime.tmp
                                        .add_to_cart_shipping_method_id,
                                    O = e.getLadiSaleCheckoutCartProducts(),
                                    C = {
                                      cart_token: i,
                                      checkout_token: a,
                                      discount_code: S,
                                      customer_first_name: g,
                                      customer_email: o,
                                      customer_phone: r,
                                      customer_address: f,
                                      customer_note: v,
                                      customer_country_code: h,
                                      customer_state_id: E,
                                      customer_district_id: L,
                                      customer_ward_id: A,
                                      shipping_method_id: T,
                                      shipping_first_name: g,
                                      shipping_address: f,
                                      shipping_phone: r,
                                      shipping_email: o,
                                      shipping_country_code: h,
                                      shipping_state_id: E,
                                      shipping_district_id: L,
                                      shipping_ward_id: A,
                                      shipping_note: v,
                                      checkout_config_id:
                                        e.runtime.shopping_config_checkout_id,
                                      is_export_invoice: 0,
                                      is_generate_url: !0,
                                      variants: O,
                                      utm: {
                                        url_page: window.location.href,
                                        utm_source: l,
                                        utm_medium: c,
                                        utm_campaign: u,
                                        utm_term: m,
                                        utm_content: y,
                                      },
                                    };
                                  e.runtime.has_popupx &&
                                    (C.utm.origin_url_page =
                                      e.runtime.tmp.popupx_origin_url),
                                    e.isEmpty(_) ||
                                      (C.utm[e.const.REF_NAME] = _),
                                    (C.custom_fields = {});
                                  var N = Object.keys(n);
                                  (N = N.except([
                                    "name",
                                    "email",
                                    "phone",
                                    "address",
                                    "country",
                                    "state",
                                    "district",
                                    "ward",
                                    "message",
                                    "coupon",
                                  ])).forEach(function (t) {
                                    C.custom_fields[t] = n[t];
                                  }),
                                    e.sendRequest(
                                      "POST",
                                      e.const.API_LADISALE_CHECKOUT_CREATE,
                                      JSON.stringify(C),
                                      !0,
                                      { "Content-Type": "application/json" },
                                      function (t, i, a) {
                                        if (
                                          a.readyState == XMLHttpRequest.DONE
                                        ) {
                                          if (200 == i)
                                            try {
                                              var n = JSON.parse(t);
                                              if (
                                                200 == n.code &&
                                                e.isObject(n.data) &&
                                                !e.isEmpty(n.data.url)
                                              )
                                                return void window
                                                  .ladi(n.data.url)
                                                  .open_url();
                                              if (!e.isEmpty(n.message))
                                                return void e.showMessage(
                                                  n.message
                                                );
                                            } catch (t) {}
                                          e.showMessage(
                                            e.const.LANG.REQUEST_SEND_ERROR
                                          );
                                        }
                                      }
                                    );
                                }
                              })(i),
                              !1
                            );
                        }
                        c({ isFormOtp: !1 });
                      }
                    } else {
                      var v = o[r].querySelectorAll('[type="checkbox"]');
                      if (v.length > 0) {
                        v[0].setAttribute("required", "required");
                        for (var h = 0; h < v.length; h++)
                          v[h].removeEventListener("change", H),
                            v[h].addEventListener("change", H);
                        i.querySelector(".prime-form").reportValidity();
                      }
                    }
                  }
                  return !1;
                },
                G = function (t, i, a, n) {
                  var o = e.findAncestor(t.target, "prime-form");
                  if (!e.isEmpty(o)) {
                    var r = e.findAncestor(o, "prime-element");
                    if (
                      !(
                        e.isEmpty(r) ||
                        ((a = i ? a : e.runtime.eventData[r.id]),
                        e.isEmpty(a) ||
                          (!a["option.is_buy_now"] &&
                            !a["option.is_add_to_cart"]) ||
                          e.isEmpty(a["option.product_type"]) ||
                          e.isEmpty(a["option.product_id"]))
                      )
                    ) {
                      var s = function () {
                        var o = e.generateVariantProduct(
                          a,
                          !1,
                          null,
                          null,
                          null,
                          null,
                          !0,
                          !0,
                          s
                        );
                        if (
                          e.isEmpty(o) ||
                          e.isEmpty(o.store_info) ||
                          e.isEmpty(o.product)
                        )
                          e.isEmpty(
                            e.runtime.tmp.timeout_product_info[
                              a["option.product_type"]
                            ][a["option.product_id"]]
                          ) &&
                            e.showMessage(e.const.LANG.ADD_TO_CART_NO_PRODUCT, {
                              name: e.getMessageNameProduct(),
                            });
                        else {
                          var l = -1,
                            c = null;
                          if (i) {
                            var d = r.querySelector('[data-variant="true"]');
                            (c = e.getProductVariantId(d, o.product)),
                              e.isEmpty(c) ||
                                (l = o.product.variants.findIndex(function (t) {
                                  return t.product_variant_id == c;
                                }));
                          } else l = e.getProductVariantIndex(r.id, a);
                          if (-1 != l) {
                            var p = o.product.variants[l].product_id;
                            c = o.product.variants[l].product_variant_id;
                            var u = o.product.name,
                              m = o.product.variants[l].title,
                              _ = o.product.variants[l].price,
                              y = o.product.variants[l].quantity,
                              g = o.product.variants[l].quantity_stock;
                            y = e.isNull(g) ? y : g;
                            var f = e.isEmpty(o.product.variants[l].src)
                              ? ""
                              : o.product.variants[l].src;
                            if (
                              ((f =
                                e.isEmpty(f) && e.isObject(o.product.image)
                                  ? o.product.image.src
                                  : f),
                              e.isEmpty(f) ||
                                !e.isString(f) ||
                                f.startsWith("http://") ||
                                f.startsWith("https://") ||
                                f.startsWith("//") ||
                                (f =
                                  "https://" +
                                  e.const.STATIC_W_DOMAIN +
                                  "/" +
                                  f),
                              0 == o.product.select_many_service &&
                                e.isArray(e.runtime.tmp.cart) &&
                                -1 !=
                                  e.runtime.tmp.cart.findIndex(function (t) {
                                    return (
                                      t.product_id == p &&
                                      t.product_variant_id != c &&
                                      t.quantity > 0
                                    );
                                  }))
                            )
                              e.showMessage(
                                e.const.LANG.ADD_TO_CART_PRODUCT_ONLY_ONE,
                                {
                                  name: e.getMessageNameProduct(
                                    o.product.variants[l]
                                  ),
                                }
                              );
                            else {
                              var v = e.runtime.tmp.cart.findIndex(function (
                                  t
                                ) {
                                  return t.product_variant_id == c;
                                }),
                                h = !1;
                              if (-1 == v) {
                                h = !0;
                                var E = {
                                  store_id: o.store_info.id,
                                  product_id: p,
                                  product_variant_id: c,
                                  name: u,
                                  title: m,
                                  price: _,
                                  image: f,
                                  quantity: 0,
                                  min_buy: o.product.variants[l].min_buy,
                                  max_buy: o.product.variants[l].max_buy,
                                  inventory_checked:
                                    o.product.variants[l].inventory_checked,
                                  available_quantity: y,
                                  currency: o.store_info.currency,
                                  product_type:
                                    o.product.variants[l].product_type,
                                  package_quantity:
                                    o.product.variants[l].package_quantity,
                                };
                                e.isObject(E.currency) &&
                                  !e.isEmpty(E.currency.code) &&
                                  (E.currency.symbol = e.formatCurrency(
                                    null,
                                    E.currency.code,
                                    !1,
                                    !0
                                  )),
                                  e.runtime.tmp.cart.push(E),
                                  (v = e.runtime.tmp.cart.length - 1);
                              }
                              var P = r.querySelector('input[name="quantity"]');
                              if (e.isEmpty(P) || e.isEmpty(P.value))
                                e.showMessage(
                                  e.const.LANG.ADD_TO_CART_QUANTITY_REQUIRED
                                );
                              else {
                                var b = parseInt(P.value) || 0;
                                if (b <= 0)
                                  return void e.showMessage(
                                    e.const.LANG.ADD_TO_CART_QUANTITY_REQUIRED
                                  );
                                var L = null,
                                  A = 1;
                                A = o.product.variants[l].min_buy || A;
                                var w = o.product.variants[l].max_buy;
                                A > e.runtime.tmp.cart[v].quantity + b &&
                                  (b = A - e.runtime.tmp.cart[v].quantity);
                                var S = !1;
                                if (
                                  (!e.isEmpty(w) &&
                                    e.runtime.tmp.cart[v].quantity + b > w &&
                                    (b = w - e.runtime.tmp.cart[v].quantity) <=
                                      0 &&
                                    ((S = !0), (L = w)),
                                  1 == o.product.variants[l].inventory_checked)
                                ) {
                                  if (A > y)
                                    return void e.showMessage(
                                      e.const.LANG.ADD_TO_CART_NO_QUANTITY,
                                      {
                                        name: e.getMessageNameProduct(
                                          o.product.variants[l],
                                          !0
                                        ),
                                      }
                                    );
                                  e.runtime.tmp.cart[v].quantity + b > y &&
                                    ((b = y - e.runtime.tmp.cart[v].quantity),
                                    y > 0 &&
                                      ((S = !0),
                                      (e.isEmpty(L) || L > y) && (L = y)));
                                }
                                if (S || (b <= 0 && y > 0))
                                  e.showMessage(
                                    e.const.LANG.ADD_TO_CART_MAX_QUANTITY,
                                    {
                                      max: L,
                                      name: e.getMessageNameProduct(
                                        o.product.variants[l]
                                      ),
                                    },
                                    function () {
                                      var i = t.target;
                                      (i = e.findAncestor(i, "prime-button")),
                                        e.isEmpty(i) ||
                                          (i = e.findAncestor(
                                            i,
                                            "prime-element"
                                          ));
                                      var a = e.runtime.eventData[i.id];
                                      if (!e.isEmpty(a)) {
                                        var n = a["option.data_event"];
                                        if (
                                          !e.isArray(n) &&
                                          ((n = []),
                                          e.isObject(a["option.data_action"]))
                                        ) {
                                          var o = e.copy(
                                            a["option.data_action"]
                                          );
                                          (o.action_type =
                                            e.const.ACTION_TYPE.action),
                                            n.push(o);
                                        }
                                        n.forEach(function (t) {
                                          t.action_type ==
                                            e.const.ACTION_TYPE.action &&
                                            (t.type ==
                                              e.const.DATA_ACTION_TYPE
                                                .popup_cart &&
                                              window.ladi("POPUP_CART").show(),
                                            t.type ==
                                              e.const.DATA_ACTION_TYPE
                                                .popup_checkout &&
                                              (e.runtime.shopping_third_party
                                                ? e.getThirdPartyCheckoutUrl(!0)
                                                : window
                                                    .ladi("POPUP_CHECKOUT")
                                                    .show()));
                                        });
                                      }
                                    }
                                  );
                                else if (b > 0) {
                                  var T =
                                      !e.isEmpty(
                                        o.product.variants[l].start_date
                                      ) &&
                                      new Date(
                                        o.product.variants[l].start_date
                                      ).getTime() > Date.now(),
                                    O =
                                      !e.isEmpty(
                                        o.product.variants[l].end_date
                                      ) &&
                                      new Date(
                                        o.product.variants[l].end_date
                                      ).getTime() < Date.now();
                                  if (T)
                                    e.showMessage(
                                      e.const.LANG
                                        .ADD_TO_CART_PRODUCT_BEFORE_START_DATE,
                                      {
                                        name: e.getMessageNameProduct(
                                          o.product.variants[l]
                                        ),
                                      }
                                    );
                                  else if (O)
                                    e.showMessage(
                                      e.const.LANG
                                        .ADD_TO_CART_PRODUCT_AFTER_END_DATE,
                                      {
                                        name: e.getMessageNameProduct(
                                          o.product.variants[l]
                                        ),
                                      }
                                    );
                                  else {
                                    var C = function () {
                                      e.runtime.tmp.cart[v].quantity += b;
                                      var t = {
                                        product_id: p,
                                        product_variant_id: c,
                                        quantity: b,
                                      };
                                      (t.product_type =
                                        e.runtime.tmp.cart[v].product_type),
                                        (t.package_quantity =
                                          e.runtime.tmp.cart[
                                            v
                                          ].package_quantity),
                                        e.addCartCookie(
                                          o.store_info.id,
                                          t,
                                          function () {
                                            e.isFunction(n) && n();
                                            var t =
                                              document.getElementsByClassName(
                                                "prime-form-remove-coupon"
                                              )[0];
                                            e.isEmpty(t) || t.click(),
                                              e.updateCartPromotion();
                                          },
                                          function (t) {
                                            (e.runtime.tmp.cart[v].quantity -=
                                              b),
                                              h &&
                                                e.runtime.tmp.cart.splice(v, 1),
                                              e.showMessage(t.message);
                                          },
                                          function () {
                                            e.runtime.tmp.generateCart(),
                                              e.changeTotalPriceCart(),
                                              (e.runtime.tmp.is_click_add_to_cart =
                                                !1),
                                              0 == e.runtime.tmp.cart.length &&
                                                -1 !=
                                                  [
                                                    e.const.FORM_CONFIG_TYPE
                                                      .ladisales,
                                                  ].indexOf(
                                                    e.runtime
                                                      .shopping_product_type
                                                  ) &&
                                                (window
                                                  .ladi("_cart_token")
                                                  .delete_cookie(),
                                                window
                                                  .ladi("_checkout_token")
                                                  .delete_cookie()),
                                              e.runResizeAll();
                                          }
                                        );
                                    };
                                    if (
                                      e.isEmpty(
                                        window.ladi("_cart_token").get_cookie()
                                      )
                                    )
                                      if (e.runtime.tmp.is_click_add_to_cart) {
                                        var N = function () {
                                          e.runTimeout(function () {
                                            if (
                                              e.runtime.tmp.is_click_add_to_cart
                                            )
                                              return N();
                                            s();
                                          }, 100);
                                        };
                                        N();
                                      } else
                                        (e.runtime.tmp.is_click_add_to_cart =
                                          !0),
                                          C();
                                    else C();
                                  }
                                } else
                                  y <= 0 &&
                                    e.showMessage(
                                      e.const.LANG.ADD_TO_CART_NO_QUANTITY,
                                      {
                                        name: e.getMessageNameProduct(
                                          o.product.variants[l],
                                          !0
                                        ),
                                      }
                                    );
                              }
                            }
                          } else
                            e.showMessage(
                              e.const.LANG.ADD_TO_CART_PRODUCT_REQUIRED,
                              {
                                name: e.getMessageNameProduct(
                                  o.product.variants[l]
                                ),
                              }
                            );
                        }
                      };
                      s();
                    }
                  }
                },
                U = function (t) {
                  G(t, !1, null, function () {
                    var i = e.findAncestor(t.target, "prime-button");
                    i = e.findAncestor(i || t.target, "prime-element");
                    var a = e.runtime.eventData[i.id];
                    if (!e.isEmpty(a)) {
                      var n = a["option.data_event"];
                      if (
                        !e.isArray(n) &&
                        ((n = []), e.isObject(a["option.data_action"]))
                      ) {
                        var o = e.copy(a["option.data_action"]);
                        (o.action_type = e.const.ACTION_TYPE.action), n.push(o);
                      }
                      n.forEach(function (t) {
                        if (t.action_type == e.const.ACTION_TYPE.action) {
                          var i = null;
                          t.type == e.const.DATA_ACTION_TYPE.popup_cart &&
                            (i = "POPUP_CART"),
                            t.type == e.const.DATA_ACTION_TYPE.popup_checkout &&
                              (i = "POPUP_CHECKOUT"),
                            t.type == e.const.DATA_ACTION_TYPE.popup_checkout &&
                            e.runtime.shopping_third_party
                              ? e.getThirdPartyCheckoutUrl(!0)
                              : e.isEmpty(i) || window.ladi(i).show();
                        }
                      }),
                        e.runEventTracking(i.id, { is_form: !1 });
                    }
                  });
                },
                W = function (t) {
                  var i = e.findAncestor(t.target, "prime-form");
                  e.isEmpty(i) ||
                    ((i = e.findAncestor(i, "prime-element")),
                    e.isEmpty(i) || window.ladi(i.id).submit());
                },
                X = function (t) {
                  var i = e.findAncestor(t.target, "prime-form");
                  if (!e.isEmpty(i)) {
                    var a = e.findAncestor(i, "prime-element");
                    if (!e.isEmpty(a)) {
                      var n = e.runtime.eventData[a.id];
                      if (!e.isEmpty(n)) {
                        var o = {};
                        if (
                          e.isObject(n["option.form_setting"]) &&
                          n["option.form_setting"].is_multiple
                        ) {
                          if (!n["option.form_setting"].is_multiple_otp)
                            return void Y(
                              a,
                              n,
                              !0,
                              t.target.getAttribute("name"),
                              { isRunTracking: !0 }
                            );
                          (o.isFormOtp = !0), (o.isCapture = !0);
                        }
                        if (
                          n["option.form_auto_capture"] &&
                          !e.isEmpty(n["option.form_config_id"])
                        ) {
                          var r = x(a.id, n["option.form_config_id"]);
                          V(
                            a,
                            { captcha: !0 },
                            !0,
                            r,
                            t.target.getAttribute("name"),
                            o
                          );
                        }
                      }
                    }
                  }
                },
                z = {};
              T.forEach(function (t) {
                (z[t] = window.ladi("_ladipage_" + t).get_cookie()),
                  e.isEmpty(c[t]) || (z[t] = c[t]);
              }),
                (i = l(z));
              var K = 0,
                J = !1,
                $ = function (t) {
                  for (
                    var n = a[K].querySelectorAll(
                        '.prime-element .prime-form-item-container [name="' +
                          t +
                          '"]'
                      ),
                      o = 0;
                    o < n.length;
                    o++
                  ) {
                    var r = z[t];
                    if (!e.isEmpty(r))
                      if (
                        ("true" !=
                          n[o].getAttribute("data-is-select-country") &&
                          (r = i[t]),
                        "SELECT" == n[o].tagName)
                      )
                        n[o].querySelectorAll('option[value="' + r + '"]')
                          .length > 0 &&
                          ((n[o].value = r), J && e.fireEvent(n[o], "change"));
                      else {
                        if (
                          "country" == t &&
                          "true" == n[o].getAttribute("data-is-select-country")
                        )
                          continue;
                        (n[o].value = r), J && e.fireEvent(n[o], "change");
                      }
                  }
                },
                Z = function (t) {
                  (t.target.type = "date"), t.target.focus();
                },
                Q = function (t) {
                  e.isEmpty(t.target.value) && (t.target.type = "text");
                };
              for (K = 0; K < a.length; K++) {
                a[K].onsubmit = j;
                for (
                  var tt = e.findAncestor(a[K], "prime-element"),
                    et = a[K].getElementsByClassName("prime-button"),
                    it = 0;
                  it < et.length;
                  it++
                ) {
                  var at = a[K].getElementsByClassName("prime-button")[it];
                  if (!e.isEmpty(at)) {
                    var nt = e.findAncestor(at, "prime-element");
                    e.isEmpty(tt) ||
                    e.isEmpty(e.runtime.eventData[tt.id]) ||
                    !e.runtime.eventData[tt.id]["option.is_add_to_cart"]
                      ? nt.addEventListener("click", W)
                      : (nt.setAttribute("data-click", !1),
                        nt.addEventListener("click", U));
                  }
                }
                var ot = a[K].querySelector(
                  '[data-is-select-country="true"][name="country"]'
                );
                if (
                  !e.isEmpty(ot) &&
                  "true" == ot.getAttribute("data-no-ward")
                ) {
                  var rt = a[K].querySelector(
                    'select[data-is-select-country="true"][name="ward"]'
                  );
                  e.isEmpty(rt) || rt.removeAttribute("required");
                }
                for (
                  var st = a[K].querySelectorAll('input[data-type="date"]'),
                    lt = 0;
                  lt < st.length;
                  lt++
                )
                  e.isEmpty(st[lt].getAttribute("placeholder")) ||
                    (e.runtime.isDesktop || e.runtime.isBrowserDesktop
                      ? (st[lt].setAttribute("type", "text"),
                        st[lt].addEventListener("focus", Z),
                        st[lt].addEventListener("blur", Q))
                      : (st[lt].value = new Date()
                          .toISOString()
                          .substr(0, 10)));
                var ct = a[K].querySelectorAll("[tabindex]"),
                  dt = 0;
                for (lt = 0; lt < ct.length; lt++) {
                  var pt = parseInt(ct[lt].getAttribute("tabindex")) || 0;
                  pt > dt && (dt = pt),
                    ct[lt].setAttribute(
                      "tabindex",
                      e.runtime.tabindexForm + pt
                    );
                }
                e.runtime.tabindexForm += dt;
                for (var ut = 0; ut < N.length; ut++) {
                  var mt = a[K].querySelector(
                    '.prime-element .prime-form-item-container input[name="' +
                      N[ut] +
                      '"]'
                  );
                  e.isEmpty(mt) || mt.addEventListener("focusout", X);
                }
              }
              var _t = function (t, i) {
                for (J = i, K = 0; K < a.length; K++) {
                  var n = e.findAncestor(a[K], "prime-element");
                  !e.isEmpty(e.runtime.eventData[n.id]) &&
                    e.runtime.eventData[n.id]["option.form_auto_complete"] &&
                    t.forEach($);
                }
              };
              _t(e.copy(T).except(e.runtime.list_set_value_name_country)),
                e.runtime.tmp.listAfterLocation.push(function () {
                  var t = "",
                    i = "",
                    n = "",
                    o = "",
                    r = null,
                    s = function (t, e) {
                      var i = "" + r[t].name,
                        a = "" + r[e].name;
                      try {
                        return i.localeCompare(a);
                      } catch (t) {}
                      return i == a ? 0 : i > a ? 1 : -1;
                    },
                    l = function (a) {
                      var n = window.LadiLocation[i].data[a];
                      e.isEmpty(n) ||
                        (t +=
                          '<option value="' +
                          n.id +
                          ":" +
                          n.name +
                          '">' +
                          n.name +
                          "</option>");
                    },
                    c = function (t) {
                      var i =
                        window.LadiLocation[
                          t.target.getAttribute("data-country")
                        ].data[t.target.value.split(":")[0]];
                      if (((n = ""), !e.isEmpty(i))) {
                        var a = Object.keys(i.data);
                        (r = i.data),
                          a.sort(s),
                          a.forEach(function (t) {
                            var e = i.data[t];
                            n +=
                              '<option value="' +
                              e.id +
                              ":" +
                              e.name +
                              '">' +
                              e.name +
                              "</option>";
                          });
                      }
                      var o = e.findAncestor(t.target, "prime-element");
                      if (!e.isEmpty(o)) {
                        var l = o.querySelector('select[name="district"]');
                        e.isEmpty(l) ||
                          (l.setAttribute("data-selected", ""),
                          (l.innerHTML =
                            l.querySelector("option").outerHTML + n));
                        var c = o.querySelector('select[name="ward"]');
                        e.isEmpty(c) ||
                          (c.setAttribute("data-selected", ""),
                          (c.innerHTML = c.querySelector("option").outerHTML)),
                          e.reloadFeeShipping({ target: c });
                      }
                    },
                    d = function (t) {
                      var i = e.findAncestor(t.target, "prime-element");
                      if (!e.isEmpty(i)) {
                        var a = i.querySelector('select[name="ward"]');
                        if (!e.isEmpty(a)) {
                          a.setAttribute("data-selected", ""), (o = "");
                          var n = i.querySelector('select[name="state"]');
                          if (!e.isEmpty(n)) {
                            var l = n.getAttribute("data-selected");
                            if (!e.isEmpty(l)) {
                              l = l.split(":")[0];
                              var c =
                                window.LadiLocation[
                                  n.getAttribute("data-country")
                                ].data[l];
                              if (!e.isEmpty(c)) {
                                var d = c.data[t.target.value.split(":")[0]];
                                if (!e.isEmpty(d)) {
                                  var p = Object.keys(d.data);
                                  (r = d.data),
                                    p.sort(s),
                                    p.forEach(function (t) {
                                      var e = d.data[t];
                                      o +=
                                        '<option value="' +
                                        e.id +
                                        ":" +
                                        e.name +
                                        '">' +
                                        e.name +
                                        "</option>";
                                    });
                                }
                              }
                            }
                          }
                          a.innerHTML = a.querySelector("option").outerHTML + o;
                        }
                        e.reloadFeeShipping({ target: a });
                      }
                    },
                    p = function (t) {
                      var i = e.findAncestor(t.target, "prime-element");
                      if (!e.isEmpty(i)) {
                        var a = i.querySelector('select[name="ward"]');
                        e.reloadFeeShipping({ target: a });
                      }
                    };
                  for (K = 0; K < a.length; K++) {
                    var u = a[K].querySelectorAll(
                        '.prime-element .prime-form-item-container [name="state"]'
                      ),
                      m = 0,
                      _ = null;
                    for (m = 0; m < u.length; m++)
                      if (
                        ((_ = e.findAncestor(u[m], "prime-element")),
                        !e.isEmpty(_) &&
                          ((i =
                            e.runtime.eventData[_.id]["option.input_country"]),
                          !e.isEmpty(i) &&
                            ((t = ""),
                            (i = i.split(":")[0]),
                            !e.isEmpty(window.LadiLocation[i]))))
                      ) {
                        var y = window.LadiLocation[i].data,
                          g = Object.keys(y);
                        (r = y),
                          g.sort(s),
                          "VN" == i &&
                            ((g = g.except([
                              "201",
                              "202",
                              "224",
                              "220",
                              "203",
                            ])),
                            (g = ["201", "202", "224", "220", "203"].concat(
                              g
                            ))),
                          g.forEach(l),
                          (u[m].innerHTML =
                            u[m].querySelector("option").outerHTML + t),
                          u[m].setAttribute("data-country", i),
                          u[m].addEventListener("change", c);
                      }
                    var f = a[K].querySelectorAll(
                      '.prime-element .prime-form-item-container [name="district"]'
                    );
                    for (m = 0; m < f.length; m++)
                      f[m].addEventListener("change", d);
                    var v = a[K].querySelectorAll(
                      '.prime-element .prime-form-item-container [name="ward"]'
                    );
                    for (m = 0; m < v.length; m++)
                      v[m].addEventListener("change", p);
                  }
                }),
                e.runtime.tmp.listAfterLocation.push(function () {
                  _t(k, !0);
                }),
                (e.runtime.tmp.buttonAddToCartClick = G);
            })(),
            (function () {
              for (
                var t = document.querySelectorAll(
                    ".prime-form .prime-element .prime-form-control-file"
                  ),
                  i = function (t) {
                    var i = t.target,
                      a = i.getAttribute("data-click-id") || e.randomId();
                    i.setAttribute("data-click-id", a);
                    var n = document.getElementById("prime-input-file");
                    if (e.isEmpty(n)) {
                      (n = document.createElement("input")).id =
                        "prime-input-file";
                      var o = e.findAncestor(i, "prime-element");
                      (!e.isEmpty(o) && o.classList.contains("accept-all")) ||
                        n.setAttribute(
                          "accept",
                          ".jpg, .jpeg, .png, .gif, .svg, .ico, .mp3, .mp4, .ttf, .otf, .woff2, .txt, .doc, .docx, .xls, .xlsx, .pdf"
                        ),
                        n.setAttribute(
                          "style",
                          "position: absolute; top: 0; left: 0; visibility: hidden;"
                        ),
                        (n.multiple = !0),
                        (n.type = "file"),
                        document.body.appendChild(n);
                    }
                    n.setAttribute("data-file-click-id", a),
                      "true" != n.getAttribute("data-event") &&
                        (n.setAttribute("data-event", !0),
                        n.addEventListener("change", function (t) {
                          !(function (t, i, a) {
                            if (i.length > e.const.FORM_UPLOAD_FILE_LENGTH)
                              e.showMessage(
                                e.const.LANG.FORM_UPLOAD_FILE_MAX_LENGTH_ERROR,
                                { max_length: e.const.FORM_UPLOAD_FILE_LENGTH }
                              );
                            else {
                              for (
                                var n = new FormData(), o = 0, r = 0;
                                r < i.length;
                                r++
                              )
                                (o += i[r].size), n.append("file[]", i[r]);
                              if (
                                o >
                                1024 * e.const.FORM_UPLOAD_FILE_SIZE * 1024
                              )
                                e.showMessage(
                                  e.const.LANG.FORM_UPLOAD_FILE_MAX_SIZE_ERROR,
                                  { max_size: e.const.FORM_UPLOAD_FILE_SIZE }
                                );
                              else {
                                var s = {
                                  ladipage_id: e.runtime.ladipage_id,
                                  lang: e.runtime.lang,
                                };
                                n.append("json_data", JSON.stringify(s)),
                                  e.showLoadingBlur(),
                                  e.sendRequest(
                                    "POST",
                                    e.const.API_FILE_UPLOAD,
                                    n,
                                    !0,
                                    null,
                                    function (i, a, n) {
                                      if (n.readyState == XMLHttpRequest.DONE) {
                                        if ((e.hideLoadingBlur(), 200 == a))
                                          try {
                                            var o = JSON.parse(i);
                                            if (200 == o.code) {
                                              var r = [],
                                                s = [];
                                              return (
                                                o.data.success.forEach(
                                                  function (t) {
                                                    r.push(t.name),
                                                      s.push({
                                                        id: t._id,
                                                        path: t.path,
                                                        name: t.name,
                                                      });
                                                  }
                                                ),
                                                (t.value =
                                                  r.length > 0
                                                    ? "[" + r.join(", ") + "]"
                                                    : ""),
                                                void t.setAttribute(
                                                  "data-path-file",
                                                  JSON.stringify(s)
                                                )
                                              );
                                            }
                                            if (!e.isEmpty(o.message))
                                              return void e.showMessage(
                                                o.message
                                              );
                                          } catch (t) {}
                                        e.showMessage(
                                          e.const.LANG.REQUEST_SEND_ERROR
                                        );
                                      }
                                    }
                                  ),
                                  e.isFunction(a) && a();
                              }
                            }
                          })(
                            (i = document.querySelector(
                              '[data-click-id="' +
                                t.target.getAttribute("data-file-click-id") +
                                '"]'
                            )),
                            t.target.files,
                            function () {
                              t.target.value = null;
                            }
                          );
                        })),
                      n.click();
                  },
                  a = 0;
                a < t.length;
                a++
              ) {
                var n = t[a];
                (n.readOnly = !0),
                  n.style.setProperty("cursor", "pointer"),
                  n.addEventListener("click", i);
              }
            })(),
            (function () {
              for (
                var t = document.querySelectorAll(
                    ".prime-form .prime-element .prime-form-otp"
                  ),
                  i = function (t, a) {
                    var n = e.findAncestor(a, "prime-form");
                    if (
                      !e.isEmpty(n) &&
                      ((n = e.findAncestor(n, "prime-element")), !e.isEmpty(n))
                    ) {
                      var o = "_otp_time_" + n.id,
                        r = window.ladi(o).get_cookie(),
                        s = e.runtime.tmp["cookie_cache_otp_" + n.id];
                      e.isEmpty(r) &&
                        e.isObject(s) &&
                        !e.isEmpty(s[o]) &&
                        (r = s[o]);
                      var l =
                        (r = parseFloatLadiPage(r) || 0) +
                        e.runtime.time_otp -
                        Date.now();
                      (l = l < e.runtime.time_otp ? l : 0),
                        r <= 0 || l <= 0
                          ? (function (t) {
                              t.classList.add("otp-resend"),
                                t.classList.remove("otp-countdown"),
                                t.removeAttribute("data-countdown-time");
                            })(t)
                          : ((l = Math.ceil(l / 1e3)),
                            t.classList.remove("otp-resend"),
                            t.classList.add("otp-countdown"),
                            t.setAttribute("data-countdown-time", l),
                            e.runTimeout(function () {
                              i(t, a);
                            }, 1e3));
                    }
                  },
                  a = function (t) {
                    var a = e.findAncestor(t.target, "prime-form");
                    if (
                      !e.isEmpty(a) &&
                      ((a = e.findAncestor(a, "prime-element")), !e.isEmpty(a))
                    ) {
                      var n = "_otp_time_" + a.id,
                        o = !1,
                        r = e.runtime.tmp["cookie_cache_otp_" + a.id],
                        s = function () {
                          a.removeAttribute("data-start-countdown-otp");
                          var s = new Date();
                          s.setTime(s.getTime() + e.runtime.time_otp),
                            o
                              ? ((r[n] = Date.now()),
                                (e.runtime.tmp["cookie_cache_otp_" + a.id] = r))
                              : window.ladi(n).set_cookie(Date.now(), s);
                          var l = e.findAncestor(t.target, "prime-form-item");
                          e.runTimeout(function () {
                            i(l, t.target);
                          }, 1);
                        };
                      if (
                        "true" != a.getAttribute("data-start-countdown-otp")
                      ) {
                        var l = window.ladi(n).get_cookie();
                        e.isEmpty(l) &&
                          e.isObject(r) &&
                          !e.isEmpty(r[n]) &&
                          ((l = r[n]), (o = !0));
                        var c =
                          (l = parseFloatLadiPage(l) || 0) +
                          e.runtime.time_otp -
                          Date.now();
                        (c = c < e.runtime.time_otp ? c : 0),
                          (l <= 0 || c <= 0) &&
                            "true" != t.target.getAttribute("data-click") &&
                            e.isFunction(e.runtime.func_get_code_otp[a.id]) &&
                            (e.showLoadingBlur(),
                            t.target.setAttribute("data-click", !0),
                            e.runtime.func_get_code_otp[a.id](!0, function (i) {
                              t.target.removeAttribute("data-click"),
                                e.hideLoadingBlur(),
                                i && s();
                            }));
                      } else s();
                    }
                  },
                  n = 0;
                n < t.length;
                n++
              ) {
                var o = t[n],
                  r = e.findAncestor(o, "prime-form-item");
                if (!e.isEmpty(r)) {
                  r.classList.add("overflow-hidden");
                  var s = r.getElementsByClassName("button-get-code")[0];
                  e.isEmpty(s) &&
                    (((s = document.createElement("div")).className =
                      "button-get-code"),
                    (s.innerHTML = e.const.LANG.GET_CODE_BUTTON_TEXT),
                    s.addEventListener("click", a),
                    r.appendChild(s)),
                    s.classList.add("hide-visibility"),
                    o.style.setProperty(
                      "padding-right",
                      s.clientWidth + 5 + "px"
                    ),
                    s.classList.remove("hide-visibility"),
                    i(r, s);
                }
              }
            })(),
            (i = function () {
              y.forEach(function (t) {
                var i = e.runtime.eventData[t];
                if ("countdown" == i.type)
                  for (
                    var a = document.querySelectorAll("#" + t), n = 0;
                    n < a.length;
                    n++
                  ) {
                    var o = a[n],
                      r = o.getAttribute("data-type"),
                      s = 0,
                      l = 0,
                      c = Date.now();
                    if (
                      o.hasAttribute("data-date-start") ||
                      o.hasAttribute("data-date-end")
                    )
                      (s =
                        parseFloatLadiPage(o.getAttribute("data-date-start")) ||
                        0),
                        (l =
                          parseFloatLadiPage(o.getAttribute("data-date-end")) ||
                          0);
                    else {
                      if (r == e.const.COUNTDOWN_TYPE.countdown) {
                        var d = parseInt(o.getAttribute("data-minute")) || 0;
                        if (d <= 0) return;
                        for (l = e.runtime.timenow; l <= c; ) l += 60 * d * 1e3;
                      }
                      if (
                        (r == e.const.COUNTDOWN_TYPE.endtime &&
                          (l = parseInt(o.getAttribute("data-endtime")) || 0),
                        r == e.const.COUNTDOWN_TYPE.daily)
                      ) {
                        var p = o.getAttribute("data-daily-start"),
                          u = o.getAttribute("data-daily-end");
                        if (!e.isEmpty(p) && !e.isEmpty(u)) {
                          var m = new Date().toDateString();
                          (s = new Date(m + " " + p).getTime()),
                            (l = new Date(m + " " + u).getTime());
                        }
                      }
                      o.setAttribute("data-date-start", s),
                        o.setAttribute("data-date-end", l);
                    }
                    if (s > c) return;
                    var _ = l - c;
                    if (_ < 0) {
                      if (((_ = 0), "true" == o.getAttribute("data-end")))
                        return;
                      "true" != o.getAttribute("data-end") &&
                        (o.setAttribute("data-end", !0),
                        I(o, i["option.data_event"]),
                        e.runEventTracking(o.id, { is_form: !1 }));
                    }
                    for (
                      var y = e.getCountdownTime(_),
                        g = o.querySelectorAll("[data-item-type]"),
                        f = 0;
                      f < g.length;
                      f++
                    )
                      g[f].querySelectorAll(
                        ".prime-countdown-text span"
                      )[0].textContent = y[g[f].getAttribute("data-item-type")];
                  }
              });
            })(),
            (e.runtime.interval_countdown = e.runInterval(i, 1e3)),
            y.forEach(function (t) {
              var i = document.getElementById(t);
              e.isEmpty(i) || j(t, i);
            }),
            (e.runtime.interval_gallery = e.runInterval(H, 1e3)),
            y.forEach(function (t) {
              var i = e.runtime.eventData[t];
              if ("carousel" == i.type) {
                var a = document.getElementById(t);
                if (!e.isEmpty(a)) {
                  a.hasAttribute("data-scrolled") ||
                    (a.setAttribute("data-scrolled", !1),
                    (e.runtime.list_scroll_func[t] = function () {
                      a.setAttribute("data-scrolled", !0);
                    }));
                  var n =
                      i[e.runtime.device + ".option.carousel_setting.autoplay"],
                    o =
                      i[
                        e.runtime.device +
                          ".option.carousel_setting.autoplay_time"
                      ],
                    r = 0;
                  n && !e.isEmpty(o) && (r = o);
                  var s = function (i) {
                    i.stopPropagation(),
                      (i = e.getEventCursorData(i)),
                      (!e.isEmpty(e.runtime.timenext_carousel[t]) &&
                        e.runtime.timenext_carousel[t] > Date.now()) ||
                        i.target.classList.contains("prime-carousel-arrow") ||
                        ((e.runtime.timenext_carousel[t] = Date.now() + 864e5),
                        (e.runtime.current_element_mouse_down_carousel = t),
                        (e.runtime.current_element_mouse_down_carousel_position_x =
                          i.pageX),
                        a
                          .getElementsByClassName("prime-carousel-content")[0]
                          .style.setProperty("transition-duration", "0ms"),
                        a
                          .getElementsByClassName("prime-carousel-content")[0]
                          .setAttribute(
                            "data-left",
                            getComputedStyle(
                              a.getElementsByClassName(
                                "prime-carousel-content"
                              )[0]
                            ).left
                          ));
                  };
                  a
                    .getElementsByClassName("prime-carousel-arrow-left")[0]
                    .addEventListener("click", function (e) {
                      e.stopPropagation(),
                        a
                          .getElementsByClassName("prime-carousel-content")[0]
                          .style.removeProperty("transition-duration"),
                        a.setAttribute("data-is-next", !1),
                        a.setAttribute("data-next-time", Date.now() + 1e3 * r),
                        G(t, !1);
                    }),
                    -(
                      (parseFloatLadiPage(
                        i[e.runtime.device + ".option.carousel_crop.width"]
                      ) || 0) - a.clientWidth
                    ) < 0 &&
                      a
                        .getElementsByClassName("prime-carousel-arrow-right")[0]
                        .classList.remove("opacity-0"),
                    a
                      .getElementsByClassName("prime-carousel-arrow-right")[0]
                      .addEventListener("click", function (e) {
                        e.stopPropagation(),
                          a
                            .getElementsByClassName("prime-carousel-content")[0]
                            .style.removeProperty("transition-duration"),
                          a.setAttribute("data-is-next", !0),
                          a.setAttribute(
                            "data-next-time",
                            Date.now() + 1e3 * r
                          ),
                          G(t, !1);
                      }),
                    a
                      .getElementsByClassName("prime-carousel")[0]
                      .addEventListener("mousedown", s),
                    a
                      .getElementsByClassName("prime-carousel")[0]
                      .addEventListener(
                        "touchstart",
                        s,
                        e.runtime.scrollEventPassive
                      );
                }
              }
            }),
            (e.runtime.interval_carousel = e.runInterval(function () {
              y.forEach(function (t) {
                var i = e.runtime.eventData[t];
                if ("carousel" == i.type) {
                  var a = document.getElementById(t);
                  if (
                    !e.isEmpty(a) &&
                    "true" == a.getAttribute("data-scrolled") &&
                    "true" != a.getAttribute("data-stop")
                  ) {
                    var n =
                        i[
                          e.runtime.device + ".option.carousel_setting.autoplay"
                        ],
                      o =
                        i[
                          e.runtime.device +
                            ".option.carousel_setting.autoplay_time"
                        ],
                      r = 0;
                    if ((n && !e.isEmpty(o) && (r = o), r > 0)) {
                      var s = a.getAttribute("data-next-time"),
                        l = Date.now();
                      e.isEmpty(s) &&
                        ((s = l + 1e3 * (r - 1)),
                        a.setAttribute("data-next-time", s)),
                        l >= s &&
                          (G(t, !0),
                          a.setAttribute("data-next-time", l + 1e3 * r));
                    }
                  }
                }
              });
            }, 1e3)),
            (function () {
              var t = ["phone", "email", "coupon"],
                i = document.querySelectorAll(".prime-form .prime-button");
              e.runtime.tmp.list_form_checkout = [];
              for (
                var a = function (t, i) {
                    var a = e.findAncestor(t.target, "prime-form");
                    if (
                      !e.isEmpty(a) &&
                      ((a = a.querySelector("[data-submit-form-id]")),
                      !e.isEmpty(a))
                    ) {
                      var n = a.getAttribute("data-submit-form-id");
                      if (!e.isEmpty(n)) {
                        var o = document.querySelector(
                          "#" + n + ' .prime-form-item input[name="coupon"]'
                        );
                        e.isEmpty(o) ||
                          ((o.value = t.target.value), e.fireEvent(o, i));
                      }
                    }
                  },
                  n = function (t) {
                    a(t, "change");
                  },
                  o = function (t) {
                    a(t, "input");
                  },
                  r = function (t) {
                    if (e.isEmpty(e.runtime.tmp.current_use_coupon)) {
                      var i = e.findAncestor(t.target, "prime-form"),
                        a = i.querySelector('input[name="coupon"]');
                      e.isEmpty(a) || a.setAttribute("required", "required"),
                        i.reportValidity() && e.reloadPriceDiscount(t);
                    }
                  },
                  s = 0;
                s < i.length;
                s++
              ) {
                var l = e.findAncestor(i[s], "prime-element");
                if (!e.isEmpty(l)) {
                  var c = e.findAncestor(i[s], "prime-form");
                  if (!e.isEmpty(c)) {
                    var d = e.findAncestor(c, "prime-element");
                    if (!e.isEmpty(d)) {
                      var p = e.runtime.eventData[d.id];
                      if (!e.isEmpty(p)) {
                        var u = e.runtime.eventData[l.id];
                        if (
                          e.isEmpty(u) ||
                          e.isEmpty(u["option.data_submit_form_id"])
                        ) {
                          if (
                            !p["option.is_form_login"] &&
                            !p["option.is_form_coupon"]
                          ) {
                            var m = e.findAncestor(d, "prime-popup");
                            e.isEmpty(m) ||
                              (m = e.findAncestor(m, "prime-element")),
                              e.isEmpty(m) ||
                                "POPUP_CHECKOUT" != m.id ||
                                e.runtime.tmp.list_form_checkout.push(d.id);
                          }
                        } else if (p["option.is_form_coupon"]) {
                          l.setAttribute(
                            "data-submit-form-id",
                            u["option.data_submit_form_id"]
                          ),
                            l.addEventListener("click", r),
                            (c.onsubmit = function () {
                              return !1;
                            });
                          var _ = c.querySelector(
                            '.prime-form-item input[name="coupon"]'
                          );
                          if (!e.isEmpty(_)) {
                            _.addEventListener("change", n),
                              _.addEventListener("input", o);
                            var y = document.querySelector(
                              "#" +
                                u["option.data_submit_form_id"] +
                                ' .prime-form-item input[name="coupon"]'
                            );
                            e.isEmpty(y) ||
                              y.setAttribute("data-replace-coupon", !0);
                          }
                          e.runtime.tmp.list_form_checkout.push(
                            u["option.data_submit_form_id"]
                          );
                        }
                      }
                    }
                  }
                }
              }
              e.runtime.tmp.list_form_checkout =
                e.runtime.tmp.list_form_checkout.unique();
              for (
                var g = function (t) {
                    -1 ==
                      [
                        e.const.FORM_CONFIG_TYPE.sapo,
                        e.const.FORM_CONFIG_TYPE.haravan,
                        e.const.FORM_CONFIG_TYPE.shopify,
                        e.const.FORM_CONFIG_TYPE.wordpress,
                      ].indexOf(e.runtime.shopping_product_type) &&
                      e.reloadPriceDiscount();
                  },
                  f = 0;
                f < e.runtime.tmp.list_form_checkout.length;
                f++
              )
                for (
                  var v = document.querySelectorAll(
                      "#" +
                        e.runtime.tmp.list_form_checkout[f] +
                        " .prime-form-item input.prime-form-control"
                    ),
                    h = 0;
                  h < v.length;
                  h++
                )
                  -1 != t.indexOf(v[h].getAttribute("name")) &&
                    (v[h].addEventListener("change", g),
                    v[h].addEventListener("input", g));
            })(),
            (function () {
              y.forEach(function (t) {
                var i = document.getElementById(t);
                if (!e.isEmpty(i) && "true" != i.getAttribute("data-action")) {
                  var a = e.runtime.eventData[t],
                    n = a["option.data_event"];
                  if (
                    !e.isArray(n) &&
                    ((n = []), e.isObject(a["option.data_action"]))
                  ) {
                    var o = e.copy(a["option.data_action"]);
                    (o.action_type = e.const.ACTION_TYPE.action), n.push(o);
                  }
                  !(function (t, i) {
                    i.forEach(function (i) {
                      if (
                        i.action_type == e.const.ACTION_TYPE.action &&
                        i.type == e.const.DATA_ACTION_TYPE.link
                      )
                        if (e.isEmpty(t.getAttribute("href")))
                          t.removeAttribute("href");
                        else {
                          var a = e.getLinkUTMRedirect(t.href, null);
                          t.setAttribute("data-replace-href", a),
                            (t.href = e.convertDataReplaceStr(a, !0));
                        }
                    });
                  })(i, n);
                }
              });
              for (
                var t = document.querySelectorAll(
                    ".prime-headline a[href], .prime-paragraph a[href], .prime-list-paragraph a[href]"
                  ),
                  i = 0;
                i < t.length;
                i++
              )
                if (e.isEmpty(t[i].getAttribute("href")))
                  t[i].removeAttribute("href");
                else {
                  var a = e.getLinkUTMRedirect(t[i].href, null);
                  t[i].setAttribute("data-replace-href", a),
                    (t[i].href = e.convertDataReplaceStr(a, !0));
                }
            })(),
            t &&
              (e.runtime.is_popupx ||
                e.const.TIME_ONPAGE_TRACKING.forEach(function (t) {
                  e.runTimeout(function () {
                    e.isFunction(window.gtag) &&
                      window.gtag("event", "TimeOnPage_" + t + "_seconds", {
                        event_category: "LadiPageTimeOnPage",
                        event_label:
                          window.location.host + window.location.pathname,
                        non_interaction: !0,
                      }),
                      e.isFunction(window.fbq) &&
                        window.fbq(
                          "trackCustom",
                          "TimeOnPage_" + t + "_seconds"
                        );
                  }, 1e3 * t);
                })),
            y.forEach(function (t) {
              var i = document.getElementById(t);
              if (!e.isEmpty(i)) {
                var a = e.runtime.eventData[t],
                  n = a["option.data_tooltip.text"];
                if (!e.isEmpty(n)) {
                  var o =
                      a["option.data_tooltip.type"] ||
                      e.const.TOOLTIP_TYPE.default,
                    r =
                      a["option.data_tooltip.position"] ||
                      e.const.TOOLTIP_POSITION.top_middle,
                    s =
                      a["option.data_tooltip.size"] ||
                      e.const.TOOLTIP_SIZE.medium;
                  i.setAttribute("data-hint", n);
                  var l = "hint";
                  r == e.const.TOOLTIP_POSITION.top_middle &&
                    (l += "-top-middle"),
                    r == e.const.TOOLTIP_POSITION.top_left &&
                      (l += "-top-left"),
                    r == e.const.TOOLTIP_POSITION.top_right &&
                      (l += "-top-right"),
                    r == e.const.TOOLTIP_POSITION.bottom_middle &&
                      (l += "-bottom-middle"),
                    r == e.const.TOOLTIP_POSITION.bottom_left &&
                      (l += "-bottom-left"),
                    r == e.const.TOOLTIP_POSITION.bottom_right &&
                      (l += "-bottom-right"),
                    r == e.const.TOOLTIP_POSITION.left_middle &&
                      (l += "-left-middle"),
                    r == e.const.TOOLTIP_POSITION.left_top &&
                      (l += "-left-top"),
                    r == e.const.TOOLTIP_POSITION.left_bottom &&
                      (l += "-left-bottom"),
                    r == e.const.TOOLTIP_POSITION.right_middle &&
                      (l += "-right-middle"),
                    r == e.const.TOOLTIP_POSITION.right_top &&
                      (l += "-right-top"),
                    r == e.const.TOOLTIP_POSITION.right_bottom &&
                      (l += "-right-bottom"),
                    o == e.const.TOOLTIP_TYPE.info && (l += "-t-info"),
                    o == e.const.TOOLTIP_TYPE.success && (l += "-t-success"),
                    o == e.const.TOOLTIP_TYPE.error && (l += "-t-error"),
                    o == e.const.TOOLTIP_TYPE.notice && (l += "-t-notice"),
                    s == e.const.TOOLTIP_SIZE.small && (l += "-s-small"),
                    s == e.const.TOOLTIP_SIZE.medium && (l += "-s-med"),
                    s == e.const.TOOLTIP_SIZE.big && (l += "-s-big"),
                    (l += "-hint-anim-d-short"),
                    i.classList.add(l);
                }
              }
            }),
            (function () {
              var t = 2500,
                i = t,
                a = function (a) {
                  var n = !1;
                  if (
                    (e.const.ANIMATED_LIST.forEach(function (t) {
                      a.classList.contains(t) && (n = !0);
                    }),
                    n)
                  ) {
                    var o = a.getElementsByClassName(
                      "primeace-animated-words-wrapper"
                    )[0];
                    if (!e.isEmpty(o)) {
                      var r = e.isEmpty(o.getAttribute("data-word"))
                        ? []
                        : JSON.parse(o.getAttribute("data-word"));
                      if (0 != r.length) {
                        var s = !1,
                          l = e.randomId(),
                          c = function (i, a, n, o) {
                            if (!s) {
                              e.isEmpty(i) ||
                                (i.classList.remove("in"),
                                i.classList.add("out"));
                              var r = e.isEmpty(i) ? null : i.nextSibling;
                              if (
                                (e.isEmpty(r)
                                  ? n &&
                                    e.runTimeout(function () {
                                      _(p(a));
                                    }, t)
                                  : e.runTimeout(function () {
                                      c(r, a, n, o);
                                    }, o),
                                e.isEmpty(r) &&
                                  document
                                    .querySelectorAll("html")[0]
                                    .classList.contains("no-csstransitions"))
                              ) {
                                var l = p(a);
                                u(a, l);
                              }
                            }
                          },
                          d = function (i, a, n, o) {
                            if (!s) {
                              var r = a.parentElement,
                                l = r.parentElement;
                              l.classList.contains(
                                "primeace-animated-headline"
                              ) || (l = l.parentElement),
                                e.isEmpty(i) ||
                                  (i.classList.add("in"),
                                  i.classList.remove("out"));
                              var c = e.isEmpty(i) ? null : i.nextSibling;
                              e.isEmpty(c)
                                ? ((l.classList.contains("rotate-2") ||
                                    l.classList.contains("rotate-3") ||
                                    l.classList.contains("scale")) &&
                                    r.style.setProperty(
                                      "width",
                                      a.clientWidth + "px"
                                    ),
                                  e.isEmpty(e.findAncestor(a, "type")) ||
                                    e.runTimeout(function () {
                                      var t = e.findAncestor(
                                        a,
                                        "primeace-animated-words-wrapper"
                                      );
                                      e.isEmpty(t) ||
                                        t.classList.add("waiting");
                                    }, 200),
                                  n ||
                                    e.runTimeout(function () {
                                      _(a);
                                    }, t))
                                : e.runTimeout(function () {
                                    d(c, a, n, o);
                                  }, o);
                            }
                          },
                          p = function (t) {
                            if (!s) {
                              var i = t.nextSibling;
                              return e.isEmpty(i) ||
                                i.classList.contains("after")
                                ? t.parentElement.firstChild
                                : i;
                            }
                          },
                          u = function (t, e) {
                            s ||
                              (t.classList.remove("is-visible"),
                              t.classList.add("is-hidden"),
                              e.classList.remove("is-hidden"),
                              e.classList.add("is-visible"));
                          },
                          m = function (t, i) {
                            s ||
                              (e.isEmpty(e.findAncestor(t, "type"))
                                ? e.isEmpty(e.findAncestor(t, "clip")) ||
                                  (e
                                    .findAncestor(
                                      t,
                                      "primeace-animated-words-wrapper"
                                    )
                                    .style.setProperty(
                                      "width",
                                      t.clientWidth + 5 + "px"
                                    ),
                                  e.runTimeout(function () {
                                    _(t);
                                  }, 2100))
                                : (d(t.querySelectorAll("i")[0], t, !1, i),
                                  t.classList.add("is-visible"),
                                  t.classList.remove("is-hidden")));
                          },
                          _ = function (i) {
                            if (!s && !e.isEmpty(i)) {
                              var a = p(i);
                              if (e.isEmpty(e.findAncestor(i, "type")))
                                if (e.isEmpty(e.findAncestor(i, "letters")))
                                  e.isEmpty(e.findAncestor(i, "clip"))
                                    ? e.isEmpty(
                                        e.findAncestor(i, "loading-bar")
                                      )
                                      ? (u(i, a),
                                        e.runTimeout(function () {
                                          _(a);
                                        }, t))
                                      : (e
                                          .findAncestor(
                                            i,
                                            "primeace-animated-words-wrapper"
                                          )
                                          .classList.remove("is-loading"),
                                        u(i, a),
                                        e.runTimeout(function () {
                                          _(a);
                                        }, 3800),
                                        e.runTimeout(function () {
                                          e.findAncestor(
                                            i,
                                            "primeace-animated-words-wrapper"
                                          ).classList.add("is-loading");
                                        }, 800))
                                    : (e
                                        .findAncestor(
                                          i,
                                          "primeace-animated-words-wrapper"
                                        )
                                        .style.setProperty("width", "2px"),
                                      e.runTimeout(function () {
                                        u(i, a), m(a);
                                      }, 600));
                                else {
                                  var n =
                                    i.querySelectorAll("i").length >=
                                    a.querySelectorAll("i").length;
                                  c(i.querySelectorAll("i")[0], i, n, 50),
                                    d(a.querySelectorAll("i")[0], a, n, 50);
                                }
                              else {
                                var o = e.findAncestor(
                                  i,
                                  "primeace-animated-words-wrapper"
                                );
                                o.classList.add("selected"),
                                  o.classList.remove("waiting"),
                                  e.runTimeout(function () {
                                    o.classList.remove("selected"),
                                      i.classList.remove("is-visible"),
                                      i.classList.add("is-hidden");
                                    for (
                                      var t = i.querySelectorAll("i"), e = 0;
                                      e < t.length;
                                      e++
                                    )
                                      t[e].classList.remove("in"),
                                        t[e].classList.add("out");
                                  }, 500),
                                  e.runTimeout(function () {
                                    m(a, 150);
                                  }, 1300);
                              }
                            }
                          },
                          y = document.createElement(a.tagName);
                        a.parentElement.insertBefore(y, a.nextSibling),
                          (y.outerHTML = a.outerHTML),
                          (y = a.nextSibling).classList.add(
                            "primeace-animated-headline-duplicate"
                          ),
                          (e.runtime.list_scrolling_exec[l] = function () {
                            a.parentElement.removeChild(a),
                              y.classList.remove(
                                "primeace-animated-headline-duplicate"
                              ),
                              (s = !0),
                              delete e.runtime.list_scrolling_exec[l];
                          });
                        var g = o.textContent.trim();
                        if (
                          ((o.textContent = ""),
                          (o.innerHTML =
                            o.innerHTML +
                            '<b class="is-visible">' +
                            g +
                            "</b>"),
                          r.forEach(function (t) {
                            e.isEmpty(t)
                              ? (o.innerHTML = o.innerHTML + "<b>" + g + "</b>")
                              : (o.innerHTML =
                                  o.innerHTML + "<b>" + t.trim() + "</b>");
                          }),
                          !e.isEmpty(e.findAncestor(o, "type")) ||
                            !e.isEmpty(e.findAncestor(o, "loading-bar")) ||
                            !e.isEmpty(e.findAncestor(o, "clip")))
                        ) {
                          o.innerHTML =
                            o.innerHTML + '<div class="after"></div>';
                          for (
                            var f = getComputedStyle(o).color,
                              v = o.getElementsByClassName("after"),
                              h = 0;
                            h < v.length;
                            h++
                          )
                            v[h].style.setProperty("background-color", f);
                        }
                        if (
                          (a.classList.contains("type") &&
                            o.classList.add("waiting"),
                          (a.classList.contains("type") ||
                            a.classList.contains("rotate-2") ||
                            a.classList.contains("rotate-3") ||
                            a.classList.contains("scale")) &&
                            a.classList.add("letters"),
                          (function (t) {
                            if (!s)
                              for (var i = 0; i < t.length; i++) {
                                var a = t[i],
                                  n = a.textContent.trim().split(""),
                                  o = a.classList.contains("is-visible");
                                for (var r in n) {
                                  " " == n[r] && (n[r] = "&nbsp;");
                                  var l = e.findAncestor(a, "rotate-2");
                                  e.isEmpty(l) ||
                                    (n[r] = "<em>" + n[r] + "</em>"),
                                    (n[r] = o
                                      ? '<i class="in">' + n[r] + "</i>"
                                      : "<i>" + n[r] + "</i>");
                                }
                                var c = n.join("");
                                (a.innerHTML = c),
                                  a.style.setProperty("opacity", 1);
                              }
                          })(document.querySelectorAll(".letters b")),
                          a.classList.contains("loading-bar"))
                        )
                          (i = 3800),
                            e.runTimeout(function () {
                              o.classList.add("is-loading");
                            }, 800);
                        else if (a.classList.contains("clip")) {
                          var E = o.clientWidth + 5;
                          o.style.setProperty("width", E + "px");
                        }
                        e.runTimeout(function () {
                          _(a.getElementsByClassName("is-visible")[0]);
                        }, i);
                      }
                    }
                  }
                },
                n = function () {
                  for (
                    var t = document.getElementsByClassName(
                        "primeace-animated-headline"
                      ),
                      e = [],
                      i = 0;
                    i < t.length;
                    i++
                  )
                    e.push(t[i]);
                  e.forEach(a);
                };
              n();
              var o = e.randomId();
              e.runtime.list_scrolled_exec[o] = n;
            })(),
            (function () {
              for (
                var t = document.querySelectorAll(
                    ".prime-button-group > .prime-element"
                  ),
                  i = function (t) {
                    var i = e.findAncestor(t.target, "prime-button");
                    (i = e.isEmpty(i)
                      ? t.target
                      : e.findAncestor(i, "prime-element")).classList.add(
                      "selected"
                    );
                    var a = e.findAncestor(t.target, "prime-button-group");
                    if (!e.isEmpty(a))
                      for (
                        var n = (a = e.findAncestor(
                            a,
                            "prime-element"
                          )).querySelectorAll(
                            ".prime-button-group > .prime-element"
                          ),
                          o = 0;
                        o < n.length;
                        o++
                      )
                        n[o].id != i.id && n[o].classList.remove("selected");
                  },
                  a = 0;
                a < t.length;
                a++
              )
                t[a].addEventListener("click", i);
            })(),
            (function () {
              if (
                0 != document.getElementsByClassName("ladiflow-widget").length
              ) {
                var t = document.querySelector(
                  'script[src^="' +
                    e.const.LADIFLOW_SDK +
                    '"][data-time="' +
                    e.runtime.timenow +
                    '"]'
                );
                e.isEmpty(t) &&
                  e.loadScript(
                    e.const.LADIFLOW_SDK,
                    { "data-time": e.runtime.timenow },
                    !0
                  );
              }
            })(),
            U(null, !1),
            (function () {
              document.addEventListener("mouseleave", e.runEventMouseLeave),
                document.addEventListener("mousemove", e.runEventMouseMove),
                document.addEventListener(
                  "touchmove",
                  e.runEventMouseMove,
                  e.runtime.scrollEventPassive
                ),
                document.addEventListener("mouseup", e.runEventMouseUp),
                document.addEventListener("touchend", e.runEventMouseUp);
              var t = window;
              e.isObject(e.runtime.story_page) &&
                (t = document.getElementsByClassName("prime-wraper")[0]),
                t.addEventListener(
                  "scroll",
                  e.runEventScroll,
                  e.runtime.scrollEventPassive
                ),
                window.addEventListener("resize", e.runEventResize),
                window.addEventListener(
                  "orientationchange",
                  e.runEventOrientationChange
                );
              var i = document.getElementById(e.runtime.backdrop_popup_id);
              e.isEmpty(i) ||
                i.addEventListener("click", e.runEventBackdropPopupClick);
              var a = document.getElementById(e.runtime.backdrop_dropbox_id);
              e.isEmpty(a) ||
                a.addEventListener("click", e.runEventBackdropDropboxClick);
            })(),
            e.reloadLazyload(!0),
            (function () {
              if (t) {
                var i = function () {
                    if (
                      ((e.runtime.ladipage_powered_by_classname =
                        e.randomString(e.randomInt(6, 32))),
                      e.runtime.isClient)
                    ) {
                      var t = document.createElement("div");
                      document.body.insertBefore(
                        t,
                        document.body.childNodes[
                          e.randomInt(0, document.body.childNodes.length)
                        ]
                      ),
                        (t.className = e.runtime.ladipage_powered_by_classname);
                      var i =
                          "." +
                          e.runtime.ladipage_powered_by_classname +
                          ' {width: 140px; height: 30px; position: fixed; bottom: -40px; left: 10px; z-index: 10000000000; background: url("' +
                          e.const.POWERED_BY_IMAGE +
                          '") no-repeat center #fafafa; background-size: 90% 70%; border-radius: 4px 4px 0 0; display: block; animation: ' +
                          e.runtime.ladipage_powered_by_classname +
                          " 10s;} @keyframes " +
                          e.runtime.ladipage_powered_by_classname +
                          " {0% {bottom: -40px;} 10% {bottom: 0;} 90% {bottom: 0;} 100% {bottom: -40px;}}",
                        a = document.createElement("style");
                      (a.type = "text/css"),
                        document.head.insertBefore(
                          a,
                          document.head.childNodes[
                            e.randomInt(0, document.head.childNodes.length)
                          ]
                        ),
                        (a.innerHTML = i),
                        e.runTimeout(function () {
                          e.isEmpty(t) || t.parentElement.removeChild(t),
                            e.isEmpty(a) || a.parentElement.removeChild(a);
                        }, 1e4);
                    }
                  },
                  a = !1,
                  n = e.isArray(e.runtime.DOMAIN_FREE)
                    ? e.runtime.DOMAIN_FREE
                    : [],
                  o = window.location.href;
                ["/", ".", "/"].forEach(function (t) {
                  for (; o.endsWith(t); ) o = o.substr(0, o.length - t.length);
                });
                var r = e.getElementAHref(o).host.toLowerCase();
                n.forEach(function (t) {
                  a || (a = r.endsWith(t.toLowerCase()));
                }),
                  a && e.runTimeout(i, 3e3),
                  C("PageView", {}, function (t, n) {
                    if ((-1 != t || a || e.runTimeout(i, 3e3), 200 == t)) {
                      var o = JSON.parse(n),
                        r = !1,
                        s = null;
                      e.isObject(o.data)
                        ? ((r = 1 == o.data.verified_domain),
                          (s = o.data.google_captcha))
                        : (r = 1 == o.data),
                        a || r || e.runTimeout(i, 3e3),
                        e.isObject(s) &&
                          (function (t, i, a) {
                            if (!e.isEmpty(t)) {
                              var n = !1;
                              a.type ==
                                e.const.FORM_CONFIG_TYPE
                                  .google_recaptcha_enterprise && (i = !0),
                                a.type ==
                                  e.const.FORM_CONFIG_TYPE
                                    .google_recaptcha_checkbox && (n = !0),
                                (e.runtime.tmp.google_captcha = {
                                  api_key: t,
                                  enterprise: i,
                                  checkbox: n,
                                  type: a.type,
                                }),
                                (window.onloadRecaptchaCheckboxCallback =
                                  function () {
                                    for (
                                      var i = function (e) {
                                          var i = document.createElement("div");
                                          (i.className =
                                            "prime-google-recaptcha-checkbox"),
                                            e.insertBefore(i, a[n]);
                                          var o = window.grecaptcha.render(i, {
                                            sitekey: t,
                                          });
                                          i.setAttribute("data-widget-id", o);
                                        },
                                        a = document.querySelectorAll(
                                          ".prime-form .prime-button"
                                        ),
                                        n = 0;
                                      n < a.length;
                                      n++
                                    ) {
                                      var o = e.findAncestor(
                                          a[n],
                                          "prime-element"
                                        ),
                                        r = e.findAncestor(o, "prime-form");
                                      if (!e.isEmpty(r)) {
                                        r = e.findAncestor(r, "prime-element");
                                        var s = e.runtime.eventData[r.id];
                                        if (!e.isEmpty(s)) {
                                          if (
                                            s["option.is_form_login"] ||
                                            s["option.is_form_otp"] ||
                                            s["option.is_form_coupon"] ||
                                            s["option.is_add_to_cart"]
                                          )
                                            continue;
                                          if (
                                            e.isObject(
                                              s["option.form_setting"]
                                            ) &&
                                            s["option.form_setting"]
                                              .is_multiple &&
                                            !s["option.form_setting"]
                                              .is_multiple_otp
                                          )
                                            continue;
                                        }
                                      }
                                      i(o);
                                    }
                                    for (
                                      var l = document.querySelectorAll(
                                          "#POPUP_CHECKOUT .prime-button"
                                        ),
                                        c = 0;
                                      c < l.length;
                                      c++
                                    ) {
                                      var d = e.findAncestor(
                                          l[c],
                                          "prime-element"
                                        ),
                                        p = e.runtime.eventData[d.id];
                                      e.isEmpty(p) ||
                                        PrimePageScript.isEmpty(
                                          p["option.data_submit_form_id"]
                                        ) ||
                                        !p["option.is_submit_form"] ||
                                        i(d);
                                    }
                                  }),
                                i
                                  ? e.loadScript(
                                      "https://www.google.com/recaptcha/enterprise.js?render=" +
                                        t
                                    )
                                  : n
                                  ? e.loadScript(
                                      "https://www.google.com/recaptcha/api.js?onload=onloadRecaptchaCheckboxCallback&render=explicit"
                                    )
                                  : e.loadScript(
                                      "https://www.google.com/recaptcha/api.js?render=" +
                                        t
                                    );
                            }
                          })(s.site_key, s.enterprise, s);
                    }
                  });
              }
            })(),
            e.setDataReplaceStart(),
            e.resetViewport(),
            e.runConversionApi(),
            e.runStoryPage(),
            e.runThankyouPage(),
            e.runGlobalTrackingScript(),
            t || e.runAfterLocation(),
            (e.runtime.list_loaded_func = e.runtime.list_loaded_func.concat(g)),
            "complete" === document.readyState ||
            ("loading" !== document.readyState &&
              !document.documentElement.doScroll)
              ? e.documentLoaded()
              : document.addEventListener("DOMContentLoaded", e.documentLoaded);
        };
      (e.runtime.tmp.generateLadiSaleProduct = function (t, i, a) {
        var n = function () {
            y.forEach(function (n) {
              e.runtime.eventData[n], Q(n, t, i, null, null, !1, a);
            });
          },
          o = function (t) {
            if (t && e.isEmpty(a)) n();
            else {
              var i = a.target,
                r = e.findAncestor(i, "prime-element");
              if (!e.isEmpty(r)) {
                var s = e.findAncestor(r, "prime-form");
                if (!e.isEmpty(s)) {
                  var l = e.findAncestor(s, "prime-element");
                  if (!e.isEmpty(l)) {
                    var c = e.runtime.eventData[l.id];
                    if (!e.isEmpty(c)) {
                      var d = c["option.product_id"];
                      if (!e.isEmpty(d)) {
                        var p = e.generateVariantProduct(
                          c,
                          !1,
                          null,
                          null,
                          null,
                          null,
                          !0,
                          !0,
                          function (t) {
                            o(!1);
                          }
                        );
                        if (
                          e.isObject(p) &&
                          e.isObject(p.store_info) &&
                          e.isObject(p.product)
                        ) {
                          var u = e.getProductVariantIndex(l.id, c),
                            m = document.querySelectorAll(
                              '[data-variant="true"]'
                            );
                          if (-1 != u)
                            for (var _ = 0; _ < m.length; _++)
                              if (
                                m[_].id != r.id &&
                                e.isEmpty(
                                  e.findAncestor(m[_], "prime-collection")
                                )
                              ) {
                                var y = e.runtime.eventData[m[_].id];
                                if (!e.isEmpty(y)) {
                                  var g = e.findAncestor(m[_], "prime-form");
                                  if (!e.isEmpty(g)) {
                                    var f = e.findAncestor(g, "prime-element");
                                    if (!e.isEmpty(f)) {
                                      var v = e.runtime.eventData[f.id];
                                      if (
                                        !e.isEmpty(v) &&
                                        !e.isEmpty(
                                          v["option.product_variant_id"]
                                        )
                                      )
                                        continue;
                                    }
                                    var h = null,
                                      E = null,
                                      P = null,
                                      b = 0;
                                    if (
                                      y["option.product_variant_type"] ==
                                      e.const.PRODUCT_VARIANT_TYPE.combobox
                                    ) {
                                      if (!e.isArray(p.product.variants))
                                        continue;
                                      if (
                                        ((E = p.product.variants[u]),
                                        e.isString(E.option_ids))
                                      )
                                        for (
                                          P = E.option_ids.split("/"), b = 0;
                                          b < P.length;
                                          b++
                                        )
                                          (h = document.querySelector(
                                            "#" +
                                              m[_].id +
                                              ' .prime-form-item select[data-product-option-id="' +
                                              P[b] +
                                              '"]'
                                          )),
                                            e.isEmpty(h) ||
                                              h.getAttribute("data-store-id") !=
                                                p.store_info.id ||
                                              h.getAttribute(
                                                "data-product-id"
                                              ) != E.product_id ||
                                              (h.value = E["option" + (b + 1)]);
                                    }
                                    if (
                                      y["option.product_variant_type"] ==
                                      e.const.PRODUCT_VARIANT_TYPE.label
                                    ) {
                                      if (!e.isArray(p.product.variants))
                                        continue;
                                      if (
                                        ((E = p.product.variants[u]),
                                        e.isString(E.option_ids))
                                      )
                                        for (
                                          P = E.option_ids.split("/"), b = 0;
                                          b < P.length;
                                          b++
                                        )
                                          (h = document.querySelector(
                                            "#" +
                                              m[_].id +
                                              ' .prime-form-label-container[data-product-option-id="' +
                                              P[b] +
                                              '"]'
                                          )),
                                            e.isEmpty(h) ||
                                              h.getAttribute("data-store-id") !=
                                                p.store_info.id ||
                                              h.getAttribute(
                                                "data-product-id"
                                              ) != E.product_id ||
                                              e.runtime.tmp.updateLabelValue(
                                                h,
                                                E["option" + (b + 1)]
                                              );
                                    }
                                    if (
                                      y["option.product_variant_type"] ==
                                      e.const.PRODUCT_VARIANT_TYPE.combined
                                    ) {
                                      if (
                                        ((h = m[_].querySelector(
                                          "select.prime-form-control"
                                        )),
                                        e.isEmpty(h) ||
                                          h.getAttribute("data-store-id") !=
                                            p.store_info.id ||
                                          h.getAttribute("data-product-id") !=
                                            p.product.product_id)
                                      )
                                        continue;
                                      var L = e.getProductVariantId(r, p);
                                      if (!e.isEmpty(L)) {
                                        var A = h.querySelector(
                                          'option[data-product-variant-id="' +
                                            L +
                                            '"]'
                                        );
                                        e.isEmpty(A) ||
                                          (u = A.getAttribute("value"));
                                      }
                                      h.value = u + "";
                                    }
                                  }
                                }
                              }
                          for (var w = 0; w < m.length; w++) {
                            var S = e.findAncestor(m[w], "prime-form");
                            if (!e.isEmpty(S)) {
                              var T = S.querySelector('input[name="quantity"]');
                              e.isEmpty(T) || e.fireEvent(T, "input");
                            }
                          }
                          n();
                        }
                      }
                    }
                  }
                }
              }
            }
          };
        o(!0);
      }),
        (e.runtime.tmp.generateCart = function () {
          y.forEach(function (i) {
            !(function (i, a) {
              if ("cart" == a) {
                var n = e.runtime.eventData[i];
                if (!e.isEmpty(n)) {
                  var o = document.getElementById(i);
                  e.isEmpty(o) ||
                    e.showParentVisibility(o, function () {
                      var i =
                          parseFloatLadiPage(getComputedStyle(o).height) || 0,
                        a =
                          parseFloatLadiPage(o.getAttribute("data-height")) ||
                          0;
                      o.hasAttribute("data-height") ||
                        (o.setAttribute("data-height", i), (a = i));
                      var r = e.generateHtmlCart(
                        n["option.cart_layout"],
                        n["option.message_no_product"],
                        t
                      );
                      o.getElementsByClassName("prime-cart")[0].innerHTML = r;
                      var s =
                        o.getElementsByClassName("prime-cart")[0].scrollHeight;
                      if (i != (s = s < a ? a : s)) {
                        o.style.setProperty("height", s + "px");
                        var l = e.findAncestor(o.parentElement, "prime-element");
                        e.isEmpty(l) &&
                          (l = e.findAncestor(o.parentElement, "prime-section")),
                          e.updateHeightElement(!0, o, l, i, s);
                      }
                    });
                }
              }
            })(i, e.runtime.eventData[i].type);
          });
        }),
        (e.runtime.tmp.runButtonSectionClose = R),
        (e.runtime.tmp.runOptionAction = x),
        (e.runtime.tmp.runOptionHover = D),
        (e.runtime.tmp.runElementClickSelected = U),
        (e.runtime.tmp.runTrackingAnalytics = C),
        (e.runtime.tmp.runLadiSaleProductKey = Q),
        (e.runtime.tmp.eventClickGalleryControlItem = V),
        (e.runtime.tmp.runGallery = Y),
        (e.runtime.tmp.setGalleryStart = j),
        (e.runtime.tmp.updateImageGalleryProduct = Z),
        (e.runtime.tmp.runOptionCountdown = F),
        (e.runtime.tmp.runOptionCountdownItem = q),
        (e.runtime.tmp.getOptionLabelValue = X),
        (e.runtime.tmp.updateLabelValue = z),
        (e.runtime.tmp.getLabelValue = K),
        (e.runtime.tmp.clickLabelProductChangeCallback = J),
        (e.runtime.tmp.fireEventLabelChange = function (t) {
          var i = t.querySelector(".prime-form-label-item.selected");
          e.isEmpty(i) || e.fireEvent(i, "click", { is_fire_event: !0 });
        }),
        (e.runtime.tmp.showPopupX = nt),
        (e.runtime.tmp.runActionPopupX = tt),
        e.runtime.is_popupx
          ? ((e.runtime.tmp.popupx_iframe_id = e.randomId()),
            tt({
              ladipage_id: e.runtime.ladipage_id,
              action: { type: "set_iframe_loaded" },
            }),
            window.addEventListener("message", function (t) {
              try {
                var i = JSON.parse(t.data);
                if ("POPUPX" != i.type) return;
                i.iframe_id == e.runtime.tmp.popupx_iframe_id &&
                  i.action.value.forEach(function (t) {
                    !(function (t, i) {
                      var a = null,
                        n = null;
                      if ("set_style_device" == t) {
                        if (
                          (rt(i.is_desktop),
                          (n = document.getElementById(
                            e.runtime.tmp.popupx_current_element_id
                          )),
                          e.isEmpty(n))
                        )
                          return;
                        "none" != getComputedStyle(n).display &&
                          (e.runtime.tmp.popupx_is_inline
                            ? ot(e.runtime.tmp.popupx_current_element_id, !1)
                            : nt(e.runtime.tmp.popupx_current_element_id, !1));
                      }
                      if ("set_iframe_info" == t) {
                        e.isEmpty(f) &&
                          ((f = i.ladi_client_id || e.randomId()),
                          window.ladi("LADI_CLIENT_ID").set_cookie(f, 3650)),
                          (e.runtime.tmp.popupx_is_desktop = i.is_desktop),
                          (e.runtime.isDesktop = i.is_desktop),
                          (e.runtime.device = e.runtime.isDesktop
                            ? e.const.DESKTOP
                            : e.const.MOBILE),
                          (e.runtime.tmp.popupx_origin_store_id =
                            i.origin_store_id),
                          (e.runtime.tmp.popupx_origin_referer =
                            i.origin_referer),
                          (e.runtime.tmp.popupx_origin_domain =
                            i.origin_domain),
                          (e.runtime.tmp.popupx_origin_url = i.origin_url),
                          (e.runtime.tmp.popupx_is_inline = i.is_inline),
                          rt(i.is_desktop);
                        var o =
                          "#" +
                          e.runtime.builder_section_popup_id +
                          " .prime-container {width: 100% !important;}";
                        e.runtime.tmp.popupx_is_inline &&
                          (o +=
                            ".prime-section > .prime-section-close {display: none !important;}"),
                          e.createStyleElement("style_popup_container", o),
                          (e.runtime.has_popupx = !0),
                          st();
                      }
                      "hide_popupx" == t &&
                        ((a = e.runtime.eventData[i]),
                        (n = document.getElementById(i)),
                        e.isEmpty(a) || e.isEmpty(n) || window.ladi(i).hide()),
                        "show_popupx" == t && nt(i, !0),
                        "show_popupx_inline_iframe" == t && ot(i, !0),
                        "show_message_callback" == t &&
                          (e.isFunction(
                            e.runtime.tmp.popupx_show_message_callback
                          ) && e.runtime.tmp.popupx_show_message_callback(),
                          delete e.runtime.tmp.popupx_show_message_callback);
                    })(i.action.type, t);
                  });
              } catch (t) {}
            }))
          : st(),
        (e.runtime.isRun = !0);
    } else e.loadHtmlGlobal(t);
  }),
  (PrimePageScriptV2.prototype.getListProductByTagId = function (t, e, i, a, n) {
    var o = this,
      r = t["option.form_account_id"],
      s = t["option.product_type"],
      l = t["option.ladisale_store_id"] || null,
      c = t["option.product_tag_id"],
      d = t["option.data_setting.value"],
      p = t["option.data_setting.type_dataset"],
      u = t["option.collection_setting.type"],
      m = t["option.data_setting.sort_name"],
      _ = t["option.data_setting.sort_by"],
      y = null,
      g = null,
      f = null,
      v = null,
      h = null;
    if (o.isArray(c) && c.length > 0) {
      if (
        (o.isEmpty(o.runtime.tmp.product_tag_info[s]) &&
          (o.runtime.tmp.product_tag_info[s] = {}),
        o.isEmpty(o.runtime.tmp.timeout_product_tag_info[s]) &&
          (o.runtime.tmp.timeout_product_tag_info[s] = {}),
        c.sort(),
        (f = JSON.stringify(c) + "_page_" + i + "_limit_" + e),
        (v = o.runtime.tmp.product_tag_info[s][f]),
        -1 !=
          [
            o.const.FORM_CONFIG_TYPE.ladisales,
            o.const.FORM_CONFIG_TYPE.sapo,
            o.const.FORM_CONFIG_TYPE.haravan,
            o.const.FORM_CONFIG_TYPE.shopify,
            o.const.FORM_CONFIG_TYPE.wordpress,
          ].indexOf(s))
      ) {
        g = function () {
          var t = null;
          return (
            o.isObject(v) &&
              o.isArray(v.products) &&
              ((t = { products: v.products, total_record: v.total_record }),
              o.isEmpty(o.runtime.tmp.product_info[s]) &&
                (o.runtime.tmp.product_info[s] = {}),
              t.products.forEach(function (t) {
                o.runtime.tmp.product_info[s][t.product_id] = {
                  store_info: v.store_info,
                  product: t,
                };
              })),
            t
          );
        };
        var E = null;
        if ((o.isString(v) && ((E = v), (v = null)), o.isNull(v))) {
          o.runtime.tmp.product_tag_info[s][f] = !0;
          var P = function () {
              (o.runtime.tmp.product_tag_info[s][f] = !1),
                o.isEmpty(o.runtime.tmp.timeout_product_tag_info[s][f]) ||
                  (o.removeTimeout(
                    o.runtime.tmp.timeout_product_tag_info[s][f]
                  ),
                  delete o.runtime.tmp.timeout_product_tag_info[s][f]);
            },
            b = function (t) {
              if (((v = t.data), o.isObject(v))) {
                if (!o.isObject(v.store_info)) {
                  var a = o.runtime.currency;
                  o.runtime.isClient ||
                    (a = window.$rootScope.getStoreCurrency()),
                    (v.store_info = {
                      currency: {
                        code: a,
                        symbol: o.formatCurrency(null, a, !1, !0),
                      },
                    });
                }
                if (
                  (s != o.const.FORM_CONFIG_TYPE.ladisales &&
                    (v.store_info.id = -1),
                  o.isObject(v.store_info.currency) &&
                    !o.isEmpty(v.store_info.currency.code) &&
                    (v.store_info.currency.symbol = o.formatCurrency(
                      null,
                      v.store_info.currency.code,
                      !1,
                      !0
                    )),
                  o.isArray(v.products))
                )
                  for (
                    var r = null,
                      l = function (t) {
                        return t.option1 == r;
                      },
                      d = 0;
                    d < v.products.length;
                    d++
                  )
                    if (
                      o.isArray(v.products[d].options) &&
                      o.isArray(v.products[d].variants)
                    ) {
                      var p = v.products[d].options.map(function (t) {
                        return t.product_option_id;
                      });
                      p = p.join("/");
                      for (var u = 0; u < v.products[d].variants.length; u++)
                        -1 != [o.const.FORM_CONFIG_TYPE.ladisales].indexOf(s) &&
                          1 == v.products[d].variants[u].allow_sold_out &&
                          (v.products[d].variants[u].inventory_checked = 0),
                          o.isNull(v.products[d].variants[u].compare_price) &&
                            (v.products[d].variants[u].compare_price =
                              v.products[d].variants[u].price_compare),
                          o.isNull(
                            v.products[d].variants[u].variant_start_date
                          ) &&
                            (v.products[d].variants[u].variant_start_date =
                              v.products[d].variants[u].start_date),
                          o.isNull(
                            v.products[d].variants[u].variant_end_date
                          ) &&
                            (v.products[d].variants[u].variant_end_date =
                              v.products[d].variants[u].end_date),
                          o.isNull(
                            v.products[d].variants[u].variant_timezone
                          ) &&
                            (v.products[d].variants[u].variant_timezone =
                              v.products[d].variants[u].timezone),
                          o.isEmpty(v.products[d].variants[u].option_ids) &&
                            (v.products[d].variants[u].option_ids = p),
                          -1 !=
                            [
                              o.const.FORM_CONFIG_TYPE.sapo,
                              o.const.FORM_CONFIG_TYPE.haravan,
                              o.const.FORM_CONFIG_TYPE.shopify,
                            ].indexOf(s) &&
                            1 == v.products[d].variants.length &&
                            "Default Title" ==
                              v.products[d].variants[u].title &&
                            ((v.products[d].variants[u].title = null),
                            (v.products[d].variants[u].option1 = null),
                            (v.products[d].options = [])),
                          -1 !=
                            [o.const.FORM_CONFIG_TYPE.wordpress].indexOf(s) &&
                            1 == v.products[d].variants.length &&
                            v.products[d].variants[u].title ==
                              v.products[d].variants[u].product_name &&
                            ((v.products[d].variants[u].title = null),
                            (v.products[d].variants[u].option1 = null),
                            (v.products[d].options = [])),
                          o.isEmpty(
                            v.products[d].variants[u].package_quantity
                          ) ||
                            o.isEmpty(
                              v.products[d].variants[u].package_quantity_unit
                            ) ||
                            (o.isNull(v.products[d].variants[u].title_old) &&
                              (v.products[d].variants[u].title_old =
                                v.products[d].variants[u].title),
                            (v.products[d].variants[u].title =
                              v.products[d].variants[u].title_old +
                              " (" +
                              v.products[d].variants[u].package_quantity +
                              " " +
                              v.products[d].variants[u].package_quantity_unit +
                              ")"));
                      if (
                        o.isArray(v.products[d].options) &&
                        1 == v.products[d].options.length &&
                        o.isArray(v.products[d].options[0].values)
                      )
                        for (
                          var m = 0;
                          m < v.products[d].options[0].values.length;
                          m++
                        ) {
                          r = v.products[d].options[0].values[m].name;
                          var _ = v.products[d].variants.find(l);
                          (v.products[d].options[0].values[m].name_new =
                            v.products[d].options[0].values[m].label ||
                            v.products[d].options[0].values[m].name),
                            o.isEmpty(_) ||
                              o.isEmpty(_.package_quantity) ||
                              o.isEmpty(_.package_quantity_unit) ||
                              (v.products[d].options[0].values[m].name_new =
                                v.products[d].options[0].values[m].name_new +
                                " (" +
                                _.package_quantity +
                                " " +
                                _.package_quantity_unit +
                                ")");
                        }
                    }
                if (o.isString(v.page_next)) {
                  var h =
                    JSON.stringify(c) + "_page_" + (i + 1) + "_limit_" + e;
                  o.runtime.tmp.product_tag_info[s][h] = v.page_next;
                }
                (o.runtime.tmp.product_tag_info[s][f] = v),
                  (y = g()),
                  o.isFunction(n) && n(y);
              } else P();
            },
            L = { product_tag_ids: c, limit: e };
          o.isEmpty(u) ? (L.type = "group") : (L.paged = i),
            o.isEmpty(m) ||
              o.isEmpty(_) ||
              ((L.sort = {}),
              (L.sort[m] = _ == o.const.SORT_BY_TYPE.desc ? -1 : 1));
          var A = null,
            w = "POST";
          return (
            o.runLimitRequest(20, function () {
              if (o.runtime.isClient) {
                var a = o.const.API_LADISALE_COLLECTION_PRODUCT;
                s == o.const.FORM_CONFIG_TYPE.ladisales
                  ? (((A = { "Content-Type": "application/json" })["Store-Id"] =
                      l),
                    (L = JSON.stringify(L)))
                  : s == o.const.FORM_CONFIG_TYPE.wordpress
                  ? ((w = "GET"),
                    (a =
                      window.location.origin +
                      "/ladipage/api?action=product_list&category_ids=" +
                      c.join("|") +
                      "&page=" +
                      i +
                      "&limit=" +
                      e),
                    (L = null))
                  : ((A = { "Content-Type": "application/json" }),
                    (a = o.const.API_COLLECTION_PRODUCT),
                    (L = { form_account_id: r, tags: c, limit: e }),
                    o.isEmpty(E) ? (L.page = i) : (L.page_info = E),
                    (L = JSON.stringify(L))),
                  o.sendRequest(w, a, L, !0, A, function (t, e, i) {
                    if (i.readyState == XMLHttpRequest.DONE)
                      try {
                        var a = JSON.parse(t);
                        b(a);
                      } catch (t) {
                        P();
                      }
                  });
              } else {
                var n = function (t) {
                    if (o.isArray(t) && t.length > 1) {
                      var e = { products: [] };
                      t.forEach(function (t) {
                        if (
                          o.isNull(t.product) &&
                          !o.isNull(t.store_info) &&
                          !o.isNull(t.total_record)
                        )
                          return (
                            (e.store_info = t.store_info),
                            void (e.total_record = t.total_record)
                          );
                        o.isNull(t.product) || e.products.push(t.product);
                      }),
                        b({ data: e });
                    }
                  },
                  d = LadiPage.mapping_attribute_option_product_id(
                    t.element,
                    "",
                    !1,
                    c,
                    e,
                    i,
                    !0,
                    function (t) {
                      n(t);
                    }
                  );
                n(d);
              }
            }),
            y
          );
        }
      }
    } else
      o.isEmpty(d) ||
        ((g = function () {
          var t = null;
          if (o.isArray(h)) {
            var a = o.copy(h);
            (t = {
              products: (a = a.splice((i - 1) * e, e)),
              total_record: h.length,
            }),
              o.isEmpty(o.runtime.tmp.product_info[s]) &&
                (o.runtime.tmp.product_info[s] = {}),
              h.forEach(function (t) {
                o.runtime.tmp.product_info[s][t.id] = {
                  store_info: {},
                  product: t,
                };
              });
          }
          return t;
        }),
        (h = o.loadDataset(d, d, p, m, _, !0, o.runtime.isClient, function (t) {
          (h = t), (y = g()), o.isFunction(n) && n(y);
        })));
    return (
      o.isFunction(g) &&
        (!0 === v
          ? (o.runtime.tmp.timeout_product_tag_info[s][f] = o.runTimeout(
              function () {
                o.getListProductByTagId(t, e, i, !1, n);
              },
              100
            ))
          : ((y = g()), !a && o.isFunction(n) && n(y))),
      y
    );
  }),
  (PrimePageScriptV2.prototype.getLadiSaleCheckoutCartProducts = function () {
    var t = [];
    return (
      this.isArray(this.runtime.tmp.cart) &&
        this.runtime.tmp.cart.forEach(function (e) {
          e.quantity <= 0 ||
            t.push({
              product_id: e.product_id,
              product_variant_id: e.product_variant_id,
              quantity: e.quantity,
            });
        }),
      t
    );
  }),
  (PrimePageScriptV2.prototype.getCartProducts = function () {
    var t = this,
      e = function () {
        var e = [];
        return (
          t.runtime.tmp.cart.forEach(function (i) {
            if (!(i.quantity <= 0)) {
              var a = i.name;
              t.isEmpty(i.title) || a == i.title || (a += " - " + i.title);
              var n = [
                [
                  i.product_variant_id,
                  i.quantity,
                  i.price,
                  i.weight,
                  i.weight_unit,
                ].join(":"),
                a,
              ];
              t.isObject(i.promotion) &&
                n.push(
                  [
                    i.promotion.discount.discount_id,
                    i.promotion.discount.note,
                    i.promotion.total,
                  ].join(":")
                ),
                (n = n.join("|")),
                e.push(n);
            }
          }),
          e
        );
      };
    if (
      -1 !=
      [
        t.const.FORM_CONFIG_TYPE.sapo,
        t.const.FORM_CONFIG_TYPE.haravan,
        t.const.FORM_CONFIG_TYPE.shopify,
        t.const.FORM_CONFIG_TYPE.wordpress,
      ].indexOf(t.runtime.shopping_product_type)
    )
      return e();
    var i = window.ladi("_checkout_token").get_cookie();
    return this.isArray(this.runtime.tmp.cart) && !this.isEmpty(i)
      ? e()
      : void 0;
  }),
  (PrimePageScriptV2.prototype.getCartCheckoutPrice = function (t) {
    var e = this.changeTotalPriceCart(!0);
    if (
      -1 !=
      [
        this.const.FORM_CONFIG_TYPE.sapo,
        this.const.FORM_CONFIG_TYPE.haravan,
        this.const.FORM_CONFIG_TYPE.shopify,
        this.const.FORM_CONFIG_TYPE.wordpress,
      ].indexOf(this.runtime.shopping_product_type)
    )
      return e.cart_checkout_price;
    var i = window.ladi("_checkout_token").get_cookie();
    return this.isArray(this.runtime.tmp.cart) && !this.isEmpty(i)
      ? e.cart_checkout_price
      : t;
  }),
  (PrimePageScriptV2.prototype.createCartData = function (t) {
    (this.runtime.tmp.cart = []),
      (this.runtime.tmp.add_to_cart_discount = 0),
      (this.runtime.tmp.add_to_cart_fee_shipping = 0);
    var e = this,
      i = function () {
        e.runtime.tmp.generateCart(),
          e.changeTotalPriceCart(),
          e.runResizeAll(),
          e.isFunction(t) && t();
      };
    if (
      (e.isEmpty(e.runtime.shopping_product_type) ||
        (window.ladi("_shopping_product_type").get_cookie() !=
          e.runtime.shopping_product_type &&
          (window.ladi("_cart_token").delete_cookie(),
          window.ladi("_checkout_token").delete_cookie(),
          window
            .ladi("_shopping_product_type")
            .set_cookie(e.runtime.shopping_product_type, 30))),
      -1 ==
        [
          e.const.FORM_CONFIG_TYPE.sapo,
          e.const.FORM_CONFIG_TYPE.haravan,
          e.const.FORM_CONFIG_TYPE.wordpress,
        ].indexOf(e.runtime.shopping_product_type))
    )
      if (
        -1 ==
        [e.const.FORM_CONFIG_TYPE.shopify].indexOf(
          e.runtime.shopping_product_type
        )
      ) {
        var a = window.ladi("_cart_token").get_cookie(),
          n = { "Content-Type": "application/json" };
        if (!this.isEmpty(a))
          return (
            (n["cart-token"] = a),
            void this.sendRequest(
              "POST",
              this.const.API_LADISALE_SHOW,
              JSON.stringify({ type: "LP" }),
              !0,
              n,
              function (t, a, n) {
                if (n.readyState == XMLHttpRequest.DONE) {
                  if (200 == a)
                    try {
                      var o = JSON.parse(t);
                      200 == o.code &&
                        ((e.runtime.tmp.ladisales_checkout_url = o.data.url),
                        o.data.items.forEach(function (t) {
                          var i = e.isEmpty(t.src) ? "" : t.src;
                          e.isEmpty(i) ||
                            !e.isString(i) ||
                            i.startsWith("http://") ||
                            i.startsWith("https://") ||
                            i.startsWith("//") ||
                            (i =
                              "https://" + e.const.STATIC_W_DOMAIN + "/" + i);
                          var a = e.runtime.tmp.cart.findIndex(function (e) {
                              return (
                                e.store_id == t.store_id &&
                                e.product_id == t.product_id &&
                                e.product_variant_id == t.product_variant_id
                              );
                            }),
                            n = t.quantity,
                            r = t.option_name,
                            s = t.price;
                          if (
                            (e.isEmpty(t.package_quantity) ||
                              e.isEmpty(t.package_quantity_unit) ||
                              (r =
                                r +
                                " (" +
                                t.package_quantity +
                                " " +
                                t.package_quantity_unit +
                                ")"),
                            -1 == a)
                          ) {
                            var l = {
                              store_id: t.store_id,
                              product_id: t.product_id,
                              product_variant_id: t.product_variant_id,
                              name: t.product_name,
                              title: r,
                              price: s,
                              image: i,
                              quantity: n,
                              inventory_checked: t.inventory_checked,
                              available_quantity: t.available_quantity,
                              min_buy: t.min_buy,
                              max_buy: t.max_buy,
                              currency: o.data.store_info.currency,
                              product_type: t.product_type,
                              package_quantity: t.package_quantity,
                            };
                            e.isObject(l.currency) &&
                              !e.isEmpty(l.currency.code) &&
                              (l.currency.symbol = e.formatCurrency(
                                null,
                                l.currency.code,
                                !1,
                                !0
                              )),
                              e.runtime.tmp.cart.push(l);
                          }
                        }),
                        e.updateCartPromotion());
                    } catch (t) {}
                  i();
                }
              }
            )
          );
        i();
      } else
        e.getCheckoutShopify(
          null,
          function (t, a) {
            e.updateCartPromotion(null, !0, i);
          },
          function () {
            e.updateCartPromotion(null, !0, i);
          }
        );
    else this.updateCartPromotion(null, !0, i);
  }),
  (PrimePageScriptV2.prototype.changeTotalPriceCart = function (t) {
    var e = this,
      i = 0,
      a = 0;
    this.runtime.tmp.cart.forEach(function (t) {
      (a += t.quantity),
        e.isObject(t.promotion)
          ? (i += t.promotion.total)
          : (i += t.price * t.quantity);
    }),
      (i = i < 0 ? 0 : i);
    var n = this.runtime.tmp.add_to_cart_fee_shipping || 0,
      o = this.runtime.tmp.add_to_cart_discount || 0,
      r = i + n - o;
    if (((r = r < 0 ? 0 : r), t))
      return {
        cart_price: i,
        cart_checkout_price: r,
        cart_fee_shipping: n,
        cart_discount: o,
        total_quantity: a,
      };
    var s = this.formatNumber(i, 3),
      l = this.formatNumber(r, 3),
      c = this.formatNumber(n, 3),
      d = this.formatNumber(o, 3);
    if (
      this.runtime.tmp.cart.length > 0 &&
      !this.isEmpty(this.runtime.tmp.cart[0].currency) &&
      !this.isEmpty(this.runtime.tmp.cart[0].currency.symbol)
    ) {
      var p = this.runtime.tmp.cart[0].currency.symbol;
      (s = this.formatCurrency(i, p, !0)),
        (l = this.formatCurrency(r, p, !0)),
        (c = this.formatCurrency(n, p, !0)),
        (d = this.formatCurrency(o, p, !0));
    }
    this.setDataReplaceStr("cart_price", s),
      this.setDataReplaceStr("cart_checkout_price", l),
      this.setDataReplaceStr("cart_fee_shipping", c),
      this.setDataReplaceStr("cart_discount", d),
      this.setDataReplaceStr("cart_quantity", a),
      this.setDataReplaceElement(!1);
  }),
  (PrimePageScriptV2.prototype.removeAddToCartProduct = function (t, e, i, a) {
    var n = this,
      o = { product_variant_id: t };
    try {
      (t = decodeURIComponentLadiPage(t)), (o = JSON.parse(t));
    } catch (t) {}
    var r = function (t) {
        return n.isEmpty(t.cart_item_key)
          ? t.product_variant_id == o.product_variant_id
          : t.cart_item_key == o.cart_item_key;
      },
      s = this.runtime.tmp.cart.findIndex(r);
    -1 != s &&
      this.updateCartCookie(
        {
          cart_item_key: this.runtime.tmp.cart[s].cart_item_key,
          product_variant_id: this.runtime.tmp.cart[s].product_variant_id,
          quantity: 0,
        },
        i,
        function () {
          -1 != (s = n.runtime.tmp.cart.findIndex(r)) &&
            n.runtime.tmp.cart.splice(s, 1),
            i && (n.runtime.tmp.cart = []),
            n.runtime.tmp.generateCart(),
            n.changeTotalPriceCart();
          var t = document.getElementsByClassName("prime-form-remove-coupon")[0];
          n.isEmpty(t) || t.click(),
            n.updateCartPromotion(),
            0 == n.runtime.tmp.cart.length &&
              -1 !=
                [n.const.FORM_CONFIG_TYPE.ladisales].indexOf(
                  n.runtime.shopping_product_type
                ) &&
              (window.ladi("_cart_token").delete_cookie(),
              window.ladi("_checkout_token").delete_cookie()),
            n.runResizeAll(),
            n.isFunction(a) && a(!0);
        },
        function (t) {
          e && n.showMessage(t.message), n.isFunction(a) && a(!1);
        }
      );
  }),
  (PrimePageScriptV2.prototype.buttonAddToCartProductQuantity = function (t, e) {
    if (
      this.isArray(this.runtime.tmp.cart) &&
      0 != this.runtime.tmp.cart.length
    ) {
      var i = this.findAncestor(t, "prime-cart-quantity");
      if (!this.isEmpty(i)) {
        var a = i.querySelector("input");
        if (!this.isEmpty(a)) {
          var n = parseInt(a.value) || 0;
          (a.value =
            n + e < a.getAttribute("min") ? a.getAttribute("min") : n + e),
            a.value != n && this.fireEvent(a, "input");
        }
      }
    }
  }),
  (PrimePageScriptV2.prototype.changeAddToCartProductQuantity = function (
    t,
    e,
    i
  ) {
    var a = this,
      n = { product_variant_id: e };
    try {
      (e = decodeURIComponentLadiPage(e)), (n = JSON.parse(e));
    } catch (t) {}
    var o = function (t) {
        return a.isEmpty(t.cart_item_key)
          ? t.product_variant_id == n.product_variant_id
          : t.cart_item_key == n.cart_item_key;
      },
      r = this.runtime.tmp.cart.findIndex(o);
    if (-1 != r) {
      var s = !1,
        l = -1,
        c = 1,
        d = null,
        p = null,
        u = this.runtime.tmp.cart[r];
      a.isEmpty(u) || ((c = u.min_buy || c), (d = u.max_buy));
      var m = this.runtime.tmp.cart[r].quantity;
      if (this.isEmpty(t.value))
        i && (t.value = c), (this.runtime.tmp.cart[r].quantity = c);
      else {
        var _ = parseInt(t.value) || 0;
        1 == this.runtime.tmp.cart[r].inventory_checked &&
          _ > (l = this.runtime.tmp.cart[r].available_quantity) &&
          ((_ = l), (s = !0), (p = l)),
          (_ = _ < c ? c : _),
          !this.isEmpty(d) &&
            _ > d &&
            ((_ = d), (s = !0), (a.isEmpty(p) || p > d) && (p = d)),
          (this.runtime.tmp.cart[r].quantity = _),
          (t.value = _);
      }
      s &&
        this.showMessage(this.const.LANG.ADD_TO_CART_MAX_QUANTITY, {
          max: p,
          name: a.getMessageNameProduct(u),
        });
      var y = this.runtime.tmp.cart[r].quantity;
      if (m == y) return void (t.value = m);
      var g = {
        cart_item_key: this.runtime.tmp.cart[r].cart_item_key,
        product_variant_id: this.runtime.tmp.cart[r].product_variant_id,
        quantity: y,
      };
      (g.product_type = this.runtime.tmp.cart[r].product_type),
        (g.package_quantity = this.runtime.tmp.cart[r].package_quantity),
        this.updateCartCookie(
          g,
          !1,
          function () {
            var t = document.getElementsByClassName(
              "prime-form-remove-coupon"
            )[0];
            a.isEmpty(t) || t.click(), a.updateCartPromotion();
          },
          function (t) {
            (a.runtime.tmp.cart[r].quantity -= y - m), a.showMessage(t.message);
          },
          function () {
            if (
              (a.changeTotalPriceCart(),
              -1 != (r = a.runtime.tmp.cart.findIndex(o)))
            ) {
              var e =
                  a.runtime.tmp.cart[r].price * a.runtime.tmp.cart[r].quantity,
                i = a.formatNumber(e, 3);
              a.isObject(a.runtime.tmp.cart[r].currency) &&
                !a.isEmpty(a.runtime.tmp.cart[r].currency.symbol) &&
                (i = a.formatCurrency(
                  e,
                  a.runtime.tmp.cart[r].currency.symbol,
                  !0
                ));
              for (
                var n = document.querySelectorAll(
                    ".prime-cart-price span[data-product-variant-id]"
                  ),
                  s = 0;
                s < n.length;
                s++
              )
                if (
                  n[s].getAttribute("data-store-id") ==
                    a.runtime.tmp.cart[r].store_id &&
                  n[s].getAttribute("data-product-id") ==
                    a.runtime.tmp.cart[r].product_id
                ) {
                  if (a.isEmpty(a.runtime.tmp.cart[r].cart_item_key)) {
                    if (
                      n[s].getAttribute("data-product-variant-id") !=
                      a.runtime.tmp.cart[r].product_variant_id
                    )
                      continue;
                  } else if (
                    n[s].getAttribute("data-cart-item-key") !=
                    a.runtime.tmp.cart[r].cart_item_key
                  )
                    continue;
                  n[s].innerHTML = i;
                  var l = a.findAncestor(n[s], "prime-cart-row");
                  if (!a.isEmpty(l)) {
                    var c = a.findAncestor(t, "prime-element"),
                      d = a.findAncestor(l, "prime-element");
                    if (!a.isEmpty(c) && !a.isEmpty(d) && c.id == d.id)
                      continue;
                    var p = l.querySelector(".prime-cart-image-quantity");
                    a.isEmpty(p) ||
                      (p.innerHTML = a.runtime.tmp.cart[r].quantity);
                    var u = l.querySelector(".prime-cart-quantity input");
                    a.isEmpty(u) || (u.value = a.runtime.tmp.cart[r].quantity);
                  }
                }
            }
          }
        );
    }
  }),
  (PrimePageScriptV2.prototype.updateProductVariantSelectOption = function (
    t,
    e,
    i,
    a,
    n
  ) {
    var o = this,
      r = t.target,
      s = o.generateVariantProduct(
        e,
        !1,
        null,
        null,
        null,
        null,
        !0,
        !0,
        function (r) {
          o.updateProductVariantSelectOption(t, e, i, a, n);
        }
      );
    if (o.isObject(s)) {
      var l = o.getProductVariantId(r, s.product),
        c = o.findAncestor(r, "prime-collection-item"),
        d = [],
        p = 0;
      if (o.isEmpty(c))
        for (
          var u = document.querySelectorAll('[data-variant="true"]'), m = 0;
          m < u.length;
          m++
        ) {
          var _ = o.findAncestor(u[m], "prime-form");
          if (
            !o.isEmpty(_) &&
            ((_ = o.findAncestor(_, "prime-element")),
            o.isEmpty(o.findAncestor(_, "prime-collection")))
          ) {
            var y = o.runtime.eventData[_.id];
            o.isEmpty(y) ||
              y["option.product_type"] != e["option.product_type"] ||
              y["option.product_id"] != e["option.product_id"] ||
              d.push(u[m]);
          }
        }
      else d = c.querySelectorAll('[data-variant="true"]');
      var g = [];
      for (p = 0; p < d.length; p++) {
        if (a) {
          var f = o.findAncestor(d[p], "prime-popup");
          if (o.isEmpty(f)) continue;
          if (
            "POPUP_PRODUCT" != (f = o.findAncestor(f, "prime-element")).id &&
            "POPUP_BLOG" != f.id
          )
            continue;
        }
        g.push(d[p]);
      }
      var v = r.getAttribute("data-product-option-id"),
        h = null,
        E = null,
        P = null;
      if (o.isArray(s.product.variants) && 0 != s.product.variants.length) {
        if (o.isString(s.product.variants[0].option_ids)) {
          for (
            P = s.product.variants[0].option_ids.split("/"), p = 0;
            p < P.length;
            p++
          )
            if (P[p] == v) {
              E = p;
              break;
            }
          if (!o.isEmpty(E)) {
            h = {};
            var b = r.value;
            r.classList.contains("prime-form-label-container") &&
              (b = o.runtime.tmp.getLabelValue(r)),
              s.product.variants.forEach(function (t) {
                if (o.isEmpty(b) || b == t["option" + (E + 1)])
                  for (p = 0; p < P.length; p++)
                    o.isArray(h[P[p]]) || (h[P[p]] = []),
                      p != E && h[P[p]].push(t["option" + (p + 1)]);
              });
          }
        }
        for (var L = h, A = [], w = [], S = null, T = 0; T < g.length; T++) {
          var O = o.runtime.eventData[g[T].id];
          if (!o.isEmpty(O)) {
            h = o.copy(L);
            var C = 0,
              N = 0,
              I = 0,
              k = 0,
              x = null,
              D = null,
              R = null,
              F = null;
            if (
              O["option.product_variant_type"] ==
              o.const.PRODUCT_VARIANT_TYPE.combobox
            ) {
              if (
                ((S = g[T].querySelectorAll("select[data-product-option-id]")),
                o.isObject(h))
              ) {
                for (C = 0; C < S.length; C++)
                  if ((x = S[C].getAttribute("data-product-option-id")) != v)
                    for (
                      (o.isArray(h[x]) && -1 != h[x].indexOf(S[C].value)) ||
                        (S[C].value = ""),
                        D = S[C].getElementsByTagName("option"),
                        N = 0;
                      N < D.length;
                      N++
                    )
                      o.isEmpty(D[N].getAttribute("value")) ||
                        D[N].removeAttribute("disabled");
                for (C = 0; C < S.length; C++) {
                  for (
                    x = S[C].getAttribute("data-product-option-id"),
                      h = {},
                      I = 0;
                    I < s.product.variants.length;
                    I++
                  )
                    if (
                      ((F = s.product.variants[I]),
                      (P = F.option_ids.split("/")),
                      -1 != (E = P.indexOf(x)) &&
                        (o.isEmpty(S[C].value) ||
                          S[C].value == F["option" + (E + 1)]))
                    )
                      for (p = 0; p < P.length; p++)
                        o.isArray(h[P[p]]) || (h[P[p]] = []),
                          p != E && h[P[p]].push(F["option" + (p + 1)]);
                  for (k = 0; k < S.length; k++)
                    if ((R = S[k].getAttribute("data-product-option-id")) != x)
                      for (
                        D = S[k].getElementsByTagName("option"), N = 0;
                        N < D.length;
                        N++
                      )
                        o.isEmpty(D[N].getAttribute("value")) ||
                          (o.isArray(h[R]) &&
                            -1 != h[R].indexOf(D[N].getAttribute("value"))) ||
                          D[N].setAttribute("disabled", "");
                }
              }
              if (!o.isObject(h)) for (C = 0; C < S.length; C++) A.push(S[C]);
            }
            if (
              O["option.product_variant_type"] ==
              o.const.PRODUCT_VARIANT_TYPE.label
            ) {
              if (
                ((S = g[T].querySelectorAll(
                  ".prime-form-label-container[data-product-option-id]"
                )),
                o.isObject(h))
              ) {
                for (C = 0; C < S.length; C++)
                  if ((x = S[C].getAttribute("data-product-option-id")) != v) {
                    var q = o.runtime.tmp.getLabelValue(S[C]);
                    for (
                      (o.isArray(h[x]) && -1 != h[x].indexOf(q)) ||
                        o.runtime.tmp.updateLabelValue(S[C], null),
                        D = S[C].getElementsByClassName("prime-form-label-item"),
                        N = 0;
                      N < D.length;
                      N++
                    )
                      D[N].classList.contains("no-value") ||
                        D[N].classList.remove("disabled");
                  }
                for (C = 0; C < S.length; C++) {
                  for (
                    x = S[C].getAttribute("data-product-option-id"),
                      h = {},
                      I = 0;
                    I < s.product.variants.length;
                    I++
                  )
                    if (
                      ((F = s.product.variants[I]),
                      (P = F.option_ids.split("/")),
                      -1 != (E = P.indexOf(x)))
                    ) {
                      var B = o.runtime.tmp.getLabelValue(S[C]);
                      if (o.isEmpty(B) || B == F["option" + (E + 1)])
                        for (p = 0; p < P.length; p++)
                          o.isArray(h[P[p]]) || (h[P[p]] = []),
                            p != E && h[P[p]].push(F["option" + (p + 1)]);
                    }
                  for (k = 0; k < S.length; k++)
                    if ((R = S[k].getAttribute("data-product-option-id")) != x)
                      for (
                        D = S[k].getElementsByClassName("prime-form-label-item"),
                          N = 0;
                        N < D.length;
                        N++
                      )
                        if (!D[N].classList.contains("no-value")) {
                          var M = o.runtime.tmp.getOptionLabelValue(D[N]);
                          (o.isArray(h[R]) && -1 != h[R].indexOf(M)) ||
                            D[N].classList.add("disabled");
                        }
                }
              }
              if (!o.isObject(h)) for (C = 0; C < S.length; C++) w.push(S[C]);
            }
          }
        }
        !o.isEmpty(l) && o.isFunction(n) && n();
        for (var Y = null; A.length > 0; )
          (Y = A.shift()), o.fireEvent(Y, "change");
        for (; w.length > 0; )
          (Y = w.shift()), o.runtime.tmp.fireEventLabelChange(Y);
        for (
          S = document.querySelectorAll(
            ".prime-form .prime-form-label-container"
          ),
            T = 0;
          T < S.length;
          T++
        ) {
          var V = o.runtime.tmp.getLabelValue(S[T]);
          o.isEmpty(V) ||
            (o
              .findAncestor(S[T], "prime-element")
              .getAttribute("data-title-type") ==
              o.const.PRODUCT_VARIANT_TITLE.top &&
              (V = ": " + V));
          var H = o.findAncestor(S[T], "prime-form-item-box");
          o.isEmpty(H) ||
            ((H = H.querySelector(".prime-form-item-title-value")),
            o.isEmpty(H) || (H.innerHTML = V));
        }
      }
    }
  }),
  (PrimePageScriptV2.prototype.updateProductVariantSelectOptionFirst = function (
    t,
    e,
    i
  ) {
    var a = this,
      n = a.generateVariantProduct(
        t,
        !1,
        null,
        null,
        null,
        null,
        !0,
        !0,
        function () {
          a.updateProductVariantSelectOptionFirst(t, e, i);
        }
      );
    if (a.isObject(n) && a.isObject(n.product)) {
      var o = i.querySelectorAll("select.prime-form-control"),
        r = i.querySelectorAll(".prime-form-label-container"),
        s = 0;
      if (a.isArray(n.product.variants) && 0 != n.product.variants.length) {
        var l = null,
          c = null;
        if (
          (a.isEmpty(t["option.product_variant_id"]) ||
            (l = n.product.variants.find(function (e) {
              return e.product_variant_id == t["option.product_variant_id"];
            })),
          a.isEmpty(l) && (l = n.product.variants[0]),
          e["option.product_variant_type"] ==
            a.const.PRODUCT_VARIANT_TYPE.combined)
        )
          for (s = 0; s < o.length; s++)
            (c = o[s].querySelector(
              'option[data-product-variant-id="' + l.product_variant_id + '"]'
            )),
              a.isEmpty(c) ||
                ((o[s].value = c.getAttribute("value")),
                a.fireEvent(o[s], "change"));
        e["option.product_variant_type"] ==
          a.const.PRODUCT_VARIANT_TYPE.combobox &&
          a.isString(l.option_ids) &&
          l.option_ids.split("/").forEach(function (t, e) {
            for (s = 0; s < o.length; s++)
              o[s].getAttribute("data-product-option-id") == t &&
                ((o[s].value = l["option" + (e + 1)] || ""),
                a.fireEvent(o[s], "change"));
          }),
          e["option.product_variant_type"] ==
            a.const.PRODUCT_VARIANT_TYPE.label &&
            a.isString(l.option_ids) &&
            l.option_ids.split("/").forEach(function (t, e) {
              for (s = 0; s < r.length; s++)
                r[s].getAttribute("data-product-option-id") == t &&
                  (a.runtime.tmp.updateLabelValue(
                    r[s],
                    l["option" + (e + 1)] || ""
                  ),
                  a.runtime.tmp.fireEventLabelChange(r[s]));
            });
      } else {
        for (s = 0; s < o.length; s++)
          (o[s].value = ""), a.fireEvent(o[s], "change");
        for (s = 0; s < r.length; s++)
          a.runtime.tmp.updateLabelValue(r[s], null),
            a.runtime.tmp.fireEventLabelChange(r[s]);
      }
    }
  }),
  (PrimePageScriptV2.prototype.generateHtmlCart = function (t, e, i) {
    var a = this,
      n = [];
    if (i) n = a.runtime.tmp.cart;
    else {
      var o = window.$rootScope.getStoreCurrency();
      n = [
        {
          name: "Product Name",
          title: "Product Variant",
          price: 9999,
          image: window.$rootScope.logoUrlColor,
          quantity: 1,
          inventory_checked: 0,
          available_quantity: 999,
          currency: { code: o, symbol: a.formatCurrency(null, o, !1, !0) },
        },
      ];
    }
    var r = "";
    return (
      (r += "<tbody>"),
      0 == n.length
        ? (r += '<tr><td class="prime-cart-no-product">' + e + "</td></tr>")
        : n.forEach(function (e, n) {
            if (!(e.quantity <= 0)) {
              var o = {
                product_variant_id: e.product_variant_id,
                cart_item_key: e.cart_item_key,
              };
              (o = JSON.stringify(o)), (o = encodeURIComponent(o));
              var s = a.getOptimizeImage(e.image, 60, 60, !0, !1, !0, i);
              (r +=
                '<tr class="prime-cart-row' +
                (a.isObject(e.promotion) ? " has-promotion" : "") +
                '"><td class="prime-cart-image"><img src="' +
                s +
                '" />'),
                t == a.const.CART_LAYOUT.viewonly &&
                  (r +=
                    '<span class="prime-cart-image-quantity">' +
                    e.quantity +
                    "</span>"),
                (r += "</td>"),
                (r +=
                  '<td class="prime-cart-title"><span class="prime-cart-title-name">' +
                  e.name +
                  '</span><span class="prime-cart-title-variant">' +
                  (e.title || "") +
                  "</span>"),
                a.isObject(e.promotion) &&
                  (r +=
                    '<span class="promotion-name">' +
                    e.promotion.discount.note +
                    "</span>"),
                (r += "</td>"),
                t == a.const.CART_LAYOUT.editable &&
                  (r +=
                    '<td class="prime-cart-quantity"><div class="prime-cart-quantity-content"><div onclick="javascript: PrimePageScript.buttonAddToCartProductQuantity(this, -1);" class="button"><span>-</span></div><input type="number" min="1" value="' +
                    e.quantity +
                    '" oninput="javascript: PrimePageScript.changeAddToCartProductQuantity(this, \'' +
                    o +
                    '\', true);" /><div onclick="javascript: PrimePageScript.buttonAddToCartProductQuantity(this, 1);" class="button"><span>+</span></div></div></td>'),
                (r +=
                  '<td class="prime-cart-price"><span' +
                  (a.isNull(e.store_id)
                    ? ""
                    : ' data-store-id="' + e.store_id + '"') +
                  ' data-product-id="' +
                  e.product_id +
                  '" data-product-variant-id="' +
                  e.product_variant_id +
                  '" data-cart-item-key="' +
                  e.cart_item_key +
                  '">');
              var l = e.price * e.quantity,
                c = a.formatNumber(l, 3);
              a.isObject(e.currency) &&
                !a.isEmpty(e.currency.symbol) &&
                (c = a.formatCurrency(l, e.currency.symbol, !0)),
                (r += c + "</span>"),
                a.isObject(e.promotion) &&
                  ((l = e.promotion.total),
                  (c = a.formatNumber(l, 3)),
                  a.isObject(e.currency) &&
                    !a.isEmpty(e.currency.symbol) &&
                    (c = a.formatCurrency(l, e.currency.symbol, !0)),
                  (r += '<span class="price-compare">' + c + "</span>")),
                (r += "</td>"),
                t == a.const.CART_LAYOUT.editable &&
                  ((r +=
                    '<td class="prime-cart-action"><div onclick="javascript: PrimePageScript.removeAddToCartProduct(\'' +
                    o +
                    '\', true);" class="button"><span>X</span></div></td>'),
                  (r += "</tr>"));
            }
          }),
      (r += "</tbody>")
    );
  }),
  (PrimePageScriptV2.prototype.getProductVariantId = function (t, e) {
    var i = null,
      a = this.findAncestor(t, "prime-element"),
      n = this;
    if (!this.isEmpty(a)) {
      var o = this.runtime.eventData[a.id];
      if (
        !this.isEmpty(o) &&
        o["option.product_variant_type"] ==
          this.const.PRODUCT_VARIANT_TYPE.combined
      ) {
        var r = a.querySelector("select.prime-form-control");
        if (!this.isEmpty(r) && !this.isEmpty(r.value)) {
          var s = r.querySelector('option[value="' + r.value + '"]');
          this.isEmpty(s) || (i = s.getAttribute("data-product-variant-id"));
        }
      }
      var l = null,
        c = null;
      if (
        !this.isEmpty(o) &&
        (o["option.product_variant_type"] ==
          this.const.PRODUCT_VARIANT_TYPE.combobox ||
          o["option.product_variant_type"] ==
            this.const.PRODUCT_VARIANT_TYPE.label)
      ) {
        (c = {}),
          (l = a.querySelectorAll(
            ".prime-form-item select[data-product-option-id]"
          ));
        for (var d = 0; d < l.length; d++)
          c[l[d].getAttribute("data-product-option-id")] = (
            n.isEmpty(l[d].value) ? "" : l[d].value
          ).trim();
        for (
          l = a.querySelectorAll(
            ".prime-form-label-container[data-product-option-id]"
          ),
            d = 0;
          d < l.length;
          d++
        )
          c[l[d].getAttribute("data-product-option-id")] = n.runtime.tmp
            .getLabelValue(l[d])
            .trim();
        this.isArray(e.variants) &&
          e.variants.forEach(function (t) {
            if (n.isEmpty(i)) {
              var e = !0;
              if (n.isString(t.option_ids))
                for (var a = t.option_ids.split("/"), o = 0; o < a.length; o++)
                  if (c[a[o].trim()] != (t["option" + (o + 1)] || "").trim()) {
                    e = !1;
                    break;
                  }
              e && (i = t.product_variant_id);
            }
          });
      }
    }
    return i;
  }),
  (PrimePageScriptV2.prototype.getProductVariantIndex = function (t, e) {
    var i = this,
      a = -1,
      n = e["option.product_type"],
      o = e["option.ladisale_store_id"] || null,
      r = e["option.product_id"],
      s = i.generateVariantProduct(e, !1, null, null, null, null, !0, !0);
    return i.isObject(s) &&
      i.isObject(s.store_info) &&
      i.isObject(s.product) &&
      i.isArray(s.product.variants) &&
      !(s.product.variants.length <= 0)
      ? (this.runtime.isClient
          ? Object.keys(this.runtime.eventData).forEach(function (e) {
              if ((i.isEmpty(t) || t == e) && -1 == a) {
                var l = i.runtime.eventData[e];
                if (
                  "form" == l.type &&
                  l["option.is_add_to_cart"] &&
                  l["option.product_type"] == n &&
                  l["option.product_id"] == r &&
                  l["option.ladisale_store_id"] == o
                ) {
                  var c = document.getElementById(e);
                  if (!i.isEmpty(c)) {
                    var d = c.querySelector('[data-variant="true"]');
                    if (!i.isEmpty(d)) {
                      var p = i.runtime.eventData[d.id];
                      if (!i.isEmpty(p)) {
                        var u = null;
                        if (
                          (p["option.product_variant_type"] ==
                            i.const.PRODUCT_VARIANT_TYPE.combobox &&
                            ((u = d.querySelectorAll(
                              ".prime-form-item select[data-product-option-id]"
                            )),
                            (a = s.product.variants.findIndex(function (t) {
                              for (
                                var e = !0,
                                  a = null,
                                  n = function (t) {
                                    return t == a;
                                  },
                                  o = 0;
                                o < u.length;
                                o++
                              )
                                if (
                                  u[o].getAttribute("data-store-id") ==
                                    s.store_info.id &&
                                  u[o].getAttribute("data-product-id") ==
                                    t.product_id
                                ) {
                                  a = u[o].getAttribute(
                                    "data-product-option-id"
                                  );
                                  var r = u[o].value;
                                  if (i.isString(t.option_ids)) {
                                    var l = t.option_ids
                                      .split("/")
                                      .findIndex(n);
                                    if (-1 != l && t["option" + (l + 1)] != r) {
                                      e = !1;
                                      break;
                                    }
                                  }
                                }
                              return e;
                            }))),
                          p["option.product_variant_type"] ==
                            i.const.PRODUCT_VARIANT_TYPE.label &&
                            ((u = d.querySelectorAll(
                              ".prime-form-label-container[data-product-option-id]"
                            )),
                            (a = s.product.variants.findIndex(function (t) {
                              for (
                                var e = !0,
                                  a = null,
                                  n = function (t) {
                                    return t == a;
                                  },
                                  o = 0;
                                o < u.length;
                                o++
                              )
                                if (
                                  u[o].getAttribute("data-store-id") ==
                                    s.store_info.id &&
                                  u[o].getAttribute("data-product-id") ==
                                    t.product_id
                                ) {
                                  a = u[o].getAttribute(
                                    "data-product-option-id"
                                  );
                                  var r = i.runtime.tmp.getLabelValue(u[o]);
                                  if (i.isString(t.option_ids)) {
                                    var l = t.option_ids
                                      .split("/")
                                      .findIndex(n);
                                    if (-1 != l && t["option" + (l + 1)] != r) {
                                      e = !1;
                                      break;
                                    }
                                  }
                                }
                              return e;
                            }))),
                          p["option.product_variant_type"] ==
                            i.const.PRODUCT_VARIANT_TYPE.combined)
                        ) {
                          var m = d.querySelector(".prime-form-control");
                          if (
                            i.isEmpty(m) ||
                            m.getAttribute("data-store-id") !=
                              s.store_info.id ||
                            m.getAttribute("data-product-id") !=
                              s.product.product_id
                          )
                            return;
                          (a = m.value), (a = i.isEmpty(a) ? -1 : parseInt(a));
                        }
                      }
                    }
                  }
                }
              }
            })
          : (a = 0),
        a)
      : a;
  }),
  (PrimePageScriptV2.prototype.generateProductKey = function (
    t,
    e,
    i,
    a,
    n,
    o,
    r,
    s
  ) {
    var l = this;
    l.isEmpty(r) || (a["option.product_id"] = r.product_id);
    var c = a["option.product_type"],
      d = a["option.product_mapping_name"],
      p = l.generateVariantProduct(
        a,
        !1,
        null,
        null,
        null,
        null,
        !0,
        !0,
        function (i) {
          l.generateProductKey(t, e, !1, a, n, o, r, s);
        }
      ),
      u = null;
    function m(t) {
      return (t >= 10 ? "" : "0") + t;
    }
    if (l.isObject(p) && l.isObject(p.product)) {
      var _ = null,
        y = null;
      p.product.type == l.const.PRODUCT_TYPE.event &&
        "description" == d &&
        (d = "content");
      var g = function () {
        if (
          -1 !=
          [
            l.const.FORM_CONFIG_TYPE.ladisales,
            l.const.FORM_CONFIG_TYPE.sapo,
            l.const.FORM_CONFIG_TYPE.haravan,
            l.const.FORM_CONFIG_TYPE.shopify,
            l.const.FORM_CONFIG_TYPE.wordpress,
          ].indexOf(c)
        ) {
          var t = !1;
          if (
            (-1 !=
              [
                "name",
                "description",
                "content",
                "location",
                "timezone",
                "external_link",
              ].indexOf(d) && ((_ = p.product[d]), (e = _), (t = !0)),
            -1 != ["start_date", "end_date"].indexOf(d))
          ) {
            _ = p.product[d];
            try {
              (y = new Date(_)).toISOString() == _ &&
                (_ =
                  y.getFullYear() +
                  "-" +
                  m(y.getMonth() + 1) +
                  "-" +
                  m(y.getDate()) +
                  " " +
                  m(y.getHours()) +
                  ":" +
                  m(y.getMinutes()) +
                  ":" +
                  m(y.getSeconds()));
            } catch (t) {}
            (e = _), (t = !0);
          }
          if (
            (-1 != ["image"].indexOf(d) &&
              ((_ = p.product[d]),
              l.isObject(_) &&
                ((e = _.src),
                l.isEmpty(e) ||
                  !l.isString(e) ||
                  e.startsWith("http://") ||
                  e.startsWith("https://") ||
                  e.startsWith("//") ||
                  (e = "https://" + l.const.STATIC_W_DOMAIN + "/" + e),
                (t = !0))),
            -1 != ["images"].indexOf(d) &&
              ((_ = p.product[d]),
              l.isArray(_) &&
                ((e = []),
                _.forEach(function (t) {
                  l.isEmpty(t.src) ||
                    (!l.isString(t.src) ||
                    t.src.startsWith("http://") ||
                    t.src.startsWith("https://") ||
                    t.src.startsWith("//")
                      ? e.push({ src: t.src })
                      : e.push({
                          src:
                            "https://" + l.const.STATIC_W_DOMAIN + "/" + t.src,
                        }));
                }),
                (t = !0))),
            t && (t = !l.isEmpty(e)),
            l.isArray(p.product.variants) && p.product.variants.length > 0)
          ) {
            var r = n ? 0 : l.getProductVariantIndex(null, a);
            l.isEmpty(o) ||
              (r = p.product.variants.findIndex(function (t) {
                return t.product_variant_id == o;
              }));
            var f = null;
            if ((-1 != r && ((f = p.product.variants[r]), (u = f)), !t))
              if (-1 != r)
                if (
                  -1 != ["variant_start_date", "variant_end_date"].indexOf(d)
                ) {
                  _ = f[d];
                  try {
                    (y = new Date(_)).toISOString() == _ &&
                      (_ =
                        y.getFullYear() +
                        "-" +
                        m(y.getMonth() + 1) +
                        "-" +
                        m(y.getDate()) +
                        " " +
                        m(y.getHours()) +
                        ":" +
                        m(y.getMinutes()) +
                        ":" +
                        m(y.getSeconds()));
                  } catch (t) {}
                  e = _;
                } else if (-1 != ["sku", "variant_timezone"].indexOf(d))
                  (_ = f[d]), (e = _);
                else if (-1 != ["title"].indexOf(d))
                  (_ = f[d] || f.product_name), (e = _);
                else if (-1 != ["text_quantity"].indexOf(d))
                  (_ = 1 == f.inventory_checked ? f[d] : ""), (e = _);
                else if (-1 != ["weight"].indexOf(d))
                  (_ = f[d]),
                    l.isEmpty(f.weight_unit) || (_ += f.weight_unit),
                    (e = _);
                else if (-1 != ["price", "compare_price"].indexOf(d))
                  l.isEmpty(f[d])
                    ? (_ = "")
                    : ((_ = l.formatNumber(f[d], 3)),
                      l.isObject(p.store_info) &&
                        l.isObject(p.store_info.currency) &&
                        !l.isEmpty(p.store_info.currency.symbol) &&
                        (_ = l.formatCurrency(
                          f[d],
                          p.store_info.currency.symbol,
                          !0
                        ))),
                    (e = _);
                else if (-1 != ["price_sale"].indexOf(d)) {
                  var v = 0;
                  l.isEmpty(f.price) ||
                    l.isEmpty(f.compare_price) ||
                    (v = f.compare_price - f.price),
                    0 != v
                      ? ((_ = l.formatNumber(v, 3)),
                        l.isObject(p.store_info) &&
                          l.isObject(p.store_info.currency) &&
                          !l.isEmpty(p.store_info.currency.symbol) &&
                          (_ = l.formatCurrency(
                            v,
                            p.store_info.currency.symbol,
                            !0
                          )))
                      : (_ = ""),
                    (e = _);
                } else if (-1 != ["price_sale_percent"].indexOf(d)) {
                  var h = 0;
                  l.isEmpty(f.price) ||
                    l.isEmpty(f.compare_price) ||
                    (h = Math.floor(
                      ((f.compare_price - f.price) / f.compare_price) * 100
                    )),
                    (e = _ = 0 != h ? h + "%" : "");
                } else if (-1 != ["src"].indexOf(d)) {
                  if (((_ = f[d]), l.isEmpty(_))) return (d = "image"), g();
                  !l.isString(_) ||
                    _.startsWith("http://") ||
                    _.startsWith("https://") ||
                    _.startsWith("//") ||
                    (_ = "https://" + l.const.STATIC_W_DOMAIN + "/" + _),
                    (e = _);
                } else
                  ["description"].indexOf(d),
                    (_ = f[d]),
                    l.isEmpty(_) || (e = _);
              else e = _ = "";
          }
        } else {
          if (((_ = p.product[d]), l.isBoolean(_)))
            _ = _ ? l.const.LANG.OPTION_TRUE : l.const.LANG.OPTION_FALSE;
          else
            try {
              (y = new Date(_)).toISOString() == _ &&
                (_ =
                  y.getFullYear() +
                  "-" +
                  m(y.getMonth() + 1) +
                  "-" +
                  m(y.getDate()) +
                  " " +
                  m(y.getHours()) +
                  ":" +
                  m(y.getMinutes()) +
                  ":" +
                  m(y.getSeconds()));
            } catch (t) {}
          e = _;
        }
        !i && l.isFunction(s) && s(e);
      };
      g();
    }
    return t ? { product: p, variant: u, value: e } : e;
  }),
  (PrimePageScriptV2.prototype.generateVariantProduct = function (
    t,
    e,
    i,
    a,
    n,
    o,
    r,
    s,
    l
  ) {
    var c = e ? "" : null,
      d = this,
      p = function (t) {
        if (!e) return d.isObject(t) ? t : null;
        var s = "";
        if (d.isObject(t)) {
          if (!d.isObject(t.product)) return s;
          i == d.const.PRODUCT_VARIANT_TYPE.combined &&
            ((s +=
              '<div class="prime-form-item-container"><div class="prime-form-item-background"></div><div class="prime-form-item"><select' +
              (d.isObject(t.store_info) && !d.isNull(t.store_info.id)
                ? ' data-store-id="' + t.store_info.id + '"'
                : "") +
              ' data-product-id="' +
              t.product.product_id +
              '" required tabindex="' +
              o +
              '" class="prime-form-control prime-form-control-select" data-selected=""' +
              (r ? "" : ' onmousedown="javascript: event.preventDefault();"') +
              ">"),
            d.runtime.isClient &&
              (s +=
                '<option value="" data-product-variant-id="">' +
                d.const.LANG.OPTION_NO_SELECT +
                "</option>"),
            d.isArray(t.product.variants) &&
              t.product.variants.forEach(function (e, i) {
                var a = e.title || e.product_name;
                if (n) {
                  var o = d.formatNumber(e.price, 3);
                  d.isObject(t.store_info) &&
                    d.isObject(t.store_info.currency) &&
                    !d.isEmpty(t.store_info.currency.symbol) &&
                    (o = d.formatCurrency(
                      e.price,
                      t.store_info.currency.symbol,
                      !0
                    )),
                    (a += " - " + o);
                }
                s +=
                  '<option value="' +
                  i +
                  '" data-product-variant-id="' +
                  e.product_variant_id +
                  '">' +
                  a +
                  "</option>";
              }),
            (s += "</select></div></div>")),
            i == d.const.PRODUCT_VARIANT_TYPE.combobox &&
              d.isArray(t.product.options) &&
              t.product.options.forEach(function (e) {
                if (e.is_tmp) s += '<div class="prime-form-item-box"></div>';
                else if (d.isArray(e.values) && 0 != e.values.length) {
                  (s += '<div class="prime-form-item-box">'),
                    d.isEmpty(a) ||
                      (s +=
                        '<div class="prime-form-item-title"><span>' +
                        e.name +
                        "</span></div>"),
                    (s +=
                      '<div class="prime-form-item-container"><div class="prime-form-item-background"></div><div class="prime-form-item"><select' +
                      (d.isObject(t.store_info) && !d.isNull(t.store_info.id)
                        ? ' data-store-id="' + t.store_info.id + '"'
                        : "") +
                      ' data-product-id="' +
                      e.product_id +
                      '" data-product-option-id="' +
                      e.product_option_id +
                      '" required tabindex="' +
                      o +
                      '" class="prime-form-control prime-form-control-select" data-selected=""' +
                      (r
                        ? ""
                        : ' onmousedown="javascript: event.preventDefault();"') +
                      ">"),
                    d.runtime.isClient &&
                      (s +=
                        '<option value="">' +
                        d.const.LANG.OPTION_NO_SELECT +
                        "</option>");
                  var i = null;
                  d.isArray(t.product.variants) && (i = t.product.variants[0]);
                  var n = null;
                  d.isEmpty(i) ||
                    (d.isString(i.option_ids) && (n = i.option_ids.split("/"))),
                    e.values.forEach(function (t) {
                      var a = (function (t) {
                        var a = "";
                        return (
                          d.isArray(n) &&
                            n.forEach(function (n, o) {
                              e.product_option_id == n &&
                                t == i["option" + (o + 1)] &&
                                (a = " selected");
                            }),
                          a
                        );
                      })(t.name);
                      s +=
                        "<option" +
                        a +
                        ' value="' +
                        t.name +
                        '">' +
                        (t.name_new || t.name) +
                        "</option>";
                    }),
                    (s += "</select></div></div></div>");
                }
              }),
            i == d.const.PRODUCT_VARIANT_TYPE.label &&
              d.isArray(t.product.options) &&
              t.product.options.forEach(function (e) {
                if (d.isArray(e.values) && 0 != e.values.length) {
                  (s += '<div class="prime-form-item-box">'),
                    d.isEmpty(a) ||
                      ((s += '<div class="prime-form-item-title">'),
                      (s += "<span>" + e.name + "</span>"),
                      (s +=
                        '<span class="prime-form-item-title-value">' +
                        (r ? "" : e.values[0].name) +
                        "</span>"),
                      (s += "</div>")),
                    (s +=
                      '<div class="prime-form-label-container"' +
                      (d.isObject(t.store_info) && !d.isNull(t.store_info.id)
                        ? ' data-store-id="' + t.store_info.id + '"'
                        : "") +
                      ' data-product-id="' +
                      e.product_id +
                      '" data-product-option-id="' +
                      e.product_option_id +
                      '" data-selected="">');
                  var i = null;
                  d.isArray(t.product.variants) && (i = t.product.variants[0]);
                  var n = null;
                  d.isEmpty(i) ||
                    (d.isString(i.option_ids) && (n = i.option_ids.split("/"))),
                    e.values.forEach(function (t, a) {
                      0 == a &&
                        (t.type == d.const.PRODUCT_VARIANT_OPTION_TYPE.image
                          ? (s +=
                              '<div class="prime-form-label-item image no-value" data-value=""></div>')
                          : t.type == d.const.PRODUCT_VARIANT_OPTION_TYPE.color
                          ? (s +=
                              '<div class="prime-form-label-item color no-value" data-value=""></div>')
                          : (s +=
                              '<div class="prime-form-label-item text no-value" data-value="">&nbsp;</div>'));
                      var o = (function (t) {
                        var a = "";
                        return (
                          d.isArray(n) &&
                            n.forEach(function (n, o) {
                              e.product_option_id == n &&
                                t == i["option" + (o + 1)] &&
                                (a = " selected");
                            }),
                          a
                        );
                      })(t.name);
                      if (t.type == d.const.PRODUCT_VARIANT_OPTION_TYPE.image) {
                        var r = t.value;
                        d.isEmpty(r) ||
                          !d.isString(r) ||
                          r.startsWith("http://") ||
                          r.startsWith("https://") ||
                          r.startsWith("//") ||
                          (r = "https://" + d.const.STATIC_W_DOMAIN + "/" + r),
                          (r = d.getOptimizeImage(r, 100, 100, !1, !1, !1, !0)),
                          (s +=
                            '<div class="prime-form-label-item image' +
                            o +
                            '" style=\'background-image: url("' +
                            r +
                            '");\' title="' +
                            (t.name_new || t.name) +
                            '" data-value="' +
                            t.name +
                            '"></div>');
                      } else t.type == d.const.PRODUCT_VARIANT_OPTION_TYPE.color ? (s += '<div class="prime-form-label-item color' + o + "\" style='background-color: " + t.value + ";' title=\"" + (t.name_new || t.name) + '" data-value="' + t.name + '"></div>') : (s += '<div class="prime-form-label-item text' + o + '" data-value="' + t.name + '">' + (t.name_new || t.name) + "</div>");
                    }),
                    (s += "</div></div>");
                }
              });
        }
        return s;
      };
    if (!d.isNull(t.dataProduct)) return p(t.dataProduct);
    var u = t["option.form_account_id"],
      m = t["option.product_type"],
      _ = t["option.ladisale_store_id"] || null,
      y = t["option.product_id"],
      g = t["option.data_setting.value"],
      f = t["option.data_setting.type_dataset"],
      v = t["option.data_setting.sort_name"],
      h = t["option.data_setting.sort_by"],
      E = null,
      P = null;
    if (
      -1 !=
      [
        d.const.FORM_CONFIG_TYPE.ladisales,
        d.const.FORM_CONFIG_TYPE.sapo,
        d.const.FORM_CONFIG_TYPE.haravan,
        d.const.FORM_CONFIG_TYPE.shopify,
        d.const.FORM_CONFIG_TYPE.wordpress,
      ].indexOf(m)
    ) {
      if (!d.isEmpty(y)) {
        if (
          (d.isEmpty(d.runtime.tmp.product_info[m]) &&
            (d.runtime.tmp.product_info[m] = {}),
          d.isEmpty(d.runtime.tmp.timeout_product_info[m]) &&
            (d.runtime.tmp.timeout_product_info[m] = {}),
          (y = parseInt(y) || y),
          (E = d.runtime.tmp.product_info[m][y]),
          (P = function () {
            return p(E);
          }),
          d.isNull(E))
        ) {
          d.runtime.tmp.product_info[m][y] = !0;
          var b = function () {
              (d.runtime.tmp.product_info[m][y] = !1),
                d.isEmpty(d.runtime.tmp.timeout_product_info[m][y]) ||
                  (d.removeTimeout(d.runtime.tmp.timeout_product_info[m][y]),
                  delete d.runtime.tmp.timeout_product_info[m][y]);
            },
            L = function (t) {
              if (
                ((E = d.isObject(d.runtime.tmp.product_info[m][y])
                  ? d.runtime.tmp.product_info[m][y]
                  : t.data),
                d.isObject(E))
              ) {
                if (!d.isObject(E.store_info)) {
                  var e = d.runtime.currency;
                  d.runtime.isClient ||
                    (e = window.$rootScope.getStoreCurrency()),
                    (E.store_info = {
                      currency: {
                        code: e,
                        symbol: d.formatCurrency(null, e, !1, !0),
                      },
                    });
                }
                if (
                  (m != d.const.FORM_CONFIG_TYPE.ladisales &&
                    (E.store_info.id = -1),
                  d.isObject(E.store_info.currency) &&
                    !d.isEmpty(E.store_info.currency.code) &&
                    (E.store_info.currency.symbol = d.formatCurrency(
                      null,
                      E.store_info.currency.code,
                      !1,
                      !0
                    )),
                  d.isObject(E.product) &&
                    d.isArray(E.product.options) &&
                    d.isArray(E.product.variants))
                ) {
                  var i = E.product.options.map(function (t) {
                    return t.product_option_id;
                  });
                  i = i.join("/");
                  for (var a = 0; a < E.product.variants.length; a++)
                    -1 != [d.const.FORM_CONFIG_TYPE.ladisales].indexOf(m) &&
                      1 == E.product.variants[a].allow_sold_out &&
                      (E.product.variants[a].inventory_checked = 0),
                      d.isNull(E.product.variants[a].compare_price) &&
                        (E.product.variants[a].compare_price =
                          E.product.variants[a].price_compare),
                      d.isNull(E.product.variants[a].variant_start_date) &&
                        (E.product.variants[a].variant_start_date =
                          E.product.variants[a].start_date),
                      d.isNull(E.product.variants[a].variant_end_date) &&
                        (E.product.variants[a].variant_end_date =
                          E.product.variants[a].end_date),
                      d.isNull(E.product.variants[a].variant_timezone) &&
                        (E.product.variants[a].variant_timezone =
                          E.product.variants[a].timezone),
                      d.isEmpty(E.product.variants[a].option_ids) &&
                        (E.product.variants[a].option_ids = i),
                      -1 !=
                        [
                          d.const.FORM_CONFIG_TYPE.sapo,
                          d.const.FORM_CONFIG_TYPE.haravan,
                          d.const.FORM_CONFIG_TYPE.shopify,
                        ].indexOf(m) &&
                        1 == E.product.variants.length &&
                        "Default Title" == E.product.variants[a].title &&
                        ((E.product.variants[a].title = null),
                        (E.product.variants[a].option1 = null),
                        (E.product.options = [])),
                      -1 != [d.const.FORM_CONFIG_TYPE.wordpress].indexOf(m) &&
                        1 == E.product.variants.length &&
                        E.product.variants[a].title ==
                          E.product.variants[a].product_name &&
                        ((E.product.variants[a].title = null),
                        (E.product.variants[a].option1 = null),
                        (E.product.options = [])),
                      d.isEmpty(E.product.variants[a].package_quantity) ||
                        d.isEmpty(
                          E.product.variants[a].package_quantity_unit
                        ) ||
                        (d.isNull(E.product.variants[a].title_old) &&
                          (E.product.variants[a].title_old =
                            E.product.variants[a].title),
                        (E.product.variants[a].title =
                          E.product.variants[a].title_old +
                          " (" +
                          E.product.variants[a].package_quantity +
                          " " +
                          E.product.variants[a].package_quantity_unit +
                          ")"));
                  if (
                    d.isArray(E.product.options) &&
                    1 == E.product.options.length &&
                    d.isArray(E.product.options[0].values)
                  )
                    for (
                      var n = null,
                        o = function (t) {
                          return t.option1 == n;
                        },
                        r = 0;
                      r < E.product.options[0].values.length;
                      r++
                    ) {
                      n = E.product.options[0].values[r].name;
                      var s = E.product.variants.find(o);
                      (E.product.options[0].values[r].name_new =
                        E.product.options[0].values[r].label ||
                        E.product.options[0].values[r].name),
                        d.isEmpty(s) ||
                          d.isEmpty(s.package_quantity) ||
                          d.isEmpty(s.package_quantity_unit) ||
                          (E.product.options[0].values[r].name_new =
                            E.product.options[0].values[r].name_new +
                            " (" +
                            s.package_quantity +
                            " " +
                            s.package_quantity_unit +
                            ")");
                    }
                }
                (d.runtime.tmp.product_info[m][y] = E),
                  (c = P()),
                  d.isFunction(l) && l(c);
              } else b();
            },
            A = { product_id: y },
            w = null,
            S = "POST";
          return (
            d.runLimitRequest(20, function () {
              if (d.runtime.isClient) {
                var t = d.const.API_LADISALE_SHOW_PRODUCT;
                m == d.const.FORM_CONFIG_TYPE.ladisales
                  ? (((w = { "Content-Type": "application/json" })["Store-Id"] =
                      _),
                    (A = JSON.stringify(A)))
                  : m == d.const.FORM_CONFIG_TYPE.wordpress
                  ? ((S = "GET"),
                    (t =
                      window.location.origin +
                      "/ladipage/api?action=product_info&product_id=" +
                      y),
                    (A = null))
                  : ((w = { "Content-Type": "application/json" }),
                    (t = d.const.API_SHOW_PRODUCT),
                    (A = { form_account_id: u, product_id: y }),
                    (A = JSON.stringify(A))),
                  d.sendRequest(S, t, A, !0, w, function (t, e, i) {
                    if (i.readyState == XMLHttpRequest.DONE)
                      try {
                        var a = JSON.parse(t);
                        L(a);
                      } catch (t) {
                        b();
                      }
                  });
              } else {
                var e = function (t) {
                    d.isNull(t) || L({ data: t });
                  },
                  i = LadiPage.getProductInfo(u, y, function (t) {
                    e(t);
                  });
                e(i);
              }
            }),
            c
          );
        }
        !0 === E
          ? (d.runtime.tmp.timeout_product_info[m][y] = d.runTimeout(
              function () {
                d.generateVariantProduct(t, e, i, a, n, o, r, !1, l);
              },
              100
            ))
          : ((c = P()), !s && d.isFunction(l) && l(c));
      }
    } else if (!d.isEmpty(y)) {
      if (
        (d.isEmpty(d.runtime.tmp.product_info[m]) &&
          (d.runtime.tmp.product_info[m] = {}),
        d.isEmpty(d.runtime.tmp.timeout_product_info[m]) &&
          (d.runtime.tmp.timeout_product_info[m] = {}),
        (y += ""),
        (E = d.runtime.tmp.product_info[m][y]),
        (P = function () {
          return e ? "" : d.isObject(E) ? E : null;
        }),
        d.isNull(E))
      )
        return (
          (d.runtime.tmp.product_info[m][y] = !0),
          d.loadDataset(g, g, f, v, h, !0, d.runtime.isClient, function (t) {
            (c = P()), d.isFunction(l) && l(c);
          }),
          c
        );
      !0 === E
        ? (d.runtime.tmp.timeout_product_info[m][y] = d.runTimeout(function () {
            d.generateVariantProduct(t, e, i, a, n, o, r, !1, l);
          }, 100))
        : ((c = P()), !s && d.isFunction(l) && l(c));
    }
    return c;
  }),
  (PrimePageScriptV2.prototype.generateVariantContentString = function (
    t,
    e,
    i,
    a
  ) {
    var n = [];
    i = this.isEmpty(i) ? " | " : i;
    try {
      this.isEmpty(t) ||
        (e && (t = Base64.decode(t)),
        (t = JSON.parse(t)),
        this.isArray(t.dynamic_content.hide) &&
          t.dynamic_content.hide.length > 0 &&
          n.push(
            this.const.LANG.HIDE_ELEMENT +
              " " +
              t.dynamic_content.hide.join(", ")
          ),
        this.isArray(t.dynamic_content.show) &&
          t.dynamic_content.show.length > 0 &&
          n.push(
            this.const.LANG.SHOW_ELEMENT +
              " " +
              t.dynamic_content.show.join(", ")
          ),
        this.isArray(t.dynamic_content.top) &&
          t.dynamic_content.top.length > 0 &&
          n.push(
            this.const.LANG.TOP_ELEMENT + " " + t.dynamic_content.top.join(", ")
          ),
        this.isArray(t.dynamic_content.scroll) &&
          t.dynamic_content.scroll.length > 0 &&
          n.push(
            this.const.LANG.SCROLL_ELEMENT +
              " " +
              t.dynamic_content.scroll.join(", ")
          ),
        this.isArray(t.dynamic_content.cookie) &&
          t.dynamic_content.cookie.length > 0 &&
          n.push(
            this.const.LANG.SET_COOKIE +
              " " +
              t.dynamic_content.cookie.join("; ")
          ));
    } catch (t) {}
    return a ? n : n.join(i);
  }),
  (PrimePageScriptV2.prototype.reloadFeeShipping = function (t) {
    var e = this;
    e instanceof PrimePageScriptV2 || (e = PrimePageScript);
    var i = function (t, i) {
      window
        .ladi("_ladipage_" + t)
        .set_cookie(i, 365, "/", window.location.host),
        e.isArray(e.runtime.DOMAIN_SET_COOKIE) &&
          e.runtime.DOMAIN_SET_COOKIE.forEach(function (e) {
            e != window.location.host &&
              window.ladi("_ladipage_" + t).set_cookie(i, 365, "/", e);
          });
    };
    e.isEmpty(e.runtime.tmp.timeout_reload_fee_shipping) ||
      e.removeTimeout(e.runtime.tmp.timeout_reload_fee_shipping);
    var a = function (t, i) {
      (e.runtime.tmp.add_to_cart_fee_shipping = t),
        e.changeTotalPriceCart(),
        e.isNull(i) &&
          ((e.runtime.tmp.add_to_cart_shipping_method_id = null), (i = []));
      for (
        var a = null,
          n = e.runtime.tmp.add_to_cart_shipping_method_id,
          o = function (t) {
            l.setAttribute("data-selected", t.target.value || ""),
              (e.runtime.tmp.add_to_cart_shipping_method_id = e.isEmpty(
                l.getAttribute("data-selected")
              )
                ? null
                : l.getAttribute("data-selected")),
              (parseFloatLadiPage(
                e.runtime.tmp.add_to_cart_shipping_method_id
              ) || 0) == e.runtime.tmp.add_to_cart_shipping_method_id &&
                (e.runtime.tmp.add_to_cart_shipping_method_id =
                  parseFloatLadiPage(
                    e.runtime.tmp.add_to_cart_shipping_method_id
                  ) || 0),
              (a = t.target.querySelector(
                'option[value="' + t.target.value + '"]'
              )),
              (e.runtime.tmp.add_to_cart_fee_shipping = e.isEmpty(a)
                ? 0
                : parseFloatLadiPage(a.getAttribute("data-fee")) || 0),
              e.changeTotalPriceCart();
          },
          r = document.querySelectorAll(
            'select[data-combobox-type="' +
              e.const.COMBOBOX_TYPE.delivery_method +
              '"]'
          ),
          s = 0;
        s < r.length;
        s++
      ) {
        var l = r[s],
          c = l.getElementsByTagName("option");
        for (
          "true" != l.getAttribute("data-event") &&
            (l.setAttribute("data-event", !0), l.addEventListener("change", o)),
            e.isNull(l.getAttribute("data-placeholder")) &&
              l.setAttribute("data-placeholder", c[0].innerHTML);
          c.length > 0;

        )
          c[0].parentElement.removeChild(c[0]);
        i.length <= 0 &&
          (((a = document.createElement("option")).innerHTML =
            l.getAttribute("data-placeholder")),
          l.appendChild(a));
        for (var d = 0; d < i.length; d++) {
          a = document.createElement("option");
          var p = e.formatNumber(i[d].fee, 3);
          if (
            e.runtime.tmp.cart.length > 0 &&
            !e.isEmpty(e.runtime.tmp.cart[0].currency) &&
            !e.isEmpty(e.runtime.tmp.cart[0].currency.symbol)
          ) {
            var u = e.runtime.tmp.cart[0].currency.symbol;
            p = e.formatCurrency(i[d].fee, u, !0);
          }
          (a.innerHTML = i[d].name + " - " + p),
            a.setAttribute("data-fee", i[d].fee),
            a.setAttribute("value", i[d].shipping_method_id),
            n == i[d].shipping_method_id &&
              a.setAttribute("selected", "selected"),
            l.appendChild(a);
        }
        l.setAttribute("data-selected", n || "");
      }
    };
    e.runtime.tmp.timeout_reload_fee_shipping = e.runTimeout(function () {
      if (0 != e.runtime.tmp.list_form_checkout.length) {
        var n = null;
        if (
          ((n = e.isEmpty(t)
            ? document.getElementById(e.runtime.tmp.list_form_checkout[0])
            : e.findAncestor(t.target, "prime-form")),
          e.isEmpty(n))
        )
          a(0);
        else {
          var o = n.querySelector('.prime-form-item select[name="state"]'),
            r = n.querySelector('.prime-form-item select[name="district"]'),
            s = n.querySelector('.prime-form-item select[name="ward"]');
          if (e.isEmpty(o) || e.isEmpty(r) || e.isEmpty(s)) a(0);
          else {
            var l = o.value,
              c = r.value,
              d = s.value;
            if (e.isEmpty(l) || e.isEmpty(c)) return void a(0);
            var p = {
              state_id: l.split(":")[0],
              district_id: c.split(":")[0],
              ward_id: d.split(":")[0],
            };
            (p.state_id = parseInt(p.state_id) || p.state_id),
              (p.district_id = parseInt(p.district_id) || p.district_id),
              (p.ward_id = parseInt(p.ward_id) || p.ward_id);
            var u = null,
              m = null,
              _ = null;
            if (
              e.runtime.shopping_product_type == e.const.FORM_CONFIG_TYPE.sapo
            )
              return (
                (m = function (t, n) {
                  (t = t.querySelector("#checkoutForm")),
                    (u = new FormData()).append("_method", "patch"),
                    u.append("billingProvince", p.state_id),
                    u.append("billingDistrict", p.district_id),
                    u.append("billingWard", p.ward_id),
                    e.sendRequest(
                      "POST",
                      window.location.origin + t.getAttribute("action"),
                      u,
                      !0,
                      _,
                      function (t, n, o) {
                        if (o.readyState == XMLHttpRequest.DONE) {
                          if (200 == n)
                            try {
                              for (
                                var r = 0,
                                  s = [],
                                  d = e
                                    .createTmpElement("div", t, null, !1)
                                    .querySelectorAll(
                                      '[for^="shippingMethod-"]'
                                    ),
                                  p = 0;
                                p < d.length;
                                p++
                              ) {
                                var u = "",
                                  m = 0,
                                  _ = "",
                                  y = d[p].querySelector(
                                    "span.radio__label__primary"
                                  );
                                e.isEmpty(y) || (u = y.textContent.trim()),
                                  (y = d[p].querySelector(
                                    "span.content-box__emphasis"
                                  )),
                                  e.isEmpty(y) ||
                                    (m =
                                      parseFloatLadiPage(
                                        y.textContent
                                          .trim()
                                          .replaceAll(".", "")
                                          .replaceAll(",", ".")
                                      ) || 0);
                                var g = e.findAncestor(
                                  d[p],
                                  "content-box__row"
                                );
                                e.isEmpty(g) ||
                                  ((g = g.querySelector(
                                    'input[name="shippingMethod"]'
                                  )),
                                  e.isEmpty(g) || (_ = g.value)),
                                  e.isEmpty(u) ||
                                    e.isEmpty(_) ||
                                    s.push({
                                      name: u,
                                      fee: m,
                                      shipping_method_id: _,
                                    });
                              }
                              return (
                                -1 ==
                                  s.findIndex(function (t) {
                                    return (
                                      t.shipping_method_id ==
                                      e.runtime.tmp
                                        .add_to_cart_shipping_method_id
                                    );
                                  }) &&
                                  (e.runtime.tmp.add_to_cart_shipping_method_id =
                                    null),
                                s.forEach(function (t) {
                                  e.isEmpty(
                                    e.runtime.tmp.add_to_cart_shipping_method_id
                                  ) &&
                                    (e.runtime.tmp.add_to_cart_shipping_method_id =
                                      t.shipping_method_id),
                                    e.runtime.tmp
                                      .add_to_cart_shipping_method_id ==
                                      t.shipping_method_id && (r = t.fee || 0);
                                }),
                                i("state", l),
                                i("district", c),
                                void a(r, s)
                              );
                            } catch (t) {}
                          a(0);
                        }
                      }
                    );
                }),
                void e.getCheckoutSapo(null, m, a)
              );
            if (
              e.runtime.shopping_product_type ==
              e.const.FORM_CONFIG_TYPE.haravan
            )
              return (
                (m = function (t) {
                  _ = { "X-Requested-With": "XMLHttpRequest" };
                  var n = e.createTmpElement("a", "", { href: t });
                  (n.search =
                    n.search +
                    (e.isEmpty(n.search) ? "?" : "&") +
                    "customer_shipping_province=" +
                    p.state_id +
                    "&customer_shipping_district=" +
                    p.district_id +
                    "&form_name=form_update_location"),
                    (t = n.href),
                    e.sendRequest("GET", t, null, !0, _, function (t, n, o) {
                      if (o.readyState == XMLHttpRequest.DONE) {
                        if (200 == n)
                          try {
                            for (
                              var r = 0,
                                s = [],
                                d = e
                                  .createTmpElement("div", t, null, !1)
                                  .querySelectorAll(
                                    '[for^="shipping_rate_id_"]'
                                  ),
                                p = 0;
                              p < d.length;
                              p++
                            ) {
                              var u = "",
                                m = 0,
                                _ = "",
                                y = d[p].querySelector(
                                  "span.radio-label-primary"
                                );
                              e.isEmpty(y) || (u = y.textContent.trim()),
                                (y = d[p].querySelector(
                                  "span.content-box-emphasis"
                                )),
                                e.isEmpty(y) ||
                                  (m =
                                    parseFloatLadiPage(
                                      y.textContent.trim().replaceAll(",", "")
                                    ) || 0);
                              var g = d[p].querySelector("input");
                              e.isEmpty(g) || (_ = g.value),
                                e.isEmpty(u) ||
                                  e.isEmpty(_) ||
                                  s.push({
                                    name: u,
                                    fee: m,
                                    shipping_method_id: _,
                                  });
                            }
                            return (
                              -1 ==
                                s.findIndex(function (t) {
                                  return (
                                    t.shipping_method_id ==
                                    e.runtime.tmp.add_to_cart_shipping_method_id
                                  );
                                }) &&
                                (e.runtime.tmp.add_to_cart_shipping_method_id =
                                  null),
                              s.forEach(function (t) {
                                e.isEmpty(
                                  e.runtime.tmp.add_to_cart_shipping_method_id
                                ) &&
                                  (e.runtime.tmp.add_to_cart_shipping_method_id =
                                    t.shipping_method_id),
                                  e.runtime.tmp
                                    .add_to_cart_shipping_method_id ==
                                    t.shipping_method_id && (r = t.fee || 0);
                              }),
                              i("state", l),
                              i("district", c),
                              void a(r, s)
                            );
                          } catch (t) {}
                        a(0);
                      }
                    });
                }),
                void e.getThirdPartyCheckoutUrl(!1, m)
              );
            if (
              e.runtime.shopping_product_type ==
              e.const.FORM_CONFIG_TYPE.shopify
            )
              return void a(0);
            if (
              e.runtime.shopping_product_type ==
              e.const.FORM_CONFIG_TYPE.wordpress
            )
              return void a(0);
            var y = window.ladi("_cart_token").get_cookie(),
              g = window.ladi("_checkout_token").get_cookie();
            if (e.isEmpty(y) || e.isEmpty(g)) return void a(0);
            (_ = { "Content-Type": "application/json", "cart-token": y }),
              e.sendRequest(
                "POST",
                e.const.API_LADISALE_GET_SHIPPING.format(g),
                JSON.stringify(p),
                !0,
                _,
                function (t, i, n) {
                  if (n.readyState == XMLHttpRequest.DONE) {
                    if (200 == i)
                      try {
                        var o = JSON.parse(t);
                        if (200 == o.code) {
                          var r = 0;
                          return (
                            e.isArray(o.data.shipping_methods) &&
                              (-1 ==
                                o.data.shipping_methods.findIndex(function (t) {
                                  return (
                                    t.shipping_method_id ==
                                    e.runtime.tmp.add_to_cart_shipping_method_id
                                  );
                                }) &&
                                (e.runtime.tmp.add_to_cart_shipping_method_id =
                                  null),
                              o.data.shipping_methods.forEach(function (t) {
                                e.isEmpty(
                                  e.runtime.tmp.add_to_cart_shipping_method_id
                                ) &&
                                  (e.runtime.tmp.add_to_cart_shipping_method_id =
                                    t.shipping_method_id),
                                  e.runtime.tmp
                                    .add_to_cart_shipping_method_id ==
                                    t.shipping_method_id && (r = t.fee || 0);
                              })),
                            void a(r, o.data.shipping_methods)
                          );
                        }
                      } catch (t) {}
                    a(0);
                  }
                }
              );
          }
        }
      }
    }, 1e3);
  }),
  (PrimePageScriptV2.prototype.getThirdPartyCheckoutUrl = function (t, e) {
    var i = this,
      a = window.location.origin + "/checkout",
      n = function (n, o, r) {
        t &&
          ((a = i.getLinkUTMRedirect(a, null)),
          (a = i.convertDataReplaceStr(a, !0)),
          window.ladi(a).open_url()),
          i.isFunction(e) && e(a, n, o, r);
      };
    -1 !=
      [
        i.const.FORM_CONFIG_TYPE.sapo,
        i.const.FORM_CONFIG_TYPE.haravan,
        i.const.FORM_CONFIG_TYPE.shopify,
      ].indexOf(i.runtime.shopping_product_type) &&
      i.sendRequest("GET", a, null, !0, null, function (t, e, i) {
        i.readyState == XMLHttpRequest.HEADERS_RECEIVED && (a = i.responseURL),
          i.readyState == XMLHttpRequest.DONE && 200 == e && n(t, e, i);
      }),
      -1 !=
        [i.const.FORM_CONFIG_TYPE.wordpress].indexOf(
          i.runtime.shopping_product_type
        ) && n(),
      -1 !=
        [i.const.FORM_CONFIG_TYPE.ladisales].indexOf(
          i.runtime.shopping_product_type
        ) &&
        (i.isNull(i.runtime.shopping_config_checkout_id)
          ? ((a = i.runtime.tmp.ladisales_checkout_url), n())
          : (t && window.ladi("POPUP_CHECKOUT").show(),
            i.isFunction(e) && e()));
  }),
  (PrimePageScriptV2.prototype.getCheckoutAll = function (t, e, i) {
    var a = this;
    a.getThirdPartyCheckoutUrl(!1, function (t, i, n, o) {
      try {
        var r = a.createTmpElement("div", i, null, !1);
        return void (a.isFunction(e) && e(r, i));
      } catch (t) {}
    });
  }),
  (PrimePageScriptV2.prototype.getCheckoutSapo = function (t, e, i) {
    this.getCheckoutAll(t, e, i);
  }),
  (PrimePageScriptV2.prototype.getCheckoutHaravan = function (t, e, i) {
    this.getCheckoutAll(t, e, i);
  }),
  (PrimePageScriptV2.prototype.getCheckoutWordpress = function (t, e, i) {
    this.getCheckoutAll(t, e, i);
  }),
  (PrimePageScriptV2.prototype.getCheckoutShopify = function (t, e, i) {
    return this.getCheckoutAll(t, e, i);
  }),
  (PrimePageScriptV2.prototype.reloadCheckoutSapo = function () {
    var t = this;
    t.getCheckoutSapo(null, function (e, i) {
      var a = 0,
        n = (e = e.querySelector("#checkoutForm")).querySelector(
          "#discountCode .discount-code span.ui-tag__label"
        );
      if (!t.isEmpty(n)) {
        var o = e.querySelector("#discountCode .discount-code .col-block");
        t.isEmpty(o) ||
          (a =
            parseFloatLadiPage(
              o.textContent.trim().replaceAll(".", "").replaceAll(",", ".")
            ) || 0);
        var r = document.querySelector(
          "#POPUP_CHECKOUT .prime-form .prime-element[data-submit-form-id]"
        );
        if (!t.isEmpty(r)) {
          var s = t
            .findAncestor(r, "prime-form")
            .querySelector('input[name="coupon"]');
          t.isEmpty(s) ||
            ((s.value = n.textContent.trim()),
            t.fireEvent(s, "change"),
            (t.runtime.tmp.current_use_coupon = s.value),
            t.reloadPriceDiscount({ target: r, discount_price: a }));
        }
      }
    });
  }),
  (PrimePageScriptV2.prototype.reloadCheckoutHaravan = function () {
    var t = this;
    t.getCheckoutHaravan(null, function (e, i) {
      var a = 0,
        n = e.querySelector(".applied-reduction-code-information");
      if (!t.isEmpty(n)) {
        var o = e.querySelector(
          ".total-line-price span[data-checkout-discount-amount-target]"
        );
        t.isEmpty(o) ||
          (a =
            (parseFloatLadiPage(
              o.getAttribute("data-checkout-discount-amount-target")
            ) || 0) / 100);
        var r = document.querySelector(
          "#POPUP_CHECKOUT .prime-form .prime-element[data-submit-form-id]"
        );
        if (!t.isEmpty(r)) {
          var s = t
            .findAncestor(r, "prime-form")
            .querySelector('input[name="coupon"]');
          t.isEmpty(s) ||
            ((s.value = n.textContent.trim()),
            t.fireEvent(s, "change"),
            (t.runtime.tmp.current_use_coupon = s.value),
            t.reloadPriceDiscount({
              target: r,
              discount_price: a,
              docCrawl: e,
            }));
        }
      }
    });
  }),
  (PrimePageScriptV2.prototype.reloadCheckoutWordpress = function () {
    var t = this;
    this.getCheckoutWordpress(null, function (e, i) {
      var a = 0,
        n = e.querySelector("#order_review .cart-discount");
      if (!t.isEmpty(n)) {
        var o = n.querySelector("a[data-coupon]");
        if (!t.isEmpty(o)) {
          var r = n.querySelector(".woocommerce-Price-amount");
          t.isEmpty(r) ||
            (a =
              parseFloatLadiPage(
                r.textContent.trim().replaceAll(".", "").replaceAll(",", ".")
              ) || 0);
          var s = document.querySelector(
            "#POPUP_CHECKOUT .prime-form .prime-element[data-submit-form-id]"
          );
          if (!t.isEmpty(s)) {
            var l = t
              .findAncestor(s, "prime-form")
              .querySelector('input[name="coupon"]');
            t.isEmpty(l) ||
              ((l.value = o.getAttribute("data-coupon")),
              t.fireEvent(l, "change"),
              (t.runtime.tmp.current_use_coupon = l.value),
              t.reloadPriceDiscount({
                target: s,
                discount_price: a,
                docCrawl: e,
              }));
          }
        }
      }
    });
  }),
  (PrimePageScriptV2.prototype.reloadPriceDiscount = function (t) {
    var e = this;
    e instanceof PrimePageScriptV2 || (e = PrimePageScript);
    var i = e.runtime.tmp.current_use_coupon,
      a = function (n, o) {
        (e.runtime.tmp.add_to_cart_discount = n),
          (e.runtime.tmp.is_click_check_price_discount = !1),
          e.changeTotalPriceCart();
        var r = null,
          s = document.getElementsByClassName("prime-form-remove-coupon")[0];
        e.isEmpty(s) ||
          ((r = e.findAncestor(s, "prime-form")),
          s.parentElement.removeChild(s));
        var l = document.querySelector(
          '.prime-form-item input[name="coupon"].pointer-events-none'
        );
        if (
          (e.isEmpty(l) || l.classList.remove("pointer-events-none"),
          !e.isEmpty(e.runtime.tmp.current_use_coupon))
        ) {
          if (
            (e.isEmpty(r) &&
              !e.isEmpty(t) &&
              (r = e.findAncestor(t.target, "prime-form")),
            e.isEmpty(r))
          )
            return;
          if (
            ((l = r.querySelector('.prime-form-item input[name="coupon"]')),
            e.isEmpty(l))
          )
            return;
          ((s = document.createElement("span")).className =
            "prime-form-remove-coupon"),
            (s.innerHTML = "⌫"),
            s.addEventListener("click", function (t) {
              t.stopPropagation();
              var i = e.runtime.tmp.current_use_coupon;
              if (
                ((l.value = ""),
                e.fireEvent(l, "change"),
                (e.runtime.tmp.current_use_coupon = null),
                a(0),
                -1 !=
                  [
                    e.const.FORM_CONFIG_TYPE.sapo,
                    e.const.FORM_CONFIG_TYPE.haravan,
                    e.const.FORM_CONFIG_TYPE.shopify,
                    e.const.FORM_CONFIG_TYPE.wordpress,
                  ].indexOf(e.runtime.shopping_product_type))
              ) {
                var n = document.querySelector(
                  "#POPUP_CHECKOUT .prime-form .prime-element[data-submit-form-id]"
                );
                e.isEmpty(n) ||
                  e.reloadPriceDiscount({
                    isRemoveCoupon: !0,
                    couponOld: i,
                    target: n,
                  });
              }
            }),
            e.findAncestor(l, "prime-form-item").appendChild(s),
            l.classList.add("pointer-events-none");
        }
        if (
          (i != e.runtime.tmp.current_use_coupon ||
            e.isEmpty(t) ||
            !t.isRemoveCoupon ||
            !e.isEmpty(t.docCrawl)) &&
          e.runtime.shopping_product_type == e.const.FORM_CONFIG_TYPE.haravan
        ) {
          if (
            (delete e.runtime.tmp.use_cart_line_price_original,
            !e.isEmpty(e.runtime.tmp.current_use_coupon))
          ) {
            var c = o.querySelector(
              ".order-summary-emphasis[data-checkout-subtotal-price-target]"
            );
            e.isEmpty(c) ||
              ((c =
                (parseFloatLadiPage(
                  c.getAttribute("data-checkout-subtotal-price-target")
                ) || 0) / 100),
              e.changeTotalPriceCart(!0).cart_price != c &&
                (e.runtime.tmp.use_cart_line_price_original = !0));
          }
          e.updateCartPromotion();
        }
      };
    e.isEmpty(t) || e.isEmpty(t.discount_price)
      ? e.runtime.tmp.is_click_check_price_discount ||
        (e.isEmpty(t) && e.isEmpty(e.runtime.tmp.current_use_coupon)) ||
        (e.isEmpty(e.runtime.tmp.timeout_reload_price_discount) ||
          e.removeTimeout(e.runtime.tmp.timeout_reload_price_discount),
        (e.runtime.tmp.timeout_reload_price_discount = e.runTimeout(
          function () {
            if (0 != e.runtime.tmp.list_form_checkout.length) {
              var i = null;
              if (
                (e.isEmpty(t)
                  ? (i = document.getElementById(
                      e.runtime.tmp.list_form_checkout[0]
                    ))
                  : ((i = e.findAncestor(t.target, "prime-form")),
                    e.isEmpty(i) ||
                      ((i = i.querySelector("[data-submit-form-id]")),
                      e.isEmpty(i) ||
                        (i = document.getElementById(
                          i.getAttribute("data-submit-form-id")
                        )))),
                e.isEmpty(i))
              )
                a(0);
              else {
                var n = null,
                  o = i.querySelector('.prime-form-item input[name="email"]'),
                  r = i.querySelector('.prime-form-item input[name="phone"]'),
                  s = i.querySelector('.prime-form-item input[name="coupon"]');
                if (e.isEmpty(s)) a(0);
                else {
                  var l = e.runtime.tmp.current_use_coupon;
                  e.runtime.tmp.current_use_coupon = null;
                  var c = s.value;
                  if (e.isEmpty(c) && (e.isEmpty(t) || !t.isRemoveCoupon))
                    return void a(0);
                  if (e.isEmpty(t) && c != l) return void a(0);
                  (n = { discount_code: c }),
                    e.isEmpty(o) || (n.email = o.value),
                    e.isEmpty(r) || (n.phone = r.value),
                    (e.runtime.tmp.is_click_check_price_discount = !0);
                  var d = null,
                    p = null,
                    u = null;
                  if (
                    e.runtime.shopping_product_type ==
                    e.const.FORM_CONFIG_TYPE.sapo
                  )
                    return (
                      (p = function (i, o) {
                        (i = i.querySelector("#checkoutForm")),
                          (u = new FormData()).append("_method", "patch"),
                          u.append("reductionCode", n.discount_code),
                          u.append("email", n.email),
                          e.sendRequest(
                            "POST",
                            window.location.origin + i.getAttribute("action"),
                            u,
                            !0,
                            d,
                            function (i, n, o) {
                              if (o.readyState == XMLHttpRequest.DONE) {
                                if (200 == n)
                                  try {
                                    var r = 0,
                                      s = e.createTmpElement(
                                        "div",
                                        i,
                                        null,
                                        !1
                                      ),
                                      l = s.querySelector(
                                        "#discountCode .discount-code span.ui-tag__label"
                                      );
                                    if (e.isEmpty(l)) {
                                      var d = s.querySelector(
                                        "#discountCode .field__message--error"
                                      );
                                      e.isEmpty(d) ||
                                        e.isEmpty(d.textContent.trim()) ||
                                        e.isEmpty(t) ||
                                        e.showMessage(d.textContent.trim());
                                    } else {
                                      e.runtime.tmp.current_use_coupon = c;
                                      var p = s.querySelector(
                                        "#discountCode .discount-code .col-block"
                                      );
                                      e.isEmpty(p) ||
                                        (r =
                                          parseFloatLadiPage(
                                            p.textContent
                                              .trim()
                                              .replaceAll(".", "")
                                              .replaceAll(",", ".")
                                          ) || 0);
                                    }
                                    return void a(r);
                                  } catch (t) {}
                                e.isEmpty(t) ||
                                  e.showMessage(
                                    e.const.LANG.REQUEST_SEND_ERROR
                                  ),
                                  a(0);
                              }
                            }
                          );
                      }),
                      void e.getCheckoutSapo(t, p, a)
                    );
                  if (
                    e.runtime.shopping_product_type ==
                    e.const.FORM_CONFIG_TYPE.haravan
                  )
                    return (
                      (p = function (i) {
                        d = { "X-Requested-With": "XMLHttpRequest" };
                        var o = e.createTmpElement("a", "", { href: i });
                        e.isEmpty(n.discount_code)
                          ? (o.search =
                              o.search +
                              (e.isEmpty(o.search) ? "?" : "&") +
                              "form_name=form_discount_remove")
                          : (o.search =
                              o.search +
                              (e.isEmpty(o.search) ? "?" : "&") +
                              "discount.code=" +
                              n.discount_code +
                              "&form_name=form_discount_add"),
                          (i = o.href),
                          e.sendRequest(
                            "GET",
                            i,
                            null,
                            !0,
                            d,
                            function (i, n, o) {
                              if (o.readyState == XMLHttpRequest.DONE) {
                                if (200 == n)
                                  try {
                                    var r = 0,
                                      s = e.createTmpElement(
                                        "div",
                                        i,
                                        null,
                                        !1
                                      ),
                                      l = s.querySelector(
                                        ".applied-reduction-code-information"
                                      );
                                    if (
                                      e.isEmpty(l) ||
                                      l.textContent.trim().toLowerCase() !=
                                        c.trim().toLowerCase()
                                    ) {
                                      var d = s.querySelector(
                                        "#form_discount_add .field-message-error"
                                      );
                                      e.isEmpty(d) ||
                                        e.isEmpty(d.textContent.trim()) ||
                                        e.isEmpty(t) ||
                                        e.showMessage(d.textContent.trim());
                                    } else {
                                      e.runtime.tmp.current_use_coupon = c;
                                      var p = s.querySelector(
                                        ".total-line-price span[data-checkout-discount-amount-target]"
                                      );
                                      e.isEmpty(p) ||
                                        (r =
                                          (parseFloatLadiPage(
                                            p.getAttribute(
                                              "data-checkout-discount-amount-target"
                                            )
                                          ) || 0) / 100);
                                    }
                                    return void a(r, s);
                                  } catch (t) {}
                                e.isEmpty(t) ||
                                  e.showMessage(
                                    e.const.LANG.REQUEST_SEND_ERROR
                                  ),
                                  a(0);
                              }
                            }
                          );
                      }),
                      void e.getThirdPartyCheckoutUrl(!1, p)
                    );
                  if (
                    e.runtime.shopping_product_type ==
                    e.const.FORM_CONFIG_TYPE.shopify
                  )
                    return void a(0);
                  if (
                    e.runtime.shopping_product_type ==
                    e.const.FORM_CONFIG_TYPE.wordpress
                  ) {
                    var m = window.location.origin + "/ladipage/api";
                    return (
                      e.isEmpty(n.discount_code)
                        ? (m +=
                            "?action=remove_coupon&coupon=" +
                            (e.isEmpty(t) ? "" : t.couponOld))
                        : (m +=
                            "?action=apply_coupon&coupon=" + n.discount_code),
                      void e.sendRequest(
                        "GET",
                        m,
                        null,
                        !0,
                        d,
                        function (i, n, o) {
                          if (o.readyState == XMLHttpRequest.DONE) {
                            if (200 == n)
                              try {
                                var r = JSON.parse(i);
                                if (200 == r.code)
                                  return (
                                    (e.runtime.tmp.current_use_coupon = c),
                                    void a(r.data)
                                  );
                                e.isEmpty(r.message) ||
                                  e.isEmpty(t) ||
                                  e.showMessage(r.message);
                              } catch (t) {}
                            a(0);
                          }
                        }
                      )
                    );
                  }
                  var _ = window.ladi("_cart_token").get_cookie(),
                    y = window.ladi("_checkout_token").get_cookie();
                  if (e.isEmpty(_) || e.isEmpty(y)) return void a(0);
                  (d = { "Content-Type": "application/json", "cart-token": _ }),
                    e.sendRequest(
                      "POST",
                      e.const.API_LADISALE_VALIDATE_DISCOUNT.format(y),
                      JSON.stringify(n),
                      !0,
                      d,
                      function (i, n, o) {
                        if (o.readyState == XMLHttpRequest.DONE) {
                          if (200 == n)
                            try {
                              var r = JSON.parse(i);
                              if (200 == r.code)
                                return (
                                  e.isEmpty(r.data.discount_error)
                                    ? (e.runtime.tmp.current_use_coupon = c)
                                    : e.isEmpty(t) ||
                                      e.showMessage(r.data.discount_error),
                                  void a(r.data.discount_price)
                                );
                            } catch (t) {}
                          a(0);
                        }
                      }
                    );
                }
              }
            }
          },
          e.isEmpty(t) ? 1e3 : 0
        )))
      : a(t.discount_price, t.docCrawl);
  }),
  (PrimePageScriptV2.prototype.updateCartPromotion = function (t, e, i) {
    var a = this,
      n = function (t) {
        for (var e = 0; e < a.runtime.tmp.cart.length; e++) {
          var i = a.runtime.tmp.cart[e];
          if (
            i.product_id == t.product_id &&
            i.product_variant_id == t.product_variant_id
          ) {
            a.runtime.tmp.use_cart_line_price_original || t.sub_total == t.total
              ? delete i.promotion
              : (i.promotion = t),
              (a.runtime.tmp.cart[e] = i);
            for (
              var n = document.querySelectorAll(
                  '.prime-cart .prime-cart-row .prime-cart-price span[data-product-variant-id="' +
                    i.product_variant_id +
                    '"]'
                ),
                o = 0;
              o < n.length;
              o++
            ) {
              var r = a.findAncestor(n[o], "prime-cart-row"),
                s = r.querySelector(".prime-cart-price"),
                l = r.querySelector(".prime-cart-title"),
                c = s.querySelector("span.price-compare"),
                d = l.querySelector("span.promotion-name");
              if (a.isObject(i.promotion)) {
                r.classList.add("has-promotion"),
                  a.isEmpty(c) &&
                    (((c = document.createElement("span")).className =
                      "price-compare"),
                    s.appendChild(c));
                var p = a.formatNumber(t.total, 3);
                a.isObject(i.currency) &&
                  !a.isEmpty(i.currency.symbol) &&
                  (p = a.formatCurrency(t.total, i.currency.symbol, !0)),
                  (c.innerHTML = p),
                  a.isEmpty(d) &&
                    (((d = document.createElement("span")).className =
                      "promotion-name"),
                    l.appendChild(d)),
                  (d.innerHTML = i.promotion.discount.note);
              } else
                r.classList.remove("has-promotion"),
                  a.isEmpty(c) || c.parentElement.removeChild(c),
                  a.isEmpty(d) || d.parentElement.removeChild(d);
            }
          }
        }
      },
      o = function () {
        var t = [];
        return (
          a.isArray(a.runtime.tmp.cart) &&
            a.runtime.tmp.cart.forEach(function (e) {
              var i = e.quantity;
              t.push({
                product_id: e.product_id,
                product_variant_id: e.product_variant_id,
                quantity: i,
              });
            }),
          { variants: t }
        );
      },
      r = o(),
      s = JSON.stringify(r);
    if (
      (a.isObject(a.runtime.tmp.info_update_cart_promotion) ||
        (a.runtime.tmp.info_update_cart_promotion = {}),
      !a.isEmpty(t))
    ) {
      var l = [],
        c = a.runtime.tmp.cart;
      return (
        (a.runtime.tmp.cart = []),
        -1 !=
          [
            a.const.FORM_CONFIG_TYPE.sapo,
            a.const.FORM_CONFIG_TYPE.haravan,
            a.const.FORM_CONFIG_TYPE.shopify,
          ].indexOf(a.runtime.shopping_product_type) &&
          t.items.forEach(function (e) {
            var i = null;
            -1 !=
              [a.const.FORM_CONFIG_TYPE.sapo].indexOf(
                a.runtime.shopping_product_type
              ) &&
              e.name == e.title &&
              "Default Title" == e.variant_title &&
              (e.variant_title = null),
              -1 !=
                [
                  a.const.FORM_CONFIG_TYPE.haravan,
                  a.const.FORM_CONFIG_TYPE.shopify,
                ].indexOf(a.runtime.shopping_product_type) &&
                e.product_title == e.title &&
                "Default Title" == e.variant_title &&
                (e.variant_title = null),
              -1 !=
                [
                  a.const.FORM_CONFIG_TYPE.sapo,
                  a.const.FORM_CONFIG_TYPE.haravan,
                ].indexOf(a.runtime.shopping_product_type) &&
                (i = {
                  store_id: -1,
                  product_id: e.product_id,
                  product_variant_id: e.variant_id,
                  name: e.title,
                  title: e.variant_title,
                  image: e.image,
                  quantity: e.quantity,
                  inventory_checked: 0,
                  available_quantity: 0,
                  min_buy: 1,
                  max_buy: null,
                }),
              -1 !=
                [a.const.FORM_CONFIG_TYPE.shopify].indexOf(
                  a.runtime.shopping_product_type
                ) &&
                (i = {
                  store_id: -1,
                  product_id: e.product_id,
                  product_variant_id: e.variant_id,
                  name: e.product_title,
                  title: e.variant_title,
                  image: e.image,
                  quantity: e.quantity,
                  inventory_checked: 0,
                  available_quantity: 0,
                  min_buy: 1,
                  max_buy: null,
                });
            var n = a.runtime.currency;
            a.runtime.isClient || (n = window.$rootScope.getStoreCurrency()),
              -1 ==
                [a.const.FORM_CONFIG_TYPE.shopify].indexOf(
                  a.runtime.shopping_product_type
                ) ||
                a.isEmpty(t.currency) ||
                (n = t.currency),
              (i.currency = {
                code: n,
                symbol: a.formatCurrency(null, n, !1, !0),
              });
            var o = c.findIndex(function (t) {
              return (
                t.product_id == i.product_id &&
                t.product_variant_id == i.product_variant_id
              );
            });
            -1 != o && (i.title = c[o].title);
            var r = null,
              s = null;
            if (
              -1 !=
              [
                a.const.FORM_CONFIG_TYPE.sapo,
                a.const.FORM_CONFIG_TYPE.haravan,
              ].indexOf(a.runtime.shopping_product_type)
            ) {
              if (((i.price = e.price_original), a.isObject(e.properties))) {
                var d = Object.keys(e.properties);
                d.length > 0 &&
                  ((r = e.properties[d[0]]),
                  -1 !=
                    [a.const.FORM_CONFIG_TYPE.haravan].indexOf(
                      a.runtime.shopping_product_type
                    ) &&
                    ((r = r.split("-")).shift(),
                    (r = (r = r.join("-")).trim())),
                  (s = {
                    product_id: e.product_id,
                    product_variant_id: e.variant_id,
                    sub_total: e.line_price_original,
                    total: e.line_price,
                    discount: { note: r },
                  }),
                  (i.promotion = s),
                  l.push(s));
              }
              a.isEmpty(i.promotion) &&
                -1 !=
                  [a.const.FORM_CONFIG_TYPE.sapo].indexOf(
                    a.runtime.shopping_product_type
                  ) &&
                (i.price = e.price);
            }
            -1 !=
              [a.const.FORM_CONFIG_TYPE.shopify].indexOf(
                a.runtime.shopping_product_type
              ) &&
              ((i.price = e.original_price),
              e.original_line_price != e.line_price &&
                a.isArray(e.discounts) &&
                e.discounts.length > 0 &&
                ((r = e.discounts[0].title),
                (s = {
                  product_id: e.product_id,
                  product_variant_id: e.variant_id,
                  sub_total: e.original_line_price,
                  total: e.line_price,
                  discount: { note: r },
                }),
                (i.promotion = s),
                l.push(s))),
              -1 !=
                [
                  a.const.FORM_CONFIG_TYPE.haravan,
                  a.const.FORM_CONFIG_TYPE.shopify,
                ].indexOf(a.runtime.shopping_product_type) &&
                ((i.price = i.price / 100),
                a.isEmpty(i.promotion) ||
                  ((i.promotion.total = i.promotion.total / 100),
                  (i.promotion.sub_total = i.promotion.sub_total / 100))),
              a.runtime.tmp.cart.push(i);
          }),
        -1 !=
          [a.const.FORM_CONFIG_TYPE.wordpress].indexOf(
            a.runtime.shopping_product_type
          ) &&
          t.data.forEach(function (t) {
            var e = null;
            a.isObject(t.variation) &&
              (e = Object.values(t.variation).join(", "));
            var i = {
                store_id: -1,
                product_id: t.product_id,
                product_variant_id: t.variation_id,
                name: t.product_name,
                title: e,
                min_buy: 1,
                max_buy: null,
                price: t.price,
                image: t.image || "",
                quantity: t.quantity,
                inventory_checked: 0,
                available_quantity: 0,
              },
              n = a.runtime.currency;
            a.runtime.isClient || (n = window.$rootScope.getStoreCurrency()),
              (i.currency = {
                code: n,
                symbol: a.formatCurrency(null, n, !1, !0),
              });
            var o = c.findIndex(function (t) {
              return (
                t.product_id == i.product_id &&
                t.product_variant_id == i.product_variant_id
              );
            });
            -1 != o && (i.title = c[o].title),
              (i.cart_item_key = t.key),
              a.runtime.tmp.cart.push(i);
          }),
        (r = o()),
        (s = JSON.stringify(r)),
        l.forEach(n),
        void (a.runtime.tmp.info_update_cart_promotion[s] = l)
      );
    }
    if (!0 !== a.runtime.tmp.info_update_cart_promotion[s]) {
      if (a.isArray(a.runtime.tmp.info_update_cart_promotion[s]))
        return (
          a.runtime.tmp.info_update_cart_promotion[s].forEach(n),
          void a.changeTotalPriceCart()
        );
      if (
        ((a.runtime.tmp.info_update_cart_promotion[s] = !0),
        -1 ==
          [
            a.const.FORM_CONFIG_TYPE.sapo,
            a.const.FORM_CONFIG_TYPE.haravan,
            a.const.FORM_CONFIG_TYPE.shopify,
          ].indexOf(a.runtime.shopping_product_type))
      )
        if (
          -1 ==
          [a.const.FORM_CONFIG_TYPE.wordpress].indexOf(
            a.runtime.shopping_product_type
          )
        ) {
          var d = window.ladi("_cart_token").get_cookie(),
            p = { "Content-Type": "application/json" };
          a.isEmpty(d) || (p["cart-token"] = d),
            a.sendRequest(
              "POST",
              a.const.API_LADISALE_PROMOTION,
              JSON.stringify(r),
              !0,
              p,
              function (t, e, o) {
                if (o.readyState == XMLHttpRequest.DONE) {
                  if (200 == e)
                    try {
                      var r = JSON.parse(t);
                      if (
                        200 == r.code &&
                        a.isObject(r.data) &&
                        a.isArray(r.data.variants)
                      )
                        return (
                          r.data.variants.forEach(n),
                          (a.runtime.tmp.info_update_cart_promotion[s] =
                            r.data.variants),
                          a.changeTotalPriceCart(),
                          void (a.isFunction(i) && i())
                        );
                    } catch (t) {}
                  delete a.runtime.tmp.info_update_cart_promotion[s],
                    a.isFunction(i) && i();
                }
              }
            );
        } else
          a.sendRequest(
            "GET",
            window.location.origin + "/ladipage/api?action=cart_info",
            null,
            !0,
            null,
            function (t, n, o) {
              if (o.readyState == XMLHttpRequest.DONE) {
                if (200 == n)
                  try {
                    var r = JSON.parse(t);
                    return (
                      delete a.runtime.tmp.info_update_cart_promotion[s],
                      a.updateCartPromotion(r),
                      e &&
                        a.runtime.tmp.cart.length > 0 &&
                        a.reloadCheckoutWordpress(),
                      a.changeTotalPriceCart(),
                      void (a.isFunction(i) && i())
                    );
                  } catch (t) {}
                delete a.runtime.tmp.info_update_cart_promotion[s],
                  a.isFunction(i) && i();
              }
            }
          );
      else
        a.sendRequest(
          "GET",
          window.location.origin + "/cart.js",
          null,
          !0,
          null,
          function (t, n, o) {
            if (o.readyState == XMLHttpRequest.DONE) {
              if (200 == n)
                try {
                  var r = JSON.parse(t);
                  return (
                    delete a.runtime.tmp.info_update_cart_promotion[s],
                    a.updateCartPromotion(r),
                    e &&
                      a.runtime.tmp.cart.length > 0 &&
                      (-1 !=
                        [a.const.FORM_CONFIG_TYPE.sapo].indexOf(
                          a.runtime.shopping_product_type
                        ) && a.reloadCheckoutSapo(),
                      -1 !=
                        [a.const.FORM_CONFIG_TYPE.haravan].indexOf(
                          a.runtime.shopping_product_type
                        ) && a.reloadCheckoutHaravan(),
                      [a.const.FORM_CONFIG_TYPE.shopify].indexOf(
                        a.runtime.shopping_product_type
                      )),
                    a.changeTotalPriceCart(),
                    void (a.isFunction(i) && i())
                  );
                } catch (t) {}
              delete a.runtime.tmp.info_update_cart_promotion[s],
                a.isFunction(i) && i();
            }
          }
        );
    }
  }),
  (PrimePageScriptV2.prototype.addCartCookie = function (t, e, i, a, n) {
    var o = this,
      r = {},
      s = null,
      l = null;
    if (
      -1 !=
      [
        o.const.FORM_CONFIG_TYPE.sapo,
        o.const.FORM_CONFIG_TYPE.haravan,
        o.const.FORM_CONFIG_TYPE.shopify,
      ].indexOf(o.runtime.shopping_product_type)
    )
      return (
        -1 !=
          [o.const.FORM_CONFIG_TYPE.sapo].indexOf(
            o.runtime.shopping_product_type
          ) &&
          ((l = new FormData()).append("variantId", e.product_variant_id),
          l.append("quantity", e.quantity)),
        -1 !=
          [o.const.FORM_CONFIG_TYPE.haravan].indexOf(
            o.runtime.shopping_product_type
          ) &&
          ((s = {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Requested-With": "XMLHttpRequest",
          }),
          (l = { id: e.product_variant_id, quantity: e.quantity }),
          (l = Object.keys(l)
            .reduce(function (t, e) {
              return t.push(e + "=" + encodeURIComponent(l[e])), t;
            }, [])
            .join("&"))),
        -1 !=
          [o.const.FORM_CONFIG_TYPE.shopify].indexOf(
            o.runtime.shopping_product_type
          ) &&
          ((s = {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Requested-With": "XMLHttpRequest",
          }),
          (l = {
            id: e.product_variant_id,
            quantity: e.quantity,
            form_type: "product",
          }),
          (l = Object.keys(l)
            .reduce(function (t, e) {
              return t.push(e + "=" + encodeURIComponent(l[e])), t;
            }, [])
            .join("&"))),
        void this.sendRequest(
          "POST",
          window.location.origin + "/cart/add.js",
          l,
          !0,
          s,
          function (t, e, s) {
            if (s.readyState == XMLHttpRequest.DONE) {
              if (200 == e)
                try {
                  if (((r = JSON.parse(t)), !o.isEmpty(r.id)))
                    return (
                      o.isFunction(i) && i(),
                      o.isFunction(n) && n(),
                      o.reloadPriceDiscount(),
                      void o.reloadFeeShipping()
                    );
                } catch (t) {}
              o.isFunction(a) && a(r), o.isFunction(n) && n();
            }
          }
        )
      );
    if (
      -1 ==
      [o.const.FORM_CONFIG_TYPE.wordpress].indexOf(
        o.runtime.shopping_product_type
      )
    ) {
      var c = window.ladi("_cart_token").get_cookie();
      ((s = { "Content-Type": "application/json" })["store-id"] = t),
        this.isEmpty(c) || (s["cart-token"] = c);
      var d = e.quantity;
      (l = JSON.stringify({
        type: "LP",
        product_variant_id: e.product_variant_id,
        quantity: d,
      })),
        this.sendRequest(
          "POST",
          this.const.API_LADISALE_ADD,
          l,
          !0,
          s,
          function (t, e, s) {
            if (s.readyState == XMLHttpRequest.DONE) {
              if (200 == e)
                try {
                  if (200 == (r = JSON.parse(t)).code)
                    return (
                      window
                        .ladi("_cart_token")
                        .set_cookie(r.data.cart_token, 30),
                      window
                        .ladi("_checkout_token")
                        .set_cookie(r.data.checkout_token, 30),
                      (o.runtime.tmp.ladisales_checkout_url = r.data.url),
                      o.isFunction(i) && i(),
                      o.isFunction(n) && n(),
                      o.reloadPriceDiscount(),
                      void o.reloadFeeShipping()
                    );
                } catch (t) {}
              o.isFunction(a) && a(r), o.isFunction(n) && n();
            }
          }
        );
    } else
      this.sendRequest(
        "POST",
        window.location.origin +
          "/ladipage/api?action=cart_add&product_id=" +
          e.product_id +
          "&variant_id=" +
          e.product_variant_id +
          "&qty=" +
          e.quantity,
        l,
        !0,
        s,
        function (t, e, s) {
          if (s.readyState == XMLHttpRequest.DONE) {
            if (200 == e)
              try {
                if (200 == (r = JSON.parse(t)).code)
                  return (
                    o.updateCartPromotion(r),
                    o.isFunction(i) && i(),
                    o.isFunction(n) && n(),
                    o.reloadPriceDiscount(),
                    void o.reloadFeeShipping()
                  );
              } catch (t) {}
            o.isFunction(a) && a(r), o.isFunction(n) && n();
          }
        }
      );
  }),
  (PrimePageScriptV2.prototype.updateCartCookie = function (t, e, i, a, n) {
    var o = this,
      r = {},
      s = null,
      l = null,
      c = null;
    if (
      -1 ==
      [
        o.const.FORM_CONFIG_TYPE.sapo,
        o.const.FORM_CONFIG_TYPE.haravan,
        o.const.FORM_CONFIG_TYPE.shopify,
      ].indexOf(o.runtime.shopping_product_type)
    ) {
      if (
        -1 !=
        [o.const.FORM_CONFIG_TYPE.wordpress].indexOf(
          o.runtime.shopping_product_type
        )
      )
        return (
          (c =
            window.location.origin +
            "/ladipage/api?action=cart_update_item_qty&cart_item_key=" +
            t.cart_item_key +
            "&qty=" +
            t.quantity),
          e &&
            (c =
              0 == t.quantity
                ? window.location.origin + "/ladipage/api?action=cart_empty"
                : window.location.origin +
                  "/ladipage/api?action=cart_update_item_qty&qty=" +
                  t.quantity),
          void this.sendRequest("POST", c, s, !0, l, function (t, e, s) {
            if (s.readyState == XMLHttpRequest.DONE) {
              if (200 == e)
                try {
                  if (200 == (r = JSON.parse(t)).code)
                    return (
                      o.updateCartPromotion(r),
                      o.isFunction(i) && i(),
                      o.isFunction(n) && n(),
                      o.reloadPriceDiscount(),
                      void o.reloadFeeShipping()
                    );
                } catch (t) {}
              o.isFunction(a) && a(r), o.isFunction(n) && n();
            }
          })
        );
      var d = window.ladi("_cart_token").get_cookie();
      (l = { "Content-Type": "application/json" }),
        this.isEmpty(d) || (l["cart-token"] = d);
      var p = t.quantity;
      (s = JSON.stringify({
        product_variant_id: t.product_variant_id,
        quantity: p,
      })),
        this.sendRequest(
          "POST",
          this.const.API_LADISALE_UPDATE,
          s,
          !0,
          l,
          function (t, e, s) {
            if (s.readyState == XMLHttpRequest.DONE) {
              if (200 == e)
                try {
                  if (200 == (r = JSON.parse(t)).code)
                    return (
                      o.isFunction(i) && i(),
                      o.isFunction(n) && n(),
                      o.reloadPriceDiscount(),
                      void o.reloadFeeShipping()
                    );
                } catch (t) {}
              o.isFunction(a) && a(r), o.isFunction(n) && n();
            }
          }
        );
    } else {
      if (
        (-1 !=
          [o.const.FORM_CONFIG_TYPE.sapo].indexOf(
            o.runtime.shopping_product_type
          ) &&
          ((c = window.location.origin + "/cart/change.js"),
          (s = new FormData()).append("variantId", t.product_variant_id),
          s.append("quantity", t.quantity)),
        -1 !=
          [o.const.FORM_CONFIG_TYPE.haravan].indexOf(
            o.runtime.shopping_product_type
          ) &&
          ((c = window.location.origin + "/cart/update.js"),
          (l = {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Requested-With": "XMLHttpRequest",
          }),
          (s = o.runtime.tmp.cart
            .reduce(function (i, a) {
              var n = a.quantity;
              return (
                (e || a.product_variant_id == t.product_variant_id) &&
                  (n = t.quantity),
                i.push("updates[]=" + encodeURIComponent(n)),
                i
              );
            }, [])
            .join("&"))),
        -1 !=
          [o.const.FORM_CONFIG_TYPE.shopify].indexOf(
            o.runtime.shopping_product_type
          ))
      ) {
        (c = window.location.origin + "/cart/change.js"),
          (l = {
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
          });
        var u = o.runtime.tmp.cart.findIndex(function (e) {
          return e.product_variant_id == t.product_variant_id;
        });
        if (-1 == u) return;
        s = JSON.stringify({ line: u + 1, quantity: t.quantity });
      }
      this.sendRequest("POST", c, s, !0, l, function (t, e, s) {
        if (s.readyState == XMLHttpRequest.DONE) {
          if (200 == e)
            try {
              if (((r = JSON.parse(t)), !o.isEmpty(r.token)))
                return (
                  o.updateCartPromotion(r),
                  o.isFunction(i) && i(),
                  o.isFunction(n) && n(),
                  o.reloadPriceDiscount(),
                  void o.reloadFeeShipping()
                );
            } catch (t) {}
          o.isFunction(a) && a(r), o.isFunction(n) && n();
        }
      });
    }
  }),
  (PrimePageScriptV2.prototype.getMessageNameProduct = function (t, e) {
    var i = this.const.LANG.PRODUCT;
    return (
      this.isObject(t) &&
        (t.product_type == this.const.PRODUCT_TYPE.event
          ? (i = this.const.LANG.TICKET)
          : t.product_type == this.const.PRODUCT_TYPE.service &&
            (i = this.const.LANG.SERVICE)),
      e || (i = i.toLowerCase()),
      i
    );
  }),
  (PrimePageScriptV2.prototype.loadCollectionData = function (t, e, i, a, n) {
    var o = this,
      r = e["option.product_type"],
      s = e["option.ladisale_store_id"],
      l = e["option.product_tag_id"],
      c = e["option.collection_setting.type"],
      d = o.runtime.eventData[t];
    if (!o.isEmpty(d) && "collection" == d.type) {
      var p = this.runtime.isDesktop ? this.const.DESKTOP : this.const.MOBILE,
        u = parseFloatLadiPage(d[p + ".option.collection_setting.row"]) || 0,
        m = parseFloatLadiPage(d[p + ".option.collection_setting.column"]) || 0,
        _ = parseFloatLadiPage(d[p + ".option.collection_setting.margin"]) || 0,
        y = function (t, e, i, a, n, r) {
          if (
            ((i = o.copy(i)),
            Object.keys(a).forEach(function (t) {
              i[t] = a[t];
            }),
            a["option.input_type"] == o.const.INPUT_TYPE.product_variant)
          ) {
            var s = o.generateVariantProduct(
                i,
                !0,
                a["option.product_variant_type"],
                a["option.product_variant_title"],
                a["option.product_variant_price"],
                a["option.input_tabindex"],
                o.runtime.isClient,
                !0,
                function (o) {
                  y(t, e, i, a, n, r);
                }
              ),
              l = function (e) {
                o.updateProductVariantSelectOption(e, i, a, !1, function () {
                  var i = o.getProductVariantId(e.target, n),
                    a = n.variants.findIndex(function (t) {
                      return t.product_variant_id == i;
                    }),
                    s = o.findAncestor(e.target, "prime-element");
                  if (!o.isEmpty(s)) {
                    var l = o.findAncestor(s, "prime-collection-item");
                    if (!o.isEmpty(l))
                      for (
                        var c = l.querySelectorAll('[data-variant="true"]'),
                          d = 0;
                        d < c.length;
                        d++
                      )
                        if (c[d].id != s.id) {
                          var p = o.runtime.eventData[c[d].id],
                            u = null,
                            m = null,
                            _ = null,
                            y = 0;
                          if (
                            p["option.product_variant_type"] ==
                              o.const.PRODUCT_VARIANT_TYPE.combobox &&
                            ((m = n.variants[a]),
                            o.isObject(m) && o.isString(m.option_ids))
                          )
                            for (
                              _ = m.option_ids.split("/"), y = 0;
                              y < _.length;
                              y++
                            )
                              (u = c[d].querySelector(
                                'select[data-product-option-id="' + _[y] + '"]'
                              )),
                                o.isEmpty(u) ||
                                  (u.value = m["option" + (y + 1)]);
                          if (
                            p["option.product_variant_type"] ==
                              o.const.PRODUCT_VARIANT_TYPE.label &&
                            ((m = n.variants[a]),
                            o.isObject(m) && o.isString(m.option_ids))
                          )
                            for (
                              _ = m.option_ids.split("/"), y = 0;
                              y < _.length;
                              y++
                            )
                              (u = c[d].querySelector(
                                '.prime-form-label-container[data-product-option-id="' +
                                  _[y] +
                                  '"]'
                              )),
                                o.isEmpty(u) ||
                                  o.runtime.tmp.updateLabelValue(
                                    u,
                                    m["option" + (y + 1)]
                                  );
                          if (
                            p["option.product_variant_type"] ==
                            o.const.PRODUCT_VARIANT_TYPE.combined
                          ) {
                            if (
                              ((u = c[d].querySelector(
                                "select.prime-form-control"
                              )),
                              !o.isEmpty(i))
                            ) {
                              var g = u.querySelector(
                                'option[data-product-variant-id="' + i + '"]'
                              );
                              o.isEmpty(g) || (a = g.getAttribute("value"));
                            }
                            u.value = (-1 == a ? "" : a) + "";
                          }
                        }
                  }
                  r(t, n, !1, i, !0, !0);
                });
              },
              c = o.runtime.tmp.getOptionLabelValue,
              d = o.runtime.tmp.updateLabelValue,
              p = o.runtime.tmp.getLabelValue,
              u = function (t) {
                o.runtime.tmp.clickLabelProductChangeCallback(t, function (t) {
                  l({ target: t });
                });
              };
            o.showParentVisibility(e, function () {
              for (
                var t = e.clientHeight,
                  n = t,
                  r = e.querySelectorAll("select.prime-form-control"),
                  m = {},
                  _ = 0;
                _ < r.length;
                _++
              )
                m[
                  r[_].getAttribute("data-store-id") +
                    "_" +
                    r[_].getAttribute("data-product-id") +
                    "_" +
                    r[_].getAttribute("data-product-option-id")
                ] = r[_].value;
              var y = e.querySelectorAll(".prime-form-label-container");
              for (_ = 0; _ < y.length; _++)
                m[
                  y[_].getAttribute("data-store-id") +
                    "_" +
                    y[_].getAttribute("data-product-id") +
                    "_" +
                    y[_].getAttribute("data-product-option-id")
                ] = p(y[_]);
              e.innerHTML = s;
              for (
                var g = e.querySelectorAll("select.prime-form-control"),
                  f = null,
                  v = null,
                  h = 0;
                h < g.length;
                h++
              )
                g[h].removeEventListener("change", l),
                  g[h].addEventListener("change", l),
                  (f =
                    m[
                      g[h].getAttribute("data-store-id") +
                        "_" +
                        g[h].getAttribute("data-product-id") +
                        "_" +
                        g[h].getAttribute("data-product-option-id")
                    ]),
                  o.isNull(f) &&
                    ((v = g[h].querySelector("option")),
                    o.isEmpty(v) || (f = v.getAttribute("value"))),
                  (g[h].value = f);
              var E = e.querySelectorAll(".prime-form-label-container");
              for (h = 0; h < E.length; h++) {
                for (
                  var P = E[h].querySelectorAll(".prime-form-label-item"), b = 0;
                  b < P.length;
                  b++
                )
                  o.tapEventListener(P[b], u);
                (f =
                  m[
                    E[h].getAttribute("data-store-id") +
                      "_" +
                      E[h].getAttribute("data-product-id") +
                      "_" +
                      E[h].getAttribute("data-product-option-id")
                  ]),
                  o.isNull(f) && ((v = P[1]), o.isEmpty(v) || (f = c(v))),
                  d(E[h], f);
              }
              if (
                (o.updateProductVariantSelectOptionFirst(i, a, e),
                a["option.product_variant_type"] !=
                  o.const.PRODUCT_VARIANT_TYPE.combined &&
                  (e.style.setProperty("height", "auto"),
                  (n = e.clientHeight),
                  e.style.removeProperty("height"),
                  n > 0 && t != n))
              ) {
                e.style.setProperty("height", n + "px");
                var L = o.findAncestor(e, "prime-form");
                o.isEmpty(L) ||
                  ((L = o.findAncestor(L, "prime-element")),
                  o.updateHeightElement(!0, e, L, t, n));
              }
            });
          }
          if (a["option.input_type"] == o.const.INPUT_TYPE.number) {
            var m = e.querySelector('input[name="quantity"]'),
              _ = function (t) {
                if (!o.isEmpty(t.target.value)) {
                  var e = o.generateVariantProduct(
                    i,
                    !1,
                    null,
                    null,
                    null,
                    null,
                    !0,
                    !0,
                    function () {
                      _(t);
                    }
                  );
                  if (
                    !(
                      o.isEmpty(e) ||
                      o.isEmpty(e.store_info) ||
                      o.isEmpty(e.product)
                    )
                  ) {
                    var a = t.target;
                    a = (a = o.findAncestor(a, "prime-form")).querySelector(
                      '[data-variant="true"]'
                    );
                    var n = o.getProductVariantId(a, e.product),
                      r = e.product.variants.findIndex(function (t) {
                        return t.product_variant_id == n;
                      });
                    if (-1 != r) {
                      var s = e.product.variants[r].quantity,
                        l = e.product.variants[r].quantity_stock;
                      s = o.isNull(l) ? s : l;
                      var c = parseInt(t.target.value) || 0,
                        d = 1;
                      d = e.product.variants[r].min_buy || d;
                      var p = e.product.variants[r].max_buy,
                        u = 0,
                        m = o.runtime.tmp.cart.findIndex(function (t) {
                          return (
                            t.product_id == e.product.variants[r].product_id &&
                            t.product_variant_id ==
                              e.product.variants[r].product_variant_id
                          );
                        });
                      -1 != m && (u = o.runtime.tmp.cart[m].quantity),
                        d > c + u && (c = d - u),
                        1 == e.product.variants[r].inventory_checked &&
                          c + u > s &&
                          (c = s - u),
                        !o.isEmpty(p) && c + u > p && (c = p - u),
                        (c = c < 1 ? 1 : c),
                        t.target.setAttribute("min", d),
                        o.isEmpty(p) || t.target.setAttribute("max", p),
                        (t.target.value = c);
                    }
                  }
                }
              };
            m.addEventListener("input", _), o.fireEvent(m, "input");
            var g = e.querySelectorAll(".button")[0],
              f = e.querySelectorAll(".button")[1];
            if (o.isEmpty(g) || o.isEmpty(f)) return;
            g.addEventListener("click", function (t) {
              (m.value = (parseFloatLadiPage(m.value) || 0) - 1),
                o.fireEvent(m, "input");
            }),
              f.addEventListener("click", function (t) {
                (m.value = (parseFloatLadiPage(m.value) || 0) + 1),
                  o.fireEvent(m, "input");
              });
          }
          if (
            "button" == a.type &&
            (a["option.is_buy_now"] || a["option.is_add_to_cart"])
          ) {
            var v = function () {
              var t = i["option.data_event"];
              if (
                !o.isArray(t) &&
                ((t = []), o.isObject(i["option.data_action"]))
              ) {
                var a = o.copy(i["option.data_action"]);
                (a.action_type = o.const.ACTION_TYPE.action), t.push(a);
              }
              t.forEach(function (t) {
                t.action_type == o.const.ACTION_TYPE.action &&
                  (t.type == o.const.DATA_ACTION_TYPE.popup_cart &&
                    (window.ladi("POPUP_CART").show(),
                    o.runEventTracking(e.id, { is_form: !1 })),
                  t.type == o.const.DATA_ACTION_TYPE.popup_checkout &&
                    (o.runtime.shopping_third_party
                      ? o.getThirdPartyCheckoutUrl(!0)
                      : window.ladi("POPUP_CHECKOUT").show(),
                    o.runEventTracking(e.id, { is_form: !1 })));
              });
            };
            e.setAttribute("data-click", !1),
              e.addEventListener("click", function (t) {
                o.runtime.tmp.buttonAddToCartClick(t, !0, i, v);
              });
          }
        },
        g = function (t, e, i, a, n, r) {
          var s = !o.isEmpty(e["option.product_mapping_name"]),
            l = JSON.stringify(e),
            c = null,
            d = null;
          if (s)
            if (a && o.isEmpty(i)) c = "";
            else if (
              !o.isEmpty(e["option.product_id"]) &&
              l ===
                (c = (d = o.generateProductKey(
                  !0,
                  l,
                  !0,
                  e,
                  !0,
                  i,
                  null,
                  function (o) {
                    g(t, e, i, a, n, r);
                  }
                )).value)
            )
              return;
          var p = e.type,
            u = null,
            m = null;
          if (
            (s &&
              "headline" == p &&
              ((m = t.getElementsByClassName("prime-headline")[0]),
              o.isEmpty(m) || (m.innerHTML = o.isNull(c) ? "" : c)),
            s &&
              "paragraph" == p &&
              ((m = t.getElementsByClassName("prime-paragraph")[0]),
              o.isEmpty(m) || (m.innerHTML = o.isNull(c) ? "" : c)),
            s && "image" == p)
          ) {
            u = o.getOptimizeImage(
              c,
              t.clientWidth,
              t.clientHeight,
              !0,
              !1,
              !1,
              !0
            );
            var _ = t.getElementsByClassName("prime-image-background")[0];
            o.isEmpty(_) ||
              (o.isEmpty(u)
                ? _.style.setProperty("background-image", "none")
                : _.style.setProperty("background-image", 'url("' + u + '")'));
          }
          if ("gallery" == p) {
            if (s && !o.isArray(c)) return;
            if (r && !n && "true" == t.getAttribute("data-collection"))
              return void o.runtime.tmp.updateImageGalleryProduct(t, d, e);
            t.setAttribute("data-collection", !0),
              t.removeAttribute("data-stop"),
              t.removeAttribute("data-loaded"),
              t.removeAttribute("data-scrolled"),
              t.removeAttribute("data-current"),
              t.removeAttribute("data-is-next"),
              t.removeAttribute("data-runtime-id"),
              t.removeAttribute("data-next-time");
            var y = t.querySelector(".prime-gallery-view-item.selected");
            o.isEmpty(y) || y.classList.remove("selected");
            var f = t.querySelector(".prime-gallery-control-item.selected");
            o.isEmpty(f) || f.classList.remove("selected");
            var v = t.getElementsByClassName("prime-gallery-view")[0];
            (f = t.getElementsByClassName("prime-gallery-control-item")[0]),
              (y = t.getElementsByClassName("prime-gallery-view-item")[0]),
              o.isEmpty(y) || y.classList.add("selected"),
              o.isEmpty(f) || f.classList.add("selected");
            var h = t.getElementsByClassName("prime-gallery-control-box")[0];
            if ((o.isEmpty(h) || h.style.removeProperty("left"), s)) {
              for (
                var E = t.getElementsByClassName("prime-gallery-view-item");
                E.length < c.length;

              ) {
                var P = o.createTmpElement(
                  "div",
                  '<div class="prime-gallery-view-item" data-index="' +
                    E.length +
                    '"></div>',
                  null,
                  !0
                );
                t.getElementsByClassName("prime-gallery-view")[0].appendChild(P);
              }
              for (; E.length > c.length; )
                E[E.length - 1].parentElement.removeChild(E[E.length - 1]);
              for (
                var b = t.getElementsByClassName("prime-gallery-control-item"),
                  L = function (e) {
                    o.runtime.tmp.eventClickGalleryControlItem(e, t);
                  };
                b.length < c.length;

              ) {
                var A = o.createTmpElement(
                  "div",
                  '<div class="prime-gallery-control-item" data-index="' +
                    b.length +
                    '"></div>',
                  null,
                  !0
                );
                A.addEventListener("click", L),
                  t
                    .getElementsByClassName("prime-gallery-control-box")[0]
                    .appendChild(A);
              }
              for (; b.length > c.length; )
                b[b.length - 1].parentElement.removeChild(b[b.length - 1]);
              for (
                var w = t.querySelectorAll(
                    ".prime-gallery .prime-gallery-view .prime-gallery-view-arrow"
                  ),
                  S = 0;
                S < w.length;
                S++
              )
                c.length <= 1
                  ? w[S].style.setProperty("display", "none")
                  : w[S].style.removeProperty("display");
              for (
                w = t.querySelectorAll(".prime-gallery > .prime-gallery-control"),
                  S = 0;
                S < w.length;
                S++
              )
                c.length <= 1
                  ? w[S].style.setProperty("display", "none")
                  : w[S].style.removeProperty("display");
              for (
                w = t.querySelectorAll(".prime-gallery > .prime-gallery-view"),
                  S = 0;
                S < w.length;
                S++
              )
                c.length <= 1
                  ? w[S].style.setProperty("height", "100%")
                  : w[S].style.removeProperty("height");
              c.forEach(function (e, i) {
                (u = e.src),
                  o.isEmpty(v) ||
                    (u = o.getOptimizeImage(
                      e.src,
                      v.clientWidth,
                      v.clientHeight,
                      !0,
                      !1,
                      !1,
                      o.runtime.isClient
                    ));
                var a = t.querySelector(
                  '.prime-gallery .prime-gallery-view-item[data-index="' +
                    i +
                    '"]'
                );
                o.isEmpty(a) ||
                  a.style.setProperty("background-image", 'url("' + u + '")'),
                  o.isEmpty(f) ||
                    (u = o.getOptimizeImage(
                      e.src,
                      f.clientWidth,
                      f.clientHeight,
                      !0,
                      !1,
                      !1,
                      o.runtime.isClient
                    )),
                  (a = t.querySelector(
                    '.prime-gallery .prime-gallery-control-item[data-index="' +
                      i +
                      '"]'
                  )),
                  o.isEmpty(a) ||
                    a.style.setProperty("background-image", 'url("' + u + '")');
              }),
                o.runTimeout(function () {
                  t.setAttribute("data-loaded", !0);
                }, 300);
            }
            o.runtime.tmp.runGallery(t.id, t, !0, p),
              o.runtime.tmp.setGalleryStart(t.id, t);
          }
          "countdown_item" == p &&
            o.runtime.tmp.runOptionCountdownItem(
              t.id,
              t,
              p,
              e["option.countdown_item_type"]
            ),
            "countdown" == p &&
              o.runtime.tmp.runOptionCountdown(
                t.id,
                t,
                p,
                e["option.countdown_type"],
                e["option.countdown_minute"],
                e["option.countdown_daily_start"],
                e["option.countdown_daily_end"],
                e["option.countdown_endtime"]
              );
        },
        f = function (t, e) {
          var d = u * m,
            p = o.getListProductByTagId(e, d, i, !0, function () {
              f(t, e);
            });
          if (o.isObject(p) && o.isArray(p.products)) {
            var v = document.getElementById(t);
            if (o.isEmpty(v)) return;
            if (v.getAttribute("data-page") == i) return;
            var h = i,
              E = !1,
              P = !1;
            if (!o.isEmpty(p.total_record) && i * d >= p.total_record) {
              if (c == o.const.COLLECTION_TYPE.readmore) {
                var b = v.getElementsByClassName(
                  "prime-collection-button-next"
                )[0];
                o.isEmpty(b) || b.setAttribute("data-opacity", "0");
              }
              v.setAttribute("data-max-page", h),
                (E = !0),
                i * d > p.total_record && (P = !0);
            }
            v.setAttribute("data-page", i > h ? h : i);
            var L = v.getElementsByClassName("prime-collection-arrow-left")[0],
              A = v.getElementsByClassName("prime-collection-arrow-right")[0],
              w = v.getElementsByClassName("prime-collection-button-next")[0];
            if (
              (o.isEmpty(L) || L.classList.remove("opacity-0"),
              o.isEmpty(A) || A.classList.remove("opacity-0"),
              o.isEmpty(w) || w.classList.remove("opacity-0"),
              1 == v.getAttribute("data-page") &&
                c == o.const.COLLECTION_TYPE.carousel &&
                (o.isEmpty(L) || L.classList.add("opacity-0")),
              v.getAttribute("data-page") == v.getAttribute("data-max-page") &&
                (c == o.const.COLLECTION_TYPE.readmore &&
                  (o.isEmpty(w) || w.classList.add("opacity-0")),
                c == o.const.COLLECTION_TYPE.carousel &&
                  (o.isEmpty(A) || A.classList.add("opacity-0"))),
              h < i)
            )
              return;
            var S = v.getElementsByClassName("prime-collection-item");
            if (0 == S.length) return;
            var T = 0,
              O = 0;
            if (v.hasAttribute("data-max-option-length"))
              T = parseFloatLadiPage(v.getAttribute("data-max-option-length"));
            else {
              var C = v.querySelectorAll('.prime-form [data-variant="true"]');
              for (O = 0; O < C.length; O++) {
                var N = C[O].getElementsByClassName("prime-form-item-box");
                N.length > T && (T = N.length);
              }
            }
            var I = v.getElementsByClassName("prime-collection-content")[0],
              k = { className: S[0].className, innerHTML: S[0].innerHTML };
            a && S[0].parentElement.removeChild(S[0]);
            for (
              var x = I.getElementsByClassName("prime-collection-page");
              x.length < i;

            ) {
              var D = document.createElement("div");
              (D.className = "prime-collection-page"), I.appendChild(D);
            }
            var R = x[i - 1],
              F = R.getElementsByClassName("prime-collection-item");
            if (F.length != p.products.length)
              for (; F.length > 0; ) F[0].parentElement.removeChild(F[0]);
            var q = function (t, i, a, n, c, d) {
              o.isEmpty(i.id) &&
                !o.isEmpty(i.product_id) &&
                (i.id = i.product_id);
              for (
                var p = function (t, e) {
                    if (t.classList.contains("prime-animation-hidden")) {
                      var i =
                        parseFloatLadiPage(
                          e[o.runtime.device + ".style.animation-delay"]
                        ) || 0;
                      t.classList.add("prime-animation"),
                        o.runTimeout(function () {
                          t.classList.remove("prime-animation-hidden");
                        }, 1e3 * i);
                    }
                  },
                  u = 0;
                u < t.length;
                u++
              ) {
                a &&
                  o.isFunction(o.runtime.tmp.runElementClickSelected) &&
                  o.runtime.tmp.runElementClickSelected(t[u], !0);
                var m = o.copy(o.runtime.eventData[t[u].id]);
                o.isEmpty(m) ||
                  (p(t[u], m),
                  (m["option.product_type"] = r),
                  (m["option.ladisale_store_id"] = s),
                  (m["option.product_tag_id"] = l),
                  (m["option.product_id"] = i.id),
                  a &&
                    (o.isFunction(o.runtime.tmp.runOptionAction) &&
                      o.runtime.tmp.runOptionAction(t[u], t[u].id, m.type, m),
                    o.isFunction(o.runtime.tmp.runOptionHover) &&
                      o.runtime.tmp.runOptionHover(
                        t[u],
                        t[u].id,
                        m.type,
                        m["option.data_event"] || m["option.data_hover"]
                      ),
                    y(t, t[u], e, m, i, q)),
                  g(t[u], m, n, c, a, d));
              }
            };
            for (O = 0; O < p.products.length; O++)
              o.isArray(p.products[O].options) &&
                p.products[O].options.length > T &&
                (T = p.products[O].options.length);
            for (
              v.setAttribute("data-max-option-length", T), O = 0;
              O < p.products.length;
              O++
            )
              if (!(F.length > O)) {
                var B = document.createElement("div");
                (B.className = k.className),
                  R.appendChild(B),
                  (B.innerHTML = k.innerHTML);
                for (
                  var M = B.getElementsByClassName("prime-element");
                  o.isArray(p.products[O].options) &&
                  p.products[O].options.length < T;

                )
                  p.products[O].options.push({ is_tmp: !0 });
                q(M, p.products[O], !0, null, !1, !1);
              }
            E && (R.classList.add("last"), P && R.classList.add("not-full")),
              c == o.const.COLLECTION_TYPE.carousel &&
                (function (t) {
                  var e = document.getElementById(t);
                  if (!o.isEmpty(e) && e.hasAttribute("data-page")) {
                    var i = "0",
                      a = getComputedStyle(e).width,
                      n = a,
                      r = parseFloatLadiPage(e.getAttribute("data-page")) || 1,
                      s = e
                        .getElementsByClassName("prime-collection-content")[0]
                        .getElementsByClassName("prime-collection-page"),
                      l = s[s.length - 1].getElementsByClassName(
                        "prime-collection-item"
                      ),
                      c = m - l.length,
                      d = "",
                      p = "";
                    c > 0
                      ? ((n =
                          "calc(" +
                          a +
                          " * " +
                          s.length +
                          " - (" +
                          a +
                          " / " +
                          m +
                          " * " +
                          c +
                          ") + calc(" +
                          _ +
                          "px / " +
                          m +
                          " * " +
                          l.length +
                          "))"),
                        r > 1 &&
                          (r != s.length
                            ? (i = "calc(-" + a + " * " + (r - 1) + ")")
                            : ((i =
                                "calc(-" +
                                a +
                                " * " +
                                (r - 1) +
                                " + (" +
                                a +
                                " / " +
                                m +
                                " * " +
                                c +
                                "))"),
                              (p =
                                "margin-left: calc(-" +
                                _ +
                                "px / " +
                                m +
                                " * " +
                                l.length +
                                ");"))),
                        (d +=
                          "#" +
                          t +
                          " .prime-collection .prime-collection-content .prime-collection-page.last.not-full .prime-collection-item:first-child {"),
                        (d += "margin-left: " + _ + "px;"),
                        (d += "}"),
                        (d +=
                          "#" +
                          t +
                          " .prime-collection-content .prime-collection-page.last {"),
                        (d +=
                          "width: calc(" +
                          getComputedStyle(e).width +
                          " / " +
                          m +
                          " * " +
                          l.length +
                          " + calc(" +
                          _ +
                          "px / " +
                          m +
                          " * " +
                          l.length +
                          "));"),
                        (d += "}"))
                      : (r > 1 && (i = "calc(-" + a + " * " + (r - 1) + ")"),
                        (n = "calc(" + a + " * " + s.length + ")"));
                    var u = "style_collection_" + t,
                      y = document.getElementById(u);
                    o.isEmpty(y) || y.parentElement.removeChild(y);
                    var g = "#" + t + " .prime-collection-content {";
                    (g += "width: " + n + ";"),
                      (g += "left: " + i + ";"),
                      (g += p),
                      (g += "}"),
                      (g += d),
                      o.createStyleElement(u, g);
                  }
                })(t),
              n &&
                c == o.const.COLLECTION_TYPE.readmore &&
                (function (t) {
                  var e = document.getElementById(t);
                  if (!o.isEmpty(e)) {
                    var i = e.getElementsByClassName(
                      "prime-collection-content"
                    )[0];
                    if (e.hasAttribute("data-max-page")) {
                      var a = i.querySelector(".prime-collection-page.last"),
                        n = a.getElementsByClassName("prime-collection-item"),
                        r = Math.ceil(n.length / m);
                      if (u == r) a.style.removeProperty("height");
                      else {
                        var s =
                          parseFloatLadiPage(
                            ((parseFloatLadiPage(getComputedStyle(a).height) ||
                              0) *
                              r) /
                              u
                          ) || 0;
                        (s -= parseFloatLadiPage((_ * (u - r)) / u) || 0),
                          a.style.setProperty("height", s + "px");
                      }
                    }
                    var l = parseFloatLadiPage(getComputedStyle(e).height) || 0,
                      c = i.scrollHeight;
                    if (l != c) {
                      e.style.setProperty("height", c + "px");
                      var d = o.findAncestor(e.parentElement, "prime-element");
                      o.isEmpty(d) &&
                        (d = o.findAncestor(e.parentElement, "prime-section")),
                        o.updateHeightElement(!0, e, d, l, c);
                    }
                  }
                })(t),
              o.runEventScroll(),
              o.runResizeAll();
          }
        },
        v = d["option.product_tag_id"],
        h = d["option.data_setting.value"];
      (!o.isArray(v) && o.isEmpty(h)) || f(t, d);
    }
  });
var lightbox_run = function (t, e, i, a, n, o, r, s) {
    var l = document.getElementById(PrimePageScript.runtime.lightbox_screen_id);
    if (!PrimePageScript.isEmpty(l)) {
      var c = function () {
        PrimePageScript.isEmpty(window.$rootScope) ||
        !PrimePageScript.isFunction(window.$rootScope.hideBuilderLoadingBlur)
          ? PrimePageScript.hideLoadingBlur()
          : window.$rootScope.hideBuilderLoadingBlur();
      };
      s ||
        (PrimePageScript.isEmpty(window.$rootScope) ||
        !PrimePageScript.isFunction(window.$rootScope.showBuilderLoadingBlur)
          ? PrimePageScript.showLoadingBlur()
          : window.$rootScope.showBuilderLoadingBlur());
      var d = JSON.stringify({
          html: t,
          url: e,
          is_video: i,
          video_type: o,
          video_value: r,
        }),
        p = Object.keys(PrimePageScript.runtime.list_lightbox_id);
      -1 == p.indexOf(d) &&
        (PrimePageScript.runtime.list_lightbox_id[d] = p.length + 1);
      var u = PrimePageScript.runtime.list_lightbox_id[d];
      n = n + "_" + u;
      var m = document.getElementById(n),
        _ = !1;
      PrimePageScript.isEmpty(m)
        ? ((m = document.createElement("div")),
          l.appendChild(m),
          (m.outerHTML = t),
          (m = l.lastChild),
          (_ = !0))
        : i && PrimePageScript.runEventReplayVideo(n, o, !0);
      var y = document.createElement("div");
      (y.className = "lightbox-close"),
        y.setAttribute("data-opacity", 0),
        l.appendChild(y),
        m.setAttribute("id", n),
        m.setAttribute("data-opacity", 0),
        m.classList.remove("lightbox-hidden");
      var g = function () {
          if (((m = document.getElementById(n)), !PrimePageScript.isEmpty(m))) {
            if ("IFRAME" == m.tagName) {
              var t = parseFloatLadiPage(getComputedStyle(m).width) || 0,
                e = parseFloatLadiPage(getComputedStyle(m).height) || 0;
              if (t > 0 || e > 0) {
                var i = 0.8 * document.body.clientWidth,
                  a = 0.8 * PrimePageScript.getHeightDevice(),
                  o = i,
                  r = (e / t) * o;
                r > a && (o = (r = a) * (t / e)),
                  m.style.setProperty(
                    "width",
                    (parseFloatLadiPage(o) || 0) + "px"
                  ),
                  m.style.setProperty(
                    "height",
                    (parseFloatLadiPage(r) || 0) + "px"
                  );
              }
            }
            if (
              ((y = l.getElementsByClassName("lightbox-close")[0]),
              !PrimePageScript.isEmpty(y))
            ) {
              var s = PrimePageScript.getElementBoundingClientRect(m),
                c = 10,
                d = 10;
              s.x - 5 - y.clientWidth > d && (d = s.x - 5 - y.clientWidth),
                s.y - 5 - y.clientHeight > c && (c = s.y - 5 - y.clientHeight),
                (d += PrimePageScript.runtime.widthScrollBar),
                (c -= 6),
                (d -= 6),
                y.style.setProperty("right", d + "px"),
                y.style.setProperty("top", c + "px");
            }
            var p = document.getElementById(m.id + "_button_unmute");
            PrimePageScript.isEmpty(p) ||
              (p.style.setProperty("width", getComputedStyle(m).width),
              p.style.setProperty("height", getComputedStyle(m).height));
          }
        },
        f = function () {
          PrimePageScript.runTimeout(function () {
            c(),
              (m = document.getElementById(n)),
              (y = l.getElementsByClassName("lightbox-close")[0]),
              g(),
              PrimePageScript.isEmpty(m) || m.removeAttribute("data-opacity"),
              PrimePageScript.isEmpty(y) || y.removeAttribute("data-opacity");
          }, 100);
        };
      y.style.setProperty("top", "-100px"),
        y.style.setProperty("right", "-100px");
      var v = "load";
      if (
        (i &&
          o == PrimePageScript.const.VIDEO_TYPE.direct &&
          (v = "loadedmetadata"),
        _ && (m.addEventListener(v, f), m.addEventListener("error", f)),
        i)
      ) {
        var h = e;
        o == PrimePageScript.const.VIDEO_TYPE.youtube && ((e = null), (h = r)),
          _
            ? PrimePageScript.runEventPlayVideo(
                n,
                o,
                h,
                !1,
                !1,
                !0,
                !1,
                s,
                !1,
                function (t) {
                  PrimePageScript.isEmpty(t)
                    ? f()
                    : (t.addEventListener(v, f),
                      t.addEventListener("error", f));
                }
              )
            : f();
      }
      s || l.style.setProperty("display", "block"),
        PrimePageScript.isEmpty(e) || (_ ? (m.src = e) : f());
      var E = function () {
          var t = document.getElementById(
            PrimePageScript.runtime.backdrop_popup_id
          );
          return (
            PrimePageScript.isEmpty(t) || "none" == getComputedStyle(t).display
          );
        },
        P = 0;
      E()
        ? ((P = window.scrollY), (PrimePageScript.runtime.tmp.bodyScrollY = P))
        : (P = PrimePageScript.runtime.tmp.bodyScrollY);
      var b = function () {
        c(),
          l.style.removeProperty("display"),
          (m = document.getElementById(n)),
          PrimePageScript.isEmpty(m) ||
            (a && !i
              ? m.parentElement.removeChild(m)
              : (m.classList.add("lightbox-hidden"),
                i && PrimePageScript.runEventReplayVideo(n, o, !1))),
          (y = l.getElementsByClassName("lightbox-close")[0]),
          PrimePageScript.isEmpty(y) || y.parentElement.removeChild(y);
        var t = document.getElementById("style_lightbox");
        PrimePageScript.isEmpty(t) || t.parentElement.removeChild(t);
        var e = E();
        e &&
          !PrimePageScript.isEmpty(PrimePageScript.runtime.tmp.bodyScrollY) &&
          window.scrollTo(0, PrimePageScript.runtime.tmp.bodyScrollY),
          e && (PrimePageScript.runtime.tmp.bodyScrollY = null);
      };
      if (
        (y.addEventListener("click", function (t) {
          t.stopPropagation(), b();
        }),
        !s)
      ) {
        var L = "body {";
        (L += "position: fixed !important;"),
          (L += "width: 100% !important;"),
          (L += "top: -" + P + "px !important;"),
          (L += "}"),
          PrimePageScript.createStyleElement("style_lightbox", L);
      }
      PrimePageScript.isEmpty(l.getAttribute("data-load-event")) &&
        (l.setAttribute("data-load-event", !0),
        l.addEventListener("click", function (t) {
          t.stopPropagation(),
            t.target.id == l.id &&
              ((y = l.getElementsByClassName("lightbox-close")[0]),
              PrimePageScript.isEmpty(y) || y.click());
        }),
        window.addEventListener("resize", g)),
        s && b();
    }
  },
  lightbox_iframe = function (t, e, i, a, n, o) {
    if (!PrimePageScript.isEmpty(t)) {
      var r =
          "margin: auto; position: absolute; top: 0; left: 0; bottom: 0; right: 0; max-width: 80%; max-height: 80%;",
        s =
          '<iframe id="' +
          (i = i || "lightbox_iframe") +
          '" class="lightbox-item" style="' +
          r +
          '" frameborder="0" allowfullscreen></iframe>',
        l = t,
        c = PrimePageScript.createTmpElement("iframe", l, null, !0);
      PrimePageScript.isEmpty(c) ||
        "IFRAME" != c.tagName ||
        ((l = c.src),
        (i = c.id || i),
        c.removeAttribute("src"),
        c.setAttribute("style", r),
        c.classList.add("lightbox-item"),
        (s = c.outerHTML)),
        lightbox_run(s, l, e, !0, i, a, n, o);
    }
  },
  lightbox_image = function (t) {
    PrimePageScript.isEmpty(t) ||
      lightbox_run(
        '<img class="lightbox-item" style="margin: auto; position: absolute; top: 0; left: 0; bottom: 0; right: 0; object-fit: scale-down; max-width: 80%; max-height: 80%;" />',
        t,
        !1,
        !1,
        "lightbox_image"
      );
  },
  lightbox_video = function (t, e, i) {
    if (!PrimePageScript.isEmpty(t) && !PrimePageScript.isEmpty(e)) {
      PrimePageScript.pauseAllVideo();
      var a = "lightbox_player";
      e == PrimePageScript.const.VIDEO_TYPE.youtube &&
        lightbox_iframe(
          '<iframe id="' +
            a +
            '" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0;" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>',
          !0,
          a,
          e,
          t,
          i
        ),
        e == PrimePageScript.const.VIDEO_TYPE.direct &&
          lightbox_run(
            '<video class="lightbox-item" id="lightbox_player" style="margin: auto; position: absolute; top: 0; left: 0; bottom: 0; right: 0; max-width: 80%; max-height: 80%; object-fit: cover;"></video>',
            t,
            !0,
            !1,
            a,
            e,
            null,
            i
          );
    }
  },
  LadiPageLibraryV2 = LadiPageLibraryV2 || function () {};
(LadiPageLibraryV2.prototype.get_url = function (t, e, i) {
  if (!PrimePageScript.isEmpty(this.id)) {
    var a = this.id,
      n = "";
    if (
      (e &&
        PrimePageScript.isObject(t) &&
        Object.keys(t).forEach(function (e) {
          PrimePageScript.isEmpty(n) || (n += "&");
          var a = t[e];
          i && -1 != ["email", "phone"].indexOf(e) && (a = Base64.encode(a)),
            PrimePageScript.isArray(a) &&
              a.length > 1 &&
              (a = JSON.stringify(a)),
            (a = encodeURIComponent(a)),
            (n += e + "=" + a);
        }),
      !PrimePageScript.isEmpty(n))
    ) {
      var o = PrimePageScript.createTmpElement("a", "", { href: a });
      (o.search =
        o.search + (PrimePageScript.isEmpty(o.search) ? "?" : "&") + n),
        (a = o.href);
    }
    return (
      (a = PrimePageScript.getLinkUTMRedirect(a, null)),
      PrimePageScript.convertDataReplaceStr(a, !0, null, !1, t)
    );
  }
}),
  (LadiPageLibraryV2.prototype.open_url = function (t, e) {
    if (!PrimePageScript.isEmpty(this.id))
      if (PrimePageScript.runtime.has_popupx)
        PrimePageScript.runtime.tmp.runActionPopupX({
          url: this.id,
          target: t,
          nofollow: e,
          action: { type: "open_url" },
        });
      else {
        var i = this.id,
          a = null;
        e &&
          ((a = PrimePageScript.getElementAHref(i, !1)).setAttribute(
            "rel",
            "nofollow"
          ),
          document.body.appendChild(a)),
          PrimePageScript.isEmpty(t) || "_self" == t.toLowerCase()
            ? e
              ? a.click()
              : (window.location.href = i)
            : e
            ? (a.setAttribute("target", t), a.click())
            : window.open(i, t),
          e && a.parentElement.removeChild(a);
      }
  }),
  (LadiPageLibraryV2.prototype.get_cookie = function () {
    if (!PrimePageScript.isEmpty(this.id)) {
      for (
        var t = this.id + "=", e = document.cookie.split(";"), i = "", a = 0;
        a < e.length;
        a++
      ) {
        for (var n = e[a]; " " == n.charAt(0); ) n = n.substring(1);
        if (0 == n.indexOf(t)) {
          i = n.substring(t.length, n.length);
          break;
        }
      }
      return decodeURIComponentLadiPage(i);
    }
  }),
  (LadiPageLibraryV2.prototype.delete_cookie = function (t, e) {
    this.set_cookie(null, -3650, t, e, !1);
  }),
  (LadiPageLibraryV2.prototype.set_cookie = function (t, e, i, a, n) {
    if (!PrimePageScript.isEmpty(this.id)) {
      var o = "";
      if (n) o = "expires = 0";
      else {
        var r = new Date();
        !PrimePageScript.isNull(e) && e instanceof Date
          ? (r = e)
          : ((e = PrimePageScript.isEmpty(e) ? 365 : e),
            r.setTime(r.getTime() + 24 * e * 60 * 60 * 1e3)),
          (o = "expires = " + r.toUTCString());
      }
      t = PrimePageScript.isEmpty(t) ? "" : t;
      var s = this.id + " = " + t;
      PrimePageScript.isEmpty(o) || (s += "; " + o),
        PrimePageScript.isEmpty(a) || (s += "; domain = " + a),
        (i = i || window.location.pathname),
        PrimePageScript.runtime.isIE || (s += "; path = " + i),
        "https:" == window.location.protocol &&
          (s += "; SameSite = None; secure"),
        (document.cookie = s);
    }
  }),
  (LadiPageLibraryV2.prototype.submit = function () {
    var t = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(t)) {
      var e = t.querySelector('.prime-form button[type="submit"]');
      PrimePageScript.isEmpty(e) || e.click();
    }
  }),
  (LadiPageLibraryV2.prototype.scroll = function (t, e) {
    var i = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(i))
      if (PrimePageScript.runtime.has_popupx) this.show();
      else {
        t && "none" == getComputedStyle(i).display && this.show();
        for (
          var a = document.querySelectorAll(
              "#" +
                PrimePageScript.runtime.builder_section_popup_id +
                " .prime-container > .prime-element"
            ),
            n = 0;
          n < a.length;
          n++
        )
          if (
            a[n].id != this.id &&
            a[n].hasAttribute("data-popup-backdrop") &&
            "none" != getComputedStyle(a[n]).display
          ) {
            var o = PrimePageScript.findAncestor(i, "prime-popup");
            PrimePageScript.isEmpty(o) ||
              (o = PrimePageScript.findAncestor(o, "prime-element")),
              (PrimePageScript.isEmpty(o) || o.id != a[n].id) &&
                PrimePageScript.runRemovePopup(a[n].id, !0);
          }
        var r = PrimePageScript.isObject(PrimePageScript.runtime.story_page),
          s = function (t, e, i) {
            PrimePageScript.removeTimeout(
              PrimePageScript.runtime.tmp.scroll_timeout_id
            );
            var a,
              n = function (t, e, i, a) {
                return (t /= a / 2) < 1
                  ? (i / 2) * t * t + e
                  : (-i / 2) * (--t * (t - 2) - 1) + e;
              };
            a = r
              ? "left" == t
                ? e.scrollLeft
                : e.scrollTop
              : "left" == t
              ? e.scrollX
              : e.scrollY;
            var o = "left" == t ? window.innerWidth : window.innerHeight,
              s = i - a;
            if (0 != s) {
              var l = s < 0 ? -1 * s : s,
                c = 0,
                d = 1e3;
              (d = l <= 4 * o ? 750 : d),
                (d = l <= 2 * o ? 525 : d),
                (d = l <= o ? 300 : d),
                (d = r ? 300 : d);
              var p = "left" == t ? "scrollLeft" : "scrollTop",
                u =
                  window.requestAnimationFrame ||
                  window.mozRequestAnimationFrame ||
                  window.webkitRequestAnimationFrame ||
                  window.msRequestAnimationFrame ||
                  window.oRequestAnimationFrame,
                m = null,
                _ = null,
                y = function (i) {
                  r
                    ? (e[p] = i)
                    : "left" == t
                    ? e.scrollTo(i, e.scrollY)
                    : e.scrollTo(e.scrollX, i);
                };
              if (u) {
                var g = Date.now() + d;
                u(
                  (m = function () {
                    (c = d - (g - Date.now())),
                      (_ = n(c, a, s, d)),
                      y(_),
                      c < d ? u(m) : y(i);
                  })
                );
              } else
                (m = function () {
                  (_ = n((c += 20), a, s, d)),
                    y(_),
                    c < d
                      ? (PrimePageScript.runtime.tmp.scroll_timeout_id =
                          PrimePageScript.runTimeout(m, 20))
                      : y(i);
                })();
            }
          },
          l = function (t) {
            var e = document.getElementsByClassName("prime-wraper")[0],
              a =
                PrimePageScript.getElementBoundingClientRect(i).top +
                (r ? t.scrollTop : t.scrollY);
            return {
              scrollTop: (a -=
                parseFloatLadiPage(
                  e.getAttribute("data-scroll-padding-top") || 0
                ) || 0),
            };
          },
          c = null,
          d = null,
          p = null;
        e
          ? r
            ? i.scrollIntoView()
            : ((c = l(window)), window.scrollTo({ top: c.scrollTop }))
          : PrimePageScript.runTimeout(function () {
              var t;
              r
                ? PrimePageScript.runtime.isDesktop ||
                  PrimePageScript.runtime.isBrowserDesktop
                  ? i.scrollIntoView({ behavior: "smooth" })
                  : ((p = document.getElementsByClassName("prime-wraper")[0]),
                    PrimePageScript.runtime.story_page.type ==
                      PrimePageScript.const.STORY_PAGE.horizontal &&
                      ((t = p),
                      (d = {
                        scrollLeft:
                          PrimePageScript.getElementBoundingClientRect(i).left +
                          (r ? t.scrollLeft : t.scrollX),
                      }),
                      s("left", p, d.scrollLeft)),
                    PrimePageScript.runtime.story_page.type ==
                      PrimePageScript.const.STORY_PAGE.vertical &&
                      ((c = l(p)), s("top", p, c.scrollTop)))
                : ((p = window),
                  (c = l(p)),
                  PrimePageScript.runtime.isDesktop ||
                  PrimePageScript.runtime.isBrowserDesktop
                    ? window.scrollTo({ top: c.scrollTop, behavior: "smooth" })
                    : s("top", p, c.scrollTop));
            }, 100);
      }
  }),
  (LadiPageLibraryV2.prototype.value = function (t, e, i) {
    var a = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(a)) {
      var n = [],
        o = !1,
        r = 0,
        s = PrimePageScript.isArray(t) ? t : [t],
        l = a.querySelectorAll(
          '.prime-form-item > [data-is-select-country="true"]'
        );
      if (4 == l.length)
        if (PrimePageScript.isNull(t)) {
          for (r = 0; r < l.length; r++) n.push(l[r].value);
          o = !0;
        } else
          s.forEach(function (t, e) {
            PrimePageScript.isEmpty(l[e]) ||
              ((l[e].value = t), PrimePageScript.fireEvent(l[e], "change"));
          });
      else {
        var c = document.querySelectorAll(
            "#" +
              this.id +
              " > ." +
              [
                "prime-button .prime-headline",
                "prime-headline",
                "prime-paragraph",
                "prime-list-paragraph",
              ].join(", #" + this.id + " > .")
          ),
          d = document.querySelectorAll(
            "#" +
              this.id +
              " > ." +
              [
                "prime-form-item-container .prime-form-item > input",
                "prime-form-item-container .prime-form-item > textarea",
                "prime-form-item-container .prime-form-item > select",
              ].join(", #" + this.id + " > .")
          ),
          p = document.querySelectorAll(
            "#" +
              this.id +
              " > ." +
              [
                "prime-form-item-container .prime-form-checkbox-item > input",
              ].join(", #" + this.id + " > .")
          ),
          u = document.querySelectorAll(
            "#" + this.id + " > .prime-image .prime-image-background"
          ),
          m = document.querySelectorAll("#" + this.id + " > .prime-shape"),
          _ = document.querySelectorAll("#" + this.id + " > .prime-video"),
          y = document.querySelectorAll(
            "#" + this.id + " > .prime-survey > .prime-survey-option"
          ),
          g = function (t) {
            var e = [];
            return (
              PrimePageScript.isArray(t) &&
                t.forEach(function (t) {
                  e.push(t.name);
                }),
              (e = e.length > 0 ? "[" + e.join(", ") + "]" : "")
            );
          };
        for (r = 0; r < c.length; r++)
          if (PrimePageScript.isNull(t))
            PrimePageScript.isObject(i) && i.only_text
              ? n.push(c[r].innerText)
              : n.push(c[r].innerHTML);
          else if (((c[r].innerHTML = t), !PrimePageScript.isEmpty(e))) {
            var f = PrimePageScript.findAncestor(c[r], "prime-element");
            PrimePageScript.isEmpty(f) || f.classList.add(e);
          }
        for (r = 0; r < d.length; r++)
          if (PrimePageScript.isNull(t))
            if (d[r].classList.contains("prime-form-control-file")) {
              var v = d[r].getAttribute("data-path-file") || "[]";
              (v = JSON.parse(v)), n.push(v);
            } else n.push(d[r].value);
          else
            d[r].classList.contains("prime-form-control-file")
              ? (d[r].setAttribute("data-path-file", JSON.stringify(t)),
                (d[r].value = g(t)))
              : ((d[r].value = t),
                "date" == d[r].getAttribute("data-type") &&
                  (PrimePageScript.isEmpty(t)
                    ? d[r].setAttribute("type", "text")
                    : d[r].setAttribute("type", "date")));
        var h = !1;
        for (r = 0; r < p.length; r++)
          PrimePageScript.isNull(t)
            ? (p[r].checked && n.push(p[r].value),
              "checkbox" == p[r].getAttribute("type").toLowerCase() && (o = !0))
            : ((h = !1),
              "checkbox" == p[r].getAttribute("type").toLowerCase() &&
                -1 != s.indexOf(p[r].value) &&
                (h = !0),
              "radio" == p[r].getAttribute("type").toLowerCase() &&
                s.length > 0 &&
                s[0] == p[r].value &&
                (h = !0),
              h ? p[r].checked || p[r].click() : p[r].checked && p[r].click());
        for (r = 0; r < u.length; r++)
          if (PrimePageScript.isNull(t)) {
            var E = getComputedStyle(u[r]).backgroundImage;
            (E = E || "").startsWith('url("') &&
              (E = E.substring('url("'.length)),
              E.endsWith('")') && (E = E.substring(0, E.length - '")'.length)),
              n.push(E);
          } else if (PrimePageScript.isEmpty(t))
            u[r].style.setProperty("background-image", "none");
          else {
            var P = PrimePageScript.findAncestor(u[r], "prime-element"),
              b = PrimePageScript.getOptimizeImage(
                t,
                P.clientWidth,
                P.clientHeight,
                !0,
                !1,
                !1,
                !0
              );
            u[r].style.setProperty("background-image", 'url("' + b + '")');
          }
        for (r = 0; r < m.length; r++)
          if (PrimePageScript.isNull(t)) n.push(m[r].innerHTML);
          else
            try {
              "svg" ==
                PrimePageScript.createTmpElement("svg", t, null, !0).tagName &&
                (m[r].innerHTML = t);
            } catch (t) {}
        for (r = 0; r < _.length; r++) {
          var L = PrimePageScript.runtime.eventData[this.id];
          if (PrimePageScript.isNull(t))
            PrimePageScript.isEmpty(L) ||
              n.push({
                type: L["option.video_type"],
                value: L["option.video_value"],
              });
          else {
            L["option.video_value"] = t;
            var A = _[r].getElementsByClassName("iframe-video-preload")[0],
              w = null;
            if (
              L["option.video_type"] == PrimePageScript.const.VIDEO_TYPE.youtube
            ) {
              var S =
                  "https://img.youtube.com/vi/" +
                  (w = PrimePageScript.getVideoId(L["option.video_type"], t)) +
                  "/hqdefault.jpg",
                T = _[r].getElementsByClassName("prime-video-background")[0];
              PrimePageScript.isEmpty(T) ||
                T.style.setProperty("background-image", 'url("' + S + '")');
            }
            if (PrimePageScript.isEmpty(A))
              PrimePageScript.playVideo(
                this.id,
                L["option.video_type"],
                L["option.video_value"],
                L["option.video_control"]
              );
            else {
              PrimePageScript.pauseAllVideo();
              var O = !1;
              if (
                L["option.video_type"] ==
                PrimePageScript.const.VIDEO_TYPE.youtube
              ) {
                var C = window.YT.get(A.id);
                !PrimePageScript.isEmpty(C) &&
                  PrimePageScript.isFunction(C.loadVideoById) &&
                  (C.loadVideoById(w, 0), C.seekTo(0), (O = !0));
              }
              L["option.video_type"] ==
                PrimePageScript.const.VIDEO_TYPE.direct &&
                PrimePageScript.isFunction(A.play) &&
                ((A.src = t), (A.currentTime = 0), (O = !0)),
                O &&
                  PrimePageScript.runEventReplayVideo(
                    A.id,
                    L["option.video_type"],
                    !0
                  );
            }
          }
        }
        for (r = 0; r < y.length; r++)
          PrimePageScript.isNull(t)
            ? (y[r].classList.contains("selected") &&
                n.push(y[r].getAttribute("data-value")),
              "true" == a.getAttribute("data-multiple") && (o = !0))
            : ((h = !1),
              -1 != s.indexOf(y[r].getAttribute("data-value")) && (h = !0),
              h
                ? y[r].classList.contains("selected") || y[r].click()
                : y[r].classList.contains("selected") && y[r].click());
      }
      return o ? n : n.length > 0 ? n[0] : null;
    }
  }),
  (LadiPageLibraryV2.prototype.set_value_2nd = function (t) {
    var e = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(e)) {
      PrimePageScript.isObject(PrimePageScript.runtime.tmp.value_2nd_source) ||
        (PrimePageScript.runtime.tmp.value_2nd_source = {}),
        PrimePageScript.isObject(
          PrimePageScript.runtime.tmp.value_2nd_click_time
        ) || (PrimePageScript.runtime.tmp.value_2nd_click_time = {});
      var i = e.getAttribute("data-source-id");
      if (
        (PrimePageScript.isEmpty(i) &&
          ((i = PrimePageScript.randomString(10)),
          e.setAttribute("data-source-id", i)),
        e.classList.contains("prime-accordion-shape"))
      ) {
        if (
          (PrimePageScript.runtime.tmp.value_2nd_click_time[i] || 0) + 250 >
          Date.now()
        )
          return;
        PrimePageScript.runtime.tmp.value_2nd_click_time[i] = Date.now();
      }
      var a = PrimePageScript.runtime.tmp.value_2nd_source[i] || this.value(),
        n = parseFloatLadiPage(e.getAttribute("data-count-click")) || 0;
      n++,
        (PrimePageScript.runtime.tmp.value_2nd_source[i] = a),
        e.setAttribute("data-count-click", n),
        n % 2 == 0 ? this.value(a) : this.value(t);
    }
  }),
  (LadiPageLibraryV2.prototype.top = function () {
    var t = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(t) && t.classList.contains("prime-section"))
      try {
        var e = t.parentElement.firstChild;
        PrimePageScript.isEmpty(e) ||
          e.id != PrimePageScript.runtime.builder_section_background_id ||
          (e = e.nextElementSibling),
          t.parentElement.insertBefore(t, e),
          PrimePageScript.reloadLazyload(!1);
      } catch (t) {}
  }),
  (LadiPageLibraryV2.prototype.pause = function () {
    var t = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(t)) {
      var e = PrimePageScript.runtime.eventData[this.id];
      if (!PrimePageScript.isEmpty(e) && "video" == e.type) {
        var i = t.querySelector(".iframe-video-preload:not(.no-pause)");
        PrimePageScript.isEmpty(i) ||
          PrimePageScript.runEventReplayVideo(
            i.id,
            i.getAttribute("data-video-type"),
            !1
          );
      }
    }
  }),
  (LadiPageLibraryV2.prototype.play = function () {
    var t = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(t)) {
      var e = PrimePageScript.runtime.eventData[this.id];
      if (!PrimePageScript.isEmpty(e) && "video" == e.type) {
        var i = e["option.video_type"],
          a = e["option.video_value"],
          n = e["option.video_control"];
        PrimePageScript.playVideo(this.id, i, a, n);
      }
    }
  }),
  (LadiPageLibraryV2.prototype.prevSectionTabs = function () {
    var t = document.querySelectorAll(
      '.prime-section[data-tab-id="' + this.id + '"]'
    );
    if (0 != t.length)
      for (var e = 0; e < t.length; e++)
        if (t[e].classList.contains("selected")) {
          var i = e - 1;
          (i = i < 0 ? 0 : i), (this.doc = t[i]), this.show();
          break;
        }
  }),
  (LadiPageLibraryV2.prototype.nextSectionTabs = function () {
    var t = document.querySelectorAll(
      '.prime-section[data-tab-id="' + this.id + '"]'
    );
    if (0 != t.length)
      for (var e = 0; e < t.length; e++)
        if (t[e].classList.contains("selected")) {
          var i = e + 1;
          (i = i >= t.length ? t.length - 1 : i),
            (this.doc = t[i]),
            this.show();
          break;
        }
  }),
  (LadiPageLibraryV2.prototype.indexSectionTabs = function (t) {
    var e = document.querySelectorAll(
      '.prime-section[data-tab-id="' + this.id + '"]'
    );
    e.length < t || ((this.doc = e[t - 1]), this.show());
  }),
  (LadiPageLibraryV2.prototype.prev = function () {
    var t = this.doc || document.getElementById(this.id);
    if (PrimePageScript.isEmpty(t)) this.prevSectionTabs();
    else {
      var e = PrimePageScript.runtime.eventData[this.id];
      if (!PrimePageScript.isEmpty(e)) {
        var i = null;
        if (
          ("gallery" == e.type &&
            (i = t.querySelector(
              ".prime-gallery-view-arrow.prime-gallery-view-arrow-left"
            )),
          "carousel" == e.type &&
            (i = t.querySelector(
              ".prime-carousel-arrow.prime-carousel-arrow-left"
            )),
          "collection" == e.type &&
            (i = t.querySelector(
              ".prime-collection-arrow.prime-collection-arrow-left"
            )),
          "tabs" == e.type)
        ) {
          var a = t.querySelector(
            ".prime-tabs > .prime-element.selected[data-index]"
          );
          return (
            PrimePageScript.isEmpty(a) &&
              (a = t.querySelector(".prime-tabs > .prime-element")),
            void (
              PrimePageScript.isEmpty(a) ||
              PrimePageScript.isEmpty(a.previousElementSibling) ||
              (a.previousElementSibling.classList.add("selected"),
              a.classList.remove("selected"),
              PrimePageScript.reloadLazyload(!1))
            )
          );
        }
        PrimePageScript.isEmpty(i) || i.click();
      }
    }
  }),
  (LadiPageLibraryV2.prototype.next = function () {
    var t = this.doc || document.getElementById(this.id);
    if (PrimePageScript.isEmpty(t)) this.nextSectionTabs();
    else {
      var e = PrimePageScript.runtime.eventData[this.id];
      if (!PrimePageScript.isEmpty(e)) {
        var i = null;
        if (
          ("gallery" == e.type &&
            (i = t.querySelector(
              ".prime-gallery-view-arrow.prime-gallery-view-arrow-right"
            )),
          "carousel" == e.type &&
            (i = t.querySelector(
              ".prime-carousel-arrow.prime-carousel-arrow-right"
            )),
          "collection" == e.type &&
            (i = t.querySelector(
              ".prime-collection-arrow.prime-collection-arrow-right, .prime-collection-button-next"
            )),
          "survey" == e.type &&
            (i = t.querySelector(".prime-survey-button-next button")),
          "tabs" == e.type)
        ) {
          var a = t.querySelector(
            ".prime-tabs > .prime-element.selected[data-index]"
          );
          return (
            PrimePageScript.isEmpty(a) &&
              (a = t.querySelector(".prime-tabs > .prime-element")),
            void (
              PrimePageScript.isEmpty(a) ||
              PrimePageScript.isEmpty(a.nextElementSibling) ||
              (a.nextElementSibling.classList.add("selected"),
              a.classList.remove("selected"),
              PrimePageScript.reloadLazyload(!1))
            )
          );
        }
        PrimePageScript.isEmpty(i) || i.click();
      }
    }
  }),
  (LadiPageLibraryV2.prototype.index = function (t) {
    var e = this.doc || document.getElementById(this.id);
    if (PrimePageScript.isEmpty(e)) this.indexSectionTabs(t);
    else {
      var i = PrimePageScript.runtime.eventData[this.id];
      if (!PrimePageScript.isEmpty(i)) {
        var a = 0;
        ("gallery" != i.type && "carousel" != i.type) ||
          ((a = parseFloatLadiPage(e.getAttribute("data-current")) || 0),
          (a += 1)),
          "collection" == i.type &&
            (a = parseFloatLadiPage(e.getAttribute("data-page")) || 1);
        var n = null;
        if (
          ("tabs" == i.type &&
            ((n = e.querySelector(
              ".prime-tabs > .prime-element.selected[data-index]"
            )),
            PrimePageScript.isEmpty(n) &&
              (n = e.querySelector(".prime-tabs > .prime-element")),
            PrimePageScript.isEmpty(n) ||
              (a = parseFloatLadiPage(n.getAttribute("data-index")) || 1)),
          PrimePageScript.isEmpty(t))
        )
          return a;
        if ("tabs" != i.type) {
          if (
            (("gallery" != i.type && "carousel" != i.type) ||
              ((t -= 1), (a -= 1)),
            t == a)
          )
            return (
              "carousel" == i.type && e.setAttribute("data-stop", !0),
              void (
                "gallery" == i.type &&
                e.hasAttribute("data-loaded") &&
                e.setAttribute("data-stop", !0)
              )
            );
          t > a
            ? (("gallery" != i.type && "carousel" != i.type) ||
                e.setAttribute("data-current", t - 1),
              "collection" == i.type && e.setAttribute("data-page", t - 1),
              this.next())
            : (("gallery" != i.type && "carousel" != i.type) ||
                e.setAttribute("data-current", t + 1),
              "collection" == i.type && e.setAttribute("data-page", t + 1),
              this.prev());
        } else {
          var o = e.querySelector(
            '.prime-tabs > .prime-element[data-index="' + t + '"]'
          );
          PrimePageScript.isEmpty(o) ||
            (PrimePageScript.isEmpty(n) || n.classList.remove("selected"),
            o.classList.add("selected"),
            PrimePageScript.reloadLazyload(!1));
        }
      }
    }
  }),
  (LadiPageLibraryV2.prototype.readmore = function () {
    var t = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(t)) {
      var e = t.getElementsByClassName("prime-section-arrow-down")[0];
      PrimePageScript.isEmpty(e) || e.click();
    }
  }),
  (LadiPageLibraryV2.prototype.collapse = function (t) {
    var e = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(e) && !e.classList.contains("prime-section")) {
      var i = e.getElementsByClassName("prime-popup")[0];
      if (PrimePageScript.isEmpty(i)) {
        var a = PrimePageScript.isNull(t),
          n = document.querySelector(
            [
              "#" + this.id + " > .prime-headline",
              "#" + this.id + " > .prime-paragraph",
              "#" + this.id + " > .prime-list-paragraph",
            ].join(", ")
          ),
          o = PrimePageScript.isEmpty(n),
          r = 0,
          s = 0,
          l = null,
          c = this,
          d = function (t) {
            var i = PrimePageScript.findAncestor(
              e.parentElement,
              "prime-element"
            );
            PrimePageScript.isEmpty(i) &&
              (i = PrimePageScript.findAncestor(
                e.parentElement,
                "prime-section"
              )),
              o || (t = !1),
              PrimePageScript.updateHeightElement(!0, e, i, r, s, t);
          };
        if ("none" == getComputedStyle(e).display) {
          if (a || t)
            if (a && o) {
              if (
                ((l =
                  parseFloatLadiPage(e.getAttribute("data-timeout-id")) ||
                  null),
                !PrimePageScript.isEmpty(l))
              )
                return;
              e.classList.remove("height-0"),
                e.classList.remove("overflow-hidden"),
                e.classList.remove("transition-collapse"),
                c.show(),
                (s = parseFloatLadiPage(getComputedStyle(e).height) || 0),
                e.classList.add("overflow-hidden"),
                e.classList.add("height-0"),
                (l = PrimePageScript.runTimeout(function () {
                  e.classList.add("transition-collapse"),
                    e.classList.remove("height-0"),
                    (l = PrimePageScript.runTimeout(function () {
                      e.classList.remove("overflow-hidden"),
                        e.classList.remove("transition-collapse"),
                        e.removeAttribute("data-timeout-id");
                    }, 1e3 *
                      parseFloatLadiPage(
                        getComputedStyle(e).transitionDuration
                      ))),
                    e.setAttribute("data-timeout-id", l),
                    d(!0);
                }, 100)),
                e.setAttribute("data-timeout-id", l);
            } else
              c.show(),
                (s = parseFloatLadiPage(getComputedStyle(e).height) || 0),
                d();
        } else if (a || !t)
          if (a && o) {
            if (
              ((l =
                parseFloatLadiPage(e.getAttribute("data-timeout-id")) || null),
              !PrimePageScript.isEmpty(l))
            )
              return;
            e.classList.remove("height-0"),
              e.classList.remove("overflow-hidden"),
              e.classList.remove("transition-collapse"),
              (r = parseFloatLadiPage(getComputedStyle(e).height) || 0),
              e.classList.add("overflow-hidden"),
              (l = PrimePageScript.runTimeout(function () {
                e.classList.add("transition-collapse"),
                  e.classList.add("height-0"),
                  (l = PrimePageScript.runTimeout(function () {
                    e.classList.remove("overflow-hidden"),
                      e.classList.remove("transition-collapse"),
                      e.classList.remove("height-0"),
                      e.removeAttribute("data-timeout-id"),
                      c.hide();
                  }, 1e3 *
                    parseFloatLadiPage(
                      getComputedStyle(e).transitionDuration
                    ))),
                  e.setAttribute("data-timeout-id", l),
                  d(!0);
              }, 100)),
              e.setAttribute("data-timeout-id", l);
          } else
            (r = parseFloatLadiPage(getComputedStyle(e).height) || 0),
              c.hide(),
              d();
      }
    }
  }),
  (LadiPageLibraryV2.prototype.hide = function (t) {
    var e = this,
      i = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(i)) {
      var a = !1;
      0 == i.getElementsByClassName("prime-popup").length
        ? (PrimePageScript.runtime.has_popupx &&
            i.classList.contains("prime-section") &&
            "none" != getComputedStyle(i).display &&
            (a = !0),
          i.style.setProperty("display", "none", "important"),
          PrimePageScript.reloadLazyload(!1),
          !t &&
            a &&
            PrimePageScript.runtime.tmp.runActionPopupX({
              id: this.id,
              dimension: { display: "none" },
              action: { type: "set_iframe_dimension" },
            }))
        : PrimePageScript.runRemovePopup(this.id, !0, function () {
            for (
              var t = document.querySelectorAll(
                  "#" +
                    PrimePageScript.runtime.builder_section_popup_id +
                    " .prime-container > .prime-element"
                ),
                i = 0;
              i < t.length;
              i++
            )
              t[i].id != e.id &&
                t[i].hasAttribute("data-popup-backdrop") &&
                "none" != getComputedStyle(t[i]).display &&
                PrimePageScript.runRemovePopup(t[i].id, !0);
          }),
        e.hideDropbox();
    }
  }),
  (LadiPageLibraryV2.prototype.show = function (t) {
    var e = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(e)) {
      if (e.classList.contains("prime-section")) {
        var i = e.getAttribute("data-tab-id");
        if (!PrimePageScript.isEmpty(i))
          for (
            var a = document.querySelectorAll(
                '.prime-section[data-tab-id="' + i + '"]'
              ),
              n = 0;
            n < a.length;
            n++
          )
            a[n].id == e.id
              ? a[n].classList.add("selected")
              : (a[n].classList.remove("selected"),
                window.ladi(a[n].id).hide());
      }
      var o = this,
        r = function () {
          if (PrimePageScript.isObject(PrimePageScript.runtime.eventData)) {
            var t = PrimePageScript.runtime.eventData[o.id];
            PrimePageScript.isEmpty(t) ||
              PrimePageScript.startAutoScroll(
                o.id,
                t.type,
                t[PrimePageScript.const.DESKTOP + ".option.auto_scroll"],
                t[PrimePageScript.const.MOBILE + ".option.auto_scroll"]
              );
            for (
              var i = e.getElementsByClassName("prime-element"), a = 0;
              a < i.length;
              a++
            ) {
              var n = PrimePageScript.runtime.eventData[i[a].id];
              PrimePageScript.isEmpty(n) ||
                PrimePageScript.startAutoScroll(
                  i[a].id,
                  n.type,
                  n[PrimePageScript.const.DESKTOP + ".option.auto_scroll"],
                  n[PrimePageScript.const.MOBILE + ".option.auto_scroll"]
                );
            }
          }
        },
        s = function () {
          for (
            var t = e.getElementsByClassName("prime-element"), i = -1;
            i < t.length;
            i++
          ) {
            var a = -1 == i ? e : t[i];
            "true" == a.getAttribute("data-sticky") &&
              (a.removeAttribute("data-top"),
              a.removeAttribute("data-left"),
              a.removeAttribute("data-sticky"),
              a.style.removeProperty("top"),
              a.style.removeProperty("left"),
              a.style.removeProperty("width"),
              a.style.removeProperty("position"),
              a.style.removeProperty("z-index"));
          }
          PrimePageScript.runEventScroll();
        };
      if (t)
        e.getElementsByClassName("prime-popup").length > 0
          ? (PrimePageScript.runShowPopup(
              !1,
              this.id,
              null,
              null,
              !0,
              function () {
                for (
                  var t = document.querySelectorAll(
                      "#" +
                        PrimePageScript.runtime.builder_section_popup_id +
                        " .prime-container > .prime-element"
                    ),
                    e = 0;
                  e < t.length;
                  e++
                )
                  t[e].id != o.id &&
                    t[e].hasAttribute("data-popup-backdrop") &&
                    "none" != getComputedStyle(t[e]).display &&
                    PrimePageScript.runRemovePopup(t[e].id, !0);
              }
            ),
            PrimePageScript.removeLazyloadPopup(this.id),
            r(),
            s(),
            PrimePageScript.runResizeSectionBackground())
          : (e.style.setProperty("display", "block", "important"),
            r(),
            s(),
            PrimePageScript.runResizeSectionBackground(),
            PrimePageScript.reloadLazyload(!1));
      else {
        if (
          PrimePageScript.runtime.has_popupx &&
          PrimePageScript.runtime.tmp.showPopupX(e.id, !0)
        )
          return;
        o.show(!0);
      }
    }
  }),
  (LadiPageLibraryV2.prototype.showDropbox = function (t, e, i) {
    var a = this.doc || document.getElementById(this.id);
    if (
      !(
        PrimePageScript.isEmpty(a) ||
        "true" != a.getAttribute("data-dropbox") ||
        (i &&
          a.getAttribute("data-from-doc-id") == t.id &&
          "true" != a.getAttribute("data-dropbox-backdrop") &&
          "block" == getComputedStyle(a).display)
      )
    ) {
      a.classList.add("opacity-0"),
        this.show(),
        a.style.removeProperty("display"),
        a.style.removeProperty("top"),
        a.style.removeProperty("left"),
        a.style.removeProperty("bottom"),
        a.style.removeProperty("right"),
        PrimePageScript.isObject(e) || (e = {}),
        (e.padding = parseFloatLadiPage(e.padding) || 0),
        (e.animation_duration = parseFloatLadiPage(e.animation_duration) || 0),
        t.insertBefore(a, t.firstChild),
        a.setAttribute("data-from-doc-id", t.id),
        a.setAttribute("data-style", a.getAttribute("style") || ""),
        PrimePageScript.isEmpty(a.getAttribute("data-style")) &&
          a.removeAttribute("data-style"),
        t.setAttribute("data-style", t.getAttribute("style") || ""),
        PrimePageScript.isEmpty(t.getAttribute("data-style")) &&
          t.removeAttribute("data-style");
      var n = a.getElementsByClassName("prime-popup")[0];
      PrimePageScript.isEmpty(n) ||
        (n.setAttribute("data-style", n.getAttribute("style") || ""),
        PrimePageScript.isEmpty(n.getAttribute("data-style")) &&
          n.removeAttribute("data-style"));
      var o = PrimePageScript.getElementBoundingClientRect(t),
        r = PrimePageScript.getElementBoundingClientRect(a),
        s = "";
      (e.position != PrimePageScript.const.TOOLTIP_POSITION.top_left &&
        e.position != PrimePageScript.const.TOOLTIP_POSITION.top_middle &&
        e.position != PrimePageScript.const.TOOLTIP_POSITION.top_right) ||
        (a.style.setProperty("top", "auto"),
        a.style.setProperty("bottom", o.height + e.padding + "px"),
        e.padding > 0 &&
          (s +=
            'content: ""; position: absolute; width: 100%; height: ' +
            e.padding +
            "px; bottom: " +
            -e.padding +
            "px; left: 0;")),
        e.position == PrimePageScript.const.TOOLTIP_POSITION.top_middle &&
          a.style.setProperty("left", (o.width - r.width) / 2 + "px"),
        e.position == PrimePageScript.const.TOOLTIP_POSITION.top_right &&
          (a.style.setProperty("left", "auto"),
          a.style.setProperty("right", "0")),
        (e.position != PrimePageScript.const.TOOLTIP_POSITION.bottom_left &&
          e.position != PrimePageScript.const.TOOLTIP_POSITION.bottom_middle &&
          e.position != PrimePageScript.const.TOOLTIP_POSITION.bottom_right) ||
          (a.style.setProperty("bottom", "auto"),
          a.style.setProperty("top", o.height + e.padding + "px"),
          e.padding > 0 &&
            (s +=
              'content: ""; position: absolute; width: 100%; height: ' +
              e.padding +
              "px; top: " +
              -e.padding +
              "px; left: 0;")),
        e.position == PrimePageScript.const.TOOLTIP_POSITION.bottom_middle &&
          a.style.setProperty("left", (o.width - r.width) / 2 + "px"),
        e.position == PrimePageScript.const.TOOLTIP_POSITION.bottom_right &&
          (a.style.setProperty("left", "auto"),
          a.style.setProperty("right", "0")),
        (e.position != PrimePageScript.const.TOOLTIP_POSITION.left_top &&
          e.position != PrimePageScript.const.TOOLTIP_POSITION.left_middle &&
          e.position != PrimePageScript.const.TOOLTIP_POSITION.left_bottom) ||
          (a.style.setProperty("left", "auto"),
          a.style.setProperty("right", o.width + e.padding + "px"),
          e.padding > 0 &&
            (s +=
              'content: ""; position: absolute; width: ' +
              e.padding +
              "px; height: 100%; top: 0; right: " +
              -e.padding +
              "px;")),
        e.position == PrimePageScript.const.TOOLTIP_POSITION.left_top &&
          a.style.setProperty("bottom", "auto"),
        e.position == PrimePageScript.const.TOOLTIP_POSITION.left_bottom &&
          a.style.setProperty("top", "auto"),
        (e.position != PrimePageScript.const.TOOLTIP_POSITION.right_top &&
          e.position != PrimePageScript.const.TOOLTIP_POSITION.right_middle &&
          e.position != PrimePageScript.const.TOOLTIP_POSITION.right_bottom) ||
          (a.style.setProperty("right", "auto"),
          a.style.setProperty("left", o.width + e.padding + "px"),
          e.padding > 0 &&
            (s +=
              'content: ""; position: absolute; width: ' +
              e.padding +
              "px; height: 100%; top: 0; left: " +
              -e.padding +
              "px;")),
        e.position == PrimePageScript.const.TOOLTIP_POSITION.right_top &&
          a.style.setProperty("bottom", "auto"),
        e.position == PrimePageScript.const.TOOLTIP_POSITION.right_bottom &&
          a.style.setProperty("top", "auto"),
        a.style.setProperty("z-index", "90000090"),
        "fixed" == getComputedStyle(t).position &&
          t.style.setProperty("z-index", "90000090");
      var l = "dropbox-" + a.id;
      if (i && !PrimePageScript.isEmpty(s))
        (s = "#" + a.id + ":before {" + s + "}"),
          PrimePageScript.createStyleElement(l, s);
      else {
        var c = document.getElementById(l);
        PrimePageScript.isEmpty(c) || c.parentElement.removeChild(c);
      }
      i
        ? a.removeAttribute("data-dropbox-backdrop")
        : (a.setAttribute("data-dropbox-backdrop", !0),
          document
            .getElementById(PrimePageScript.runtime.backdrop_dropbox_id)
            .style.setProperty("display", "block")),
        PrimePageScript.isEmpty(e.animation_name) ||
          PrimePageScript.isEmpty(n) ||
          (n.style.setProperty("animation-name", e.animation_name),
          n.style.setProperty("-webkit-animation-name", e.animation_name),
          n.style.setProperty("animation-duration", e.animation_duration + "s"),
          n.style.setProperty(
            "-webkit-animation-duration",
            e.animation_duration + "s"
          )),
        a.classList.remove("opacity-0");
    }
  }),
  (LadiPageLibraryV2.prototype.hideDropbox = function () {
    var t = this.doc || document.getElementById(this.id);
    if (
      !PrimePageScript.isEmpty(t) &&
      "true" == t.getAttribute("data-dropbox")
    ) {
      t.setAttribute("style", t.getAttribute("data-style") || ""),
        t.removeAttribute("data-style");
      var e = document.getElementById(t.getAttribute("data-from-doc-id"));
      PrimePageScript.isEmpty(e) ||
        (e.setAttribute("style", e.getAttribute("data-style") || ""),
        e.removeAttribute("data-style"));
      var i = t.getElementsByClassName("prime-popup")[0];
      PrimePageScript.isEmpty(i) ||
        i.setAttribute("style", i.getAttribute("data-style") || "");
      var a = "dropbox-" + t.id,
        n = document.getElementById(a);
      PrimePageScript.isEmpty(n) || n.parentElement.removeChild(n);
      for (
        var o = t.querySelectorAll('[data-dropbox-backdrop="true"]'), r = 0;
        r < o.length;
        r++
      )
        window.ladi(o[r].id).hide();
      t.removeAttribute("data-dropbox-backdrop"),
        0 ==
          (o = document.querySelectorAll('[data-dropbox-backdrop="true"]'))
            .length &&
          document
            .getElementById(PrimePageScript.runtime.backdrop_dropbox_id)
            .style.removeProperty("display"),
        document
          .querySelector(
            "#" +
              PrimePageScript.runtime.builder_section_popup_id +
              " > .prime-container"
          )
          .appendChild(t);
    }
  }),
  (LadiPageLibraryV2.prototype.toggle = function (t) {
    var e = this.doc || document.getElementById(this.id);
    PrimePageScript.isEmpty(e) ||
      ("none" == getComputedStyle(e).display ? this.show(t) : this.hide(t));
  }),
  (LadiPageLibraryV2.prototype.set_style = function (t, e, i) {
    var a = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(a)) {
      var n = e.action_type,
        o = "set-style-" + t.id + "-" + a.id + "-" + n,
        r = "set-style-" + t.id + "-" + a.id + "-" + n + "-transition",
        s = document.getElementById(i ? r : o);
      PrimePageScript.isEmpty(s) || s.parentElement.removeChild(s),
        a.classList.remove(o);
      var l = {};
      PrimePageScript.isEmpty(e.color) || (l.color = e.color),
        PrimePageScript.isEmpty(e.background_color) ||
          ((l.background = "none"),
          (l["background-color"] = e.background_color)),
        PrimePageScript.isEmpty(e.border_color) ||
          (l["border-color"] = e.border_color),
        PrimePageScript.isEmpty(e.opacity) || (l.opacity = e.opacity / 100),
        PrimePageScript.isEmpty(e.transform_scale) ||
          (l.transform = "scale(" + e.transform_scale / 100 + ")"),
        e.ontop && (l["z-index"] = "9000000090 !important"),
        PrimePageScript.isObject(e.custom_css) &&
          Object.keys(e.custom_css).forEach(function (t) {
            l[t] = e.custom_css[t];
          });
      var c = [],
        d = "",
        p = "",
        u = "",
        m = "",
        _ = "",
        y = "",
        g = !1;
      Object.keys(l).forEach(function (t) {
        "z-index" != t.toLowerCase()
          ? "background" == t.toLowerCase() ||
            t.toLowerCase().startsWith("background-")
            ? (u += t + ": " + l[t] + ";")
            : "color" == t.toLowerCase() ||
              "font" == t.toLowerCase() ||
              t.toLowerCase().startsWith("font-") ||
              t.toLowerCase().startsWith("text-") ||
              t.toLowerCase().startsWith("line-")
            ? (m += t + ": " + l[t] + ";")
            : (p += t + ": " + l[t] + ";")
          : (y += t + ": " + l[t] + ";");
      });
      var f = function (t) {
        for (var e = !1, n = 0; n < c.length; n++) {
          var r = c[n];
          if (PrimePageScript.isEmpty(r)) {
            i &&
              (g || (_ += "#" + a.id + " {transition: all 150ms linear 0s;}")),
              (d += "#" + a.id + "." + o + " {" + t + "}"),
              (e = !0);
            break;
          }
          if (document.querySelectorAll("#" + a.id + " > " + r).length > 0) {
            i &&
              (_ +=
                "#" + a.id + " > " + r + " {transition: all 150ms linear 0s;}"),
              (d += "#" + a.id + "." + o + " > " + r + " {" + t + "}"),
              (e = !0);
            break;
          }
        }
        return e;
      };
      if (!PrimePageScript.isEmpty(m)) {
        var v = function (t) {
          t = PrimePageScript.isEmpty(t) ? "" : "." + t;
          var e = "";
          return (
            (e += "#" + a.id + t + ", "),
            (e += "#" + a.id + t + " .prime-headline, "),
            (e += "#" + a.id + t + " .prime-paragraph, ") +
              "#" +
              a.id +
              t +
              " .prime-list-paragraph"
          );
        };
        document.querySelectorAll(v()).length > 0 &&
          (i && ((g = !0), (_ += v() + " {transition: all 150ms linear 0s;}")),
          (d += v(o) + " {" + m + "}"));
      }
      PrimePageScript.isEmpty(u) ||
        ((c = [
          ".prime-section-background",
          ".prime-popup .prime-popup-background",
          ".prime-button .prime-button-background",
          ".prime-box",
          ".prime-video .prime-video-background",
          ".prime-form .prime-form-item-background",
          ".prime-frame .prime-frame-background",
          ".prime-survey .prime-survey-option-background",
          ".prime-combobox .prime-combobox-item-background",
          ".prime-countdown .prime-countdown-background",
          ".prime-notify",
        ]),
        f(u) || (p += u)),
        PrimePageScript.isEmpty(p) ||
          ((c = [
            ".prime-group",
            ".prime-accordion",
            ".prime-popup",
            ".prime-image",
            ".prime-gallery",
            ".prime-button",
            ".prime-button-group",
            ".prime-headline",
            ".prime-paragraph",
            ".prime-list-paragraph",
            ".prime-line",
            ".prime-box",
            ".prime-collection",
            ".prime-tabs",
            ".prime-shape",
            ".prime-video",
            ".prime-form",
            ".prime-carousel",
            ".prime-html-code",
            ".prime-frame",
            ".prime-table",
            ".prime-survey",
            ".prime-combobox",
            ".prime-countdown",
            ".prime-notify",
            ".prime-spin-lucky",
          ]),
          f(p) || (y += p)),
        PrimePageScript.isEmpty(y) || ((c = [""]), f(y)),
        i
          ? PrimePageScript.createStyleElement(r, _)
          : PrimePageScript.createStyleElement(o, d),
        i || a.classList.add(o);
    }
  }),
  (LadiPageLibraryV2.prototype.remove_style = function (t, e) {
    var i = this.doc || document.getElementById(this.id);
    if (!PrimePageScript.isEmpty(i)) {
      var a = e.action_type,
        n = "set-style-" + t.id + "-" + i.id + "-" + a;
      i.classList.remove(n);
      var o = document.getElementById(n);
      PrimePageScript.isEmpty(o) || o.parentElement.removeChild(o);
    }
  }),
  (LadiPageLibraryV2.prototype.element = function () {
    return this.doc || document.getElementById(this.id);
  }),
  ["start", "add_turn"].forEach(function (t) {
    LadiPageLibraryV2.prototype[t] = function () {
      var e = PrimePageScript.runtime.eventData[this.id];
      if (!PrimePageScript.isEmpty(e)) {
        var i = LadiPageApp[e.type + PrimePageScript.const.APP_RUNTIME_PREFIX];
        if (!PrimePageScript.isEmpty(i)) {
          var a = i(e, !0);
          PrimePageScript.isFunction(a[t]) && a[t](this.id);
        }
      }
    };
  });
var LadiPageAppV2,
  ladi =
    ladi ||
    function (t, e) {
      if (!PrimePageScript.isEmpty(t)) {
        var i = new LadiPageLibraryV2();
        return (i.id = t), (i.doc = e), i;
      }
    },
  ladi_fbq = function (t, e, i, a) {
    PrimePageScript.isFunction(window.fbq) &&
      (PrimePageScript.isObject(window.ladi_conversion_api) &&
        PrimePageScript.isObject(window.ladi_conversion_api.facebook) &&
        PrimePageScript.isArray(window.ladi_conversion_api.facebook.pixels) &&
        (PrimePageScript.isObject(a) || (a = {}),
        (a.eventID =
          "ladi." +
          Date.now() +
          "." +
          (Math.floor(9e10 * Math.random()) + 1e10))),
      window.fbq(t, e, i, a),
      PrimePageScript.runConversionApi("facebook", "events", [
        { key: t, name: e, custom_data: i, data: a },
      ]));
  };
PrimePageScript.isArray(window.ladi_fbq_data) &&
  (window.ladi_fbq_data.forEach(function (t) {
    ladi_fbq(t[0], t[1], t[2], t[3]);
  }),
  delete window.ladi_fbq_data),
  ((LadiPageAppV2 = LadiPageAppV2 || function () {}).prototype.notify_runtime =
    function (t, e) {
      var i = function () {},
        a = "top_left",
        n = "top_center",
        o = "top_right",
        r = "bottom_left",
        s = "bottom_center",
        l = "bottom_right";
      return (
        (i.prototype.run = function (e, i) {
          PrimePageScript.isObject(PrimePageScript.runtime.tmp.timeout_notify) ||
            (PrimePageScript.runtime.tmp.timeout_notify = {});
          var c = t["option.sheet_id"],
            d = t.dataset_value,
            p = document.getElementById(e);
          if (
            (p.classList.add("prime-hidden"),
            !PrimePageScript.isEmpty(c) || !PrimePageScript.isEmpty(d))
          ) {
            var u = i
                ? PrimePageScript.const.DESKTOP
                : PrimePageScript.const.MOBILE,
              m = t[u + ".option.position"],
              _ = 1e3 * (parseFloatLadiPage(t["option.time_show"]) || 5),
              y = 1e3 * (parseFloatLadiPage(t["option.time_delay"]) || 10);
            y = y < 501 ? 501 : y;
            var g = "https://w.ladicdn.com/source/notify.svg",
              f = [
                { key: "gsx$title", className: ".prime-notify-title" },
                { key: "gsx$content", className: ".prime-notify-content" },
                { key: "gsx$time", className: ".prime-notify-time" },
                { key: "gsx$image", className: ".prime-notify-image img" },
              ];
            p.classList.remove("prime-hidden");
            var v = function () {
              p.style.setProperty("opacity", 0),
                (m != a && m != n && m != o) ||
                  p.style.setProperty("top", -p.clientHeight - 100 + "px"),
                (m != r && m != s && m != l) ||
                  p.style.setProperty("bottom", -p.clientHeight - 100 + "px");
            };
            v(),
              f.forEach(function (t) {
                "gsx$image" == t.key
                  ? (p.querySelectorAll(t.className)[0].src = g)
                  : (p.querySelectorAll(t.className)[0].textContent = "");
              });
            var h = function (t) {
                t = t.sort(function () {
                  return 0.5 - Math.random();
                });
                var i = -1,
                  c = function () {
                    if (i + 1 < t.length) {
                      var d = t[++i],
                        u = Object.keys(d);
                      p.style.removeProperty("opacity"),
                        (m != a && m != n && m != o) ||
                          p.style.removeProperty("top"),
                        (m != r && m != s && m != l) ||
                          p.style.removeProperty("bottom"),
                        f.forEach(function (t) {
                          -1 != u.indexOf(t.key) &&
                            ("gsx$image" == t.key
                              ? (p.querySelectorAll(t.className)[0].src =
                                  PrimePageScript.isEmpty(d[t.key].$t)
                                    ? g
                                    : d[t.key].$t)
                              : (p.querySelectorAll(
                                  t.className
                                )[0].textContent = d[t.key].$t));
                        });
                      var E = function () {
                        var a = f.findIndex(function (t) {
                          return "gsx$image" == t.key;
                        });
                        if (-1 != a) {
                          var n = t[i + 1 >= t.length ? 0 : i + 1];
                          n.hasOwnProperty(f[a].key) &&
                            (p.querySelectorAll(f[a].className)[0].src =
                              PrimePageScript.isEmpty(n[f[a].key].$t)
                                ? g
                                : n[f[a].key].$t);
                        }
                        PrimePageScript.runtime.tmp.timeout_notify[e] =
                          PrimePageScript.runTimeout(c, y - 500);
                      };
                      PrimePageScript.runtime.tmp.timeout_notify[e] =
                        PrimePageScript.runTimeout(function () {
                          v(),
                            (PrimePageScript.runtime.tmp.timeout_notify[e] =
                              PrimePageScript.runTimeout(E, 500));
                        }, _);
                    } else h(t);
                  };
                PrimePageScript.runtime.tmp.timeout_notify[e] =
                  PrimePageScript.runTimeout(c, y);
              },
              E = function (t) {
                p.querySelector(".prime-notify").classList.remove("prime-hidden"),
                  p.classList.add("prime-notify-transition"),
                  h(t);
              };
            PrimePageScript.isEmpty(d) || E(d),
              PrimePageScript.isEmpty(c) ||
                PrimePageScript.sendRequest(
                  "GET",
                  "https://docs.google.com/spreadsheets/d/" +
                    c +
                    "/gviz/tq?tqx=out:json",
                  null,
                  !0,
                  null,
                  function (t, e, i) {
                    if (i.readyState == XMLHttpRequest.DONE && 200 == e) {
                      t = (t = t.substr(
                        t.indexOf('"table":{') + '"table":'.length
                      )).substr(0, t.indexOf("});"));
                      var a = JSON.parse(t),
                        n = [],
                        o = a.cols;
                      PrimePageScript.isObject(a) &&
                        0 == a.parsedNumHeaders &&
                        PrimePageScript.isArray(a.rows) &&
                        a.rows.length > 0 &&
                        PrimePageScript.isObject(a.rows[0]) &&
                        PrimePageScript.isArray(a.rows[0].c) &&
                        a.rows[0].c.length > 0 &&
                        ((o = []),
                        a.rows[0].c.forEach(function (t) {
                          o.push({
                            label: PrimePageScript.isObject(t) ? t.v : "",
                          });
                        }),
                        a.rows.shift()),
                        PrimePageScript.isObject(a) &&
                          PrimePageScript.isArray(a.rows) &&
                          PrimePageScript.isArray(o) &&
                          a.rows.forEach(function (t) {
                            if (PrimePageScript.isObject(t)) {
                              var e = {};
                              o.forEach(function (i, a) {
                                if (PrimePageScript.isArray(t.c)) {
                                  var n = t.c[a];
                                  PrimePageScript.isObject(i) &&
                                    !PrimePageScript.isEmpty(i.label) &&
                                    PrimePageScript.isObject(n) &&
                                    (e["gsx$" + i.label.trim().toLowerCase()] =
                                      { $t: n.v });
                                }
                              }),
                                n.push(e);
                            }
                          }),
                        E(n);
                    }
                  }
                );
          }
        }),
        new i()
      );
    }),
  ((LadiPageAppV2 =
    LadiPageAppV2 || function () {}).prototype.spinlucky_runtime = function (
    t,
    e
  ) {
    var i = function () {},
      a = function (t) {
        return (
          parseFloatLadiPage(window.ladi("_total_turn_" + t).get_cookie()) || 0
        );
      };
    return (
      (i.prototype.getEventTrackingCategory = function () {
        return "LadiPageFinish";
      }),
      (i.prototype.run = function (e, i) {
        var n = t["option.spinlucky_setting.list_value"],
          o = t.dataset_value,
          r = t["option.spinlucky_setting.result_popup_id"],
          s = t["option.spinlucky_setting.result_message"],
          l = t["option.spinlucky_setting.max_turn"],
          c = a(e);
        if (!PrimePageScript.isEmpty(n) || !PrimePageScript.isEmpty(o)) {
          (n = n || o),
            PrimePageScript.setDataReplaceStr("spin_turn_left", l - c);
          var d = document.getElementById(e),
            p = d.getElementsByClassName("prime-spin-lucky-start")[0],
            u = d.getElementsByClassName("prime-spin-lucky-screen")[0],
            m = "";
          n.forEach(function (t, e) {
            var i = Base64.decode(t).split("|");
            if (3 == i.length) {
              var a = (360 / n.length) * e + 180 / n.length,
                o = "rotate(" + (a *= -1) + "deg) translateY(-50%)";
              m +=
                '<div class="prime-spin-lucky-label" style="transform: ' +
                o +
                "; -webkit-transform: " +
                o +
                ';">' +
                decodeURIComponentLadiPage(i[1].trim()) +
                "</div>";
            }
          }),
            (u.innerHTML = m);
          var _ = !1;
          p.addEventListener("click", function (t) {
            if ((t.stopPropagation(), !_))
              if ((c = a(e)) >= l)
                PrimePageScript.showMessage(
                  PrimePageScript.const.LANG.GAME_MAX_TURN_MESSAGE.format(l)
                );
              else {
                _ = !0;
                var i = [],
                  o = 0,
                  d = 1;
                n.forEach(function (t, e) {
                  var a = Base64.decode(t).split("|"),
                    n = decodeURIComponentLadiPage(a[0].trim()),
                    r = decodeURIComponentLadiPage(a[1].trim()),
                    s =
                      parseFloatLadiPage(
                        decodeURIComponentLadiPage(a[2].trim())
                      ) || 0;
                  i.push({
                    min: d,
                    max: d + s - 1,
                    value: n,
                    text: r,
                    percent: s,
                    index: e,
                  }),
                    (d += s),
                    (o += s);
                });
                for (
                  var p = PrimePageScript.randomInt(1, o), m = null, y = 0;
                  y < i.length;
                  y++
                )
                  if (i[y].min <= p && i[y].max >= p) {
                    m = i[y];
                    break;
                  }
                if (PrimePageScript.isEmpty(m)) _ = !1;
                else {
                  var g =
                      parseFloatLadiPage(u.getAttribute("data-rotate")) || 0,
                    f =
                      m.index * (360 / i.length) +
                      360 * (4 + Math.ceil(g / 360)) +
                      180 / i.length,
                    v = "rotate(" + f + "deg)";
                  u.setAttribute("data-rotate", f),
                    u.style.setProperty("transform", v),
                    u.style.setProperty("-webkit-transform", v),
                    "NEXT_TURN" != m.value.toUpperCase() &&
                      (c++, window.ladi("_total_turn_" + e).set_cookie(c, 1)),
                    PrimePageScript.runTimeout(function () {
                      "NEXT_TURN" == m.value.toUpperCase()
                        ? PrimePageScript.isEmpty(m.text) ||
                          PrimePageScript.showMessage(m.text)
                        : (PrimePageScript.setDataReplaceStr("coupon", m.value),
                          PrimePageScript.setDataReplaceStr(
                            "coupon_code",
                            m.value
                          ),
                          PrimePageScript.setDataReplaceStr(
                            "coupon_text",
                            m.text
                          ),
                          PrimePageScript.setDataReplaceStr(
                            "spin_turn_left",
                            l - c
                          ),
                          PrimePageScript.setDataReplaceElement(
                            !0,
                            !1,
                            null,
                            null,
                            [
                              "coupon",
                              "coupon_code",
                              "coupon_text",
                              "spin_turn_left",
                            ]
                          ),
                          r == PrimePageScript.const.GAME_RESULT_TYPE.default
                            ? PrimePageScript.isEmpty(s) ||
                              PrimePageScript.showMessage(s)
                            : window.ladi(r).show(),
                          PrimePageScript.runEventTracking(e, { is_form: !1 })),
                        (_ = !1);
                    }, 1e3 *
                      parseFloatLadiPage(
                        getComputedStyle(u).transitionDuration
                      ) +
                      1e3);
                }
              }
          });
        }
      }),
      (i.prototype.start = function (t) {
        var e = document.getElementById(t);
        if (
          !PrimePageScript.isEmpty(e) &&
          e.getElementsByClassName("prime-spin-lucky").length > 0
        ) {
          var i = e.getElementsByClassName("prime-spin-lucky-start")[0];
          PrimePageScript.isEmpty(i) || i.click();
        }
      }),
      (i.prototype.add_turn = function (e) {
        var i = t["option.spinlucky_setting.max_turn"],
          n = a(e);
        n > 0 && n--,
          window.ladi("_total_turn_" + e).set_cookie(n, 1),
          PrimePageScript.setDataReplaceStr("spin_turn_left", i - n),
          PrimePageScript.setDataReplaceElement(!1);
      }),
      new i()
    );
  });
